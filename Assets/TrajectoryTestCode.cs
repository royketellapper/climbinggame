using UnityEngine;
using System.Collections;
using System;
using System.Security.Cryptography.X509Certificates;

public class TrajectoryTestCode : MonoBehaviour 
{
	
	public float gravity;
	public float velocity;
	public float test;
	public Vector2 angle;
	public Transform moveObj;
	public Transform bullet;
	public Transform rangeObj;
	public Transform heightObj;
	public float A1;
	public float B1;
	public float C1;
	public float A2;
	public float B2;
	public float C2;
	public float B;

	float v = 0;
	
	private Vector3 oldLocation;
	
	void Start () 
	{
	  gravity = Mathf.Abs(Physics.gravity.y);
	}
	
	void Update () 
	{
		if (moveObj != null)
			DrawLine(25);
		
		// Debug.DrawLine(transform.position, Trajectory.TrajectoryDirection(v, angle));
		
		if (bullet != null && Input.GetButton("Fire1"))
			ShootBullet();
	}
	
	void ShootBullet()
	{
		Rigidbody projectile = bullet.GetComponent<Rigidbody>();
		bullet.transform.position = transform.position;

        float a = AngleHeight(heightObj.transform.position.y, transform.position, rangeObj.transform.position);
        float v = LaunchVelocity(transform.position, rangeObj.transform.position, a);


        projectile.velocity = Trajectory.Direction(v, new Vector2(Mathf.Atan2(transform.position.z - rangeObj.position.z, transform.position.x - rangeObj.position.x), a));
    }
		

	float calculateVerticalVelocityForHeight( float desiredHeight )
	{
		if ( desiredHeight <= 0 )
			return 0; //wanna go down? just let it drop
		
		//gravity is given per second but we want time step values here
		
		//quadratic equation setup (ax² + bx + c = 0)
		float a = 0.5f / Physics.gravity.y;
		float b = 0.5f;
		float c = desiredHeight;
		
		//check both possible solutions
		float quadraticSolution1 = ( -b - Mathf.Sqrt( b*b - 4*a*c ) ) / (2*a);
		float quadraticSolution2 = ( -b + Mathf.Sqrt( b*b - 4*a*c ) ) / (2*a);
		
		//use the one which is positive
		float v = quadraticSolution1;
		if ( v < 0 )
			v = quadraticSolution2;
		
		//convert answer back to seconds
		return v;
	}

	float Quadratic(float a, float b, float c, float t)
	{
		return a * t * t + b * t + c;
	}

	float LaunchVelocity (Vector3 startLocation, Vector3 endLocation, float angle)
	{
		float range = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2));
		float offsetHeight = endLocation.y - startLocation.y;
		float gravity = Physics.gravity.y;

		float velocity = range * range * gravity;
		velocity /= range * Mathf.Sin(2 * angle) + 2 * offsetHeight * Mathf.Pow(Mathf.Cos(angle),2);
		return Mathf.Sqrt(velocity);
	}

	float GetAngle(Vector3 startLocation, Vector3 endLocation)
	{
		float range = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2));
		float y = endLocation.y - startLocation.y; 
		float a = Mathf.Atan ((y + Vector3.Distance(startLocation, endLocation)) / range); 

		return a;
	}


	float GetAngle2(Vector3 startLocation, Vector3 endLocation)
	{
		float range = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2));
		//float y = endLocation.y - startLocation.y; 



		float a = Mathf.Atan ( range ); 
		
		return a;
	}

	float AngleHeight(float height, Vector3 startLocation, Vector3 endLocation)
	{
		float range = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2));
		float offsetHeight = endLocation.y - startLocation.y;
		float g = -Physics.gravity.y;

		float VerticalSpeed = Mathf.Sqrt(2 * gravity * height);
		float TravelTime = Mathf.Sqrt(2 * (height - offsetHeight) / g) + Mathf.Sqrt(2 * height / g);
		float HorizontalSpeed = range / TravelTime;
		float velocity = Mathf.Sqrt(Mathf.Pow(VerticalSpeed,2) +  Mathf.Pow(HorizontalSpeed, 2));

		return -Mathf.Atan2(VerticalSpeed / velocity, HorizontalSpeed / velocity) + Mathf.PI;
	}

	void DrawLine(int maxpoints)
	{

		//Debug.Log(1.25f * Mathf.Rad2Deg);

		//v = Trajectory.TrajectoryMinimumVelocity(transform.position, moveObj.transform.position , gravity) + velocity;

		//v = velocity; 

		//v = Trajectory.TrajectoryMaximaleVelocity(transform.position, moveObj.transform.position , gravity) + velocity;

		//angle = Trajectory.TrajectoryMaximaleAngle(transform.position,moveObj.transform.position,v,gravity);

		//angle = Trajectory.TrajectoryAngleTest(transform.position,moveObj.transform.position,v,gravity, test);

		//Debug.Log(Trajectory.TrajectoryPathVelocity(transform.position,moveObj.transform.position,velocity,gravity,angle.y));

		//float a = CalculateLaunchAngle(rangeObj.transform.position, transform.position, -Physics.gravity.y);

		//float v = Trajectory.TrajectoryMaximaleVelocity(transform.position, moveObj.transform.position , gravity) + velocity;
		//Vector2 a = Trajectory.TrajectoryMinimumAngle(transform.position, rangeObj.transform.position, v, -Physics.gravity.y);

		//Werkt goed
		float ax = Mathf.Atan2(transform.position.z - rangeObj.position.z, transform.position.x - rangeObj.position.x);
        float maxHeight = Math.Max(transform.position.y, rangeObj.transform.position.y) + A1;
		float ay = Trajectory.AngleHeight(maxHeight, -Physics.gravity.y, transform.position, rangeObj.transform.position);
       // float a = CalculateLaunchAngle(rangeObj.transform.position, transform.position, -Physics.gravity.y);
        float v = Trajectory.LaunchVelocity(transform.position, rangeObj.transform.position, ay);

		oldLocation = transform.position;
		for (int x = 0; x <= maxpoints; x++)
		{
			float time = (float)x / maxpoints;

            //alpha *= Trajectory.TrajectoryMinimumTravelTime(transform.position, moveObj.transform.position, v, gravity, angle.y);

            

            //Vector3 

            Vector3 loc = Vector3.zero;

			//loc.y = Quadratic(A1, B1, C1, time);
			//loc.x = alpha * B;// Quadratic(A1, B1, C1, alpha); //Quadratic(A2, B2, C2, alpha);


			//float LaunchVelocity (Vector3 startLocation, Vector3 endLocation, float height)




            time *= Trajectory.MaximaleTravelTime(transform.position, rangeObj.transform.position, v, -Physics.gravity.y, ay);


			loc = Trajectory.TrajectoryPath(transform.position, time, v, -Physics.gravity.y, new Vector2(Mathf.Atan2(transform.position.z - rangeObj.position.z, transform.position.x - rangeObj.position.x),ay)); //new Vector2(0,a)


			Debug.DrawLine(oldLocation, loc);

			oldLocation = loc;
		}

        Vector3 pos = Trajectory.CalculateHighestPoint(transform.position, rangeObj.transform.position, v, ay);
        //Vector3 pos = Trajectory.CalculateHighestPoint(transform.position, v, new Vector2(ax, ay));



        DebugSimbols.DrawSphere(pos, 0.1f, 10, true);


        //float dis = CalculateMidpoint(transform.position, rangeObj.transform.position, v, a);



        //      Vector3 dif = moveObj.position - transform.position;
        //      dif.y = 0;
        //      Vector3 normal = Vector3.Normalize(dif);

        //Debug.DrawRay(transform.position, normal * dis, Color.red);


        //float dis = ParaboolDistance (v, gravity, angle);

        //Debug.DrawLine(Vector3.zero, new Vector3(dis,0,0));
    }

    //float CalculateLaunchAngle(Vector3 startPosition, Vector3 endPosition, float velocity, float gravity)
    //{
    //    float dx = endPosition.x - startPosition.x;
    //    float dy = endPosition.y - startPosition.y;
    //    float dz = endPosition.z - startPosition.z;
    //    float d = Mathf.Sqrt(dx * dx + dz * dz);

    //    float vSquared = velocity * velocity;
    //    float vFourth = vSquared * vSquared;

    //    float rootTerm = Mathf.Sqrt(vFourth - gravity * (gravity * d * d + 2 * dy * vSquared));

    //    float angle1 = Mathf.Atan((vSquared + rootTerm) / (gravity * d));
    //    float angle2 = Mathf.Atan((vSquared - rootTerm) / (gravity * d));

    //    // Return the smaller angle in degrees
    //    return Mathf.Min(angle1, angle2);
    //}

    float CalculateLaunchAngle(Vector3 startPosition, Vector3 endPosition, float gravity)
    {
        float dx = endPosition.x - startPosition.x;
        float dy = endPosition.y - startPosition.y;
        float dz = endPosition.z - startPosition.z;
        float d = Mathf.Sqrt(dx * dx + dz * dz);

        // If the end point is at the same level or above the start point
        if (dy > d)
        {
            Debug.LogError("The end point is not reachable with a simple ballistic trajectory.");
            return -1;
        }

        float term1 = gravity * d;
        float term2 = Mathf.Sqrt(gravity * (gravity * d * d + 2 * dy * d));

        float angle1 = Mathf.Atan((term1 + term2) / (gravity * d));
        float angle2 = Mathf.Atan((term1 - term2) / (gravity * d));

		return Mathf.Min(angle1, angle2) + Mathf.PI;

		//return angle2;
    }
}
