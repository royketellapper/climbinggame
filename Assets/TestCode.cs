//http://en.wikipedia.org/wiki/Trajectory_of_a_projectile
//http://hyperphysics.phy-astr.gsu.edu/hbase/traj.html 

using UnityEngine;
using System.Collections;

public class TestCode : MonoBehaviour 
{

	public float gravity;
	public float velocity;
	public float angle1;
	public Transform TestObj;
	
	private Vector3 oldLocation;
	
	public Vector3 testVector;
	
	public float num1;
	public float num2;
	public float num3;
	public float num4;
	
	void Start () 
	{
	}
	
	void DrawLine(int maxpoints)
	{
		//Debug.Log(Mathf.Cos(90));
		
		//float v = GetVelocity3 (TestObj.position, gravity) + velocity;
		float v = velocity;
		
		float angle = ParaboolAngle (TestObj.position, v, gravity) + angle1;
		
		//float v = GetVelocity (TestObj.position, velocity, gravity, angle)
		
		//float v = GetVelocity2 (TestObj.position, gravity, angle);
		
		oldLocation = transform.position;
		for (int a = 0; a <= maxpoints; a++)
		{
			float alpha = (float)a / maxpoints;
			
			//alpha *= ParaboolTime2 (velocity, gravity, angle);
			
			alpha *= ParaboolTime3 (TestObj.position, v, gravity, angle);
			
			//alpha *= ParaboolTime4 (TestObj.position, gravity);
			
			Vector3 loc = Parabool4 (alpha, v, gravity, angle); //SimpelParabool(alpha);//
			
			Debug.DrawLine(oldLocation, loc);

			oldLocation = loc;
		}
		
		float dis = ParaboolDistance (v, gravity, angle);
			
		Debug.DrawLine(Vector3.zero, new Vector3(dis,0,0));
		
		dis = ParaboolDistance2 (TestObj.position, v, gravity, angle);
			
		Debug.DrawLine(Vector3.zero, new Vector3(dis,0,0), Color.red);
		
		
		dis = MaxDistance(v, gravity);
		Debug.DrawLine(Vector3.zero, new Vector3(dis,0,0), Color.blue);
	}
	
	void Update ()
	{
		if (TestObj != null)
			DrawLine(60);
	}
	
	Vector3 Parabool (float t, float v, float g)
	{
		Vector3 vect = new Vector3();
	
		vect.x = v * t;
		vect.y = -((g * Mathf.Pow(t, 2)) / 2);
		
		return vect;
	}	
	
	
	Vector3 Parabool2 (float t, float v, float g, float a)
	{
		Vector3 vect = new Vector3();
	
		vect.x = v * Mathf.Cos(a) * t;
		vect.y = v * Mathf.Sin(a) * -((g * Mathf.Pow(t, 2)) / 2);
		
		return vect;
	}
	
	Vector3 Parabool3 (float t, float v, float g, float a)
	{
		Vector3 vect = new Vector3();
	
		vect.x = v * t *  Mathf.Cos(a);
		vect.y = v * t * Mathf.Sin(a) * - (0.5f * (g * Mathf.Pow(t, 2)));
		
		return vect;
	}
	
	Vector3 Parabool5 (float t, float v, float g, float a)
	{
		Vector3 vect = new Vector3();
	
		vect.x = v * Mathf.Cos(a) * t;
		vect.y = v * Mathf.Sin(a) * t * - 0.5f * g * Mathf.Pow(t, 2);
		
		return vect;
	}
	
	float ParaboolDistance (float v, float g, float a)
	{
		return (Mathf.Pow(v, 2) * Mathf.Sin(2 * a)) / g;
	}
	
	float ParaboolDistance2 (Vector3 loc, float v, float g, float a)
	{
		float som = (v * Mathf.Cos(a)) / g;
		som *= v * Mathf.Sin(a) + Mathf.Sqrt(Mathf.Pow(v * Mathf.Sin(a), 2) + 2 * g * -loc.y);
		
		return som;
	}
	
	float ParaboolTime2 (float v, float g, float a)
	{
		float distance = ParaboolDistance (v, g, a);
		
		return distance / (v * Mathf.Cos(a));
		
	}
	
	//Fix me in negative waarde
	float ParaboolTime4 (Vector3 loc,float g)
	{
		return Mathf.Sqrt(2 * loc.y / g);
	}
	
	float ParaboolTime3 (Vector3 loc, float v, float g, float a)
	{
		float som = v * Mathf.Sin(a) + Mathf.Sqrt(Mathf.Pow(v * Mathf.Sin(a), 2) + 2 * g * -loc.y);
		
		return som / g;
	}
	
	float ParaboolAngle (Vector3 loc, float v, float g)
	{
		float som1 = (g * Mathf.Pow(loc.x, 2)) / Mathf.Pow(v, 2);
		
		float som2 = som1 * -loc.y + Mathf.Sqrt(Mathf.Pow(loc.x, 2) * -Mathf.Pow(loc.y, 2) + Mathf.Pow(loc.x, 4) - Mathf.Pow(som1,2) * Mathf.Pow(loc.x, 2));
		
		som2 /= (Mathf.Pow(loc.x, 2) + Mathf.Pow(loc.y, 2));
		
		return Mathf.Acos(som2) / 2;
	}
	
	
	float ParaboolAngle2 (Vector3 loc, float v, float g)
	{
		float vPow = Mathf.Pow(v,2);
		
		float root = Mathf.Sqrt(Mathf.Pow(v,4) - g * (g * Mathf.Pow(loc.x,2) + 2 * loc.y * vPow));
		root += vPow;
		
		float som = root / (g * loc.x);
		
		return Mathf.Atan(som);
	}
	
	float ParaboolAngle3 (Vector3 loc, float v, float g)
	{
		float vPow = Mathf.Pow(v,2);
		
		float root = Mathf.Sqrt(Mathf.Pow(v,4) - g * (g * Mathf.Pow(loc.x,2) + 2 * loc.y * vPow));
		root += vPow;
		
		float som = root / (g * loc.x);
		
		return Mathf.Atan(som);
	}
	
	
	float ParaboolAngle4 (Vector3 loc, float v, float g)
	{
		float gv = (-g * Mathf.Pow(loc.x,2)) / (2 * Mathf.Pow(v,2));
		
		float ggv = (-g * Mathf.Pow(loc.x,2)) / (2 * Mathf.Pow(v,2) - loc.y);
		
		float f = -loc.x - Mathf.Sqrt(Mathf.Pow(loc.x,2) - 4 * gv * ggv);
		
		return Mathf.Atan(f / (2 * gv));
	}
	
	float ParaboolAngle5 (Vector3 loc, float v, float g)
	{
		float reachAngle = Mathf.Asin((g * MaxDistance(v, g)) / Mathf.Pow(v, 2)) / 2;
		
		float m = Mathf.Sqrt(Mathf.Pow(loc.x,2) + Mathf.Pow(loc.x,2)); 
		
		float v2 = Mathf.Pow(v, 2);
		
		float root = v2 + Mathf.Sqrt(Mathf.Pow(v, 4) - g * (g * Mathf.Pow(m, 2) * Mathf.Pow(Mathf.Cos(reachAngle), 2) + 2 * v2 * m * Mathf.Sin(reachAngle)));
		
		
		return Mathf.Atan((root / g * m * Mathf.Cos(reachAngle)));
	}
	
	
	
	float MaxParaboolAngle (Vector3 loc, float v, float g)
	{
		float dis = MaxDistance(v, g);
		
		return Mathf.Sin((g * dis)/Mathf.Pow(v,2));
	}
	
	Vector3 Parabool4 (float t, float v, float g, float a)
	{
		Vector3 vect = new Vector3();
	
		vect.x = v * t * Mathf.Cos(a);
		
		//t = vect.x / (v * Mathf.Cos(a));
		
		//vect.y = vect.x * Mathf.Tan(a) - ((g * Mathf.Pow(vect.x, 2)) / (Mathf.Pow(v, 2) * Mathf.Pow(Mathf.Cos(a),2)));
		
		vect.y = vect.x * Mathf.Tan(a) - ((g * Mathf.Pow(vect.x, 2)) / (2 * Mathf.Pow(v, 2))) * (1 + Mathf.Pow(Mathf.Tan(a), 2));
		
		return vect;
	}
	
	
	float GetVelocity (float distance, float v, float g, float a)
	{
		float som1 = (g * distance) / (v * Mathf.Cos(a));
		float som2 = Mathf.Pow(v, 2) - 2 * g * distance * Mathf.Tan(a) + Mathf.Pow(som1,2);
		
		return Mathf.Sqrt(som2);
	}
	
	float GetVelocity2 (Vector3 position, float g, float a)
	{
		float dis = position.x;
		
		return Mathf.Sqrt((dis * g) / Mathf.Sin(2 * a));
	}
	
	float GetVelocity3 (Vector3 position, float g)
	{
		//return Mathf.Sqrt(position.x * g);

		 float theta = Mathf.Atan ((position.y + Vector3.Distance(position, Vector3.zero)) / position.x); 
		 if (theta < 0) theta += Mathf.PI; 
		
		 return Mathf.Sqrt(-0.5f * g * position.x * position.x * (1 + Mathf.Tan(theta) * Mathf.Tan(theta)) / (position.y - position.x * Mathf.Tan(theta))); 
	}
	
	float MaxDistance(float v, float g)
	{
		return Mathf.Pow(v, 2) / g;
	}
	
	Vector2 SimpelParabool(float t)
	{
		
		float x = t * num1 - num1 / 2;
		
		float som = -Mathf.Pow(x, 2) + num2;
		
		return new Vector2(t + num4, som);
	}
}
