﻿using UnityEngine;

public struct ClimbPointHitInfo
{
    public Vector3 HitPoint;
    public IClimbLine ClimbLine;
}
