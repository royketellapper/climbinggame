﻿using UnityEngine;

public struct TriangleInfo
{
    public EdgeInfo EdgeA;
    public EdgeInfo EdgeB;

    public void DrawLines()
    {
        EdgeA.DrawLines(Color.red);
        EdgeB.DrawLines(Color.blue);
    }
}
