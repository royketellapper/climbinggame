﻿using UnityEngine;

public struct EdgeInfo
{
    public EdgeInfo(Vector3 a, Vector3 b, Vector3 c, int[] conection)
    {
        points = new Vector3[3];
        points[0] = a;
        points[1] = b;
        points[2] = c;
        Conection = conection;
    }

    public Vector3[] points;
    public int[] Conection;

    public Vector3 GetConectionVertex(int index)
    {
        return points[Conection[index]];
    }

    public Vector3 Up()
    {
        return Vector3.Cross(points[0] - points[1], Vector3.Normalize(points[0] - points[2])).normalized;
    }

    public Vector3 Forward()
    {
        Vector3 a = points[Conection[0]];
        Vector3 b = points[Conection[1]];
        Vector3 c = points[Conection[2]];
        Vector3 normal = Vector3.Cross(a - b, c - b).normalized;
        return Vector3.Cross(normal, b - a).normalized;
    }

    public Plane GetPlane()
    {
        return new Plane(points[0], points[1], points[2]);
    }

    public Vector3 Left()
    {
        return Vector3.Normalize(points[Conection[0]] - points[Conection[1]]);
    }

    public Vector3 MidPoint()
    {
        return (points[0] + points[1] + points[2]) / 3;
    }

    public void DrawLines(Color color)
    {
        Debug.DrawLine(points[0], points[1]);
        Debug.DrawLine(points[1], points[2]);
        Debug.DrawLine(points[2], points[0]);
        Debug.DrawRay(MidPoint(), Up());
        Debug.DrawRay(MidPoint(), Forward(), color);
        Debug.DrawRay(MidPoint(), Left());
    }
}