﻿using System.Collections.Generic;

public class ConectionInfo
{
    public List<int> ConectionA = new List<int>();
    public List<int> ConectionB = new List<int>();

    public int Count { get { return ConectionA.Count; } }

    public bool Add(int a, int b)
    {
        if (!ConectionA.Contains(a) && !ConectionB.Contains(b))
        {
            ConectionA.Add(a);
            ConectionB.Add(b);

            return true;
        }

        return false;
    }

    public void Clear()
    {
        ConectionA.Clear();
        ConectionB.Clear();
    }
}