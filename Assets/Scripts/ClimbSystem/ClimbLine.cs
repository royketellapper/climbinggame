﻿using UnityEngine;

[System.Serializable]
public class ClimbLine : ScriptableObject, IClimbLine
{
    public virtual IClimbVertex PosA { get { return _posA; } set { _posA = value as ClimbVertex; } }
    public virtual IClimbVertex PosB { get { return _posB; } set { _posB = value as ClimbVertex; } }
    public bool UseDirection { get { return _UseDirection; } set { _UseDirection = value; } }

    [SerializeField, HideInInspector]
    protected ClimbVertex _posA, _posB;
    [SerializeField]
    protected bool _UseDirection = true;

    public Vector3 LocalNormalize(Transform transform)
    {
        return Vector3.Normalize(PosA.Position - PosB.Position);
    }

    public Vector3 WorldNormalize(Transform transform)
    {
        return transform.TransformDirection(Vector3.Normalize(PosA.Position - PosB.Position));
    }

    public float Magnitude(Transform transform)
    {
        return Vector3.Magnitude(transform.TransformPoint(PosA.Position) - transform.TransformPoint(PosB.Position));
    }

    public float SqrMagnitude(Transform transform)
    {
        return Vector3.SqrMagnitude(transform.TransformPoint(PosA.Position) - transform.TransformPoint(PosB.Position));
    }

    public Quaternion Direction(Transform transform)
    {
        Vector3 a = transform.TransformPoint(PosA.Position);
        Vector3 b = transform.TransformPoint(PosB.Position);

        Vector3 normal = Vector3.Normalize(new Vector3(a.z - b.z, 0, a.x - b.x));
        return Quaternion.FromToRotation(normal, Vector3.forward);
    }

    public void SetDirection(Vector3 normal, Transform transform)
    {
        UseDirection = true;
        if (Vector3.Dot(Direction(transform) * Vector3.forward, normal) < 0)
            ClimbLineExtensions.FlipDirection(this);
    }

    public Vector3 MidPoint()
    {
        return PosA.Position - ((PosA.Position - PosB.Position) / 2); ;
    }

    public Vector3 Time(float t)
    {
        return PosA.Position + (PosA.Position - PosB.Position) * t;
    }

    public Vector3 ProjectPointOnLine(Vector3 localPos)
    {
        return Math3D.ProjectPointOnLine(PosA.Position, PosB.Position, localPos);
    }

    public bool ClampPoint(ref Vector3 point)
    {
        Vector3 normal = Vector3.Normalize(PosA.Position - PosB.Position);
        float a = Vector3.Dot(normal, PosA.Position - point);
        float b = Vector3.Dot(normal, point - PosB.Position);

        if (a < 0)
        {
            point = PosA.Position;
            return false;
        }
        else if (b < 0)
        {
            point = PosB.Position;
            return false;
        }

        return true;
    }

    public bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineNormal)
    {
        Vector3 normal = Vector3.Normalize(PosA.Position - PosB.Position);
        bool hit1 = Math3D.ClosestPointsOnTwoLines(out closestPointLine1, out closestPointLine2, PosA.Position, normal, linePoint1, lineNormal);
        bool hit2 = ClampPoint(ref closestPointLine1);
        return hit1 && hit2;
    }
}
