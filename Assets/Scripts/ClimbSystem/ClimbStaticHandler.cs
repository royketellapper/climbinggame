﻿using UnityEngine;

public class ClimbStaticHandler : MonoBehaviour, IClimbHandler
{
    public virtual ClimbData ClimbData
    {
        get
        {
            if (_climbDataClone == null)
                return _climbData;

            return _climbDataClone;
        }


        set { _climbData = value; }
    }
    [SerializeField]
    protected ClimbData _climbData;
    protected ClimbData _climbDataClone;

    public virtual IClimbVertex GetVertex(int index)
    {
        if (ClimbData == null)
            return null;

        return ClimbData.GetVertex(index);
    }

    public Vector3 GetVertexPosition(int index)
    {
        if (ClimbData == null)
            return Vector3.zero;

        return transform.TransformPoint(ClimbData.GetVertexPosition(index));
    }

    public virtual int GetVertexCount()
    {
        if (ClimbData == null)
            return 0;

        return ClimbData.GetVertexCount();
    }

    public virtual IClimbLine GetLine(int index)
    {
        if (ClimbData == null)
            return null;

        return ClimbData.GetLine(index);
    }

    public virtual int GetLineCount()
    {
        if (ClimbData == null)
            return 0;

        return ClimbData.GetLineCount();
    }

    public virtual T AddVertex<T>(Vector3 localPos) where T : UnityEngine.Object, IClimbVertex
    {
        if (ClimbData == null)
            return null;

        CreateClimbDataClone();
        return AddVertex<T>(localPos);
    }

    public virtual IClimbVertex AddVertex(Vector3 pos)
    {
        if (ClimbData == null)
            return null;

        CreateClimbDataClone();
        return ClimbData.AddVertex(transform.InverseTransformPoint(pos));
    }

    public virtual bool CanConnectLine(IClimbVertex vertA, IClimbVertex vertB)
    {
        if (ClimbData == null)
            return false;

        return ClimbData.CanConnectLine(vertA, vertB);
    }

    public virtual T AddLine<T>(IClimbVertex vertA, IClimbVertex vertB) where T : UnityEngine.Object, IClimbLine
    {
        if (ClimbData == null)
            return null;

        CreateClimbDataClone();
        return ClimbData.AddLine<T>(vertA, vertB);
    }

    public virtual IClimbLine AddLine(IClimbVertex vertA, IClimbVertex vertB)
    {
        if (ClimbData == null)
            return null;

        CreateClimbDataClone();
        return ClimbData.AddLine(vertA, vertB);
    }

    public virtual InsertPointInfo InsertPoint(IClimbLine climbLine, Vector3 pos)
    {
        if (ClimbData == null)
            return new InsertPointInfo();

        CreateClimbDataClone();
        return ClimbData.InsertPoint(climbLine, transform.InverseTransformPoint(pos));
    }

    public virtual void RemoveVertex(IClimbVertex vertex, bool autoConnect = false)
    {
        if (ClimbData == null)
            return;

        CreateClimbDataClone();
        ClimbData.RemoveVertex(vertex, autoConnect);
    }

    public virtual void RemoveLine(IClimbLine line)
    {
        if (ClimbData == null)
            return;

        CreateClimbDataClone();
        ClimbData.RemoveLine(line);
    }

    public void Clear()
    {
        if (ClimbData != null)
            ClimbData.Clear();
    }

    public virtual ClimbPointHitInfo ClosedPoint(Vector3 worldPos)
    {
        if (ClimbData == null)
            return new ClimbPointHitInfo();

        ClimbPointHitInfo hitInfo = ClimbData.ClosedPoint(transform.InverseTransformPoint(worldPos));
        hitInfo.HitPoint = transform.TransformPoint(hitInfo.HitPoint);
        return hitInfo;
    }

    public virtual bool ClosestPointsOnTwoLines(out ClimbPointHitInfo hitInfo, out Vector3 closestPointLine, float radius, Vector3 linePoint1, Vector3 lineNormal)
    {
        if (ClimbData == null)
        {
            hitInfo = new ClimbPointHitInfo();
            closestPointLine = Vector3.zero;
            return false;
        }

        bool collision = ClimbData.ClosestPointsOnTwoLines(out hitInfo, out closestPointLine, radius, transform.InverseTransformPoint(linePoint1), transform.InverseTransformVector(lineNormal));
        closestPointLine = transform.TransformPoint(closestPointLine);
        hitInfo.HitPoint = transform.TransformPoint(hitInfo.HitPoint);
        return collision;
    }

    public virtual bool ClosestPointsOnTwoLines(out ClimbPointHitInfo hitInfo, out Vector3 closestPointLine, Vector3 linePoint1, Vector3 lineNormal)
    {
        if (ClimbData == null)
        {
            hitInfo = new ClimbPointHitInfo();
            closestPointLine = Vector3.zero;
            return false;
        }

        bool collision = ClimbData.ClosestPointsOnTwoLines(out hitInfo, out closestPointLine, transform.InverseTransformPoint(linePoint1), transform.InverseTransformVector(lineNormal));
        closestPointLine = transform.TransformPoint(closestPointLine);
        hitInfo.HitPoint = transform.TransformPoint(hitInfo.HitPoint);
        return collision;
    }

    protected void CreateClimbDataClone()
    {
        if (_climbDataClone == null && Application.isPlaying)
            _climbDataClone = ScriptableObject.Instantiate(_climbData);
    }
}
