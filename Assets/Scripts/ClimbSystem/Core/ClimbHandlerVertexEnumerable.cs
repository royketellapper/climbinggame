﻿using System.Collections;
using System.Collections.Generic;

public struct ClimbHandlerVertexEnumerable : IEnumerable<IClimbVertex>, IEnumerator<IClimbVertex>
{
    private IClimbHandler _climbHandler;
    private int _index;

    public ClimbHandlerVertexEnumerable(IClimbHandler climbHandler)
    {
        _climbHandler = climbHandler;
        _index = -1;
    }

    IClimbVertex IEnumerator<IClimbVertex>.Current
    {
        get
        {
            return _climbHandler.GetVertex(_index);
        }
    }

    public object Current
    {
        get
        {
            return _climbHandler.GetVertex(_index);
        }
    }

    public bool MoveNext()
    {
        _index++;
        return _index < _climbHandler.GetVertexCount();
    }

    public void Reset()
    {
        _index = -1;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this;
    }

    public IEnumerator<IClimbVertex> GetEnumerator()
    {
        return this;
    }

    public void Dispose()
    {
        _index = -1;
        _climbHandler = null;
    }
}