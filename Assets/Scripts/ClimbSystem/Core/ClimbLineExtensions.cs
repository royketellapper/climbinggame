﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ClimbLineExtensions
{ 
    public static IClimbVertex GetConected(this IClimbLine IClimbLine, IClimbVertex climbVertex)
    {
        if (IClimbLine.PosA != climbVertex)
            return IClimbLine.PosA;

        return IClimbLine.PosB;
    }

    public static bool Contains(this IClimbLine IClimbLine, IClimbVertex climbVertex)
    {
        return IClimbLine.PosA == climbVertex || IClimbLine.PosB == climbVertex;
    }

    public static void FlipDirection(this IClimbLine IClimbLine)
    {
        var posA = IClimbLine.PosA;
        var posB = IClimbLine.PosB;
        IClimbLine.PosA = posB;
        IClimbLine.PosB = posA;
    }
}
