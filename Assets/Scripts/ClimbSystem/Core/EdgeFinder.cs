﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class EdgeFinder
{
    public float MaxTopAngle = 0.5f;
    public float MaxSideAngle = 0.5f;

    public List<TriangleInfo> _edgeTriangles = new List<TriangleInfo>();

    //Mesh data
    protected enum TransfomType
    {
        None,
        Matrix,
        Transfom
    }
    protected Mesh _mesh;
    protected Matrix4x4 _transfomMatrix;
    protected Transform _transfom;
    protected TransfomType _transfomType = TransfomType.None;
    protected IClimbHandler _climbHandler;

    //Progress
    public Action<float, int, string> OnProgress;
    public bool CancelProgress = false;

    public Action OnComplete;
    protected int _progress;
    protected MinMax _calculateTriangleProgress;

    public EdgeFinder(Mesh mesh)
    {
        _mesh = mesh;

        float length = (float)_mesh.triangles.Length / 3;
        _calculateTriangleProgress.Max = Mathf.RoundToInt(0.5f * length * (length + 1));
    }

    public void SetClimbHandler(IClimbHandler climbHandler)
    {
        _climbHandler = climbHandler;
    }

    public void SetTransform(Transform transfom)
    {
        _transfomType = TransfomType.Transfom;
        _transfom = transfom;
    }

    public void SetTransform(Matrix4x4 matrix)
    {
        _transfomType = TransfomType.Matrix;
        _transfomMatrix = matrix;
    }

    public void SetTransform()
    {
        _transfomType = TransfomType.None;
    }

    protected void OnProgressCallBack()
    {
        _progress++;
        
        if (OnProgress != null)
            OnProgress(_calculateTriangleProgress.GetAverageClamp(), _progress, "Calculate edge " + _calculateTriangleProgress.ToString());
    }

    protected void Reset()
    {
        _edgeTriangles.Clear();
        _calculateTriangleProgress.Min = 0;
        _progress = 0;
    }

    protected void FindEdges(TriangleInfo triangleData)
    {
        if (FindEdges(triangleData.EdgeA, triangleData.EdgeB))
            _edgeTriangles.Add(triangleData);
        else if (FindEdges(triangleData.EdgeB, triangleData.EdgeA))
        {
            //Swap position
            EdgeInfo EdgeA = triangleData.EdgeA;
            EdgeInfo EdgeB = triangleData.EdgeB;
            triangleData.EdgeA = EdgeB;
            triangleData.EdgeB = EdgeA;

            _edgeTriangles.Add(triangleData);
        }
    }

    protected bool FindEdges(EdgeInfo edgeA, EdgeInfo edgeB)
    {
        return Vector3.Dot(edgeA.Up(), Vector3.up) > MaxTopAngle &&
               Vector3.Dot(edgeB.Up(), edgeA.Forward()) > MaxSideAngle &&
               Vector3.Dot(edgeB.Up(), edgeB.GetPlane().ClosestPointOnPlane(edgeA.GetConectionVertex(2)) - edgeA.GetConectionVertex(2)) > 0;
    }

    protected int OuterVertex(List<int> list)
    {
        List<int> outer = new List<int>();
        for (int i = 0; i < 3; i++)
            outer.Add(i);

        for (int i = 0; i < list.Count; i++)
        {
            outer.Remove(list[i]);

            if (outer.Count == 1)
                return outer[0];
        }
        return 0;
    }

    protected ConectionInfo FindConection(Vector3[] vertices1, Vector3[] vertices2)
    {
        ConectionInfo conectionInfo = new ConectionInfo();

        for (int a = 0; a < 3; a++)
        {
            int b = (a + 1) % 3;
            int c = (a + 2) % 3;

            if (vertices1[a] == vertices2[a])
            {
                if (!conectionInfo.Add(a, a))
                {
                    conectionInfo.Clear();
                    return conectionInfo;
                }
            }

            if (vertices1[a] == vertices2[b])
            {
                if (!conectionInfo.Add(a, b))
                {
                    conectionInfo.Clear();
                    return conectionInfo;
                }
            }

            if (vertices1[a] == vertices2[c])
            {
                if (!conectionInfo.Add(a, c))
                {
                    conectionInfo.Clear();
                    return conectionInfo;
                }
            }
        }

        return conectionInfo;
    }

    protected Vector3 TransformPoint(Vector3 point)
    {
        switch (_transfomType)
        {
            case TransfomType.Matrix:
                return _transfomMatrix.MultiplyPoint3x4(point);
            case TransfomType.Transfom:
                return _transfom.TransformPoint(point);
        }

        return point;
    }

    public void CalulateEdges()
    {
        Reset();

        Vector3[] vertices1 = new Vector3[3];
        Vector3[] vertices2 = new Vector3[3];
        for (int a = 0; a < _mesh.triangles.Length; a += 3)
        {
            vertices1[0] = _mesh.vertices[_mesh.triangles[a]];
            vertices1[1] = _mesh.vertices[_mesh.triangles[a + 1]];
            vertices1[2] = _mesh.vertices[_mesh.triangles[a + 2]];

            int connectedTriagle = 0;
            //Find connected triangles
            for (int b = a + 3; b < _mesh.triangles.Length; b += 3)
            {
                vertices2[0] = _mesh.vertices[_mesh.triangles[b]];
                vertices2[1] = _mesh.vertices[_mesh.triangles[b + 1]];
                vertices2[2] = _mesh.vertices[_mesh.triangles[b + 2]];

                //var conections = FindConection(a, i, _mesh);
                var conections = FindConection(vertices1, vertices2);
                if (conections.Count == 2)
                {
                    TriangleInfo triangleData = new TriangleInfo();
                    conections.Add(OuterVertex(conections.ConectionA), OuterVertex(conections.ConectionB));

                    triangleData.EdgeA = new EdgeInfo(
                        TransformPoint(vertices1[0]),
                        TransformPoint(vertices1[1]),
                        TransformPoint(vertices1[2]),
                        conections.ConectionA.ToArray());

                    triangleData.EdgeB = new EdgeInfo(
                        TransformPoint(vertices2[0]),
                        TransformPoint(vertices2[1]),
                        TransformPoint(vertices2[2]),
                        conections.ConectionB.ToArray());

                    FindEdges(triangleData);
                    connectedTriagle++;
                }

                if (connectedTriagle > 2)
                {
                    _calculateTriangleProgress.Min += (_mesh.triangles.Length / 3) - a;
                    break;
                }

                if (CancelProgress)
                    return;

                _calculateTriangleProgress.Min++;
                OnProgressCallBack();
            }

            _calculateTriangleProgress.Min++;
            OnProgressCallBack();
            if (CancelProgress)
                return;
        }
        OnProgressCallBack();
        CalulateLines();
    }

    protected void CalulateLines()
    {
        if (_climbHandler == null)
            return;
        
        for (int i = 0; i < _edgeTriangles.Count; i++)
        {
            var triangle = _edgeTriangles[i];
            IClimbVertex VertexA = GetVertex(triangle.EdgeA.GetConectionVertex(0));
            IClimbVertex VertexB = GetVertex(triangle.EdgeA.GetConectionVertex(1));

            if (_climbHandler.CanConnectLine(VertexA, VertexB))
            {
                IClimbLine line = _climbHandler.AddLine(VertexA, VertexB);
                line.SetDirection(triangle.EdgeB.Up(), _transfom); //Fix me
            }
        } 
    }

    protected IClimbVertex GetVertex(Vector3 pos)
    {
        int count = _climbHandler.GetVertexCount();
        for (int i = 0; i < count; i++)
        {
            if (_climbHandler.GetVertexPosition(i) == pos)
                return _climbHandler.GetVertex(i);
        }

        return _climbHandler.AddVertex(pos);
    }
}
