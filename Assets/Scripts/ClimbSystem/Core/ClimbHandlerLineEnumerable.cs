﻿using System.Collections;
using System.Collections.Generic;

public struct ClimbHandlerLineEnumerable : IEnumerable<IClimbLine>, IEnumerator<IClimbLine>
{
    private IClimbHandler _climbHandler;
    private int _index;

    public ClimbHandlerLineEnumerable(IClimbHandler climbHandler)
    {
        _climbHandler = climbHandler;
        _index = -1;
    }

    IClimbLine IEnumerator<IClimbLine>.Current
    {
        get
        {
            return _climbHandler.GetLine(_index);
        }
    }

    public object Current
    {
        get
        {
            return _climbHandler.GetLine(_index);
        }
    }

    public bool MoveNext()
    {
        _index++;
        return _index < _climbHandler.GetLineCount();
    }

    public void Reset()
    {
        _index = -1;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this;
    }

    public IEnumerator<IClimbLine> GetEnumerator()
    {
        return this;
    }

    public void Dispose()
    {
        _index = -1;
        _climbHandler = null;
    }
}