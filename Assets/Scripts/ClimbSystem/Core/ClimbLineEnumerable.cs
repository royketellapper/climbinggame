﻿using System.Collections;
using System.Collections.Generic;

public struct ClimbLineEnumerable : IEnumerable<IClimbLine>, IEnumerator<IClimbLine>
{
    private IClimbVertex _climbVertex;
    private int _index;

    public ClimbLineEnumerable(IClimbVertex climbVertex)
    {
        _climbVertex = climbVertex;
        _index = -1;
    }

    IClimbLine IEnumerator<IClimbLine>.Current
    {
        get
        {
            return _climbVertex.GetLine(_index);
        }
    }

    public object Current
    {
        get
        {
            return _climbVertex.GetLine(_index);
        }
    }

    public bool MoveNext()
    {
        _index++;
        return _index < _climbVertex.GetLineCount();
    }

    public void Reset()
    {
        _index = -1;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this;
    }

    public IEnumerator<IClimbLine> GetEnumerator()
    {
        return this;
    }

    public void Dispose()
    {
        _index = -1;
        _climbVertex = null;
    }
}