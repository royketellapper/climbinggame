﻿using System;
using System.Collections.Concurrent;
using UnityEngine;

public class EdgeSettings
{
    public float MaxTopAngle = 0.5f;
    public float MaxSideAngle = 0.5f;
}

public class EdgeFinderV2
{
    protected EdgeSettings _edgeSettings;

    protected FindEdge[] _tindEdges;

    protected Mesh _mesh;
    protected Transform _transfom;
    protected IClimbHandler _climbHandler;

    protected int _threadCount = 1;
    protected int _currentIndex = 0;

    public bool Complete = false;

    protected float _progress;
    protected MinMax _calculateTriangleProgress;

    protected ConcurrentBag<TriangleInfo> _edgeTriangles = new ConcurrentBag<TriangleInfo>();


    public EdgeFinderV2(IClimbHandler climbHandler, Mesh mesh, Transform transfom, EdgeSettings edgeSettings)
    {
        _mesh = mesh;
        _edgeSettings = edgeSettings;
        _transfom = transfom;
        _climbHandler = climbHandler;

        _threadCount = Environment.ProcessorCount;
        _tindEdges = new FindEdge[_threadCount];
        for (int i = 0; i < _threadCount; i++)
        {
            _tindEdges[i] = new FindEdge()
            {
                EdgeSettings = _edgeSettings,
                Vertices = _mesh.vertices,
                Triangles = _mesh.triangles,
                TransfomMatrix = transfom.localToWorldMatrix,
                EdgeTriangles = _edgeTriangles
            };
        }

        float length = (float)_mesh.triangles.Length / 3;
        _calculateTriangleProgress.Max = Mathf.RoundToInt(0.5f * length * (length + 1));
    }

    protected void Reset()
    {
        _edgeTriangles = new ConcurrentBag<TriangleInfo>();
        _calculateTriangleProgress.Min = 0;
        _progress = 0;
    }

    public float GetProgress()
    {
        return _progress;
    }

    public void Abort()
    {
        for (int i = 0; i < _threadCount; i++)
            _tindEdges[i].Abort();
    }

    public bool ProgressEdges()
    {
        bool calulateEdgeComplete = true;
        float progressCount = 0;
        for (int i = 0; i < _tindEdges.Length; i++)
        {
            var item = _tindEdges[i];

            if (!item.IsAlive)
            {
                if (_currentIndex < _mesh.triangles.Length)
                {
                    int newIndex = _currentIndex + 6;
                    item.CalulateEdges(_currentIndex, newIndex, i);
                    _currentIndex = newIndex;
                    calulateEdgeComplete = false;
                }
            }
            else
                calulateEdgeComplete = false;

            progressCount += item.Progress;
        }

        _progress = progressCount / _calculateTriangleProgress.Max;

        if (!Complete && calulateEdgeComplete)
        {
            _progress = 0.98f;
            CalulateLines();
            _progress = 1;
            Complete = true;
        }

        return Complete;
    }

    protected void CalulateLines()
    {
        if (_climbHandler == null)
            return;

        foreach (var triangle in _edgeTriangles)
        {
            IClimbVertex VertexA = GetVertex(triangle.EdgeA.GetConectionVertex(0));
            IClimbVertex VertexB = GetVertex(triangle.EdgeA.GetConectionVertex(1));

            if (_climbHandler.CanConnectLine(VertexA, VertexB))
            {
                IClimbLine line = _climbHandler.AddLine(VertexA, VertexB);
                line.SetDirection(triangle.EdgeB.Up(), _transfom); //Fix me
            }
        } 
    }

    protected IClimbVertex GetVertex(Vector3 pos)
    {
        int count = _climbHandler.GetVertexCount();
        for (int i = 0; i < count; i++)
        {
            if (_climbHandler.GetVertexPosition(i) == pos)
                return _climbHandler.GetVertex(i);
        }

        return _climbHandler.AddVertex(pos);
    }
}
