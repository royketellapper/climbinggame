﻿using System;
using System.Collections.Generic;

public static class ClimbVertexExtensions
{
    public static IClimbLine FindLine(this IClimbVertex climbVertex, Predicate<IClimbLine> match)
    {
        int count = climbVertex.GetLineCount();
        for (int i = 0; i < count; i++)
        {
            var line = climbVertex.GetLine(i);

            if (match(line))
                return line;
        }

        return null;
    }

    public static List<IClimbLine> FindAllLines(this IClimbVertex climbVertex, Predicate<IClimbLine> match)
    {
        List<IClimbLine> list = new List<IClimbLine>();
        int count = climbVertex.GetLineCount();
        for (int i = 0; i < count; i++)
        {
            var line = climbVertex.GetLine(i);

            if (match(line))
                list.Add(line);
        }

        return list;
    }

    public static int FindLineIndex(this IClimbVertex climbVertex, Predicate<IClimbLine> match)
    {
        int count = climbVertex.GetLineCount();
        for (int i = 0; i < count; i++)
        {
            var line = climbVertex.GetLine(i);
            if (match(line))
                return i;
        }

        return -1;
    }

    public static int IndexOfLine(this IClimbVertex climbVertex, IClimbLine match)
    {
        int count = climbVertex.GetLineCount();
        for (int i = 0; i < count; i++)
        {
            var line = climbVertex.GetLine(i);
            if (line == match)
                return i;
        }
        return -1;
    }

    public static IClimbLine GetConnectedLine(this IClimbVertex climbVertex, IClimbVertex match)
    {
        int count = climbVertex.GetLineCount();
        for (int i = 0; i < count; i++)
        {
            var line = climbVertex.GetLine(i);
            if (line.PosA == match || line.PosB == match)
                return line;
        }
        return null;
    }

    public static IEnumerable<IClimbLine> GetLineEnumerable(this IClimbVertex climbVertex)
    {
        return new ClimbLineEnumerable(climbVertex);
    }
}
