﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CleanupSettings
{
    public bool EnableThreshold = true;
    public float Threshold = 0.998f;
    public bool RemoveSingleVertex = true;
    public bool RemoveSmallConerLines = true;
    public float MinConer = 0.5f;
    public float MinLineMagnitude = 0.15f;
}

[System.Serializable]
public struct BoxSize
{
    public Vector3 Center;
    public Vector3 Size;
}

public static class ClimbExtensions
{
    public static BoxSize GetBoxSize(this IEnumerable<IClimbVertex> vertices, Transform transform, float sizeOffset = 0)
    {
        Vector3 highestPosition = Vector3.zero;
        Vector3 lowestPosition = Vector3.zero;

        Matrix4x4 mat = Matrix4x4.TRS(transform.position, Quaternion.identity, transform.lossyScale);
        foreach (var item in vertices)
        {
            Vector3 localPosition = mat.MultiplyPoint3x4(item.GetLocalPosition(transform)) - transform.position;     

            if (highestPosition.x < localPosition.x)
                highestPosition.x = localPosition.x;

            if (highestPosition.y < localPosition.y)
                highestPosition.y = localPosition.y;

            if (highestPosition.z < localPosition.z)
                highestPosition.z = localPosition.z;

            if (lowestPosition.x > localPosition.x)
                lowestPosition.x = localPosition.x;

            if (lowestPosition.y > localPosition.y)
                lowestPosition.y = localPosition.y;

            if (lowestPosition.z > localPosition.z)
                lowestPosition.z = localPosition.z;
        }

        BoxSize boxSize = new BoxSize();
        boxSize.Center = lowestPosition + (highestPosition - lowestPosition) / 2;
        boxSize.Size = (highestPosition - lowestPosition) + new Vector3(sizeOffset, sizeOffset, sizeOffset);

        return boxSize;
    }

    public static void RemoveLine(this IClimbHandler climbHandler, IEnumerable<IClimbLine> lines)
    {
        foreach (var item in lines)
        {
            if (item != null)
                climbHandler.RemoveLine(item);
        }
    }

    public static void RemoveVertex(this IClimbHandler climbHandler, IEnumerable<IClimbVertex> vertices)
    {
        foreach (var item in vertices)
        {
            if (item != null)
                climbHandler.RemoveVertex(item);
        }
    }

    public static IEnumerable<IClimbVertex> GetVertexEnumerable(this IClimbHandler climbHandler)
    {
        return new ClimbHandlerVertexEnumerable(climbHandler);
    }

    public static IEnumerable<IClimbLine> GetLineEnumerable(this IClimbHandler climbHandler)
    {
        return new ClimbHandlerLineEnumerable(climbHandler);
    }

    //v1
    public static void LineCleanup(this IClimbHandler climbHandler, float angleThresHold = 0.998f)
    {
        Vector3[] normal = new Vector3[2];
        for (int a = 0; a < climbHandler.GetVertexCount(); a++)
        {
            var vertex = climbHandler.GetVertex(a);
            int lineCount = vertex.GetLineCount();

            if (lineCount == 2)
            {
                for (int b = 0; b < 2; b++)
                {
                    var line = vertex.GetLine(b);
                    var conectedVertex = line.GetConected(vertex);
                    normal[b] = Vector3.Normalize(conectedVertex.Position - vertex.Position);
                }

                if (-Vector3.Dot(normal[0], normal[1]) > angleThresHold)
                {
                    climbHandler.RemoveVertex(vertex, true);
                    a--;
                }
            }
        }
    }

    //V2
    public delegate void OnProgress(int current, float progress);
    public static void LineCleanup(this IClimbHandler climbHandler, Transform transform, CleanupSettings cleanupSettings, OnProgress onProgress)
    {
        Vector3[] normal = new Vector3[2];
        IClimbVertex[] conectedVertex = new IClimbVertex[2];
        int maxCount = (cleanupSettings.RemoveSmallConerLines) ? climbHandler.GetVertexCount() * 2 : climbHandler.GetVertexCount();
        int count = 0;
        List<IClimbLine> ignoreLine = new List<IClimbLine>();

        for (int a = 0; a < climbHandler.GetVertexCount(); a++)
        {
            IClimbVertex vertex = climbHandler.GetVertex(a);
            Vector3 posA = vertex.GetWorldPosition(transform);
            int lineCount = vertex.GetLineCount();

            if (cleanupSettings.RemoveSingleVertex && lineCount == 0)
            {
                climbHandler.RemoveVertex(vertex);
                a--;
            }
            else if (cleanupSettings.EnableThreshold && lineCount == 2)
            {
                for (int b = 0; b < 2; b++)
                {
                    var line = vertex.GetLine(b);
                    conectedVertex[b] = line.GetConected(vertex);
                    Vector3 posB = conectedVertex[b].GetWorldPosition(transform);
                    normal[b] = Vector3.Normalize(posB - posA);
                }

                if (-Vector3.Dot(normal[0], normal[1]) > cleanupSettings.Threshold)
                {
                    climbHandler.RemoveVertex(vertex, true);
                    a--;
                }
            }

            //Progress
            count++;
            onProgress(count, (float)count / (float)maxCount);
        }

        if (cleanupSettings.RemoveSmallConerLines)
        {
            //Remove Small Coner Lines
            for (int a = 0; a < climbHandler.GetVertexCount(); a++)
            {
                IClimbVertex vertex = climbHandler.GetVertex(a);
                Vector3 posA = vertex.GetWorldPosition(transform);
                int lineCount = vertex.GetLineCount();

                if (lineCount == 1)
                {
                    IClimbLine line1 = vertex.GetLine(0);
                    if (ignoreLine.Contains(line1))
                        continue;

                    conectedVertex[0] = line1.GetConected(vertex);

                    if (conectedVertex[0].GetLineCount() == 2)
                    {
                        IClimbLine line2 = conectedVertex[0].FindLine(l => l != line1);
                        conectedVertex[1] = line2.GetConected(conectedVertex[0]);
                        Vector3 posB = conectedVertex[0].GetWorldPosition(transform);

                        normal[0] = Vector3.Normalize(posA - posB);
                        normal[1] = Vector3.Normalize(posB - conectedVertex[1].GetWorldPosition(transform));

                        if ((conectedVertex[1].GetLineCount() > 1 || ignoreLine.Contains(line2)) &&
                            Vector3.Dot(normal[0], normal[1]) < cleanupSettings.MinConer &&
                            Vector3.SqrMagnitude(posA - posB) < cleanupSettings.MinLineMagnitude * cleanupSettings.MinLineMagnitude)
                        {
                            ignoreLine.Add(line2);
                            climbHandler.RemoveVertex(vertex);
                            a--;
                        }
                    }
                }

                //Progress
                count++;
                onProgress(count, (float)count / (float)maxCount);
            }
        }
    }
}
