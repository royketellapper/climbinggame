﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class FindEdge
{
    public EdgeSettings EdgeSettings;
    public Vector3[] Vertices;
    public int[] Triangles;
    public Matrix4x4 TransfomMatrix;
    public int Progress;
    public bool CancelProgress;
    public ConcurrentBag<TriangleInfo> EdgeTriangles;
    public bool IsAlive = false;

    protected bool _abort = false;
    protected int _start = 0;
    protected int _end = 1;

    public void CalulateEdges(int start, int end, int index)
    {
        if (!IsAlive)
        {
            //Progress = 0;
            _start = start;
            _end = end;
            IsAlive = true;
            _abort = false;
            ThreadPool.QueueUserWorkItem(Calulate, index);
        }
    }

    public void Abort()
    {
        _abort = true;
    }

    protected void Calulate(object threadContext)
    {
        Vector3[] vertices1 = new Vector3[3];
        Vector3[] vertices2 = new Vector3[3];
        // int count = (3 * _end) + _start;
        _end = Mathf.Clamp(_end, 0 , Triangles.Length);
        for (int a = _start; a < _end; a += 3)
        {
            vertices1[0] = Vertices[Triangles[a]];
            vertices1[1] = Vertices[Triangles[a + 1]];
            vertices1[2] = Vertices[Triangles[a + 2]];

            int connectedTriagle = 0;
            //Find connected triangles
            for (int b = a + 3; b < Triangles.Length; b += 3)
            {
                vertices2[0] = Vertices[Triangles[b]];
                vertices2[1] = Vertices[Triangles[b + 1]];
                vertices2[2] = Vertices[Triangles[b + 2]];

                //var conections = FindConection(a, i, _mesh);
                var conections = FindConection(vertices1, vertices2);
                if (conections.Count == 2)
                {
                    TriangleInfo triangleData = new TriangleInfo();
                    conections.Add(OuterVertex(conections.ConectionA), OuterVertex(conections.ConectionB));

                    triangleData.EdgeA = new EdgeInfo(
                        TransfomMatrix.MultiplyPoint3x4(vertices1[0]),
                        TransfomMatrix.MultiplyPoint3x4(vertices1[1]),
                        TransfomMatrix.MultiplyPoint3x4(vertices1[2]),
                        conections.ConectionA.ToArray());

                    triangleData.EdgeB = new EdgeInfo(
                        TransfomMatrix.MultiplyPoint3x4(vertices2[0]),
                        TransfomMatrix.MultiplyPoint3x4(vertices2[1]),
                        TransfomMatrix.MultiplyPoint3x4(vertices2[2]),
                        conections.ConectionB.ToArray());

                    FindEdges(triangleData);
                    connectedTriagle++;
                }

                if (connectedTriagle > 2 || _abort)
                {
                    Progress += (Triangles.Length / 3) - b;
                    break;
                }

                Progress++;
            }

            if (_abort)
                break;

            Progress++;
        }

        IsAlive = false;
    }

    private void FindEdges(TriangleInfo triangleData)
    {
        if (FindEdges(triangleData.EdgeA, triangleData.EdgeB))
            EdgeTriangles.Add(triangleData);
        else if (FindEdges(triangleData.EdgeB, triangleData.EdgeA))
        {
            //Swap position
            EdgeInfo EdgeA = triangleData.EdgeA;
            EdgeInfo EdgeB = triangleData.EdgeB;
            triangleData.EdgeA = EdgeB;
            triangleData.EdgeB = EdgeA;

            EdgeTriangles.Add(triangleData);
        }
    }

    private bool FindEdges(EdgeInfo edgeA, EdgeInfo edgeB)
    {
        return Vector3.Dot(edgeA.Up(), Vector3.up) > EdgeSettings.MaxTopAngle &&
               Vector3.Dot(edgeB.Up(), edgeA.Forward()) > EdgeSettings.MaxSideAngle &&
               Vector3.Dot(edgeB.Up(), edgeB.GetPlane().ClosestPointOnPlane(edgeA.GetConectionVertex(2)) - edgeA.GetConectionVertex(2)) > 0;
    }

    private int OuterVertex(List<int> list)
    {
        List<int> outer = new List<int>();
        for (int i = 0; i < 3; i++)
            outer.Add(i);

        for (int i = 0; i < list.Count; i++)
        {
            outer.Remove(list[i]);

            if (outer.Count == 1)
                return outer[0];
        }
        return 0;
    }

    private ConectionInfo FindConection(Vector3[] vertices1, Vector3[] vertices2)
    {
        ConectionInfo conectionInfo = new ConectionInfo();

        for (int a = 0; a < 3; a++)
        {
            int b = (a + 1) % 3;
            int c = (a + 2) % 3;

            if (vertices1[a] == vertices2[a])
            {
                if (!conectionInfo.Add(a, a))
                {
                    conectionInfo.Clear();
                    return conectionInfo;
                }
            }

            if (vertices1[a] == vertices2[b])
            {
                if (!conectionInfo.Add(a, b))
                {
                    conectionInfo.Clear();
                    return conectionInfo;
                }
            }

            if (vertices1[a] == vertices2[c])
            {
                if (!conectionInfo.Add(a, c))
                {
                    conectionInfo.Clear();
                    return conectionInfo;
                }
            }
        }

        return conectionInfo;
    }
}
