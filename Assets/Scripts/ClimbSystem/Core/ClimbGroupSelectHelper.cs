﻿using System.Collections.Generic;
using UnityEngine;

public class GroupSelectInfo
{
    public List<IClimbLine> Lines = new List<IClimbLine>();
    public List<IClimbVertex> Vertices = new List<IClimbVertex>();

    public void Clear()
    {
        Lines.Clear();
        Vertices.Clear();
    }

    public void Remove(object obj)
    {
        if (obj is IClimbLine)
            Lines.Remove((IClimbLine)obj);

        else if (obj is IClimbVertex)
            Vertices.Remove((IClimbVertex)obj);
    }

    public void Add(object obj)
    {
        if (obj is IClimbLine && !Lines.Contains((IClimbLine)obj))
            Lines.Add((IClimbLine)obj);

        else if(obj is IClimbVertex && !Vertices.Contains((IClimbVertex)obj))
            Vertices.Add((IClimbVertex)obj);
    }

    public List<object> GetObjects()
    {
        List<object> list = new List<object>();
        list.AddRange(Lines);
        list.AddRange(Vertices);
        return list;
    }
}

public static class ClimbGroupSelectHelper
{
    public static GroupSelectInfo GroupSelect(IEnumerable<object> objs, GroupSelectInfo info = null)
    {
        if (info == null)
            info = new GroupSelectInfo();

        foreach (var item in objs)
            GroupSelect(item, info);

        return info;
    }

    public static GroupSelectInfo GroupSelect(object obj, GroupSelectInfo info = null)
    {
        if (obj == null)
            return info;

        if (obj is IClimbLine)
           return  GroupSelect((IClimbLine)obj, info);

        else if (obj is IClimbVertex)
            return GroupSelect((IClimbVertex)obj, info);

        else if (obj is IEnumerable<IClimbLine>)
            return GroupSelect((IEnumerable<IClimbLine>)obj, info);

        else if (obj is IEnumerable<IClimbVertex>)
            return GroupSelect((IEnumerable<IClimbVertex>)obj, info);

        return info;
    }

    public static GroupSelectInfo GroupSelect(this IEnumerable<IClimbLine> lines, GroupSelectInfo info = null)
    {
        if (info == null)
            info = new GroupSelectInfo();

        foreach (var line in lines)
            GroupSelect(line, info);

        return info;
    }

    public static GroupSelectInfo GroupSelect(this IClimbLine line, GroupSelectInfo info = null)
    {
        if (info == null)
            info = new GroupSelectInfo();

        FindConnections(line.PosA, info);
        FindConnections(line.PosB, info);

        return info;
    }

    public static GroupSelectInfo GroupSelect(this IEnumerable<IClimbVertex> vertices, GroupSelectInfo info = null)
    {
        if (info == null)
            info = new GroupSelectInfo();

        foreach (var vertex in vertices)
            FindConnections(vertex, info);

        return info;
    }

    public static GroupSelectInfo GroupSelect(this IClimbVertex vertex, GroupSelectInfo info = null)
    {
        if (info == null)
            info = new GroupSelectInfo();

        FindConnections(vertex, info);

        return info;
    }

    private static void FindConnections(this IClimbVertex vertex, GroupSelectInfo info)
    {
        int count = vertex.GetLineCount();
        for (int i = 0; i < count; i++)
        {
            var line = vertex.GetLine(i);

            if (!info.Vertices.Contains(vertex))
                info.Vertices.Add(vertex);

            if (!info.Lines.Contains(line))
            {
                info.Lines.Add(line);
                FindConnections(line.GetConected(vertex), info);
            }
        }
    }

    public static void Remove(this IClimbHandler climbHandler, GroupSelectInfo info)
    {
        foreach (var item in info.Vertices)
        {
            if (item != null)
                climbHandler.RemoveVertex(item);
        }

        foreach (var item in info.Lines)
        {
            if (item != null)
                climbHandler.RemoveLine(item);
        }

        info.Clear();
    }
}
