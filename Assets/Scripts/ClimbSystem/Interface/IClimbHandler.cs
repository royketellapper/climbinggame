﻿using UnityEngine;

public interface IClimbHandler
{
    IClimbVertex GetVertex(int index);
    Vector3 GetVertexPosition(int index);
    int GetVertexCount();
    IClimbLine GetLine(int index);
    int GetLineCount();
    T AddVertex<T>(Vector3 localPos) where T : Object, IClimbVertex;
    IClimbVertex AddVertex(Vector3 pos);
    T AddLine<T>(IClimbVertex vertA, IClimbVertex vertB) where T : Object, IClimbLine;
    IClimbLine AddLine(IClimbVertex vertA, IClimbVertex vertB);
    InsertPointInfo InsertPoint(IClimbLine climbLine, Vector3 pos);
    void RemoveVertex(IClimbVertex vertex, bool autoConnect = false);
    void RemoveLine(IClimbLine line);
    void Clear();
    bool CanConnectLine(IClimbVertex vertA, IClimbVertex vertB);
    ClimbPointHitInfo ClosedPoint(Vector3 pos);
    bool ClosestPointsOnTwoLines(out ClimbPointHitInfo hitInfo, out Vector3 closestPointLine, float radius, Vector3 linePoint1, Vector3 lineNormal);
    bool ClosestPointsOnTwoLines(out ClimbPointHitInfo hitInfo, out Vector3 closestPointLine, Vector3 linePoint1, Vector3 lineNormal);
}
