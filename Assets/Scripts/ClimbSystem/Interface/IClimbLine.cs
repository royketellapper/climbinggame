﻿using UnityEngine;

public interface IClimbLine
{
    IClimbVertex PosA { get; set; }
    IClimbVertex PosB { get; set; }
    bool UseDirection { get; set; }
    Vector3 WorldNormalize(Transform transform);
    Vector3 LocalNormalize(Transform transform);
    float Magnitude(Transform transform);
    float SqrMagnitude(Transform transform);
    Quaternion Direction(Transform transform);
    void SetDirection(Vector3 normal, Transform transform);
    Vector3 MidPoint();
    Vector3 Time(float t);
    Vector3 ProjectPointOnLine(Vector3 pos);
    bool ClampPoint(ref Vector3 point);
    bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineNormal);
}
