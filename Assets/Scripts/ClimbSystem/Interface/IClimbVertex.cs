﻿using System;
using UnityEngine;

public interface IClimbVertex
{
    Vector3 Position { get; set; }
    Vector3 GetWorldPosition(Transform transform);
    Vector3 GetLocalPosition(Transform transform);
    void SetWorldPosition(Vector3 pos,Transform transform);
    void SetLocalPosition(Vector3 pos, Transform transform);
    IClimbLine GetLine(int index);
    int GetLineCount();
    void AddLine(IClimbLine line);
    void RemoveLine(IClimbLine line);
    void RemoveAllLines(Predicate<IClimbLine> match);
    void ClearLines();
    void Replace(IClimbLine match, IClimbLine replace);
}
