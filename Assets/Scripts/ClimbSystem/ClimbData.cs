﻿using System;
using System.Collections.Generic;
using UnityEditor;
#if UNITY_EDITOR
using UnityEngine;
#endif

public struct InsertPointInfo
{
    public IClimbVertex Vertex;
    public IClimbLine Line;
}

[Serializable, CreateAssetMenu(fileName = "ClimbLineData", menuName = "Climbing/ClimbLineData", order = 1)]
public class ClimbData : ScriptableObject, IClimbHandler
{
    [HideInInspector, SerializeField]
    protected List<ClimbVertex> _vertices = new List<ClimbVertex>();
    [HideInInspector, SerializeField]
    protected List<ClimbLine> _lines = new List<ClimbLine>();

    public IClimbVertex GetVertex(int index)
    {
        return _vertices[index];
    }

    public Vector3 GetVertexPosition(int index)
    {
        return _vertices[index].Position;
    }

    public int GetVertexCount()
    {
        return _vertices.Count;
    }

    public IClimbLine GetLine(int index)
    {
        return _lines[index];
    }

    public int GetLineCount()
    {
        return _lines.Count;
    }

    public T AddVertex<T>(Vector3 localPos) where T : UnityEngine.Object, IClimbVertex
    {
        if (typeof(T) == typeof(ClimbVertex))
        {
            ClimbVertex vertex = CreateInstance(typeof(T)) as ClimbVertex;
            vertex.Position = localPos;
            _vertices.Add(vertex);
            vertex.hideFlags = HideFlags.HideInHierarchy;

#if UNITY_EDITOR
            if (!Application.isPlaying)
                AssetDatabase.AddObjectToAsset(vertex, this);
#endif

            return vertex as T;
        }

        return null;
    }

    public IClimbVertex AddVertex(Vector3 localPos)
    {
        return AddVertex<ClimbVertex>(localPos);
    }

    public bool CanConnectLine(IClimbVertex vertA, IClimbVertex vertB)
    {
        return vertA != vertB &&
            vertA.FindLine(a => a.PosA == vertB || a.PosB == vertB) == null &&
            vertB.FindLine(a => a.PosA == vertA || a.PosB == vertA) == null;
    }

    public virtual T AddLine<T>(IClimbVertex vertA, IClimbVertex vertB) where T : UnityEngine.Object, IClimbLine
    {
        if (typeof(T) == typeof(ClimbLine))
        {
            ClimbLine line = CreateInstance(typeof(T)) as ClimbLine;
            line.PosA = vertA;
            line.PosB = vertB;
            line.hideFlags = HideFlags.HideInHierarchy;

            vertA.AddLine(line);
            vertB.AddLine(line);
            _lines.Add(line);

#if UNITY_EDITOR
            if (!Application.isPlaying)
                AssetDatabase.AddObjectToAsset(line, this);
#endif

            return line as T;
        }

        return null;
    }

    public virtual IClimbLine AddLine(IClimbVertex vertA, IClimbVertex vertB)
    {
        return AddLine<ClimbLine>(vertA, vertB);
    }

    public virtual void Clear()
    {
        bool isPlaying = Application.isPlaying;
        for (int i = 0; i < _vertices.Count; i++)
        {
            if (isPlaying)
                Destroy(_vertices[i]);
            else
                DestroyImmediate(_vertices[i], true);
        }
        _vertices.Clear();

        for (int i = 0; i < _lines.Count; i++)
        {
            if (isPlaying)
                Destroy(_lines[i]);
            else
                DestroyImmediate(_lines[i], true);
        }
        _lines.Clear();
    }

    public virtual InsertPointInfo InsertPoint(IClimbLine climbLine, Vector3 localPos)
    {
        InsertPointInfo pointInfo = new InsertPointInfo();
        pointInfo.Vertex = AddVertex(localPos);
        climbLine.PosB.RemoveLine(climbLine);
        pointInfo.Line = AddLine(pointInfo.Vertex, climbLine.PosB);
        climbLine.PosB = pointInfo.Vertex;
        pointInfo.Vertex.AddLine(climbLine);
        return pointInfo;
    }

    public void RemoveVertex(IClimbVertex vertex, bool autoConnect = false)
    {
        int count = vertex.GetLineCount();
        bool isPlaying = Application.isPlaying;

        //Auto Connect Line
        if (autoConnect && count == 2)
        {
            var a = vertex.GetLine(1);
            var b = vertex.GetLine(0);

            if (a.PosA == vertex)
            {
                a.PosA = b.GetConected(vertex);
                a.PosA.Replace(b, a);
            }
            else if (a.PosB == vertex)
            {
                a.PosB = b.GetConected(vertex);
                a.PosB.Replace(b, a);
            }

            count--;
        }
        //Delete lines
        for (int i = 0; i < count; i++)
        {
            var item = vertex.GetLine(i);
            if (item.PosA != vertex)
                item.PosA.RemoveLine(item);

            if (item.PosB != vertex)
                item.PosB.RemoveLine(item);

            _lines.Remove(item as ClimbLine);

            if (isPlaying)
                Destroy(item as ClimbLine);
            else
                DestroyImmediate(item as ClimbLine, true);
        }

        vertex.ClearLines();
        _vertices.Remove(vertex as ClimbVertex);

        if (isPlaying)
            Destroy(vertex as ClimbVertex);
        else
            DestroyImmediate(vertex as ClimbVertex, true);
    }

    public void RemoveLine(IClimbLine line)
    {
        line.PosA.RemoveLine(line);
        line.PosB.RemoveLine(line);
        _lines.Remove(line as ClimbLine);

        if (Application.isPlaying)
            Destroy(line as ClimbLine);
        else
            DestroyImmediate(line as ClimbLine, true);
    }

    public virtual ClimbPointHitInfo ClosedPoint(Vector3 localPos)
    {
        float closedPoint = float.MaxValue;
        ClimbPointHitInfo hitInfo = new ClimbPointHitInfo();

        foreach (var item in _lines)
        {
            Vector3 point = item.ProjectPointOnLine(localPos);
            float dist = Vector3.SqrMagnitude(point - localPos);

            if (dist < closedPoint)
            {
                closedPoint = dist;
                hitInfo.HitPoint = point;
                hitInfo.ClimbLine = item;
            }
        }

        return hitInfo;
    }

    public bool ClosestPointsOnTwoLines(out ClimbPointHitInfo hitInfo, out Vector3 closestPointLine, float radius, Vector3 linePoint1, Vector3 lineNormal)
    {
        float closedPoint = float.MaxValue;
        bool collision = false;
        Vector3 Point1, Point2;
        hitInfo = new ClimbPointHitInfo();
        closestPointLine = linePoint1;

        foreach (var item in _lines)
        {
            if (item.ClosestPointsOnTwoLines(out Point1, out Point2, linePoint1, lineNormal))
            {
                float radiusDist, closedPointDist;
                closedPointDist = Vector3.Dot(lineNormal, Point1 - linePoint1);
                if (closedPointDist < 0)
                    continue;

                radiusDist = Vector3.SqrMagnitude(Point1 - Point2);

                if (closedPointDist < closedPoint && radiusDist < radius)
                {
                    closedPoint = closedPointDist;
                    collision = true;
                    hitInfo.ClimbLine = item;
                    hitInfo.HitPoint = Point1;
                    closestPointLine = Point2;
                }
            }
        }

        return collision;
    }

    public bool ClosestPointsOnTwoLines(out ClimbPointHitInfo hitInfo, out Vector3 closestPointLine, Vector3 linePoint1, Vector3 lineNormal)
    {
        //float closedPoint1 = 0;
        //float closedPoint2 = float.MaxValue;
        float closedPoint3 = float.MaxValue;

        bool collision = false;
        Vector3 Point1, Point2;
        hitInfo = new ClimbPointHitInfo();
        closestPointLine = linePoint1;

        foreach (var item in _lines)
        {
            if (item.ClosestPointsOnTwoLines(out Point1, out Point2, linePoint1, lineNormal))
            {
                float closedPointDist1, /*closedPointDist2,*/ closedPointDist3;
                closedPointDist1 = Vector3.Dot(lineNormal, Vector3.Normalize(Point1 - linePoint1));
                //closedPointDist2 = Vector3.SqrMagnitude(Point1 - linePoint1);
                closedPointDist3 = Vector3.SqrMagnitude(Point1 - Point2);

                //Debug.Log(closedPointDist1);

                if (closedPointDist1 < 0)
                    continue;

                if (closedPointDist3 < closedPoint3)
                {
                    closedPoint3 = closedPointDist3;
                    //if (closedPointDist2 < closedPoint2)
                    //{
                    //    closedPoint2 = closedPointDist2;
                    // if (closedPointDist1 > closedPoint1)
                    //  {
                    //      closedPoint1 = closedPointDist1;
                    collision = true;
                    hitInfo.ClimbLine = item;
                    hitInfo.HitPoint = Point1;
                    closestPointLine = Point2;
                    //   }
                    //}
                }
            }
        }

        return collision;
    }
}
