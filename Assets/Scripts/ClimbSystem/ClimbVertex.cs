﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ClimbVertex : ScriptableObject, IClimbVertex
{
    public virtual Vector3 Position
    {
        get { return _localPosition; }
        set { _localPosition = value; }
    }
    [SerializeField]
    protected Vector3 _localPosition;

    public virtual List<ClimbLine> Lines
    {
        get { return _lines; }
        set { _lines = value; }
    }

    [SerializeField, HideInInspector]
    protected List<ClimbLine> _lines = new List<ClimbLine>();

    public Vector3 GetWorldPosition(Transform transform)
    {
       return transform.TransformPoint(Position);
    }

    public Vector3 GetLocalPosition(Transform transform)
    {
        return Position;
    }

    public void SetWorldPosition(Vector3 pos, Transform transform)
    {
        Position = transform.InverseTransformPoint(pos);
    }

    public void SetLocalPosition(Vector3 pos, Transform transform)
    {
        Position = pos;
    }

    public IClimbLine GetLine(int index)
    {
        return Lines[index];
    }

    public int GetLineCount()
    {
        return Lines.Count;
    }

    public void AddLine(IClimbLine line)
    {
        if (line is ClimbLine)
            Lines.Add(line as ClimbLine);
        else
            Debug.LogError("ClimbVertex.AddLine: This ClimbLine type is not supported");
    }

    public void RemoveLine(IClimbLine line)
    {
        if (line is ClimbLine)
            Lines.Remove(line as ClimbLine);
        else
            Debug.LogError("ClimbVertex.AddLine: This ClimbLine type is not supported");
    }

    public void RemoveAllLines(Predicate<IClimbLine> match)
    {
        for (int i = 0; i < _lines.Count; i++)
        {
            if (match(Lines[i]))
                RemoveLine(Lines[i]);
        }
    }

    public void ClearLines()
    {
        Lines.Clear();
    }

    public void Replace(IClimbLine match, IClimbLine replace)
    {
        for (int i = 0; i < _lines.Count; i++)
        {
            if (Lines[i] as IClimbLine == match)
            {
                Lines[i] = (ClimbLine)replace;
                return;
            }
        }

        Lines.Add((ClimbLine)replace);
    }
}
