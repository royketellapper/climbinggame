﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;


public class ClimbHandlerEditor : Editor
{
    protected ClimbStaticHandler _climbHandler;

    [SerializeField]
    protected object _currentSelect { get { return (_currentSelected != null && _currentSelected.Count == 1) ? _currentSelected[0] : null; } }
    [SerializeField]
    protected List<object> _currentSelected = new List<object>();
    [SerializeField]
    protected GroupSelectInfo _groupSelectInfo = new GroupSelectInfo();

    protected Editor _currentEditor;
    protected Camera _currentCam;
    protected Vector3 _oldVertexPos;
    protected int _toolbarIndex;
    protected GUIContent[] _toolIcons;

    protected bool _connectVertex;
    [SerializeField]
    protected bool _settingsFoldout = true;
    [SerializeField]
    protected bool _toolsFoldout = true;
    [SerializeField]
    protected bool _autoConnect = true;
    [SerializeField]
    protected bool _hideDirections = false;
    [SerializeField]
    protected bool _zTest = true;

    protected virtual void OnEnable()
    {
        _climbHandler = target as ClimbStaticHandler;
        _toolIcons = new GUIContent[]
        {
            EditorGUIUtility.IconContent("VertexLine", "|VertexLine"),
            EditorGUIUtility.IconContent("Vertex", "|Vertex"),
            EditorGUIUtility.IconContent("Line", "|Line"),
            EditorGUIUtility.IconContent("TerrainInspector.TerrainToolSettings", "|Settings")
        };

        var data = EditorPrefs.GetString("ClimbHandlerEditor1", JsonUtility.ToJson(this, false));
        JsonUtility.FromJsonOverwrite(data, this);
    }

    protected virtual void OnDisable()
    {
        var data = JsonUtility.ToJson(this, false);
        EditorPrefs.SetString("ClimbHandlerEditor1", data);
    }

    protected virtual void OnDestroy()
    {
        if (_currentEditor != null)
            DestroyImmediate(_currentEditor);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        _toolbarIndex = GUILayout.Toolbar(_toolbarIndex, _toolIcons);

        _toolsFoldout = EditorGUILayout.Foldout(_toolsFoldout, "Tools", true);
        if (_climbHandler.ClimbData != null && _toolsFoldout)
        {
            //Auto connect
            _autoConnect = EditorGUILayout.Toggle("Auto connect:", _autoConnect);
            _hideDirections = EditorGUILayout.Toggle("Hide Directions:", _hideDirections);
            _zTest = EditorGUILayout.Toggle("ZTest:", _zTest);

            //Add Vertex
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(_connectVertex || _currentSelected.Count != 1);
            if (GUILayout.Button("Add Vertex"))
                AddVertex();
            EditorGUI.EndDisabledGroup();

            //Connect
            EditorGUI.BeginDisabledGroup(_currentSelected.Count != 1);
            _connectVertex = GUILayout.Toggle(_connectVertex, "Connect", "Button");
            EditorGUILayout.EndHorizontal();
            EditorGUI.EndDisabledGroup();

            //Selectgroup
            EditorGUI.BeginDisabledGroup(_connectVertex);
            if (GUILayout.Button("Select Group"))
                SelectGroup();
            EditorGUI.EndDisabledGroup();

            //Delete
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(_connectVertex || _currentSelected.Count == 0);
            if (GUILayout.Button("Delete"))
                Delete();
            EditorGUI.EndDisabledGroup();

            //Delete All
            EditorGUI.BeginDisabledGroup(_connectVertex);
            if (GUILayout.Button("Delete All"))
            {
                _climbHandler.Clear();
                SetSelected(null);
            }
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Create Box Collider"))
                CreateBoxCollider();
        }

        //Settings
        _settingsFoldout = EditorGUILayout.Foldout(_settingsFoldout, "Settings", true);
        if (_currentEditor != null && _settingsFoldout)
        {
            EditorGUILayout.BeginVertical("Box");
            _currentEditor.OnInspectorGUI();
            EditorGUILayout.EndVertical();
        }
    }

    protected virtual void SelectGroup()
    {
        _groupSelectInfo.Clear();
        _groupSelectInfo = ClimbGroupSelectHelper.GroupSelect(_currentSelected, _groupSelectInfo);

        if (_toolbarIndex == 1)
            _groupSelectInfo.Lines.Clear();
        else if (_toolbarIndex == 2)
            _groupSelectInfo.Vertices.Clear();

        _currentSelected = _groupSelectInfo.GetObjects();
    }

    protected virtual void AddVertex()
    {
        Vector3 vertexLoc = Vector3.zero;
        IClimbVertex climbVertex = _currentSelect as IClimbVertex;

        //Vertex spawn location
        if (climbVertex != null)
        {
            Vector3 dir = Vector3.zero;
            if (climbVertex.GetLineCount() != 0)
            {
                var line = climbVertex.GetLine(0);
                Vector3 a = climbVertex.GetWorldPosition(_climbHandler.transform);
                Vector3 b = line.GetConected(climbVertex).GetWorldPosition(_climbHandler.transform);
                dir = Vector3.Normalize(a - b);
                float a1 = Vector3.Dot(dir, a - (a + dir));
                float b2 = Vector3.Dot(dir, b - (a + dir));
                if (a1 > 0)
                    dir *= -1;
                else if (b2 > 0)
                    dir *= -1;
            }

            vertexLoc = climbVertex.GetWorldPosition(_climbHandler.transform) + dir;
        }

        //Create Vertex
        var vertex = _climbHandler.AddVertex(vertexLoc);

        //Auto Connect
        if (_autoConnect && climbVertex != null)
            _climbHandler.AddLine(climbVertex, vertex);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        SetSelected(vertex);
    }

    protected virtual void Delete()
    {
        _climbHandler.ClimbData.Remove(_groupSelectInfo);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        SetSelected(null);
    }

    protected virtual void ConnectVertex(IClimbVertex vertA, IClimbVertex vertB)
    {
        if (_climbHandler.CanConnectLine(vertA, vertB))
        {
            _climbHandler.AddLine<ClimbLine>(vertA, vertB);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }

    protected virtual InsertPointInfo InsertPoint(IClimbLine climbLine, Vector3 pos)
    {
        var pointInfo = _climbHandler.InsertPoint(climbLine, pos);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        return pointInfo;
    }

    protected virtual void OnSceneGUI()
    {
        if (_climbHandler.ClimbData == null)
        {
            SetSelected(null);
            return;
        }

        if (Tools.current != Tool.None)
            SetSelected(null);

        _currentCam = Camera.current;
        Handles.zTest = (_zTest) ? CompareFunction.LessEqual : CompareFunction.Always;

        DrawLines();
        DrawPivotButton();
        DrawGizmoSelection();

        if (_currentSelected.Count != 0)
            Tools.current = Tool.None;
    }

    protected virtual void LineSelect(IClimbLine climbLine, Vector3 hitPoint)
    {
        var currentSelect = _currentSelect;
        if (_connectVertex)
        {
            var vertex = currentSelect as IClimbVertex;
            bool containsLine = false;
            if (vertex != null)
                containsLine = vertex.FindLine(a => a == climbLine) != null;

            if (currentSelect != null && (currentSelect == climbLine || containsLine))
            {
                InsertPointInfo info = InsertPoint(climbLine, hitPoint);
                SetSelected((Object)info.Vertex);
                _connectVertex = false;
            }
            else if (currentSelect != climbLine && currentSelect != null && currentSelect is IClimbVertex)
            {
                InsertPointInfo info = InsertPoint(climbLine, hitPoint);
                ConnectVertex((IClimbVertex)currentSelect, info.Vertex);
                SetSelected((Object)info.Vertex);
                _connectVertex = false;
            }
        }
        else
            SetSelected((Object)climbLine);

        SceneView.RepaintAll();
    }

    protected virtual void DrawLines()
    {
        Ray point = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        ClimbPointHitInfo hit;
        Vector3 closestPointLine;
        Vector2 hit2D = Vector2.zero;
        var currentSelect = _currentSelect;

        if (_climbHandler.ClosestPointsOnTwoLines(out hit, out closestPointLine, point.origin, point.direction))
        {
            hit2D = HandleUtility.WorldToGUIPoint(hit.HitPoint);
            Quaternion lookAtRot = Quaternion.LookRotation(hit.HitPoint - _currentCam.transform.position);
            float buttonScale = _currentCam.WorldToScreenPoint(hit.HitPoint).z * 0.015f;
            float size = HandleUtility.GetHandleSize(hit.HitPoint);

            Handles.color = new Color(0, 0, 0, 0);
            float pickSize = (_connectVertex && hit.ClimbLine == currentSelect) ? size * 0.09f : 0;
            if (Handles.Button(hit.HitPoint, lookAtRot, pickSize, size * 0.12f, Handles.CircleHandleCap))
                LineSelect(hit.ClimbLine, hit.HitPoint);
            Handles.color = Color.white;
        }

        int count = _climbHandler.ClimbData.GetLineCount();
        Vector3[] points = new Vector3[2];
        for (int i = 0; i < count; i++)
        {
            var item = _climbHandler.ClimbData.GetLine(i);
            points[0] = _climbHandler.transform.TransformPoint(item.PosA.Position);
            points[1] = _climbHandler.transform.TransformPoint(item.PosB.Position);

            Rect rect = new Rect();
            rect.height = 20;
            rect.width = 20;
            rect.x = hit2D.x - 10;
            rect.y = hit2D.y - 10;

            //Selected Color
            if (_groupSelectInfo != null && _groupSelectInfo.Lines.Contains(item))
                Handles.color = Color.green;
            else if (currentSelect == item)
                Handles.color = Color.green;
            else if (rect.Contains(Event.current.mousePosition) && item == hit.ClimbLine)
                Handles.color = Color.yellow;
            else
                Handles.color = Color.white;

            Handles.DrawLine(points[0], points[1]);
            Handles.DrawPolyLine(points);

            //Draw Direction Arrows
            if (!_hideDirections)
            {
                Vector3 midpoint = points[0] - ((points[0] - points[1]) * 0.5f);
                float size = HandleUtility.GetHandleSize(midpoint);
                if (item.UseDirection && Handles.Button(midpoint, item.Direction(_climbHandler.transform), size, size, Handles.ArrowHandleCap))
                {
                    LineSelect(item, midpoint);
                    break;
                }
            }
        }

        if (currentSelect is IClimbVertex && _connectVertex)
        {
            // Handles.DrawLine(_climbHandler.transform.TransformPoint(((ClimbVertex)_currentSelected).Position), point.origin);
            Handles.DrawLine(((IClimbVertex)currentSelect).GetWorldPosition(_climbHandler.transform), point.origin);
            SceneView.RepaintAll();
        }
    }

    protected virtual void DrawPivotButton()
    {
        int count = _climbHandler.GetVertexCount();
        Rect safeArea = Screen.safeArea;
        var currentSelect = _currentSelect;

        for (int i = 0; i < count; i++)
        {
            var item = _climbHandler.GetVertex(i);

            if (item == null || item == currentSelect)
                continue;

            Vector3 vertexPos = _climbHandler.transform.TransformPoint(item.Position);
            if (!safeArea.Contains(HandleUtility.WorldToGUIPoint(vertexPos)))
                continue;

            // Quaternion lookAtRot = Quaternion.LookRotation(vertexPos - _currentCam.transform.position);
            Handles.color = (_currentSelected == item) ? Color.green : Color.white;

            float size = HandleUtility.GetHandleSize(vertexPos);
            if (Handles.Button(vertexPos, Quaternion.identity, size * 0.04f, size * 0.06f, Handles.DotHandleCap))
            {
                if (_connectVertex)
                {
                    IClimbVertex select = currentSelect as IClimbVertex;
                    if (select != item && select != null)
                    {
                        ConnectVertex(select, item);
                        _connectVertex = false;
                    }
                }
                else
                    SetSelected(item);

                Undo.RecordObject(target, "Select vertex");
                EditorUtility.SetDirty(target);
            }
        }
    }

    protected virtual void SetSelected(object obj)
    {
        //Update settings editor
        if (_currentEditor != null)
            DestroyImmediate(_currentEditor);

        _currentEditor = (obj != null && obj is Object) ? CreateEditor((Object)obj) : null;
        IParentEditor parentEditor = _currentEditor as IParentEditor;
        if (parentEditor != null)
            parentEditor.ParentEditor = this;

        //Disable Connect Vertex
        if (obj == null)
            _connectVertex = false;

        if (obj == null)
        {
            _currentSelected.Clear();
            _groupSelectInfo.Clear();
        }
        else if (Event.current.alt)
        {
            _currentSelected.Remove(obj);
            _groupSelectInfo.Remove(obj);
        }
        else if (Event.current.control && !_currentSelected.Contains(obj))
        {
            _currentSelected.Add(obj);
            _groupSelectInfo.Add(obj);
        }
        else
        {
            _currentSelected.Clear();
            _currentSelected.Add(obj);
            _groupSelectInfo.Clear();
            _groupSelectInfo.Add(obj);
        }


        //if (_currentSelected != obj || obj == null)
        //{
        //    _currentSelected = obj;
        EditorUtility.SetDirty(target);
        Undo.RegisterCompleteObjectUndo(this, "Set Selected");
        //}
    }

    protected virtual void DrawGizmoSelection() //Fix me
    {
        if (_currentSelected.Count == 1 && _currentSelected[0] is IClimbVertex)
        {
            IClimbVertex vertex = _currentSelected[0] as IClimbVertex;
            Vector3 vertexPos = _climbHandler.transform.TransformPoint(vertex.Position);
            _oldVertexPos = vertex.Position;
            vertex.Position = _climbHandler.transform.InverseTransformPoint(Handles.PositionHandle(vertexPos, Quaternion.identity));

            if (_oldVertexPos != vertex.Position)
            {
                _currentEditor.serializedObject.Update();
                EditorUtility.SetDirty(target);
                Undo.RecordObject(_currentSelected[0] as Object, "Move vertex");
            }
        }
    }

    public void CreateBoxCollider()
    {
        var tar = target as MonoBehaviour;
        if (tar == null)
            return;

        BoxCollider boxCollider = tar.gameObject.GetComponent<BoxCollider>();
        if (boxCollider == null)
            boxCollider = tar.gameObject.AddComponent<BoxCollider>();

        boxCollider.isTrigger = true;

        var box = _climbHandler.GetVertexEnumerable().GetBoxSize(tar.transform, 1);
        boxCollider.center = box.Center;
        boxCollider.size = box.Size;
    }
}