﻿using RoyFrameworkEditor.Utility;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ClimbLine))]
public class ClimbLineEditor : Editor, IParentEditor
{
    public Editor ParentEditor { get { return _parentEditor; } set { _parentEditor = value; } }
    protected Editor _parentEditor;
    protected ClimbLine _climbLine;
    protected string[] _ignoreProperty = new string[] { "_UseDirection" };
    protected SerializedProperty _useDirectionProperty;


    protected virtual void OnEnable()
    {
        _climbLine = target as ClimbLine;
        //if (Object.Equals(serializedObject, null) || _climbLine == null)
        //    return;

        _useDirectionProperty = serializedObject.FindProperty("_UseDirection");
    }

    public override void OnInspectorGUI()
    {
        if (_climbLine == null || ParentEditor == null)
            return;

        MonoBehaviour mono = ParentEditor.target as MonoBehaviour;
        EditorGUILayout.LabelField("Magnitude: " + _climbLine.Magnitude(mono.transform));

        EditorGUILayout.PropertyField(_useDirectionProperty);

        if (_useDirectionProperty.boolValue && GUILayout.Button("Flip"))
            _climbLine.FlipDirection();

        EditorUtilityFunctions.DrawDefaultInspector(serializedObject, _ignoreProperty);
        EditorUtilityFunctions.GUIChanged(serializedObject);
    }
}
