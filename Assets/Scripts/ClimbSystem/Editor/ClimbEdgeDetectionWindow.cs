﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

public class ClimbEdgeDetectionWindow : EditorWindow
{
    public MeshFilterType FilterType;
    public float MaxTopAngle = 0.5f;
    public float MaxSideAngle = 0.5f;
    public float AngleThresHold = 0.998f;
    public bool DeleteCurrentLines;
    public bool UseWorldSpace = true;
    public string SavePath;

    public enum MeshFilterType
    {
        MeshCollider,
        MeshFilter
    }

    protected struct ObjectInfo
    {
        public Mesh Mesh;
        public IClimbHandler ClimbHandler;
        public Transform Transform;
    }

    [MenuItem("Tools/Edge Detection")]
    private static void Init()
    {
        var window = EditorWindow.GetWindow<ClimbEdgeDetectionWindow>();
        window.Show();
    }

    protected void OnEnable()
    {
        var data = EditorPrefs.GetString("ClimbEdgeDetection", JsonUtility.ToJson(this, false));
        JsonUtility.FromJsonOverwrite(data, this);
    }

    protected void OnDisable()
    {
        var data = JsonUtility.ToJson(this, false);
        EditorPrefs.SetString("ClimbEdgeDetection", data);
    }

    protected virtual void OnGUI()
    {
        bool invalidPath = SavePath.StartsWith(Application.dataPath) && Directory.Exists(SavePath);
        if (!invalidPath)
            EditorGUILayout.HelpBox("Invalid Path: Make sure the Save Path is in the unity Assets folder", MessageType.Error);

        bool findMeshFilter = !Array.Find(Selection.objects, a => (a is GameObject) && ((GameObject)a).GetComponent<MeshFilter>() != null);
        if (findMeshFilter)
            EditorGUILayout.HelpBox("Select a Mesh object", MessageType.Warning);

        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        FilterType = (MeshFilterType)EditorGUILayout.EnumPopup("FilterType", FilterType);

        EditorGUILayout.BeginHorizontal();
        SavePath = EditorGUILayout.TextField("Save Path", SavePath);
        if (GUILayout.Button("Path", GUILayout.Width(80)))
        {
            if (string.IsNullOrEmpty(SavePath))
                SavePath = Application.dataPath;

            SavePath = EditorUtility.OpenFolderPanel("Save climb data path", SavePath, "");
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        GUILayout.Label("Edge Detection", EditorStyles.boldLabel);
        MaxTopAngle = EditorGUILayout.FloatField("Max Top Threshold", MaxTopAngle);
        MaxSideAngle = EditorGUILayout.FloatField("Max Side Threshold", MaxSideAngle);
        DeleteCurrentLines = EditorGUILayout.Toggle("Delete Current Lines", DeleteCurrentLines);
        UseWorldSpace = EditorGUILayout.Toggle("Use WorldSpace", UseWorldSpace);

        GUILayout.Label("Line Cleanup", EditorStyles.boldLabel);
        AngleThresHold = EditorGUILayout.FloatField("Angle ThresHold", AngleThresHold);
        

        EditorGUILayout.Space();
        EditorGUI.BeginDisabledGroup(findMeshFilter || !invalidPath);
        if (GUILayout.Button("Build"))
        {
            Build();
            EditorUtility.ClearProgressBar();
            GUIUtility.ExitGUI();
        }
        if (GUILayout.Button("Line Cleanup"))
        {
            LineCleanup();
            EditorUtility.ClearProgressBar();
            GUIUtility.ExitGUI();
        }

        EditorGUI.EndDisabledGroup();
    }

    protected List<ObjectInfo> GetAllMeshes()
    {
        List<ObjectInfo> list = new List<ObjectInfo>();
        //bool saveAssets = false;
        string savePath = SavePath.Remove(0, Application.dataPath.Length - 6);
        foreach (var item in Selection.objects)
        {
            GameObject obj = item as GameObject;
            if (obj == null)
                continue;

            Mesh mesh = null;
            switch (FilterType)
            {
                case MeshFilterType.MeshFilter:
                    MeshFilter meshFilter = obj.GetComponent<MeshFilter>();
                    if (meshFilter == null || meshFilter.sharedMesh == null)
                        continue;

                    mesh = meshFilter.sharedMesh;
                    break;
                case MeshFilterType.MeshCollider:
                    MeshCollider meshCollider = obj.GetComponent<MeshCollider>();
                    if (meshCollider == null || meshCollider.sharedMesh == null)
                        continue;

                    mesh = meshCollider.sharedMesh;
                    break;
            }

            ObjectInfo objectInfo = new ObjectInfo();
            objectInfo.Transform = obj.transform;
            objectInfo.Mesh = mesh;
            objectInfo.ClimbHandler = obj.GetComponent<IClimbHandler>();
            if (objectInfo.ClimbHandler == null)
                objectInfo.ClimbHandler = obj.AddComponent<ClimbStaticHandler>();

            //Create climb static handler climb Data scriptableObject
            ClimbStaticHandler climbStaticHandler = objectInfo.ClimbHandler as ClimbStaticHandler;
            if (climbStaticHandler != null && climbStaticHandler.ClimbData == null)
            {
                climbStaticHandler.ClimbData = ScriptableObject.CreateInstance<ClimbData>();
                string uniquePath = AssetDatabase.GenerateUniqueAssetPath(savePath + "/" + climbStaticHandler.name + ".asset");
                AssetDatabase.CreateAsset(climbStaticHandler.ClimbData, uniquePath);
                //saveAssets = true;
            }

            list.Add(objectInfo);
        }

        //if (saveAssets)
        //{
        //    AssetDatabase.SaveAssets();
        //    AssetDatabase.Refresh();
        //}

        return list;
    }

    protected void Build()
    {
        EditorApplication.isPlaying = false;
        List<ObjectInfo> objectInfo = GetAllMeshes();
        bool cancel = false;
        int count = objectInfo.Count;
        for (int i = 0; i < count; i++)
        {
            var item = objectInfo[i];
            var edgeFinder = new EdgeFinder(item.Mesh);
            edgeFinder.OnProgress += (a, b, txt) =>
            {
                if ((b + i) % 1000 == 0)
                {
                    if (EditorUtility.DisplayCancelableProgressBar("Edge Detection", txt, (a + i) / (float)count))
                    {
                        edgeFinder.CancelProgress = true;
                        cancel = true;
                    }
                }
            };
            if (cancel)
                break;

            if (DeleteCurrentLines)
                item.ClimbHandler.Clear();

            edgeFinder.MaxTopAngle = MaxTopAngle;
            edgeFinder.MaxSideAngle = MaxSideAngle;
            edgeFinder.SetTransform(item.Transform);
            edgeFinder.SetClimbHandler(item.ClimbHandler);
            edgeFinder.CalulateEdges();
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    protected virtual void LineCleanup()
    {
        List<ObjectInfo> objectInfo = GetAllMeshes();
        //bool cancel = false;
        int count = objectInfo.Count;
        for (int i = 0; i < count; i++)
        {
            var item = objectInfo[i];
            item.ClimbHandler.LineCleanup(AngleThresHold);
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}