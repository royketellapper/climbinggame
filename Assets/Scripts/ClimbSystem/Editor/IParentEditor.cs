﻿using UnityEditor;

public interface IParentEditor
{
    Editor ParentEditor { get; set; }
}
