﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

[CustomEditor(typeof(ClimbStaticHandler))]
public class ClimbStaticHandlerEditor : ClimbHandlerEditor
{
    protected ClimbData _currentData;

    protected override void OnEnable()
    {
        _climbHandler = target as ClimbStaticHandler;
        base.OnEnable();
    }

    public override void OnInspectorGUI()
    {
        if (_currentData != _climbHandler.ClimbData)
            _currentSelected.Clear();
        _currentData = _climbHandler.ClimbData;

        base.OnInspectorGUI();
    }

    //protected override void AddVertex()
    //{
    //    Vector3 vertexLoc = new Vector3(0, 0, 0);
    //    ClimbVertex climbVertex = _currentSelected as ClimbVertex;

    //    //Vertex spawn location
    //    if (climbVertex != null)
    //    {
    //        Vector3 dir = new Vector3(1, 0, 0);
    //        if (climbVertex.Lines.Count != 0)
    //            dir = climbVertex.Lines[0].Normalize();

    //        vertexLoc = _climbHandler.transform.TransformPoint(climbVertex.LocalPosition + dir);
    //    }

    //    //Create Vertex
    //    var vertex = _climbHandler.AddVertex(vertexLoc);

    //    //Auto Connect
    //    if (_autoConnect && climbVertex != null)
    //        _climbHandler.AddLine<ClimbLine>(climbVertex, vertex);

    //    AssetDatabase.SaveAssets();
    //    AssetDatabase.Refresh();
    //    SetSelected((ClimbVertex)vertex);
    //}

    //protected override void Delete()
    //{
    //    base.Delete();
    //    AssetDatabase.SaveAssets();
    //    AssetDatabase.Refresh();
    //}

    //protected override void ConnectVertex(IClimbVertex vertA, IClimbVertex vertB)
    //{
    //    if (_climbHandler.CanConnectLine(vertA, vertB))
    //    {
    //        _climbHandler.AddLine<ClimbLine>(vertA, vertB);
    //        AssetDatabase.SaveAssets();
    //        AssetDatabase.Refresh();
    //    }
    //}

    //protected override InsertPointInfo InsertPoint(IClimbLine climbLine, Vector3 pos)
    //{
    //    var pointInfo = _climbHandler.InsertPoint(climbLine, pos);
    //    AssetDatabase.SaveAssets();
    //    AssetDatabase.Refresh();
    //    return pointInfo;
    //}

    //protected override void OnSceneGUI()
    //{
    //    if (_climbHandler.ClimbData == null)
    //    {
    //        SetSelected(null);
    //        return;
    //    }

    //    if (Tools.current != Tool.None)
    //        SetSelected(null);

    //    _currentCam = Camera.current;
    //    Handles.zTest = (_zTest) ? CompareFunction.LessEqual : CompareFunction.Always;

    //    DrawLines();
    //    DrawPivotButton();
    //    DrawGizmoSelection();

    //    if (_currentSelected != null)
    //        Tools.current = Tool.None;
    //}

    //protected override void LineSelect(IClimbLine climbLine, Vector3 hitPoint)
    //{
    //    if (_connectVertex)
    //    {
    //        var vertex = _currentSelected as IClimbVertex;
    //        bool containsLine = false;
    //        if (vertex != null)
    //            containsLine = vertex.FindLine(a => a == climbLine) != null;

    //        if (_currentSelected != null && (_currentSelected == (Object)climbLine || containsLine))
    //        {
    //            InsertPointInfo info = InsertPoint(climbLine, hitPoint);
    //            SetSelected((Object)info.Vertex);
    //            _connectVertex = false;
    //        }
    //        else if (_currentSelected != (Object)climbLine && _currentSelected != null && _currentSelected is ClimbVertex)
    //        {
    //            InsertPointInfo info = InsertPoint(climbLine, hitPoint);
    //            ConnectVertex((ClimbVertex)_currentSelected, info.Vertex);
    //            SetSelected((Object)info.Vertex);
    //            _connectVertex = false;
    //        }
    //    }
    //    else
    //        SetSelected((Object)climbLine);

    //    SceneView.RepaintAll();
    //}
}
