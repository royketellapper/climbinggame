﻿using RoyFrameworkEditor.Utility;
using UnityEditor;

[CustomEditor(typeof(ClimbVertex))]
public class ClimbVertexEditor : Editor
{
    protected ClimbVertex _climbVertex;

    protected virtual void OnEnable()
    {
        _climbVertex = target as ClimbVertex;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Connected lines: " + _climbVertex.Lines.Count);
        EditorUtilityFunctions.DrawDefaultInspector(serializedObject);
        EditorUtilityFunctions.GUIChanged(serializedObject);
    }
}
