﻿using UnityEditor;

[CustomEditor(typeof(ClimbData), true), CanEditMultipleObjects]
public class ClimbDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        int vertexCount, lineCount;
        vertexCount = lineCount = 0;

        foreach (var item in targets)
        {
            var climbData = item as ClimbData;
            vertexCount += climbData.GetVertexCount();
            lineCount += climbData.GetLineCount();
        }

        EditorGUILayout.LabelField("Vertices: " + vertexCount);
        EditorGUILayout.LabelField("Lines: " + lineCount);
        EditorGUILayout.LabelField("Total: " + (vertexCount + lineCount));
    }
}