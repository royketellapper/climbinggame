using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class Pawn : MonoBehaviour 
{
	public CharacterController characterController {get; private set;}
	public Transform myTransform;
	
	private void Awake() 
	{
		myTransform = gameObject.transform;
		characterController = myTransform.GetComponent<CharacterController>();
	}
	
	void OnControllerColliderHit(ControllerColliderHit hit) 
	{
		
	}
	
	public void SetController ()
	{
		
	}
}
