using UnityEngine;
using System;
using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

public enum XMLLoadType {UnityResourses, RootPath, FilePath};

public class ObjectXMLSerializer<T> where T : class
{
	
	// Load resourses standart with the Unity Resouses Class
	public static T Load(string path)
	{
		return Load (path, XMLLoadType.UnityResourses);
	}
	
	// Select the resours loading type
	public static T Load(string path, XMLLoadType loadType)
	{
		switch(loadType)
		{
			case XMLLoadType.UnityResourses:
				return 	LoadFromTextUnityResourses(path);
			case XMLLoadType.RootPath:
				// Add the game root path
				path = Application.dataPath + "/" + path;
				if(File.Exists(path))
				{
					return 	LoadFromText(path);
				}
				break;
			case XMLLoadType.FilePath:
				if(File.Exists(path))
				{
					return 	LoadFromText(path);
				}
				break;
		}
		Debug.LogWarning("No file found: " + path);
		return null;
	}

    // Returns a list of T objects found in the given resource folder
    public static T[] LoadAllResources(string path)
    {
        return LoadAllFromTextUnityResourses(path).ToArray();
    }

    // Returns a list of T objects found in the given resource folder
    public static List<T> LoadAllResourcesToList(string path)
    {
        return LoadAllFromTextUnityResourses(path);
    }
	
	// Standard a file wil be saved in de root of the game and wil overwrite the old file
	public static void Save(string path, T serializableObject)
	{
		Save(path, serializableObject, true, XMLLoadType.RootPath);
	}
	
	public static void Save(string path, T serializableObject, bool overWriteFile, XMLLoadType loadType)
	{
		// Can not save objects with the Unity Resourses
		if(loadType == XMLLoadType.UnityResourses)
		{
			Debug.LogError("Can not save a file with the Unity Resourses class" + path);
			return;
		}
		// Add the game root path
		if(loadType == XMLLoadType.RootPath)
		{
			path = Application.dataPath + "/" + path;
		}
		// Check if file exits and is not allowd to overwrite
		if(!overWriteFile
			&& File.Exists(path))
		{
			Debug.LogError("File already exists: " + path);
			return;
		}
		SaveToXMLFile(path, serializableObject);
	}
	
	public static bool XMLFileExits(string path, XMLLoadType loadType)
	{
		// Check if unity resourses exits
		if(loadType == XMLLoadType.UnityResourses)	
		{
			try
			{
				if((TextAsset)Resources.Load(path, typeof(TextAsset)) == null)
				{
					return false;
				}
			}
			catch (InvalidCastException e)
			{
				Debug.LogError(e.ToString());
			}
			return true;
		}
		// Add the game root path
		if(loadType == XMLLoadType.RootPath)
		{
			path = Application.dataPath + "/" + path;
		}
		// Check file exits with out the Unity Resourses class
		return File.Exists(path);
	}

    // Get alle file in a directory from root with a set xml extension
    public static string[] GetAllXmlFilesInRootFolder(string path)
    {
        return GetAllFilesInRootFolder(path, "*.xml");
    }

    // Get alle file in a directory from root with a given extension
    public static string[] GetAllFilesInRootFolder(string path, string extension)
    {
        return GetAllFilesInFolder(Application.dataPath + "/" + path, extension);
    }

    // Get alle file in a directory with a given extension
    public static string[] GetAllFilesInFolder(string path, string extension)
    {
        return Directory.GetFiles(path, extension);
    }

	// Load a XML file white a standart path
	private static T LoadFromText(string path)
	{
	    T serializableObject = null;
		try
		{
		   	using (TextReader textReader = new StreamReader(path))
		    {
		        XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
		        serializableObject = xmlSerializer.Deserialize(textReader) as T;
		    }
		}
		catch (InvalidCastException e)
		{
			Debug.LogError(e.ToString());
			throw e;
		}
	    return serializableObject;
	}
	
	// Load a XML with the Unity Resourses class
	private static T LoadFromTextUnityResourses(string path)
	{
        T serializableObject = null;
		try
		{
			// Load xml into an Unity TextAsset
			TextAsset textAsset = (TextAsset)Resources.Load(path, typeof(TextAsset));
            if(textAsset)
            {
                serializableObject = LoadFromTextAsset(textAsset);
            }
            else
            {
                Debug.LogError("No folder found: " + path);
            }
		}
		catch (InvalidCastException e)
        {
            Debug.LogError(e.ToString());
			throw e;
        }
	    return serializableObject;
	}

    // Load all a XMLs in a folder, with the Unity Resourses class
    private static List<T> LoadAllFromTextUnityResourses(string path)
    {
        List<T> serializableObjects = new List<T>();
        try
        {
            // Load xml into an Unity TextAsset
            UnityEngine.Object[] textAssets = Resources.LoadAll(path);
            foreach(TextAsset textAsset in textAssets)
            {
                if(textAsset)
                {
                    serializableObjects.Add(LoadFromTextAsset(textAsset));
                }
                else
                {
                    Debug.LogError("No folder found: " + path);
                }
            }
        }
        catch (InvalidCastException e)
        {
            Debug.LogError(e.ToString());
			throw e;
        }
        return serializableObjects;
    }

    // Deserialize the TextAsset that is loaded with Unity resources
    private static T LoadFromTextAsset(TextAsset textAsset)
    {
        T serializableObject = null;
        try
        {
            if(textAsset)
            {
             StringReader stringReader = new StringReader(textAsset.text);
             XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
             // Serualize the the string text
             serializableObject = xmlSerializer.Deserialize(stringReader) as T;
            }
        }
        catch (InvalidCastException e)
        {
            Debug.LogError(e.ToString());
			throw e;
        }
        return serializableObject;
    }
	
	// Save an object type to a XML file
	private static void SaveToXMLFile(string path, T serializableObject)
	{
		if(serializableObject != null)
		{
			using (TextWriter textWriter = new StreamWriter(path))
		    {
		        XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));;
		        xmlSerializer.Serialize(textWriter, serializableObject);
		    }
		}
	}
}
