using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
//using System.Data;
//using Mono.Data.SqliteClient;

public enum PathTypes {FilePath, UnityResourses, RootPath, SaveGamePath};
public class ObjectToBinary<T> where T : class
{	
	public static string Load1(string path, PathTypes pathTypes)
	{
		switch (pathTypes)
		{
			case PathTypes.RootPath:
				return Application.dataPath + "\\";
			case PathTypes.SaveGamePath:
				return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\My Games\\" + "GameName" + "\\"; //PlayerSettings.productName <--- fix me
			case PathTypes.UnityResourses:
			break;
		}
		
		
		return String.Empty;
	}
	
	public static void Save(string path, PathTypes pathTypes , T serializableObject)
	{
		CreateFolder(path);
		
		//Stream stream = File.Open(path, FileMode.Create);	
		 FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write);
		BinaryFormatter formatter = new BinaryFormatter();
		
		try
		{	
			formatter.Serialize(stream, serializableObject);
			stream.Close();
		}catch (SerializationException se)
		{
			
		}
		finally
        {
            stream.Close();
            formatter = null;
        }
	}
	
	public static T Load(string path, PathTypes pathTypes)
	{
		//Stream stream = File.Open(path, FileMode.Open);
		Stream  stream = new FileStream(path, FileMode.Open, FileAccess.Read);
		BinaryFormatter formatter = new BinaryFormatter();
		T obj = null;
		try
		{
            // Deserialize the object from the data file.
          obj = (T)formatter.Deserialize(stream);
			
			stream.Close();
			
		}catch (SerializationException se)
		{
			Debug.Log(se.Message);
		}
		finally
        {
			stream.Close();
			formatter = null;
		}
		
		return obj;
	}
	
	public static void Load2(string path, PathTypes pathTypes , T obj)
	{
		//Stream stream = File.Open(path, FileMode.Open);
		Stream  stream = new FileStream(path, FileMode.Open, FileAccess.Read);
		BinaryFormatter formatter = new BinaryFormatter();
		try
		{
            // Deserialize the object from the data file.
          obj = (T)formatter.Deserialize(stream);
			
			stream.Close();
			
		}catch (SerializationException se)
		{
			Debug.Log(se.Message);
		}
		finally
        {
			stream.Close();
			formatter = null;
		}
	}
	
	
	public static void CreateFolder(string path)
	{
		try 
		{
			path = Path.GetDirectoryName(path);
			if ((path.Length > 0) && (!Directory.Exists(path)))
		        Directory.CreateDirectory(path);
		}
		catch (Exception e)
		{
			
		}
	}
	

	
	/*
	public static void Main()
    {
        // Create a new TestSimpleObject object.
        TestSimpleObject obj = new TestSimpleObject();

        Console.WriteLine("\n Before serialization the object contains: ");
        obj.Print();

        // Open a file and serialize the object into binary format.
        Stream stream = File.Open("DataFile.dat", FileMode.Create);
        BinaryFormatter formatter = new BinaryFormatter();

        try
        {
            formatter.Serialize(stream, obj);

            // Print the object again to see the effect of the  
            //OnSerializedAttribute.
            Console.WriteLine("\n After serialization the object contains: ");
            obj.Print();

            // Set the original variable to null.
            obj = null;
            stream.Close();

            // Open the file "DataFile.dat" and deserialize the object from it.
            stream = File.Open("DataFile.dat", FileMode.Open);

            // Deserialize the object from the data file.
            obj = (TestSimpleObject)formatter.Deserialize(stream);

            Console.WriteLine("\n After deserialization the object contains: ");
            obj.Print();
            Console.ReadLine();
        }
        catch (SerializationException se)
        {
            Console.WriteLine("Failed to serialize. Reason: " + se.Message);
            throw;
        }
        catch (Exception exc)
        {
            Console.WriteLine("An exception occurred. Reason: " + exc.Message);
            throw;
        }
        finally
        {
            stream.Close();
            obj = null;
            formatter = null;
        }

    }
*/	 
	 
}
