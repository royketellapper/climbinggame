﻿using ProtoBuf;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ProtoContract]
public class UnityObjectSurrogate
{
    [ProtoMember(1)]
    public string unityJson;

    public static implicit operator UnityEngine.Object(UnityObjectSurrogate surrogate)
    {
        if (surrogate == null || string.IsNullOrEmpty(surrogate.unityJson))
            return null;

        return JsonUtility.FromJson<UnityEngine.Object>(surrogate.unityJson);
    }

    public static implicit operator UnityObjectSurrogate(UnityEngine.Object unityClass)
    {
        if (unityClass == null)
            return null;

        UnityObjectSurrogate unityObjectSurrogate = new UnityObjectSurrogate();
        unityObjectSurrogate.unityJson = JsonUtility.ToJson(unityClass);

        return unityObjectSurrogate;
    }
}
