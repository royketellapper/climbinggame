﻿using UnityEngine;

[System.Serializable]
public class UnityObject
{
    public Object Object;
}