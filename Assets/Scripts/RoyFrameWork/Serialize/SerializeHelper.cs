﻿using Newtonsoft.Json;
using ProtoBuf.Meta;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;
using WanzyeeStudio.Json;

namespace RoyFramework.Tools
{
    public static class SerializeHelper
    {
        public static string XmlSerializeToString(this object obj, Type[] extraTypes)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType(), extraTypes);
                StringBuilder sb = new StringBuilder();
                using (TextWriter writer = new StringWriter(sb))
                    serializer.Serialize(writer, obj);

                return sb.ToString();
            }
            catch
            {
                return "";
            }
        }


        public static string XmlSerializeToString(this object obj)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                StringBuilder sb = new StringBuilder();
                using (TextWriter writer = new StringWriter(sb))
                    serializer.Serialize(writer, obj);

                return sb.ToString();
            }
            catch
            {
                return "";
            }
        }

        public static T XmlDeserializeFromString<T>(this string obj, Type[] extraTypes)
        {
            return (T)XmlDeserializeFromString(obj, typeof(T), extraTypes);
        }

        public static object XmlDeserializeFromString(this string obj, Type type, Type[] extraTypes)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(type, extraTypes);
                object result = null;
                using (TextReader reader = new StringReader(obj))
                    result = serializer.Deserialize(reader);

                return result;
            }
            catch
            {
                return null;
            }
        }

        public static T XmlDeserializeFromString<T>(this string obj)
        {
            return (T)XmlDeserializeFromString(obj, typeof(T));
        }

        public static object XmlDeserializeFromString(this string obj, Type type)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(type);
                object result = null;
                using (TextReader reader = new StringReader(obj))
                    result = serializer.Deserialize(reader);

                return result;
            }
            catch
            {
                return null;
            }
        }

        public static string JsonSerializeToString(this object obj)
        {
            return JsonConvert.SerializeObject(obj, GetJsonConverters());
        }

        public static object JsonDeserializeFromString(this string obj, Type type)
        {
            return JsonConvert.DeserializeObject(obj, type, GetJsonConverters());
        }

        public static T JsonDeserializeFromString<T>(this string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj, GetJsonConverters());
        }

        private static JsonSerializerSettings GetJsonConverters()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto;
            settings.Converters = new List<JsonConverter>();
            settings.Converters.Add(new BoundsConverter());
            settings.Converters.Add(new ColorConverter());
            settings.Converters.Add(new DictionaryConverter());
            settings.Converters.Add(new Matrix4x4Converter());
            settings.Converters.Add(new QuaternionConverter());
            settings.Converters.Add(new RectConverter());
            settings.Converters.Add(new RectOffsetConverter());
            settings.Converters.Add(new Vector2Converter());
            settings.Converters.Add(new Vector3Converter());
            settings.Converters.Add(new Vector4Converter());
            settings.Converters.Add(new UnityObjectConverter());
            settings.Error = (currentObject, errorContext) =>
            {
                Debug.Log("<Color=Red>Json serialize error:</Color> " + errorContext.ErrorContext.Error.Message);
                errorContext.ErrorContext.Handled = true;
            };

            return settings;
        }

        public static byte[] ObjectToByteArray(System.Object obj)
        {
            if (obj == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static T ByteArrayToObject<T>(byte[] arrBytes) where T : class
        {
            try
            {
                return (T)ByteArrayToObject(arrBytes);
            }
            catch
            {
                return null;
            }
        }

        public static System.Object ByteArrayToObject(byte[] arrBytes)
        {
            if (arrBytes == null)
                return null;

            MemoryStream memoryStream = null;

            try
            {
                object obj;
                memoryStream = new MemoryStream(arrBytes);

                IFormatter _BinaryFormatter = new BinaryFormatter();
                obj = _BinaryFormatter.Deserialize(memoryStream);

                return obj;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (memoryStream != null)
                    memoryStream.Close();
            }
        }

        public static void SerializeObject(SerializationInfo info, object obj)
        {
            Type type = obj.GetType();
            FieldInfo[] fieldInfo = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (var item in fieldInfo)
            {
                try
                {
                    object value = null;
                    if (item != null)
                    {
                        value = item.GetValue(obj);

                        if (info != null && value != null)
                            info.AddValue(item.Name, value);
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Serialize error: " + e);
                }
            }
        }

        public static void DeserializeObject(SerializationInfo info, object obj)
        {
            Type type = obj.GetType();
            foreach (SerializationEntry entry in info)
            {
                try
                {
                    var field = type.GetField(entry.Name, BindingFlags.Public | BindingFlags.Instance);
                    if (field != null)
                    {
                        object value = info.GetValue(entry.Name, entry.ObjectType);
                        if (value != null)
                            field.SetValue(obj, value);
                    }
                }
                catch (SerializationException e)
                {
                    Debug.Log("Deserialize error: " + e);
                }
            }
        }
    }
}
