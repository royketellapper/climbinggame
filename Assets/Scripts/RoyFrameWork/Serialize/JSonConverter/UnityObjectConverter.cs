﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using UnityEngine;

class UnityObjectConverter : JsonConverter
{
    public override bool CanRead
    {
        get { return true; }
    }

    public override bool CanWrite
    {
        get { return true; }
    }

    public override bool CanConvert(Type objectType)
    {
        Type unityObject = typeof(UnityEngine.Object);
        return (objectType.IsSubclassOf(unityObject) || objectType == unityObject);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        if (reader == null)
            return null;

        var jObject = JObject.Load(reader);

        string unityJson = (string)jObject["UnityJson"];
        
        if (!String.IsNullOrEmpty(unityJson))
        {
            string json = unityJson.Replace('\\', ' ');
            var unityObject = JsonUtility.FromJson<UnityObject>(json);
            if (unityObject != null)
                return unityObject.Object;
        }

        return null;
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        var obj = value as UnityEngine.Object;
        var jObject = new JObject();

        if (obj)
        {
            UnityObject unityObject = new UnityObject();
            unityObject.Object = (UnityEngine.GameObject)value;      
            jObject["UnityJson"] = JsonUtility.ToJson(unityObject);
        }

        jObject.WriteTo(writer);
    }
}