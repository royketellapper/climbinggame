﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using RoyFramework.Tools;

namespace RoyFramework.Audio
{
    public class AudioManager : Singleton<AudioManager>
    {
        public const string FileName = "Audio";
        public const string SavePath = "Assets/Resources/VascoGames/";

        public AudioMixer MasterMixer;
        private Dictionary<SoundType, AudioSource> AudioSources = new Dictionary<SoundType, AudioSource>();
        private AudioInfo AudioInfo;

        [Serializable]
        public class AudioVolume
        {
            public string Name;
            public float Volume = 1;
        }
        
        public List<AudioVolume> AudioVolumes = new List<AudioVolume>(); // Audio Volumes Save data
        private float masterVolumes = 1;

        protected Tween _crossFadeTween;
        public AudioClip CurrentMusic { get; protected set; }

        protected override void Ini()
        {
            AudioInfo = LoadAudioInfo();
        }

        protected void Start()
        {
            if (MasterMixer == null)
            {
                Debug.LogWarning("--------Unable To Load MasterMixer-----------");
                return;
            }

            Load();
        }

        public static AudioInfo LoadAudioInfo()
        {
            return (AudioInfo)Resources.Load("VascoGames/" + FileName) ?? ScriptableObject.CreateInstance<AudioInfo>();
        }

        protected virtual AudioSource CreateAudioAudioSource(string name)
        {
            GameObject audioGameObject = new GameObject(name+ "_Audio");
            audioGameObject.transform.localPosition = Vector3.zero;
            AudioSource audioSource = audioGameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            audioSource.bypassEffects = true;
            audioSource.bypassReverbZones = true;
            audioSource.bypassListenerEffects = true;
            audioSource.spatialBlend = 0;
            audioSource.outputAudioMixerGroup = MasterMixer.FindMatchingGroups(name)[0];
            return audioSource;
        }

        private void SetVolumeInMixer(string name, float volume)
        {
            volume = EaseOut(volume, 0, 1, 1);

            if (MasterMixer != null)
                MasterMixer.SetFloat(name, Mathf.Lerp(-80, 0, volume));
        }

        private float EaseOut(float t, float b, float c, float d)
        {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b;
        }

        public void SetVolume(string name, float volume)
        {
            volume = Mathf.Clamp01(volume);

            AudioVolume audioVolume = AudioVolumes.Find(v => v.Name == name);

            if (audioVolume == null)
                return;

            audioVolume.Volume = volume;
            SetVolumeInMixer(audioVolume.Name, audioVolume.Volume);
        }

        public void FadeVolume(string name, float enValue, float duration = 1)
        {
            MasterMixer.DOSetFloat(name, enValue, duration).SetUpdate(true);
        }

        protected void SetMasterVolume(float volume)
        {
            masterVolumes = volume;
            AudioListener.volume = volume;
        }

        public float GetMasterVolume()
        {
            return masterVolumes;
        }

        public float GetVolume(SoundType soundType)
        {
            AudioVolume audioVolume = AudioVolumes.Find(a => a.Name == soundType.ToString());

            if (audioVolume == null)
                return 1;

            return audioVolume.Volume;
        }

        public void SetVolume(SoundType soundType, float volume)
        {
            SetVolume(soundType.ToString(), volume);
        }

        public void PlayMusic(string id, float volume)
        {
            List<SoundIdSettings> list = GetAudioSettings(id, SoundType.Music);

            if (list.Count == 0)
                return;

            int random = Mathf.RoundToInt(UnityEngine.Random.Range(0, list.Count - 1));
            PlayMusic(list[random].AudioClip, volume);
        }

        public void PlayMusic(string id)
        {
            List<SoundIdSettings> list = GetAudioSettings(id, SoundType.Music);

            if (list.Count == 0)
                return;

            int random = Mathf.RoundToInt(UnityEngine.Random.Range(0, list.Count - 1));
            PlayMusic(list[random]);
        }

        public void PlayMusic(SoundIdSettings audioSettings)
        {
            PlayMusic(audioSettings.AudioClip, audioSettings.volume);
        }

        public void PlayMusic(AudioClip clip, float volume = 1)
        {
            AudioSource audioSource = GetAudioSources(SoundType.Music);

            if (audioSource.clip == clip)
                return;

            audioSource.volume = volume;
            audioSource.clip = clip;
            audioSource.loop = true;
            audioSource.Play();
            CurrentMusic = clip;
        }

        private float currentTime;
        public void CrossFadeMusic(AudioClip clip, float speed = 1)
        {
            AudioSource audioSource = GetAudioSources(SoundType.Music);

            if (_crossFadeTween != null)
                _crossFadeTween.Kill();

            float volume = audioSource.volume;

            bool isCurrentMusic = clip == CurrentMusic;

            if (!isCurrentMusic)
                currentTime = audioSource.time;

            _crossFadeTween = audioSource.DOFade(0, speed)
                .SetUpdate(true)
                .OnComplete(()=> 
                {
                    audioSource.clip = clip;
                    if (isCurrentMusic)
                        audioSource.time = currentTime;
                    audioSource.Play();

                    _crossFadeTween = audioSource.DOFade(volume, speed)
                        .SetUpdate(true);
                });
        }

        public void StopMusic()
        {
            AudioSource musicSource = GetAudioSources(SoundType.Music);
            musicSource.Stop();
        }

        public AudioMixerGroup GetAudioMixerGroup(SoundType soundType)
        {
            if (MasterMixer != null)
                return MasterMixer.FindMatchingGroups(soundType.ToString())[0];

            return null;
        }

        public void StopAllSounds(bool ignoreMusic = false)
        {
            AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
            AudioSource musicSource = GetAudioSources(SoundType.Music);
            foreach (AudioSource audio in audioSources)
            {
                if (audio == musicSource && ignoreMusic)
                    continue;

                audio.Stop();
            }
        }

        public void PlayRandomOneShot(AudioClip[] audioClip, float volume = 1, SoundType soundType = SoundType.FX)
        {
            int random = Mathf.Clamp(Mathf.RoundToInt(UnityEngine.Random.Range(0, audioClip.Length)), 0, audioClip.Length - 1);
            PlayOneShot(audioClip[random], volume, soundType);
        }

        public void PlayOneShot(string id, SoundType soundType = SoundType.FX)
        {
            List<SoundIdSettings> audioSettings = GetAudioSettings(id, soundType);

            if (audioSettings.Count == 0)
                return;

            int random = Mathf.RoundToInt(UnityEngine.Random.Range(0, audioSettings.Count - 1));
            PlayOneShot(audioSettings[random], soundType);
        }

        public void PlayOneShot(SoundIdSettings audioSettings, SoundType soundType = SoundType.FX)
        {
            if (audioSettings == null || AudioSources == null)
                return;

            AudioSource audioSource = GetAudioSources(soundType);
            audioSource.PlayOneShot(audioSettings.AudioClip, audioSettings.volume);
        }

        public void PlayOneShot(SoundClipSettings soundClipSettings, SoundType soundType)
        {
            if (soundClipSettings == null || AudioSources == null)
                return;

            AudioSource audioSource = GetAudioSources(soundType);
            audioSource.PlayOneShot(soundClipSettings.AudioClip, soundClipSettings.volume);
        }

        public void PlayOneShot(AudioClip audioClip, float volume = 1, SoundType soundType = SoundType.FX, float pitch = 1)
        {
            if (audioClip == null || AudioSources == null)
                return;

            AudioSource audioSource = GetAudioSources(soundType);

            if (audioSource != null)
            {
                audioSource.pitch = pitch;
                audioSource.PlayOneShot(audioClip, volume);
            }
        }

        public List<SoundIdSettings> GetAudioSettings(string id, SoundType soundType = SoundType.FX)
        {
            return AudioInfo.GetAudioSettings(id, soundType);
        }

        public AudioSource GetAudioSources(SoundType soundType)
        {
            AudioSource audioSource = null;

            if (AudioSources.ContainsKey(soundType))
            {
                audioSource = AudioSources[soundType];

                if (audioSource == null)
                {
                    audioSource = CreateAudioAudioSource(soundType.ToString());
                    AudioSources[soundType] = audioSource;
                }

                return audioSource;
            }

            audioSource = CreateAudioAudioSource(soundType.ToString());
            AudioSources.Add(soundType, audioSource);
            return audioSource;
        }

        //Unity call back
        /*
        private void OnLevelWasLoaded(int sceneIndex)
        {
           // string name = SceneManager.GetSceneAt(sceneIndex).name;
        }
        */

        public override void OnDestroy()
        {
            base.OnDestroy();

            if (AudioSources == null)
                return;

            foreach (var audioSource in AudioSources)
            {
                if (audioSource.Value != null)
                    Destroy(audioSource.Value.gameObject);
            }
        }
        
        //=============================== Save and load data ======================================
        public virtual void Save()
        {
            PlayerPrefs.SetFloat("MasterVolumes", masterVolumes);

            if (AudioVolumes.Count != 0)
                PlayerPrefs.SetString("AudioVolumes", AudioVolumes.XmlSerializeToString());

            PlayerPrefs.Save();
        }

        public virtual void Load()
        {
            masterVolumes = PlayerPrefs.GetFloat("MasterVolumes", 1f);
            SetMasterVolume(masterVolumes);
            AudioVolumes = PlayerPrefs.GetString("AudioVolumes", "").XmlDeserializeFromString<List<AudioVolume>>() ?? CreateDefaultAudioVolumes();

            if (AudioVolumes.Count == 0)
                AudioVolumes = CreateDefaultAudioVolumes();

            foreach (AudioVolume audioVolume in AudioVolumes)
                SetVolumeInMixer(audioVolume.Name, audioVolume.Volume);
        }

        protected virtual List<AudioVolume> CreateDefaultAudioVolumes()
        {
            List<AudioVolume> volumes = new List<AudioVolume>();
            string[] audioNames = Enum.GetNames(typeof(SoundType));
            for (int x = 0; x < audioNames.Length; x++)
            {
                AudioVolume volume = new AudioVolume();
                volume.Name = audioNames[x];
                volumes.Add(volume);
            }

            return volumes;
        }
    }
}