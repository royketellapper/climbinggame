﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using RoyFramework.Audio;

namespace VascoGamesEditor.Audio
{
    [CustomPropertyDrawer(typeof(AudioMenuAttribute))]
    public class AudioSettingsEditor : PropertyDrawer
    {
        AudioMenuAttribute settingsAttribute { get { return ((AudioMenuAttribute)attribute); } }

        /*
        List<string> sfxClips = new List<string>();
        List<string> voiceClips = new List<string>();
        List<string> musicClips = new List<string>();
        */

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            /*
            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            GetAudioClipNames();

            SoundTypes soundType = settingsAttribute.soundtype;

            if (soundType == SoundTypes.Music)
            {
                if (property.propertyType == SerializedPropertyType.Integer)
                    property.intValue = EditorGUI.Popup(position, Mathf.Clamp(property.intValue, 0, musicClips.Count - 1), musicClips.ToArray());
            }
            else if (soundType == SoundTypes.Sfx)
            {
                if (property.propertyType == SerializedPropertyType.Integer)
                    property.intValue = EditorGUI.Popup(position, Mathf.Clamp(property.intValue, 0, sfxClips.Count - 1), sfxClips.ToArray());
            }
            else if (soundType == SoundTypes.Voice)
            {
                if (property.propertyType == SerializedPropertyType.Integer)
                    property.intValue = EditorGUI.Popup(position, Mathf.Clamp(property.intValue, 0, voiceClips.Count - 1), voiceClips.ToArray());
            }


            EditorGUI.EndProperty();
            */
        }

        private void GetAudioClipNames()
        {
            /*
            for (int i = 0; i < AudioManager.instance.audiotypes.Length; i++)
            {
                for (int c = 0; c < AudioManager.instance.audiotypes[i].audiofiles.Length; c++)
                {
                    if (AudioManager.instance.audiotypes[i].audiofiles[c].clip != null)
                    {
                        SoundTypes type = AudioManager.instance.audiotypes[i].soundtype;

                        // Based on sound type add it to the list
                        if (type == SoundTypes.Music)
                            musicClips.Add(AudioManager.instance.audiotypes[i].audiofiles[c].name);
                        else if (type == SoundTypes.Sfx)
                            sfxClips.Add(AudioManager.instance.audiotypes[i].audiofiles[c].name);
                        else if (type == SoundTypes.Voice)
                            voiceClips.Add(AudioManager.instance.audiotypes[i].audiofiles[c].name);
                    }
                }
            }
            */
        }
    }
}