﻿using UnityEngine;
using UnityEditor;
using RoyFramework.Audio;

namespace VascoGamesEditor.Audio
{
    [CustomEditor(typeof(AudioInfo)), CanEditMultipleObjects]
    public class AudioInfoEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Open Audio Window"))
                AudioWindow.ShowWindow();
        }
    }
}
