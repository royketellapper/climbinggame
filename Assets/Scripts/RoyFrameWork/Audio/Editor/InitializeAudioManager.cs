﻿using UnityEngine;
using UnityEditor;
using RoyFramework.Audio;

namespace VascoGamesEditor.Audio
{
    [InitializeOnLoad]
    public class InitializeAudioManager
    {
        static InitializeAudioManager()
        {
            EditorApplication.playmodeStateChanged = () =>
            {
                if (EditorApplication.isPlayingOrWillChangePlaymode && EditorApplication.isPlaying)
                    AudioManager.Instance.hideFlags = HideFlags.DontSaveInEditor;
            };
        }
    }
}
