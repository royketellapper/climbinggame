﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using RoyFramework.Audio;
using VascoGamesEditor.UIEditor;

namespace VascoGamesEditor.Audio
{
    public class AudioWindow : EditorWindowBase
    {
        protected AudioInfo audioInfo;
        protected class AudioFoldouts
        {
            public string Name;
            public bool Foldout = false;
            public SerializedProperty AudioSettingsProperty;
            public SerializedProperty SoundTypeProperty;
        }
        protected AudioFoldouts[] audioFoldouts;
        protected SerializedObject serializedObject;
        protected SerializedProperty soundTypeListProperty;

        [MenuItem("VascoGames/Audio", false, 1)]
        public new static void ShowWindow()
        {
            AudioWindow window = EditorWindow.GetWindow<AudioWindow>(true, "Audio");
            window.minSize = new Vector2(500, 600);
            window.Show();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Load();
        }

        private string[] GetSceneNames()
        {
            List<string> sceneNames = new List<string>();
            Scene[] scenes = SceneManager.GetAllScenes();

            foreach (Scene scene in scenes)
                sceneNames.Add(scene.name);

            return sceneNames.ToArray();
        }

        protected override void OnDrawGUI() 
        {
            for (int x = 0; x < audioFoldouts.Length; x++)
            {
                AudioFoldouts audioFoldout = audioFoldouts[x];

                if (audioFoldout.Foldout = GUILayout.Toggle(audioFoldout.Foldout, audioFoldout.Name, EditorStyle.FoldoutStyle))  ///EditorGUILayout.Foldout(audioFoldout.Foldout, audioFoldout.Name))
                {
                    GUILayout.BeginVertical("TextArea");
                    BeginGUIChanged();
                    EditorGUILayout.PropertyField(audioFoldout.AudioSettingsProperty,new GUIContent("Sound IDs"), true);
                    EndGUIChanged();
                    GUILayout.EndVertical();
                }
            }

            if (GUI.changed)
               serializedObject.ApplyModifiedProperties();
        }

        //======================================= Load and Save ===================================
        protected override void Save()
        {
            try
            {
                Directory.CreateDirectory(AudioManager.SavePath);
                string assetPathAndName = AudioManager.SavePath + AudioManager.FileName + ".asset";
                AssetDatabase.CreateAsset(audioInfo, assetPathAndName);                                   
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
            catch (System.InvalidCastException e)
            {
                EditorUtility.DisplayDialog("Error", e.Message, "Ok");
            }
            Load();
            Repaint();
        }

        protected override void Load()
        {
            audioInfo = UnityEngine.Object.Instantiate<AudioInfo>(AudioManager.LoadAudioInfo());
            Repaint();
            LoadPropertys();
        }

        protected void LoadPropertys()
        {
            serializedObject = new SerializedObject(audioInfo);
            soundTypeListProperty = serializedObject.FindProperty("soundTypeList");
            LoadIAPProductFoldout();
        }

        protected void LoadIAPProductFoldout()
        {
            Array.Resize<AudioFoldouts>(ref audioFoldouts, audioInfo.soundTypeList.Length);
            for (int x = 0; x < audioFoldouts.Length; x++)
            {
                AudioFoldouts audioFoldout = new AudioFoldouts();
                audioFoldout.Name = audioInfo.soundTypeList[x].SoundType.ToString();
                audioFoldout.SoundTypeProperty = soundTypeListProperty.GetArrayElementAtIndex(x);
                audioFoldout.AudioSettingsProperty = audioFoldout.SoundTypeProperty.FindPropertyRelative("AudioSettings");

                audioFoldouts[x] = audioFoldout;
            }
        }

        protected override void Default()
        {
            audioInfo = ScriptableObject.CreateInstance<AudioInfo>();
            LoadPropertys();
            Repaint(); 
        }
    }
}
