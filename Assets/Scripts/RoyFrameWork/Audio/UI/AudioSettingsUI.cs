﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RoyFramework.Audio;

namespace RoyFramework.Audio.UI
{
    public class AudioSettingsUI : MonoBehaviour
    {
        [System.Serializable]
        public class AudioSlider
        {
            public SoundType SoundType;
            public Slider Slider;
            public AudioClip AudioClip;
            public float AudioClipDelay = 0.1f;

            [HideInInspector]
            public float time;
        }
        public AudioSlider[] AudioSliders;

        protected virtual void OnDisable()
        {
            AudioManager.Instance.Save();
        }

        protected virtual void OnEnable()
        {
            for (int i = 0; i < AudioSliders.Length; i++)
            {
                AudioSlider audioSlider = AudioSliders[i];

                if (audioSlider.Slider == null)
                    continue;

                audioSlider.Slider.value = AudioManager.Instance.GetVolume(audioSlider.SoundType);

                int tempIndex = i;
                audioSlider.Slider.onValueChanged.AddListener((float v) => OnValueChanged(i, tempIndex));
            }
        }

        protected virtual void OnValueChanged(float value, int index)
        {
            AudioSlider audioSlider = AudioSliders[index];
            AudioManager.Instance.SetVolume(audioSlider.SoundType, audioSlider.Slider.value);
            if (audioSlider.SoundType == SoundType.FX)
                AudioManager.Instance.SetVolume(SoundType.Voice, audioSlider.Slider.value);

            if (audioSlider.AudioClip != null && audioSlider.time + audioSlider.AudioClipDelay < Time.time)
            {
                AudioManager.Instance.PlayOneShot(audioSlider.AudioClip, 1, audioSlider.SoundType);
                audioSlider.time = Time.time;
            }
        }
    }
}
