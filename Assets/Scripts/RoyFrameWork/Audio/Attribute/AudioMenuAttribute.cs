﻿using UnityEngine;
using System.Collections;

namespace RoyFramework.Audio
{
    public class AudioMenuAttribute : PropertyAttribute
    {
        public readonly AudioType Soundtype;

        /// <summary>
        /// Create a menu in the inspector
        /// </summary>
        /// <param name="soundtype"></param>
        public AudioMenuAttribute(AudioType soundtype)
        {
            this.Soundtype = soundtype;
        }
    }
}