﻿namespace RoyFramework.Audio
{
    public enum SoundType
    {
        Music,
        FX,
        UI,
        Voice
    }
}
