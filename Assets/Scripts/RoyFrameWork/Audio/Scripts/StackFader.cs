using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using RoyFramework.Easing;

namespace RoyFramework.Audio
{
	public enum FadeDirection
	{
		FadeUp,
		FadeDown
	}

	public enum StackOperation
	{
		Pop,
		Push
	}

    public class StackFader : Singleton<StackFader>
    {
        protected override bool _dontDestroyOnLoad { get { return false; } }
        public bool FadeOut = true;
        AudioSource[] channels;

        uint currentChannel = 0;

        Coroutine routineA;
        Coroutine routineB;

        protected override void Ini()
        {
            if (channels == null)
                channels = new AudioSource[2];
            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = gameObject.AddComponent<AudioSource>();
                channels[i].outputAudioMixerGroup = AudioManager.Instance.MasterMixer.FindMatchingGroups("FX")[0];
                channels[i].loop = true;
            }

            CycleNextChannel();
        }

        Stack<AudioClip> clips = new Stack<AudioClip>();

        public void PushClip(AudioClip clip)
        {
            clips.Push(clip);
            Fade();
        }

        public void PopClip()
        {
            if (clips.Count == 0) return;
            clips.Pop();
            Fade();
        }

        void CycleNextChannel()
        {
            currentChannel = PeekNext();
        }

        uint PeekNext()
        {
            return (currentChannel + 1) % 2;
        }

        void Fade()
        {
            if (channels == null)
                return;

            if (routineA != null)
                StopCoroutine(routineA);
            if (routineB != null)
                StopCoroutine(routineB);

            if (clips.Count > 0)
            {
                channels[currentChannel].clip = clips.Peek();
                routineA = StartCoroutine(SpinFade(FadeDirection.FadeUp, channels[currentChannel]));
            }

            routineB = StartCoroutine(SpinFade(FadeDirection.FadeDown, channels[PeekNext()]));

            CycleNextChannel();
        }

        IEnumerator SpinFade(FadeDirection dir, AudioSource channel)
        {
            if (dir == FadeDirection.FadeUp)
                channel.Play();

            float t = 0;
            double d;


            while (t < 1.0)
            {

                d = Easing.Quad.EaseIn(t, 0, 1.0, 1.0);
                d = dir == FadeDirection.FadeDown ? 1 - d : d;

                channel.volume = (float)d;

                t += Time.deltaTime;

                yield return null;

            }
        }
    }
}