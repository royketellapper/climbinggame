﻿using UnityEngine;


namespace RoyFramework.Audio
{
    public class PlayOnAwake : MonoBehaviour
    {
        public AudioClip Clip;
        public SoundType Type;

        public float Volume = 1;
        public float Pitch = 1;

        void Awake()
        {
            AudioManager.Instance.PlayOneShot(Clip, Volume, Type, Pitch);
        }
    }
}
