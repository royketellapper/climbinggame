﻿using UnityEngine;
using System.Collections;

namespace RoyFramework.Audio
{
    [System.Serializable]
    public class SoundClipSettings
    {
        [Range(0, 1)]
        public float volume = 1f;
        public AudioClip AudioClip;
    }
}

