﻿using UnityEngine;

namespace RoyFramework.Audio
{
    [System.Serializable]
    public class MinMax
    {
        public bool Enable;
        public float Min = 0;
        public float Max = 1;
    }

    public enum PlayType
    {
        InOrder,
        Random
    }

    [RequireComponent(typeof(AudioSource))]
    public class PlaySoundOnEnable : MonoBehaviour
    {
        public SoundType SoundType;
        public PlayType PlayType;

        [Space]
        public float Delay;
        public bool DisableOnComplete;

        [Space]
        public MinMax RandomPitch;
        public MinMax RandomVolume;

        [Space]
        public AudioClip[] AudioClips;
        protected AudioSource _audioSource;
        protected int _orderCount = 0;

        protected virtual void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.outputAudioMixerGroup = AudioManager.Instance.GetAudioMixerGroup(SoundType);
        }

        protected virtual void OnEnable()
        {
            if (Delay == 0)
                PlaySound();
            else
                Invoke("PlaySound", Delay);
        }

        protected virtual void PlaySound()
        {
            if (AudioClips.Length == 0)
                return;

            AudioClip audioClip = null;

            switch (PlayType)
            {
                case PlayType.InOrder:
                    audioClip = AudioClips[_orderCount];
                    _orderCount = (int)Mathf.Repeat(_orderCount + 1, AudioClips.Length);
                    break;
                case PlayType.Random:
                    int index = Mathf.Min(Mathf.RoundToInt(Random.value * AudioClips.Length), AudioClips.Length - 1);
                    audioClip = AudioClips[index];
                    break;
            }

            if (RandomPitch.Enable)
                _audioSource.pitch = Random.Range(RandomPitch.Min, RandomPitch.Max);

            if (RandomVolume.Enable)
                _audioSource.pitch = Random.Range(RandomVolume.Min, RandomVolume.Max);

            _audioSource.PlayOneShot(audioClip);

            if (DisableOnComplete)
                Invoke("OnComplete", audioClip.length);
        }

        protected virtual void OnComplete()
        {
           gameObject.SetActive(false);
        }
    }
}