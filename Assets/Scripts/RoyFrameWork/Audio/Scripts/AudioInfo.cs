﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;

namespace RoyFramework.Audio
{
    public class AudioInfo : ScriptableObject
    {
        public SoundTypeList[] soundTypeList;

        AudioInfo()
        {
            if (soundTypeList == null)
            {
                string[] soundNames = Enum.GetNames(typeof(SoundType));
                Array.Resize<SoundTypeList>(ref soundTypeList, soundNames.Length);

                for (int x = 0; x < soundNames.Length; x++)
                {
                    SoundTypeList audioType = new SoundTypeList(); // ScriptableObject.CreateInstance<SoundTypeList>();
                    audioType.SoundType = (SoundType)x;
                    soundTypeList[x] = audioType;
                }
            }
        }

        public List<SoundIdSettings> GetAudioSettings(string id, SoundType soundType)
        {
            List<SoundIdSettings> list = new List<SoundIdSettings>();

            SoundTypeList audioType = Array.Find< SoundTypeList>(soundTypeList ,s => s.SoundType == soundType);
            if (audioType == null)
                return list;            

            if (audioType.AudioSettings == null)
                return list;

            foreach (SoundIdSettings audio in audioType.AudioSettings)
            {
                if (audio.id == id)
                    list.Add(audio);
            }

            return list;
        }
    }
}
