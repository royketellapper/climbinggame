﻿using UnityEngine;
#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif

namespace RoyFramework.Audio
{
    public class ButtonSound : ScriptableObject
    {
        public SoundType SoundType = SoundType.UI;

        public SoundClipSettings OnClick;
        public SoundClipSettings OnUp;
        public SoundClipSettings OnDown;
        public SoundClipSettings OnEnter;
        public SoundClipSettings OnExit;

#if UNITY_EDITOR

        public static string GetSelectedPath()
        {
            string path = "Assets";

            foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
            {
                path = AssetDatabase.GetAssetPath(obj);
                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
                    path = Path.GetDirectoryName(path);
                    break;
                }
            }
            return path;
        }

        [MenuItem("Assets/Create/ButtonSound")]
        public static void Create()
        {
            string path = AssetDatabase.GenerateUniqueAssetPath(GetSelectedPath() + "/ButtonSound.asset");
            ButtonSound asset = CreateInstance<ButtonSound>();
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
#endif
    }
}
