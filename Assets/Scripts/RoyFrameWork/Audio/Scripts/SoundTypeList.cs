﻿using UnityEngine;

namespace RoyFramework.Audio
{
    [System.Serializable]
    public class SoundTypeList
    {
        public SoundType SoundType;
        public SoundIdSettings[] AudioSettings;
    }
}
