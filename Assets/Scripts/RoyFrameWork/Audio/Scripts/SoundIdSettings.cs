﻿using System;

namespace RoyFramework.Audio
{
    [Serializable]
    public class SoundIdSettings : SoundClipSettings
    {
        public string id;
    }
}
