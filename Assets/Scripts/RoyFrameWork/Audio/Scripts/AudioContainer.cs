﻿using UnityEngine;
using System.Collections;
using RoyFramework.Audio;

public class AudioContainer : MonoBehaviour
{
	public AudioClip clip;

	void OnEnable(){
		StackFader.Instance.PushClip(clip);
	}

	void OnDisable(){
		StackFader.Instance.PopClip();
	}

}