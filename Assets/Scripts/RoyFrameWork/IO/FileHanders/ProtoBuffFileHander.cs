﻿using ProtoBuf;
using System;
using System.IO;
using ProtoBuf.Meta;
using UnityEngine;
using RoyFramework;

namespace RoyFramework.IO
{
    public class ProtoBuffFileHander : iFileHandler
    {
        public string Name { get { return "ProtoBuff"; } }
        public string[] Extensions  { get { return new string[] { "pr"};  } }

        public event Action<string> FileError;

        public ProtoBuffFileHander()
        {
            InitializeProtoBuf();
        }

        public static void InitializeProtoBuf()
        {
            //Is ProtoBuf initialized? 
            foreach (var item in RuntimeTypeModel.Default.GetTypes())
                if (item == typeof(Vector2))
                    return;
              
            //Converter functions
            var model = RuntimeTypeModel.Default;
            model.Add(typeof(Vector2), true).Add("x", "y");
            model.Add(typeof(IntVector2), true).Add("x", "y");            
            model.Add(typeof(Vector3), true).Add("x", "y", "z");
            model.Add(typeof(Vector4), true).Add("x", "y", "z", "w");
            model.Add(typeof(Color), true).Add("a", "b", "g", "r");
            model.Add(typeof(Quaternion), true).Add("x", "y", "z", "w");
            model.Add(typeof(Bounds), true).Add("center", "extents", "max", "min", "size");
            model.Add(typeof(Matrix4x4), true).Add(
                "m00", "m01", "m02", "m03",
                "m10", "m11", "m12", "m13",
                "m20", "m21", "m22", "m23",
                "m30", "m31", "m32", "m33");
            model.Add(typeof(Rect), true).Add(
                "bottom", "center", "height", "left", "max", "min",
                "position", "right", "size", "top", "width", "x",
                "xMax", "xMin", "y", "yMax", "yMin");
            model.Add(typeof(RectOffset), true).Add("bottom", "horizontal",
                "left", "right", "top", "vertical");

            //model.Add(typeof(UnityObjectSurrogate), true);
            //model.Add(typeof(UnityEngine.Object), false).SetSurrogate(typeof(UnityObjectSurrogate));
        }

        public T Load<T>(string path)
        {
            FileStream stream = null;
            T obj = default(T);
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                obj = Serializer.Deserialize<T>(stream);
            }
            catch (IOException ioEx)
            {
                if (FileError != null)
                    FileError(ioEx.ToString());
            }
            finally
            {
                stream.Close();
            }

            return obj;
        }

        public void Save(object obj, string path)
        {
            FileStream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Create, FileAccess.Write);
                Serializer.Serialize(stream, obj);
            }
            catch (IOException ioEx)
            {
                if (FileError != null)
                    FileError(ioEx.ToString());
            }
            finally
            {
                stream.Close();
            }
        }
    }
}