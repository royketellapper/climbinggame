﻿using RoyFramework.Tools;
using System;
using System.IO;
using System.Text;

namespace RoyFramework.IO
{
    public class JsonFileHander : iFileHandler
    {
        public string Name { get { return "Json"; } }
        public string[] Extensions { get { return new string[] { "json" }; } }
        public event Action<string> FileError;

        public T Load<T>(string path)
        {
            try
            {
                string data = File.ReadAllText(path);
                return SerializeHelper.JsonDeserializeFromString<T>(data);
            }
            catch (IOException ioEx)
            {
                if (FileError != null)
                    FileError(ioEx.ToString());

                return default(T);
            }
        }

        public void Save(object obj, string path)
        {
            FileStream fs = null;
            try
            {
                fs = File.Create(path);
                string contents = SerializeHelper.JsonSerializeToString(obj);
                byte[] info = new UTF8Encoding(true).GetBytes(contents);
                fs.Write(info, 0, info.Length);
            }
            catch (IOException ioEx)
            {
                if (FileError != null)
                    FileError(ioEx.ToString());
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
    }
}