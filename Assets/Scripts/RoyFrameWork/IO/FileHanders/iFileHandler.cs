﻿using System;

namespace RoyFramework.IO
{
    public interface iFileHandler
    {
        string Name { get; }
        string[] Extensions { get; }

        event Action<string> FileError;

        T Load<T>(string path);

        void Save(object obj, string path);
    }
}