﻿using RoyFramework.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace RoyFramework.IO
{
    public enum FileType
    {
        Json,
        ProtoBuf
    }

    public sealed class FileFactory
    {
        public static Action<string> FileError;
        public static string SavePath { get { return Application.dataPath; } }

        private static FileFactory fileFactory
        {
            get
            {
                if (_fileFactory == null)
                    _fileFactory = new FileFactory();

                return _fileFactory;
            }
        }
        private static FileFactory _fileFactory;
        private iFileHandler[] _fileHandles = new iFileHandler[2];

        private FileFactory()
        {
            //Json serializer
            _fileHandles[0] = new JsonFileHander();
            _fileHandles[0].FileError += FileError;

            //ProtoBuff serializer
            _fileHandles[1] = new ProtoBuffFileHander();
            _fileHandles[1].FileError += FileError;
        }

        public static iFileHandler GetFileHandler(FileType fileType)
        {
            return fileFactory._fileHandles[(int)fileType];
        }

        public static iFileHandler FindFileExtension(params string[] extension)
        {
            for (int i = 0; i < extension.Length; i++)
                extension[i] = extension[i].Replace(".", string.Empty);

            foreach (var handle in fileFactory._fileHandles)
            {
                foreach (var ext in extension)
                {
                    if (handle.Extensions.Contains(ext))
                        return handle;
                }
            }

            return null;
        }

        public static List<string> GetSupportedExtensions()
        {
            List<string> extension = new List<string>();
            foreach (var handle in fileFactory._fileHandles)
                extension.AddRange(handle.Extensions);

            return extension;
        }

        public static string[] GetExtensionsFilters()
        {
            List<string> extension = new List<string>();
            foreach (var handle in fileFactory._fileHandles)
            {
                string extensions = string.Join(",", handle.Extensions);
                extension.Add(handle.Name);
                extension.Add(extensions);
            }

            return extension.ToArray();
        }

        //public static T ResourceLoad<T>(string path, FileType fileType)
        //{
        //    iFileHandler iFileHandler = GetFileHandler(fileType);

        //    if (FindResourcePath(ref path, iFileHandler))
        //        return iFileHandler.Load<T>(path);

        //    return default(T);
        //}

        public static T Load<T>(string path)
        {
            iFileHandler fileHandler = FindFileExtension(Path.GetExtension(path));
            return fileHandler.Load<T>(path);
        }

        public static T Load<T>(string path, FileType fileType)
        {
            iFileHandler iFileHandler = GetFileHandler(fileType);
            return iFileHandler.Load<T>(path);
        }

        //public static void ResourceSave(object obj, string path, FileType fileType)
        //{
        //    iFileHandler fileHandler = GetFileHandler(fileType);

        //    if (FindResourcePath(ref path, fileHandler))
        //        fileHandler.Save(obj, path);
        //    else
        //    {
        //        //Directory.CreateDirectory(Application.dataPath + "/LevelMaps");
        //        string fileExtension = Path.GetExtension(path);
        //        string fileName = Path.GetFileNameWithoutExtension(path);
        //        string directoryName = Path.GetDirectoryName(path);
        //        if (!Directory.Exists(directoryName))
        //            Directory.CreateDirectory(directoryName);

        //        if (string.IsNullOrEmpty(fileExtension) || FindFileExtension(fileExtension) != fileHandler)
        //            fileExtension = fileHandler.Extensions[0];

        //        fileHandler.Save(obj, path);
        //    }
        //}


        public static void Save(object obj, string path)
        {
            iFileHandler fileHandler = FindFileExtension(Path.GetExtension(path));
            fileHandler.Save(obj, path);
        }

        public static void Save(object obj, string path, FileType fileType)
        {
            iFileHandler fileHandler = GetFileHandler(fileType);
            fileHandler.Save(obj, path);
        }

        //private static bool FindResourcePath(ref string path, iFileHandler iFileHandler)
        //{
        //    string fileExtension = Path.GetExtension(path);
        //    List<string> extensions = new List<string>(iFileHandler.Extensions);

        //    int index = extensions.FindIndex(a => a == fileExtension);
        //    if (!string.IsNullOrEmpty(fileExtension) && index != -1)
        //    {
        //        if (File.Exists(path))
        //            return true;


        //        if (index != 0)
        //            extensions.MoveElement(index, 0);
        //    }

        //    string fileName = Path.GetFileNameWithoutExtension(path);
        //    string[] dirs = FileUtility.GetResourcesDirectories();

        //    foreach (var dir in dirs)
        //    {
        //        foreach (var ext in extensions)
        //        {
        //            string newPath = dir + fileName +"."+ ext;
        //            if (File.Exists(newPath))
        //            {
        //                path = newPath;
        //                return true;
        //            }
        //        }
        //    }

        //    return false;
        //}
    }
}