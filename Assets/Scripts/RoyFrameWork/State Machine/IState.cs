﻿public interface IState<Key, TBaseClass> where TBaseClass : class
{
    TBaseClass BaseClass { get;  set; }

    StateMachine<Key, TBaseClass> StateMachine { get; set; }

    void Initialize();

    void OnStart();

    void OnUpdate();

    void OnEnd();
}
