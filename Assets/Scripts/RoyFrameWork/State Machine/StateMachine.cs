﻿using System;
using System.Collections.Generic;

public class StateMachine<TKey, TBaseClass> where TBaseClass : class
{
    public IState<TKey, TBaseClass> CurrentState { get; protected set; }
    public IState<TKey, TBaseClass> LastState { get; protected set; }
    public readonly TBaseClass BaseClass;
    public event Action<IState<TKey, TBaseClass>> Changed;
    public IState<TKey, TBaseClass> this[TKey key]
    {
        get { return Get(key); }
        set { Add(key, value); }
    }

    protected Dictionary<TKey, IState<TKey, TBaseClass>> _stateList = new Dictionary<TKey, IState<TKey, TBaseClass>>();

    public StateMachine(TBaseClass baseClass)
    {
        BaseClass = baseClass;
    }

    public virtual void Add(TKey key, IState<TKey, TBaseClass> state)
    {
        if (!_stateList.ContainsKey(key))
        {
            _stateList.Add(key, state);
            state.Initialize();
        }
    }

    public virtual void Remove(TKey key)
    {
        _stateList.Remove(key);
    }

    public virtual void SetState(TKey key)
    {
        IState<TKey, TBaseClass> state = null;
        if (_stateList.TryGetValue(key, out state))
        {
            LastState = CurrentState;
            CurrentState = state;

            if (CurrentState != null)
                CurrentState.OnEnd();

            state.OnStart();

            if (Changed != null)
                Changed(state);
        }
    }

    public virtual T Get<T>(TKey key) where T : IState<TKey, TBaseClass>
    {
        return (T)Get(key);
    }

    public virtual IState<TKey, TBaseClass> Get(TKey key)
    { 
        IState<TKey, TBaseClass> state = null;
        if (_stateList.TryGetValue(key, out state))
            return state;

        return null;
    }

    public void Update()
    {
        if (CurrentState != null)
            CurrentState.OnUpdate();
    }
}
