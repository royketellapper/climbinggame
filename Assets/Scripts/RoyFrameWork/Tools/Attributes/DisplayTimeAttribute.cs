﻿using UnityEngine;

namespace RoyFramework.TimeHelper
{
   // [AttributeUsage(AttributeTargets.Field)]
    public class DisplayTimeAttribute : PropertyAttribute
    {
        public readonly bool ShowDays;
        public readonly bool ShowHours;
        public readonly bool ShowMinutes;
        public readonly bool ShowSeconds;

        public DisplayTimeAttribute(bool showSeconds , bool showMinutes = true, bool showHours = false, bool showDays = false)
        {
            ShowSeconds = showSeconds;
            ShowMinutes = showMinutes;
            ShowHours = showHours;
            ShowDays = showDays;
        }

        public DisplayTimeAttribute()
        {
            ShowSeconds = true;
            ShowMinutes = true;
            ShowHours = false;
            ShowDays = false;
        }
    }
}