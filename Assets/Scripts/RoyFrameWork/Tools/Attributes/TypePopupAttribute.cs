﻿using System;
using UnityEngine;

namespace RoyFramework.Tools
{
    public class TypePopupAttribute : PropertyAttribute
    {
        public readonly Type Type;

        public TypePopupAttribute(Type type)
        {
            Type = type;
        }
    }
}

