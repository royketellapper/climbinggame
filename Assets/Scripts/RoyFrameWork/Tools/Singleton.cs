﻿//==============================================================================
//	Base class for Singletons
//
//	Created by Roy Ketellapper, Joseph Douthwaite
//  Version v0.6
//	© 2016, Vasco Games, Inc.  All Rights Reserved
//==============================================================================
using System;
using UnityEngine;

namespace RoyFramework
{
    public abstract class SingletonBase : MonoBehaviour
    {
        protected virtual void Awake() { }
    }

    public abstract class Singleton<S> : SingletonBase where S : Singleton<S>
    {
        private static S _instance = null;
        protected virtual bool _dontDestroyOnLoad { get { return true; } }
        private static object _instanceLock = new object();
        public static S Instance { get { return GetInstance(); } }
        public static S UnSafeInstance { get { return _instance; } set { _instance = value; } }
        public static S PrefabInstance
        {
            get
            {
                GameObject obj = (GameObject)Resources.Load("Singleton/" + typeof(S).Name, typeof(GameObject));
                if (obj != null)
                    return obj.GetComponent<S>(); ;

                return null;
            }
        }

        private bool _initialised;
        private bool _destroyed;

        public static S GetInstance()
        {
            if (_instance != null)
                return _instance;

            lock (_instanceLock)
            {
                S[] components = GameObject.FindObjectsOfType<S>();

                if (components.Length > 0)
                {
                    for (int x = 0; x < components.Length; x++)
                    {
                        if (components[x]._destroyed)
                            continue;

                        if (_instance == null)
                            _instance = components[x];
                        else
                            Destroy(components[x].gameObject);
                    }
                }

                if (_instance == null)
                {
                    string name = typeof(S).Name;
                    GameObject prefab = (GameObject)Resources.Load("Singleton/" + name, typeof(GameObject));

                    if (prefab != null)
                    {
                        _instance = (S)Instantiate(prefab).GetComponent<S>();
                        //Debug.Log("--------------Creating prefab singeton " + name + "--------------- "+ instance);
                    }
                    if (prefab != null && _instance == null)
                    {
                        Debug.LogWarning("--------------Unable To Load prefab " + name + "---------------");
                        Destroy(prefab);
                    }

                    if (_instance == null)
                    {
                        GameObject newSingleton = new GameObject(name);
                        _instance = newSingleton.AddComponent<S>();
                        //Debug.Log("--------------Creating singeton " + name + "---------------");
                    }
                    _instance.gameObject.hideFlags = HideFlags.DontSaveInEditor;
                }
            }

            return _instance;
        }

        //Don't implement Awake in inherited scripts! the code will execute for every instance
        protected sealed override void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
                return;
            }

            if (_instance == null)
                _instance = this as S;

            if (!_initialised)
            {
                _initialised = true;
                Ini();
            }

            if (_dontDestroyOnLoad)
                DontDestroyOnLoad(_instance.transform.root.gameObject);
        }

        public virtual void OnDestroy()
        {
            if (_instance == this)
                _instance = null;

            _destroyed = true;
        }

        protected abstract void Ini();
    }
}

