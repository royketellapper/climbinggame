﻿using UnityEngine;
using System;
using System.Collections;

namespace RoyFramework.Tools
{
    public static class MonoBehaviourExtentions
    {
        public static Coroutine WaitForSeconds(this MonoBehaviour self, WaitForSeconds seconds, Action callback)
        {
            return self.StartCoroutine(WaitForSecondsCoroutine(self, seconds, callback));
        }
        private static IEnumerator WaitForSecondsCoroutine(MonoBehaviour self, WaitForSeconds seconds, Action callback)
        {
            yield return seconds;
            callback();
        }

        /// <summary>
        /// Example use: this.WaitForSecondsLooping(waitForSecondsCached, coroutineCallback, coroutineCached, x => { coroutineCached = x; } );
        /// </summary>
        /// <param name="self"></param>
        /// <param name="seconds"></param>
        /// <param name="callback"></param>
        /// <param name="coroutine"></param>
        /// <param name="coroutineAssigner"></param>
        /// <returns></returns>
        public static Coroutine WaitForSecondsLooping(this MonoBehaviour self, WaitForSeconds seconds, Action callback, IEnumerator coroutine, Action<IEnumerator> coroutineAssigner)
        {
            coroutine = WaitForSecondsLoopingCoroutine(self, seconds, callback, coroutine, coroutineAssigner);
            coroutineAssigner(coroutine);
            return self.StartCoroutine(coroutine);

        }
        private static IEnumerator WaitForSecondsLoopingCoroutine(MonoBehaviour self, WaitForSeconds seconds, Action callback, IEnumerator coroutine, Action<IEnumerator> coroutineAssigner)
        {
            yield return seconds;
            callback();
            coroutine = WaitForSecondsLoopingCoroutine(self, seconds, callback, coroutine, coroutineAssigner);
            coroutineAssigner(coroutine);
            self.StartCoroutine(coroutine);
        }
    }
}