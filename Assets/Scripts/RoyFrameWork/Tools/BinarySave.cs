﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using UnityEngine;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace RoyFramework.IO
{
    public enum IOState
    {
        None,
        Loading,
        Saving
    }

    public class BinarySave<T> where T : class, ICloneable, new()
    {
        public Action<string> ErrorException;
        public Action<T> OnLoaded;
        public Action OnSaved;

        protected IOState currentState;
        protected List<IOState> waitingStates = new List<IOState>();
        protected BackgroundWorker thread;
        protected readonly bool IfNullCreateNew;
        protected readonly string filePath;
        protected bool threadCompleted;

        //Data
        protected T threadSaveData;
        protected T waitingSaveData;
        protected T loadedData;

        public BinarySave(string fileName, bool ifNullCreateNew = false)
        {
            filePath = Application.persistentDataPath + "/" + fileName;
            IfNullCreateNew = ifNullCreateNew;
            thread = new BackgroundWorker();

            
            //thread.RunWorkerCompleted += WorkCompleted;
        }
        ~BinarySave()
        {
            //thread.RunWorkerCompleted -= WorkCompleted;
            thread = null;
            threadSaveData = null;
            waitingSaveData = null;
            loadedData = null;
        }

        protected virtual void AddWaitingState(IOState state)
        {
            if (currentState == state)
            {
                if (waitingStates.FindAll(s => s == state).Count < 2)
                    waitingStates.Add(state);

            }
            else if (!waitingStates.Contains(state))
                waitingStates.Add(state);
        }

        protected virtual void RemoveState(IOState state)
        {
            waitingStates.Remove(state);
        }

        public void Update()
        {
            if (threadCompleted)
                WorkCompleted();
        }

        protected virtual void WorkCompleted()
        {
            threadCompleted = false;
            RemoveState(currentState);
            
            switch (currentState)
            {
                case IOState.Loading:
                    if (OnLoaded != null)
                        OnLoaded(loadedData);

                    loadedData = null;
                    break;
                case IOState.Saving:
                    if (OnSaved != null)
                        OnSaved();
                    break;
            }

            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            StartNextState();
        }

        protected virtual void StartNextState()
        {
            thread.DoWork -= LoadThread;
            thread.DoWork -= SavingThread;

            if (waitingStates.Count > 0)
            {
                currentState = waitingStates[0];
                switch (waitingStates[0])
                {
                    case IOState.Loading:
                        thread.DoWork += LoadThread;
                        thread.RunWorkerAsync();
                        return;
                    case IOState.Saving:
                        threadSaveData = waitingSaveData;
                        waitingSaveData = null;

                        thread.DoWork += SavingThread;
                        thread.RunWorkerAsync();
                        return;
                }
            }
            else
            {
                currentState = IOState.None;
            }
        }

        public virtual void Load()
        {
            if (thread.IsBusy || waitingStates.Count != 0)
            {
                AddWaitingState(IOState.Loading);
                return;
            }

            threadCompleted = false;

            currentState = IOState.Loading;
            thread.DoWork += LoadThread;
            thread.RunWorkerAsync();
        }

        protected virtual void LoadThread(object sender, DoWorkEventArgs eventArgs)
        { 
            loadedData = null;

            if (File.Exists(filePath))
            {
                FileStream file = null;

                try
                {
                    file = File.Open(filePath, FileMode.Open);
                    BinaryFormatter bf = new BinaryFormatter();
                    loadedData = (T)bf.Deserialize(file);
                }
                catch (Exception e)
                {
                    if (ErrorException != null)
                        ErrorException(e.Message);
                }
                finally
                {
                    if (file != null)
                    {
                        file.Flush();
                        file.Close();
                    }
                }
            }

            if (IfNullCreateNew && loadedData == null)
                loadedData = new T();

            //Debug.Log(loadedData != null);

            GC.Collect();
            GC.WaitForPendingFinalizers();

            threadCompleted = true;
        }

        public virtual void Save(T data)
        {
            if (data == null)
                return;

            if (thread.IsBusy || waitingStates.Count != 0)
            {
                AddWaitingState(IOState.Saving);
                waitingSaveData = (T)data; //.Clone();
                return;
            }

            threadCompleted = false;

            currentState = IOState.Saving;
            threadSaveData = (T)data; //.Clone();
            thread.DoWork += SavingThread;
            thread.RunWorkerAsync();
        }

        protected virtual void SavingThread(object sender, DoWorkEventArgs eventArgs)
        {         
            FileStream file = null;
            try
            {
                file = File.Create(filePath);
                BinaryFormatter bf = new BinaryFormatter();
               
                bf.Serialize(file, threadSaveData);
            }
            catch (Exception e)
            {
                if (ErrorException != null)
                    ErrorException(e.Message);
            }
            finally
            {
                if (file != null)
                {
                    file.Flush();
                    file.Close();
                }
                
            }

            threadSaveData = null;
            threadCompleted = true;
        }

        public virtual bool Delete()
        {
            Debug.Log(filePath);

            try
            {
                File.Delete(filePath);
                return true;
            }
            catch (Exception e)
            {
                if (ErrorException != null)
                    ErrorException(e.Message);

                return false;
            }
        }
    }
}
