﻿/// <summary>
/// BaseObject class is an abstract class for you to derive from.
/// Every class that will be dirived from this class will support the 
/// Clone method automaticly.
/// The class implements the interface ICloneable and there 
/// for every object that will be derived 
/// from this object will support the ICloneable interface as well.
/// </summary>
/// 
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace RoyFramework.Tools
{
    [System.Serializable]
    public abstract class CloneBaseObject : ICloneable
    {
        /// <summary>
        /// Clone the object, and returning a reference to a cloned object.
        /// </summary>
        /// <returns>Reference to the new cloned 
        /// object.</returns>
        public object Clone()
        {
            using (var memStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter(
                   null,
                   new StreamingContext(StreamingContextStates.Clone));
                binaryFormatter.Serialize(memStream, this);
                memStream.Seek(0, SeekOrigin.Begin);
                return binaryFormatter.Deserialize(memStream);
            }
        }
    }
}