﻿using System;

namespace RoyFramework.Tools
{
    /// <summary>
    /// ObservableProperty allows scripts to listen to accessors get and set, firing an event when the property is (potentially) changed
    /// </summary>   
    public class ObservableProperty<T>
    {
        public Action OnChanged;

        protected T _property;
        protected bool _changedOnGet;

        /// <summary>
        /// <para>Implementation example:  ObservableProperty<List<Type>> property = new ObservableProperty<List<Type>>(new List<Type>());</para>
        /// <para>property.OnChanged += OnPropertyChanged;</para>
        /// </summary>                        
        public ObservableProperty(T initialValue, bool changedOnGet = false)
        {
            if (typeof(T).IsGenericType || changedOnGet)
            {
                // Notify a change in the get accessor when _changedOnGet is set to true or type is of generic type
                // for when contents can be changed without accessing the set
                _changedOnGet = true;

            }
            _property = initialValue;
        }

        public virtual T Property
        {
            get
            {
                if (_changedOnGet && OnChanged != null)
                    OnChanged();
                return _property;
            }

            set
            {
                _property = value;
                if (OnChanged != null)
                    OnChanged();
            }
        }

        public virtual T HardSetProperty
        {
            get
            {
                return _property;
            }
            set
            {
                _property = value;
            }
        }

        public static implicit operator T(ObservableProperty<T> obj)
        {
            return obj.Property;
        }
    }
}
