﻿//==============================================================================
//	Don't use this class use ResourceProperty instead
//
//	Created by Roy Ketellapper
//  Version v0.1
//	© 2016, Vasco Games, Inc.  All Rights Reserved
//==============================================================================
using UnityEngine;

namespace RoyFramework.Utility
{
    [System.Serializable]
    public abstract class ResourcePropertyBase
    {
        public string Path { get { return _path; } }

        [SerializeField]
        protected string _path = "";

        [SerializeField]
        protected string _typeName;
    }
}
