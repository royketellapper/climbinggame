﻿using UnityEngine;

namespace RoyFramework.Tools
{
    public static class ArrayHelper
    {
        public static T GetRandom<T>(this T[] array)
        {
            if (array == null || array.Length == 0)
                return default(T);

            return array[Random.Range(0, array.Length - 1)];
        }
    }
}