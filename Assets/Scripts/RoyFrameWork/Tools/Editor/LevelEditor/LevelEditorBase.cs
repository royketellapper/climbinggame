﻿using UnityEngine;
using UnityEditor;
using VascoGamesEditor.UIEditor;

namespace VascoGamesEditor.Tools
{
    public abstract class LevelEditorBase : EditorWindow
    {
        protected Vector2 _windowScroll;
        protected LevelEditorFoldout[] _foldouts;

        protected virtual GUIStyle GUIBox
        {
            get
            {
                if (_GUILeftOffset == null)
                    _GUILeftOffset = new GUIStyle("HelpBox");

                return _GUILeftOffset;
            }
        }
        protected GUIStyle _GUILeftOffset;

        protected virtual void OnGUI()
        {
            _windowScroll = GUILayout.BeginScrollView(_windowScroll);
            GUILayout.Space(5);
            OnDrawGUI();
            GUILayout.EndScrollView();
        }

        protected virtual void OnDrawGUI()
        {
            for (int x = 0; x < _foldouts.Length; x++)
            {
                var foldout = _foldouts[x];
          
                if (UnityEngine.Object.Equals(_foldouts[x], null))
                {
                    Debug.LogError("Foldout " + x + " is NULL");
                    continue;
                }

                if (foldout.MenuOpen = GUILayout.Toggle(foldout.MenuOpen, foldout.MenuName, EditorStyle.FoldoutStyle))
                {
                    GUILayout.BeginVertical(GUIBox);
                    foldout.LevelEditorBase = this;
                    foldout.OnGUI();
                    GUILayout.EndHorizontal();
                }
            }
        }
    }
}
