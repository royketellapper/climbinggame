﻿using UnityEditor;

namespace VascoGamesEditor.Tools
{
    public abstract class LevelEditorFoldout : UnityEngine.Object
    {
        public bool MenuOpen
        {
            get
            {
                return EditorPrefs.GetBool(MenuName + "_Foldout");
            }
            set
            {
                EditorPrefs.SetBool(MenuName + "_Foldout", value);
            }
        }

        public LevelEditorBase LevelEditorBase;

        public abstract string MenuName { get; }
        public abstract void OnGUI();
    }
}