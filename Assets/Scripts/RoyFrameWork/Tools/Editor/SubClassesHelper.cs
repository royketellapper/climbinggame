﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;

namespace RoyFramework
{
    public static class SubClassesHelper
    {
        //Get all types that inherit a certain base class
        public static List<Type> GetSubclasses<T>() where T : class
        {
            return GetSubclasses(typeof(T));
        }

        public static List<Type> GetSubclasses(Type baseClass)
        {
            Type[] classTypes = Assembly.GetAssembly(baseClass).GetTypes();
            List<Type> names = new List<Type>();

            if (!baseClass.IsAbstract)
                names.Add(baseClass);

            foreach (Type classType in classTypes)
            {
                if (classType.IsSubclassOf(baseClass) && !classType.IsAbstract)
                    names.Add(classType);
            }

            return names;
        }

        public static List<String> GetTypeNames<T>() where T : class
        {
            return GetTypeNames(typeof(T));
        }

        public static List<String> GetTypeNames(Type baseClass)
        {
            List<String> names = new List<String>();
            Type[] classTypes = Assembly.GetAssembly(baseClass).GetTypes();

            if (!baseClass.IsAbstract)
                names.Add(GetReadableTypeName(baseClass));

            foreach (Type classType in classTypes)
            {
                if (classType.IsSubclassOf(baseClass) && !classType.IsAbstract)
                    names.Add(GetReadableTypeName(classType));
            }

            return names;
        }

        public static List<String> GetTypeNames(List<Type> types)
        {
            List<String> typeNames = new List<String>();

            foreach (Type type in types)
                typeNames.Add(GetReadableTypeName(type));

            return typeNames;
        }

        public static string GetReadableTypeName<T>()
        {
            return ObjectNames.NicifyVariableName(typeof(T).Name);
        }

        public static string GetReadableTypeName(Type type)
        {
            return ObjectNames.NicifyVariableName(type.Name);
        }       
    }
}
