﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace VascoGamesEditor.Utility
{
    public static class EditorUtilityFunctions
    {
        public static void DrawDefaultInspector(SerializedObject serializedObject, string[] IgnoreProperty = null, Action<SerializedProperty> IgnorePropertyCallBack = null)
        {
            SerializedProperty property = serializedObject.GetIterator();
            property.NextVisible(true);

            while (property.NextVisible(false))
            {
                if (IgnoreProperty != null && IgnoreProperty.Contains(property.name))
                {
                    if (IgnorePropertyCallBack != null)
                        IgnorePropertyCallBack(property);

                    continue;
                }

                EditorGUILayout.PropertyField(property, property.isExpanded);
            }
        }

        public static void GUIChanged(SerializedObject serializedObject)
        {
            if (GUI.changed)
            {
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(serializedObject.targetObject);
                serializedObject.Update();
            }
        }

        public static Type GetPropertyType(SerializedProperty property)
        {
            if (property.serializedObject.targetObject == null)
                throw new ArgumentNullException("property target Object cannot be null");

            Type parentType = property.serializedObject.targetObject.GetType();
            string[] propertyPath = property.propertyPath.Split('.');

            for (int i = 0; i < propertyPath.Length; i++)
            {
                string item = propertyPath[i];
                if (item == "Array")
                {
                    parentType = parentType.GetElementType();
                    i++;
                    continue;
                }

                parentType = parentType.GetField(item, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public).FieldType;
            }

            return parentType;
        }

        public static Type GetPropertyArrayType(SerializedProperty property)
        {
            Type type = GetPropertyType(property);

            if (type.IsGenericType)
                return type.GetMethod("get_Item").ReturnType;
            else
                return type.GetElementType();
        }
    }
}