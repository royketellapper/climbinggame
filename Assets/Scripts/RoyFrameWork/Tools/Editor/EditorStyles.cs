﻿using UnityEngine;
using UnityEditor;

namespace VascoGamesEditor.UIEditor
{
    public class EditorStyle
    {
        public static GUIStyle LineStyle
        {
            get
            {
                if (line != null)
                    if (line.normal.background == null)
                        line = null;

                if (line == null)
                {
                    line = new GUIStyle();
                    Texture2D texture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
                    texture.SetPixel(0, 0, Color.gray);
                    texture.Apply();
                    line.normal.background = texture;
                    line.stretchWidth = true;
                    line.padding.top = 2;
                    line.imagePosition = ImagePosition.ImageOnly;
                }
                return line;
            }
        }
        private static GUIStyle line;

        public static GUIStyle boldLabel
        {
            get
            {
                if (_boldLabel == null)
                {
                    //default Colors
                    Color defaultTextColor = new Color(0.705f, 0.705f, 0.705f, 1f);

                    //GUI
                    Color color = defaultTextColor;
                    _boldLabel = new GUIStyle();
                    _boldLabel.normal.textColor = color;
                    _boldLabel.hover.textColor = color;
                    _boldLabel.active.textColor = color;
                    _boldLabel.focused.textColor = color;
                    _boldLabel.onNormal.textColor = color;
                    _boldLabel.onHover.textColor = color;
                    _boldLabel.onActive.textColor = color;
                    _boldLabel.onFocused.textColor = color;
                    _boldLabel.fontStyle = FontStyle.Bold;
                }
                return _boldLabel;
            }
        }
        private static GUIStyle _boldLabel;       

        public static GUIStyle FoldoutStyle
        {
            get
            {
                if (foldoutStyle == null)
                    foldoutStyle = new GUIStyle(((GUISkin)EditorGUIUtility.Load("EditorSkin.guiskin")).toggle);

                return foldoutStyle;
            }         
        }
        private static GUIStyle foldoutStyle;

        public static GUIStyle ToggleButtonStyle
        {
            get
            {
                if (toggleButton == null)
                    toggleButton = new GUIStyle(((GUISkin)EditorGUIUtility.Load("EditorSkin.guiskin")).button);

                return toggleButton;
            }
        }
        private static GUIStyle toggleButton;


        public static GUIStyle ButtonStyle
        {
            get
            {
                if (button == null)
                    button = new GUIStyle(((GUISkin)EditorGUIUtility.Load("EditorSkin.guiskin")).label);

                return button;
            }
        }
        private static GUIStyle button;

        public static void Label(string text)
        {
            GUILayout.Label(text, boldLabel);
            Line();
        }
        public static void Line()
        {
            GUILayout.Box("", EditorStyle.LineStyle);
        }

        public static Texture2D CreateButtonTex(int x, int y, float gradientStart, float gradientEnd, Color color, Color bottom, Color top, Color left, Color right)
        {
            Texture2D texture = new Texture2D(x, y, TextureFormat.ARGB32, false);

            for (int posY = 0; posY < x; posY++)
            {
                float a = (float)posY / (float)x;
                a = Mathf.Lerp(gradientStart, gradientEnd, a); //0.13f 0.26f

                for (int posX = 0; posX < x; posX++)
                {
                    Color newColor = new Color(a, a, a) * color;

                    if (posY == 0 && posX == 0)
                        newColor = (bottom / 2) + (left / 2);

                    else if (posY == y - 1 && posX == 0)
                        newColor = (left / 2) + (top / 2);

                    else if (posY == 0 && posX == x - 1)
                        newColor = (right / 2) + (bottom / 2);

                    else if (posY == y - 1 && posX == x - 1)
                        newColor = (top / 2) + (right / 2);

                    else if (posY == 0)
                        newColor = bottom;

                    else if (posY == y - 1)
                        newColor = top;

                    else if (posX == 0)
                        newColor = left;

                    else if (posX == x - 1)
                        newColor = right;

                    texture.SetPixel(posX, posY, newColor);
                }
            }
            texture.Apply();
            return texture;
        }
    }
}
