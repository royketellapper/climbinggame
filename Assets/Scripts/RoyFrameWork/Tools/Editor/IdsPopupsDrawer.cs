﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public abstract class IdsPopupsDrawer<T> : PropertyDrawer where T : class
{
    protected string[] Names;
    protected string[] Ids;
    public abstract IEnumerable list { get; }

    public abstract string GetId(T item);

    public void GetMenuNames(string currentId)
    {
        List<string> names = new List<string>();
        List<string> ids = new List<string>();

        //if (Database.Instance == null)
        //    return;

        bool foundCurrentId = false;
        //if (Database.Instance.DialogueCharacters != null)
        //{
            foreach (var item in list)
            {
                string id = GetId((T)item);

                if (id == currentId)
                    foundCurrentId = true;

                ids.Add(id);
            }
        //}
        ids.Distinct();
        names.AddRange(ids);
        names.Insert(0, "---------------");
        ids.Insert(0, string.Empty);

        if (!foundCurrentId && currentId != string.Empty)
        {
            names.Add("*" + currentId);
            ids.Add(currentId);
        }

        Ids = ids.ToArray();
        Names = names.ToArray();
    }

    public int Selected(string id)
    {
        int index = Array.FindIndex<string>(Ids, m => m == id);

        if (index == -1)
            return 0;

        return index;
    }

    public string MenuPopup(string currentId, string label)
    {
        int index = Selected(currentId);
        index = EditorGUILayout.Popup(label, index, Names);
        return Ids[index];
    }

    public string MenuPopup(Rect rect, string currentId, GUIContent label)
    {
        int index = Selected(currentId);
        index = EditorGUI.Popup(rect, label.text, index, Names);
        return Ids[index];
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        if (Names == null)
            GetMenuNames(property.stringValue);

        if (property.propertyType == SerializedPropertyType.String)
            property.stringValue = MenuPopup(position, property.stringValue, label);
        else
            EditorGUI.PropertyField(position, property, label, true);

        EditorGUI.EndProperty();
    }
}
