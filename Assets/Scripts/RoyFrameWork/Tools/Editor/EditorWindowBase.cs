﻿using UnityEngine;
using UnityEditor;

namespace VascoGamesEditor
{
    public abstract class EditorWindowBase : EditorWindow //<T>: EditorWindow where T : ScriptableObject
    {
        protected Vector2 windowScroll;
        protected bool saveGUIChanged = false;
        //public T target;

        protected virtual void OnEnable()
        {
            EditorApplication.playmodeStateChanged = () =>
            {
                if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode)
                    Load();
            };
        }

        protected virtual void OnGUI()
        {
            windowScroll = GUILayout.BeginScrollView(windowScroll, GUILayout.Height(position.height - 50));
            GUILayout.Space(5);
            OnDrawGUI();
            GUILayout.EndScrollView();
            SaveResetCancelButtons();
        }

        protected virtual void OnDrawGUI() { }

        protected virtual void SaveResetCancelButtons()
        {
            GUILayout.BeginArea(new Rect((position.width / 2) - 250, position.height - 35, 500, 100));
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Default"))
            {
                Default();
                GUI.changed = false;
                saveGUIChanged = true;
            }

            if (GUILayout.Button("Load"))
            {
                Load();
                GUI.changed = false;
                saveGUIChanged = true;
            }

            if (GUILayout.Button("Save"))
            {
                Save();
                GUI.changed = false;
                saveGUIChanged = false;
            }

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        protected void EndGUIChanged()
        {
            if (GUI.changed)
                saveGUIChanged = true;
        }

        protected void BeginGUIChanged()
        {
            if (saveGUIChanged)
                return;

            GUI.changed = false;
        }

        public static void ShowWindow() { }
        protected abstract void Load();
        protected abstract void Save();
        protected abstract void Default();

        protected virtual void OnDestroy()
        {
            if (saveGUIChanged && EditorUtility.DisplayDialog("Save", "Save changes before closing?", "Yes", "No"))
                Save();
        }

        //protected void SetTarget(T target)
        //{

        //}

        protected bool Foldout(bool foldout, string content)
        {
            foldout = EditorGUILayout.Foldout(foldout, content);
            BeginGUIChanged();
            return foldout;
        }
    }
}
