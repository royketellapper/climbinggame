﻿using RoyFramework;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using RoyFramework;
using VascoGamesEditor.UIEditor;
using VascoGamesEditor.Utility;

namespace VascoGamesEditor.Tools
{
    public class FactoryList : UnityEngine.Object
    {
        protected readonly SerializedProperty _serializedProperty;
        protected List<Type> _menuTypes = new List<Type>();
        protected List<string> _menuNames = new List<string>();
        protected int _selectedMenuType;
        protected bool _isFactory = true;
        protected Type _arrayType;

        protected bool _draggable;
        protected bool _canDrag;
        protected int _dragItemIndex = -1;
        protected float _bottomBarY;

        protected class ItemMenu
        {
            public Editor ItemEditor
            {
                get
                {
                    if (_itemEditor == null && Property.objectReferenceValue != null)
                        _itemEditor = Editor.CreateEditor(Property.objectReferenceValue);

                    return _itemEditor;
                }
                set { _itemEditor = value; }
            }

            protected Editor _itemEditor;
            public bool Toggle
            {
                get
                {
                    if (Property == null)
                        return false;

                    return Property.isExpanded;
                }

                set
                {
                    if (Property != null)
                        Property.isExpanded = value;
                }
            }

            public Rect BoxRect;
            public Rect DragRect;
            public SerializedProperty Property;
        }
        protected List<ItemMenu> _menuItems = new List<ItemMenu>();

        protected virtual GUIStyle GUILeftOffset
        {
            get
            {
                if (_GUILeftOffset == null)
                {
                    _GUILeftOffset = new GUIStyle("HelpBox");
                    GUILeftOffset.padding.left = 15;
                }

                return _GUILeftOffset;
            }
        }
        protected GUIStyle _GUILeftOffset;

        protected virtual GUIStyle boxStyle
        {
            get
            {
                if (_boxStyle == null)
                    _boxStyle = new GUIStyle("Box");

                return _boxStyle;
            }
        }
        GUIStyle _boxStyle;

        protected virtual GUIStyle dragButtonStyle
        {
            get
            {
                if (_dragButtonStyle == null)
                    _dragButtonStyle = new GUIStyle("WindowBottomResize");

                return _dragButtonStyle;
            }
        }
        GUIStyle _dragButtonStyle;

        protected virtual GUIStyle insertionLineStyle
        {
            get
            {
                if (_insertionLineStyle == null)
                    _insertionLineStyle = new GUIStyle("PR Insertion Above");

                return _insertionLineStyle;
            }
        }
        GUIStyle _insertionLineStyle;

        public FactoryList(SerializedProperty serializedProperty)
        {
            if (serializedProperty == null || !serializedProperty.isArray)
                return;

            _serializedProperty = serializedProperty.Copy();
            _arrayType = EditorUtilityFunctions.GetPropertyArrayType(_serializedProperty);
            _isFactory = _arrayType.IsSubclassOf(typeof(ScriptableObject));

            if (_isFactory)
            {
                _menuTypes = SubClassesHelper.GetSubclasses(_arrayType);
                _menuNames = SubClassesHelper.GetTypeNames(_menuTypes);
            }

            CreateMenuItems();
        }

        protected virtual void CreateMenuItems()
        {
            _menuItems.Clear();

            int arraySize = _serializedProperty.arraySize;

            for (int x = 0; x < arraySize; x++)
            {
                SerializedProperty arrayProperty = _serializedProperty.GetArrayElementAtIndex(x);
                _menuItems.Add(CreateMenuItem(arrayProperty));
            }
        }

        protected virtual ItemMenu CreateMenuItem(SerializedProperty property)
        {
            ItemMenu itemMenu = new ItemMenu();
            itemMenu.Property = property;

            if (_isFactory && property.objectReferenceValue != null)
                itemMenu.ItemEditor = Editor.CreateEditor(property.objectReferenceValue);

            return itemMenu;
        }

        protected virtual void DrawTypePopup()
        {
            GUILayout.BeginHorizontal();

            if (_isFactory)
                _selectedMenuType = EditorGUILayout.Popup("Type", _selectedMenuType, _menuNames.ToArray());

            if (GUILayout.Button("Add", GUILayout.Width(40f)))
                AddItem();

            GUILayout.EndHorizontal();
        }

        public virtual void DrawList()
        {
            if (_serializedProperty.arraySize != _menuItems.Count)
                CreateMenuItems();

            GUILayout.BeginVertical(boxStyle);
            GUILayout.Label(ObjectNames.NicifyVariableName(_serializedProperty.name + ":"), EditorStyles.boldLabel);
            DrawTypePopup();
            GUILayout.Space(2);
            EditorStyle.Line();

            for (int x = 0; x < _menuItems.Count; x++)
            {
                var menuItem = _menuItems[x];

                SerializedProperty arrayProperty = _serializedProperty.GetArrayElementAtIndex(x);

                GUILayout.Space(4);
                menuItem.BoxRect = GUILayoutUtility.GetRect(0, 20, GUILayout.ExpandWidth(true));
                Rect dragRect = menuItem.BoxRect;
                dragRect.width = 20;
                menuItem.DragRect = dragRect;

                if (menuItem.Toggle)
                {
                    GUILayout.BeginVertical(GUILeftOffset);

                    if (_isFactory)
                    {
                        if (menuItem.ItemEditor != null)
                            menuItem.ItemEditor.OnInspectorGUI();
                    }
                    else
                    {
                        SerializedProperty arrayPropertyCopy = arrayProperty.Copy();
                        int totalPropertys = arrayProperty.Copy().CountInProperty();
                        arrayPropertyCopy.Next(true);
                        EditorGUILayout.PropertyField(arrayPropertyCopy, arrayProperty.isExpanded);

                        int arraycount = -2;
                        for (int a = 0; a < totalPropertys + Mathf.Max(arraycount, -1); a++)
                        {
                            arrayPropertyCopy.Next(false);

                            if (arrayPropertyCopy.isArray)
                            {
                                totalPropertys -= arrayPropertyCopy.Copy().CountInProperty();
                                arraycount++;
                            }

                            EditorGUILayout.PropertyField(arrayPropertyCopy, arrayProperty.isExpanded);
                        }
                    }

                    GUILayout.EndVertical();
                }
            }

            Rect lastRect = GUILayoutUtility.GetLastRect();

            _bottomBarY = lastRect.y + lastRect.height;
            ShowBars();

            GUILayout.EndVertical();
        }

        protected string GetPropertyName(SerializedProperty property)
        {
            if (property == null)
                return "NULL";

            if (_isFactory)
            {
                var obj = property.objectReferenceValue;

                if (obj != null)
                {
                    //SerializedObject serializedObject = new SerializedObject(obj);
                    //SerializedProperty tempProperty = serializedObject.GetIterator();

                    ////property.NextVisible(false);
                    ////property.NextVisible(false);
                    ////tempProperty.NextVisible(false);
                    ////tempProperty.NextVisible(true);

                    //if (tempProperty.propertyType == SerializedPropertyType.String)
                    //    return tempProperty.stringValue + " (" + SubClassesHelper.GetReadableTypeName(obj.GetType()) + ")";
                    //else
                    return SubClassesHelper.GetReadableTypeName(obj.GetType());
                }
                else
                    return "NULL";
            }
            else
            {
                SerializedProperty tempProperty = property.Copy();
                tempProperty.NextVisible(true);

                if (tempProperty.propertyType == SerializedPropertyType.String)
                    return tempProperty.stringValue;
                else
                    return SubClassesHelper.GetReadableTypeName(_arrayType);
            }
        }

        //Draw bars

        public void ShowBars()
        {
            var currentEvent = Event.current;
            Vector2 mousePos = currentEvent.mousePosition;

            //Draw bars
            for (int i = 0; i < _menuItems.Count; i++)
            {
                var menuItem = _menuItems[i];

                switch (currentEvent.type)
                {
                    case EventType.MouseDown:
                        if (menuItem.DragRect.Contains(mousePos) && currentEvent.button == 0 && !_canDrag)
                        {
                            _canDrag = true;
                            _dragItemIndex = i;
                            currentEvent.Use();
                        }
                        break;

                    case EventType.MouseUp:
                        if (_canDrag)
                        {
                            float yOffset;
                            int barIndex = FindClosedBar(mousePos.y, _dragItemIndex, out yOffset);
                            MoveItem(_dragItemIndex, barIndex);
                            _canDrag = false;
                            _dragItemIndex = -1;
                            currentEvent.Use();
                        }
                        break;

                    case EventType.MouseDrag:
                        if (_canDrag)
                        {
                            currentEvent.Use();
                        }
                        break;

                    case EventType.ContextClick:
                        if (menuItem.DragRect.Contains(mousePos))
                        {
                            RightClickMenu(i, menuItem.Property);
                            currentEvent.Use();
                        }
                        break;
                }

                if (_dragItemIndex != i)
                    DrawBar(i);
            }

            if (_dragItemIndex != -1)
            {
                var menuItem = _menuItems[_dragItemIndex];
                float mouseY = Mathf.Clamp(mousePos.y - 10, _menuItems[0].BoxRect.y, _bottomBarY - 20);

                DrawBarDrag(_dragItemIndex, mouseY);

                float yOffset;
                FindClosedBar(mousePos.y, _dragItemIndex, out yOffset);
                Rect boxRect = menuItem.BoxRect;
                boxRect.y = yOffset - 2;
                GUI.Box(boxRect, "", insertionLineStyle);
            }
        }

        //Fix me
        protected void DrawBarDrag(int index, float yOffset)
        {
            ItemMenu itemMenu = _menuItems[index];
            Rect boxRect = itemMenu.BoxRect;
            boxRect.y = yOffset;
            GUI.Box(boxRect, "", boxStyle);

            Rect dragRect = itemMenu.DragRect;
            dragRect.width = 10;
            dragRect.y = yOffset + 10;
            dragRect.x += 6;
            GUI.Box(dragRect, "", dragButtonStyle);

            string arrow = (itemMenu.Toggle) ? char.ConvertFromUtf32(9660) : char.ConvertFromUtf32(9658);
            Rect labelRect = boxRect;
            labelRect.width = Screen.width - 20;
            labelRect.x += 20;
            labelRect.y += 2;
            EditorGUI.LabelField(labelRect, arrow + " " + index + ". " + GetPropertyName(itemMenu.Property));
        }

        protected void DrawBar(int index)
        {
            ItemMenu itemMenu = _menuItems[index];
            GUI.Box(itemMenu.BoxRect, "", boxStyle);

            Rect dragRect = itemMenu.DragRect;
            dragRect.width = 10;
            dragRect.y += 8;
            dragRect.x += 6;
            GUI.Box(dragRect, "", dragButtonStyle);

            string arrow = (itemMenu.Toggle) ? char.ConvertFromUtf32(9660) : char.ConvertFromUtf32(9658);
            Rect labelRect = itemMenu.BoxRect;
            labelRect.width = itemMenu.BoxRect.xMax - 80;
            labelRect.x += 20;
            labelRect.y += 2;

            //Toggle
            if (GUI.Button(labelRect, arrow + " " + index + ". " + GetPropertyName(itemMenu.Property), EditorStyles.label))
                itemMenu.Toggle = !itemMenu.Toggle;

            Rect buttonRect = itemMenu.BoxRect;
            buttonRect.width = 20;
            buttonRect.x = itemMenu.BoxRect.xMax - 25; 

            //Delete
            if (GUI.Button(buttonRect, "X", EditorStyle.ButtonStyle))
                RemoveItem(index, itemMenu.Property);
        }

        protected int FindClosedBar(float mouseY, int selected, out float yPos)
        {
            float bestPoint = float.MaxValue;
            int bestIndex = 0;
            yPos = 0;

            float dist = 0;
            for (int i = 0; i < _menuItems.Count; i++)
            {
                Rect r = _menuItems[i].BoxRect;
                dist = Mathf.Abs(r.y - mouseY);

                if (selected + 1 == i)
                    continue;

                if (dist < bestPoint)
                {
                    bestPoint = dist;
                    yPos = r.y;
                    bestIndex = (selected < i) ? i - 1 : i;
                }
            }
            dist = Mathf.Abs(_bottomBarY - mouseY);
            if (dist < bestPoint && _menuItems.Count - 1 != selected)
            {
                yPos = _bottomBarY;
                bestIndex = _menuItems.Count - 1;
                bestPoint = dist;
            }

            return bestIndex;
        }


        #region Item_Options
        //----------------------Add and remove options---------------------------
        protected virtual GenericMenu RightClickMenu(int index, SerializedProperty property)
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Delete"), false, (b) => RemoveItem(index, property), "Delete");
            menu.AddItem(new GUIContent("Duplicate"), false, (b) => DuplicateItem(index, property), "Duplicate");
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("Collaps all"), false, (b) => CollapsAll(), "Collaps all");
            menu.ShowAsContext();
            return menu;
        }

        public virtual void AddItem()
        {
            if (_isFactory)
            {
                Type selectedType = _menuTypes[_selectedMenuType];
                var item = ScriptableObject.CreateInstance(selectedType);
                item.name = SubClassesHelper.GetReadableTypeName(selectedType);
                item.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;

                _serializedProperty.arraySize += 1;

                AssetDatabase.AddObjectToAsset(item, _serializedProperty.serializedObject.targetObject);
                AssetDatabase.SaveAssets();

                SerializedProperty arrayProperty = _serializedProperty.GetArrayElementAtIndex(_serializedProperty.arraySize - 1);
                arrayProperty.objectReferenceValue = item;
                arrayProperty.isExpanded = false;
                AssetDatabase.Refresh();
                _menuItems.Add(CreateMenuItem(arrayProperty));

                _serializedProperty.serializedObject.ApplyModifiedProperties();
            }
            else
            {
                _serializedProperty.arraySize += 1;
                //var obj = Activator.CreateInstance(_arrayType) as UnityEngine.Object;
                SerializedProperty arrayProperty = _serializedProperty.GetArrayElementAtIndex(_serializedProperty.arraySize - 1);
                //arrayProperty.objectReferenceValue = obj;
                _menuItems.Add(CreateMenuItem(arrayProperty));
            }
        }

        protected virtual void RemoveItem(int index, SerializedProperty property)
        {
            UnityEngine.Object obj = null;
            if (_isFactory)
            {
                DestroyImmediate(_menuItems[index].ItemEditor);
                obj = property.objectReferenceValue;
                property.objectReferenceValue = null;
            }

            _menuItems.RemoveAt(index);

            _serializedProperty.DeleteArrayElementAtIndex(index);
            _serializedProperty.serializedObject.ApplyModifiedProperties();

            if (obj != null)
            {
                DestroyImmediate(obj, true);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            CreateMenuItems();
        }

        protected virtual void MoveItem(int srcIndex, int dstIndex)
        {
            SerializedProperty srcProperty = _serializedProperty.GetArrayElementAtIndex(srcIndex);
            SerializedProperty dstProperty = _serializedProperty.GetArrayElementAtIndex(dstIndex);
            bool srcExpanded = srcProperty.isExpanded;
            bool dstExpanded = dstProperty.isExpanded;

            srcProperty.isExpanded = dstExpanded;
            dstProperty.isExpanded = srcExpanded;

            _serializedProperty.MoveArrayElement(srcIndex, dstIndex);
            _serializedProperty.serializedObject.ApplyModifiedProperties();
            CreateMenuItems();
        }

        protected virtual void DuplicateItem(int index, SerializedProperty property)
        {
            if (property == null)
                return;

            if (_isFactory)
            {
                var obj = property.objectReferenceValue;
                if (obj != null)
                {
                    var item = ScriptableObject.Instantiate(property.objectReferenceValue);
                    item.name = SubClassesHelper.GetReadableTypeName(obj.GetType());
                    item.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;

                    _serializedProperty.arraySize += 1;

                    AssetDatabase.AddObjectToAsset(item, _serializedProperty.serializedObject.targetObject);
                    AssetDatabase.SaveAssets();

                    SerializedProperty arrayProperty = _serializedProperty.GetArrayElementAtIndex(_serializedProperty.arraySize - 1);
                    arrayProperty.objectReferenceValue = item;
                    AssetDatabase.Refresh();
                    _menuItems.Add(CreateMenuItem(arrayProperty));
                    _serializedProperty.serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                property.DuplicateCommand();
                _serializedProperty.serializedObject.ApplyModifiedProperties();
                CreateMenuItems();
            }
        }

        public virtual void CollapsAll()
        {
            int count = _serializedProperty.arraySize;
            for (int x = 0; x < count; x++)
                _serializedProperty.GetArrayElementAtIndex(x).isExpanded = false;
        }

        #endregion
    }
}