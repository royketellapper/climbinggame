﻿//==============================================================================
//	PropertyDrawer for ResourceProperty
//
//	Created by Roy Ketellapper
//  Version v0.1
//	© 2016, Vasco Games, Inc.  All Rights Reserved
//==============================================================================
using System;
using UnityEditor;
using UnityEngine;
using RoyFramework.Utility;

namespace VascoGamesEditor.Utility
{
    [CustomPropertyDrawer(typeof(ResourcePropertyBase), true), CanEditMultipleObjects]
    public class ResourcePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Color color = GUI.color;
            GUI.color = Color.green;
            GUI.enabled = !Application.isPlaying;
            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            SerializedProperty propertyPath = property.FindPropertyRelative("_path");
            string path = propertyPath.stringValue;

            SerializedProperty propertyType = property.FindPropertyRelative("_typeName");
            Type type = Type.GetType(propertyType.stringValue, false);

            UnityEngine.Object _propertyObj = (string.IsNullOrEmpty(path)) ? null : Resources.Load(path);    
            UnityEngine.Object tempObj = _propertyObj;
            _propertyObj = EditorGUI.ObjectField(position, _propertyObj, type, true);

            if (GUI.changed)
            {
                try
                {
                    string[] assetPath = AssetDatabase.GetAssetPath(_propertyObj).Split(new string[] { "Resources/" }, StringSplitOptions.None);

                    if (_propertyObj != null && assetPath.Length <= 1)
                        EditorUtility.DisplayDialog("Error", _propertyObj.name +" is not in the resources folder.", "Ok");
                    else
                        propertyPath.stringValue = assetPath[1].Split('.')[0];
                }
                catch
                {
                    propertyPath.stringValue = String.Empty;
                }

                property.serializedObject.ApplyModifiedProperties();
            }

            EditorGUI.EndProperty();

            //Reset property
            GUI.enabled = true;
            GUI.color = color;
        }
    }
}