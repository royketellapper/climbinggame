﻿using RoyFramework;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using RoyFramework.Tools;

namespace VascoGamesEditor.Tools
{
    [CustomPropertyDrawer(typeof(TypePopupAttribute))]
    public class TypePopupProperty : PropertyDrawer
    {
        protected TypePopupAttribute _typePopupAttribute { get { return (TypePopupAttribute)attribute; } }
        protected List<string> _typeNames;
        protected List<Type> _types;

        protected virtual void LoadTypes()
        {
            _types = SubClassesHelper.GetSubclasses(_typePopupAttribute.Type);
            _typeNames = SubClassesHelper.GetTypeNames(_types);
            _typeNames.Insert(0, "------------");
            _types.Insert(0, null);
        }

        protected virtual int GetTypeIndex(string type)
        {
            int index = 0;
            for (int i = 0; i < _types.Count; i++)
            {
                if (_types[i] != null && _types[i].ToString() == type)
                    index = i;
            }
            return index;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (_types == null)
                LoadTypes();

            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            int index = EditorGUI.Popup(position, GetTypeIndex(property.stringValue), _typeNames.ToArray());

            if (_types != null)
            {
                string typeString = "";

                if (_types[index] != null)
                    typeString = _types[index].ToString();

                property.stringValue = typeString;
            }

            EditorGUI.EndProperty();
        }
    }
}



