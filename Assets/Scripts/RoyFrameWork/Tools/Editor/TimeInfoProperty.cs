﻿using UnityEngine;
using UnityEditor;
using RoyFramework.TimeHelper;

namespace VascoGamesEditor.TimeHelper
{
    [CustomPropertyDrawer(typeof(DisplayTimeAttribute))]
    public class TimeInfoProperty : PropertyDrawer
    {
        protected DisplayTimeAttribute displayTimeAttribute { get { return (DisplayTimeAttribute)attribute; } }
        protected string[] time = new string[60];
        protected string[] hours = new string[25];
        protected int totalTimeOptions;

        public TimeInfoProperty()
        {
            for (int i = 0; i < time.Length; i++)
                time[i] = i.ToString();

            for (int i = 0; i < hours.Length; i++)
                hours[i] = i.ToString();           
        }

        protected int GetTotalTimeOptions()
        {
            int total = 0;
            total += (displayTimeAttribute.ShowDays) ? 1 : 0;
            total += (displayTimeAttribute.ShowHours) ? 1 : 0;
            total += (displayTimeAttribute.ShowMinutes) ? 1 : 0;
            total += (displayTimeAttribute.ShowSeconds) ? 1 : 0;
            return total;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            totalTimeOptions = GetTotalTimeOptions();
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            int count = 0;
            int index = 0;
            if (displayTimeAttribute.ShowDays)
            {
                index = property.FindPropertyRelative("Days").intValue;
                index = DrawIntField(position, count, totalTimeOptions, index, "Days", 37);
                property.FindPropertyRelative("Days").intValue = index;
                count++;          
            }

            if (displayTimeAttribute.ShowHours)
            {
                index = property.FindPropertyRelative("Hours").intValue;
                index = DrawPopup(position, count, totalTimeOptions, index, "Hours", 37, hours);
                property.FindPropertyRelative("Hours").intValue = index;
                count++;
            }

            if (displayTimeAttribute.ShowMinutes)
            {
                index = property.FindPropertyRelative("Minutes").intValue;
                index = DrawPopup(position, count, totalTimeOptions, index, "Minutes", 47, time);
                property.FindPropertyRelative("Minutes").intValue = index;
                count++;
            }

            if (displayTimeAttribute.ShowSeconds)
            {
                index = property.FindPropertyRelative("Seconds").intValue;
                index = DrawPopup(position, count, totalTimeOptions, index, "Seconds", 50, time);
                property.FindPropertyRelative("Seconds").intValue = index;
                count++;
            }
        }

        protected int DrawPopup(Rect position, int posIndex, int maxPosIndex, int selectedIndex, string titel, int textOffset, string[] displayedOptions)
        {
            var rect = new Rect(position.x + ((position.width / maxPosIndex) * posIndex), position.y, position.width / maxPosIndex, position.height);
            EditorGUI.LabelField(rect, titel);
            rect = new Rect(rect.x + textOffset, rect.y, rect.width - textOffset, rect.height);
            return EditorGUI.Popup(rect, selectedIndex, displayedOptions);
        }

        protected int DrawIntField(Rect position, int posIndex, int maxPosIndex, int value, string titel, int textOffset)
        {
            var rect = new Rect(position.x + ((position.width / maxPosIndex) * posIndex), position.y, position.width / maxPosIndex, position.height);
            EditorGUI.LabelField(rect, titel);
            rect = new Rect(rect.x + textOffset, rect.y, rect.width - textOffset, rect.height);
            return EditorGUI.IntField(rect, value);
        }
    }
}
