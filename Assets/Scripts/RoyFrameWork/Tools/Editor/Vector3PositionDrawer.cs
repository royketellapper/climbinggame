﻿using UnityEditor;
using UnityEngine;
using RoyFramework.Tools;

namespace VascoGamesEditor.Tools
{
    [CustomPropertyDrawer(typeof(Vector3PositionAttribute))]
    public class Vector3PositionDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (GUI.Button(new Rect(position.x, position.y + 14, 20, 20), "P"))
            {
                MonoBehaviour mono = (MonoBehaviour)property.serializedObject.targetObject;

                if (property.propertyType == SerializedPropertyType.Vector3)
                    property.vector3Value = mono.transform.localPosition;

            }

            EditorGUI.BeginProperty(position, label, property);
            var indent = EditorGUI.indentLevel;
            EditorGUI.PropertyField(new Rect(position.x + 10, position.y, position.width - 10, position.height), property);
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
        {
            float extraHeight = 20.0f;
            return base.GetPropertyHeight(prop, label) + extraHeight;
        }
    }
}
