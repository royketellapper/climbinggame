﻿using System;
using System.Collections.Generic;
using UnityEngine;
using RoyFramework.States.Api;

namespace RoyFramework.States.Impl.Unity
{
    public abstract class StateMachineBase : MonoBehaviour, IStateMachine
    {
        protected List<IState> _states = new List<IState>();
        protected IState _currentState;

        public virtual void AddState(IState state)
        {
            _states.Add(state);
        }

        public virtual int Count()
        {
            return _states.Count;
        }

        public virtual IState[] GetAllStates()
        {
            return _states.ToArray();
        }

        public virtual IState GetCurrentState()
        {
            return _currentState;
        }

        public virtual IEnumerator<IState> GetEnumerator()
        {
            return _states.GetEnumerator();
        }

        public virtual IState GetState(int index)
        {
            if (index < 0 || index > _states.Count - 1)
                throw new ArgumentOutOfRangeException("index");

            return _states[index];
        }

        public virtual void RemoveState(IState state)
        {
            _states.Remove(state);
        }

        public virtual void ExecuteCurrentState()
        {
            if(_currentState != null)
                _currentState.Execute();
        }

        public virtual void SwitchState(IState state)
        {
            if (_currentState != null)
                _currentState.Exit();

            if (state != null)
            {
                _currentState = state;
                _currentState.Enter();
            }
        }

        public virtual void SwitchState(int index)
        {
            var newState = GetState(index);
            SwitchState(newState);
        }
    }
}