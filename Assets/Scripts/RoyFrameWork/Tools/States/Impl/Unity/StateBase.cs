﻿using UnityEngine;
using RoyFramework.States.Api;

namespace RoyFramework.States.Impl.Unity
{
    public abstract class StateBase : MonoBehaviour, IState
    {
        public virtual void Enter() { }
        public virtual void Execute() { }
        public virtual void Exit() { }
    }
}