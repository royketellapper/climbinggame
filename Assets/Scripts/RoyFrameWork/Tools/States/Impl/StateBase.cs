﻿using RoyFramework.States.Api;

namespace RoyFramework.States.Impl
{
    public abstract class StateBase : IState
    {
        public virtual void Enter() { }
        public virtual void Execute() { }
        public virtual void Exit() { }
    }
}