﻿using RoyFramework.States.Api;
using System.Collections.Generic;
using System;

namespace RoyFramework.States.Impl
{
    public abstract class StateMachineBase : IStateMachine
    {
        protected List<IState> _states;
        protected IState _currentState;

        public StateMachineBase()
        {
            _states = new List<IState>();
        }

        public virtual void AddState(IState state)
        {
            _states.Add(state);
        }

        public virtual void RemoveState(IState state)
        {
            _states.Remove(state);
        }

        public virtual IEnumerator<IState> GetEnumerator()
        {
            return _states.GetEnumerator();
        }

        public virtual int Count()
        {
            return _states.Count;
        }

        public virtual IState GetState(int index)
        {
            if (index < 0 || index > _states.Count - 1)
                throw new ArgumentOutOfRangeException("index");

            return _states[index];
        }

        public virtual IState GetCurrentState()
        {
            return _currentState;
        }

        public virtual IState[] GetAllStates()
        {
            return _states.ToArray();
        }

        public virtual void ExecuteCurrentState()
        {
            if (_currentState != null)
                _currentState.Execute();
        }

        public virtual void SwitchState(int index)
        {
            var newState = GetState(index);
            SwitchState(newState);
        }

        public virtual void SwitchState(IState state)
        {
            if (_currentState != null)
                _currentState.Exit();

            _currentState = state;
            _currentState.Enter();
        }
    }
}