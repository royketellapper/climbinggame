﻿using System.Collections.Generic;

namespace RoyFramework.States.Api
{
    public interface IStateMachine
    {
        void AddState(IState state);
        void RemoveState(IState state);
        IEnumerator<IState> GetEnumerator();
        int Count();
        IState GetState(int index);
        IState GetCurrentState();
        IState[] GetAllStates();
        void SwitchState(int index);
        void SwitchState(IState state);
        void ExecuteCurrentState();
    }
}