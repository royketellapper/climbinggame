﻿namespace RoyFramework.States.Api
{
    public interface IState
    {
        void Enter();
        void Execute();
        void Exit();
    }
}