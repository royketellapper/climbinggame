﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace RoyFramework.Utility
{
    public static class SecureTimeChecker
    {
        static string[] servers = new string[] {
                    "time.nist.gov",
                    "time-b.nist.gov",
                    "time-a.nist.gov",
                    "nist1-ny.ustiming.org",
                    "nist1-chi.ustiming.org",
                    "ntp-nist.ldsbc.edu",
                    "nist1-la.ustiming.org" 
                    };

        public static DateTime GetNISTDate()
        {
            DateTime date = DateTime.MinValue;
            string serverResponse = string.Empty;

            for (int i = 0; i < 7; i++)
            {
                try
                {
                    StreamReader reader = new StreamReader(new System.Net.Sockets.TcpClient(servers[i], 13).GetStream());
                    serverResponse = reader.ReadToEnd();
                    Debug.Log(serverResponse);
                    reader.Close();
                    // Check to see that the signature is there
                    if (serverResponse.Length > 47 && serverResponse.Substring(38, 9).Equals("UTC(NIST)"))
                    {
                        // Parse the date
                        int jd = int.Parse(serverResponse.Substring(1, 5));
                        int yr = int.Parse(serverResponse.Substring(7, 2));
                        int mo = int.Parse(serverResponse.Substring(10, 2));
                        int dy = int.Parse(serverResponse.Substring(13, 2));
                        int hr = int.Parse(serverResponse.Substring(16, 2));
                        int mm = int.Parse(serverResponse.Substring(19, 2));
                        int sc = int.Parse(serverResponse.Substring(22, 2));
                        if (jd > 51544)
                            yr += 2000;
                        else
                            yr += 1999;
                        date = new DateTime(yr, mo, dy, hr, mm, sc);
                        Debug.Log(date);
                        break;
                    }
                }
                catch
                {
                }
            }
            return date;
        }
    }
}