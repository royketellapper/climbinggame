﻿using System;
using System.Reflection;

namespace RoyFramework
{
    public class StateMachine<T> where T : struct, IConvertible
    {
        public T oldState { get; private set; }
        public T currentState { get; private set; }
        public T newState { get; private set; }

        //private delegate void Action();
        private Action[] StartState;
        private Action[] UpdateState;
        private Action[] EndState;
        private object stateClass;
        private bool isInitialized;

        public StateMachine(object className)
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("T must be an enumerated type");

            stateClass = className;
            CreateDelegate();
        }

        ~StateMachine()
        {
            for (int x = 0; x < StartState.Length; x++)
            {
                StartState[x] = null;
                UpdateState[x] = null;
                EndState[x] = null;
            }
        }

        public void SetState(T state, bool forceUpdate = false)
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("T must be an enumerated type");

            int newStateNum = (int)(object)state;

            if (((int)(object)currentState == newStateNum || (int)(object)newState == newStateNum) && isInitialized)
                return;

            int oldStateNum = (int)(object)currentState;

            newState = state;
            if (EndState[oldStateNum] != null && isInitialized)
                EndState[oldStateNum]();

            isInitialized = true;

            oldState = currentState;
            currentState = state;
            if (StartState[newStateNum] != null)
                StartState[newStateNum]();

            if (forceUpdate && UpdateState[newStateNum] != null)
                UpdateState[newStateNum]();
        }

        private void CreateDelegate()
        {
            string[] stateNames = Enum.GetNames(typeof(T));
            Array.Resize<Action>(ref StartState, stateNames.Length);
            Array.Resize<Action>(ref UpdateState, stateNames.Length);
            Array.Resize<Action>(ref EndState, stateNames.Length);

            for (int x = 0; x < stateNames.Length; x++)
            {
                StartState[x] = ConfigureDelegate("Begin_" + stateNames[x]);
                UpdateState[x] = ConfigureDelegate("Update_" + stateNames[x]);
                EndState[x] = ConfigureDelegate("End_" + stateNames[x]);
            }
        }

        private Action ConfigureDelegate(string functionName)
        {
            MethodInfo methodInfo = stateClass.GetType().GetMethod(functionName,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.InvokeMethod);

            if (methodInfo != null)
                return (Action)Delegate.CreateDelegate(typeof(Action), stateClass, functionName);

            return null;
        }

        public void Update()
        {
            int stateNum = (int)(object)currentState;

            if (UpdateState[stateNum] != null)
                UpdateState[stateNum]();
        }
    }
}
