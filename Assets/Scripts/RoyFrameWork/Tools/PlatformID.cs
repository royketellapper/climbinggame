﻿using System;

namespace RoyFramework
{
    [Serializable]
    public class PlatformID
    {
        public enum Platform
        {
            Android,
            Iphone,
            windowsPhone
        }

        [Serializable]
        public class ID
        {
            public String Id;
            public Platform Platform;
        }

        public string GlobalId;
        public ID[] platformID;

        public string GetId()
        {
            Platform platform;
            if (!CurrentPlatform(out platform))
            {
                ID id = Array.Find<ID>(platformID, p => p.Platform == platform);

                if (id != null)
                    return id.Id;
            }
            return GlobalId;
        }

        private bool CurrentPlatform(out Platform platform)
        {
#if UNITY_IPHONE
            platform = Platform.Iphone;
            return true;
#elif UNITY_ANDROID
            platform = Platform.Android;
            return true;
#elif UNITY_WP8 || UNITY_WP8_1
            platform = Platform.windowsPhone;
            return true;
#else
            platform = Platform.Android;
            return false;
#endif
        }
    }
}
