﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace RoyFramework.Easing
{
    class Linear : Ease
    {
        public static double EaseNone(double t, double b, double c, double d)
        {
            return c * t / d + b;
        }
        public static double EaseIn(double t, double b, double c, double d)
        {
            return c * t / d + b;
        }
        public static double EaseOut(double t, double b, double c, double d)
        {
            return c * t / d + b;
        }
        public static double EaseInOut(double t, double b, double c, double d)
        {
            return c * t / d + b;
        }

        public static float EaseNone(float t, float b, float c, float d)
        {
            return c * t / d + b;
        }
        public static float EaseIn(float t, float b, float c, float d)
        {
            return c * t / d + b;
        }
        public static float EaseOut(float t, float b, float c, float d)
        {
            return c * t / d + b;
        }
        public static float EaseInOut(float t, float b, float c, float d)
        {
            return c * t / d + b;
        }
    }
}
