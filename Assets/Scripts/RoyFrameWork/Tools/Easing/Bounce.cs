﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace RoyFramework.Easing
{
    class Bounce : Ease
    {
        public static double EaseOut(double t, double b, double c, double d)
        {
            if ((t /= d) < (1 / 2.75))
            {
                return c * (7.5625 * t * t) + b;
            }
            else if (t < (2 / 2.75))
            {
                return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
            }
            else if (t < (2.5 / 2.75))
            {
                return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
            }
            else
            {
                return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
            }
        }
        public static double EaseIn(double t, double b, double c, double d)
        {
            return c - EaseOut(d - t, 0, c, d) + b;
        }
        public static double EaseInOut(double t, double b, double c, double d)
        {
            if (t < d / 2) return EaseIn(t * 2, 0, c, d) * .5 + b;
            else return EaseOut(t * 2 - d, 0, c, d) * .5 + c * .5 + b;
        }

        public static float EaseOut(float t, float b, float c, float d)
        {
            if ((t /= d) < (1f / 2.75f))
            {
                return c * (7.5625f * t * t) + b;
            }
            else if (t < (2f / 2.75f))
            {
                return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f) + b;
            }
            else if (t < (2.5 / 2.75))
            {
                return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f) + b;
            }
            else
            {
                return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f) + b;
            }
        }
        public static float EaseIn(float t, float b, float c, float d)
        {
            return c - EaseOut(d - t, 0, c, d) + b;
        }
        public static float EaseInOut(float t, float b, float c, float d)
        {
            if (t < d / 2f) return EaseIn(t * 2f, 0f, c, d) * .5f + b;
            else return EaseOut(t * 2f - d, 0f, c, d) * .5f + c * .5f + b;
        }
    }
}