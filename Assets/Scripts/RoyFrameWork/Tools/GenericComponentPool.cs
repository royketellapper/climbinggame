﻿using System.Collections.Generic;
using UnityEngine;

namespace RoyFramework.Tools
{
    public abstract class GenericComponentPool<T> : MonoBehaviour
    {
        public class PoolObject
        {
            public PoolObject(GameObject gameObj, T component)
            {
                GameObject = gameObj;
                Component = component;
            } 
            public GameObject GameObject;
            public T Component;
        }

        [SerializeField] private GameObject pooledObjectPrefab;
        [SerializeField] private int spawnAmount;
        private List<PoolObject> availablePool, activePool;
        private int availableCount, activeCount;

        protected virtual void Awake()
        {
            availablePool = new List<PoolObject>(spawnAmount);
            activePool = new List<PoolObject>(spawnAmount);
            for (int i = 0; i < spawnAmount; i++)
            {
#if UNITY_EDITOR
                GameObject obj = Instantiate(pooledObjectPrefab, transform);
                obj.name = typeof(T).ToString() + " " + i;
#else
                GameObject obj = Instantiate(pooledObjectPrefab);
#endif
                PoolObject poolObject = new PoolObject(obj, obj.GetComponent<T>());
                availablePool.Add(poolObject);
                obj.SetActive(false);
            }
            availableCount = spawnAmount;
            activeCount = 0;
        }

        public virtual PoolObject RequestObjectFromPool()
        {
            if (availableCount == 0)
                return null;

            if (availableCount < 0)
            {
                Debug.LogError("availableCount should never be negative!");
                return null;
            }

            activePool.Add(availablePool[0]);
            availablePool.RemoveAt(0);
            activeCount++;
            availableCount--;
            return activePool[activeCount - 1];
        }

        public virtual void ReturnObjectToPool(GameObject poolObject)
        {
            int index = activePool.FindIndex(x => x.GameObject == poolObject);
            if (index == -1)
            {
                Debug.LogError("Could not find returned pool object in active pool.");
                return;
            }

            availablePool.Add(activePool[index]);
            activePool[index].GameObject.SetActive(false);
            activePool.RemoveAt(index);
            activeCount--;
            availableCount++;
        }
    }
}