﻿//==============================================================================
//	Saves the object resource path to a string
//
//	Created by Roy Ketellapper
//  Version v0.2
//	© 2017, Vasco Games, Inc.  All Rights Reserved
//==============================================================================
using UnityEngine;

namespace RoyFramework.Utility
{
    [System.Serializable]
    public class ResourceProperty<T> : ResourcePropertyBase, ISerializationCallbackReceiver  where T : UnityEngine.Object
    {
        public static implicit operator T(ResourceProperty<T> value)
        {
            return value.Value;
        }

        public T Value
        {
            get
            {
                if (_value == null)
                    _value = Load();

                return _value;
            }
            set
            {
                if (_value != value)
                    UnLoad();

                _value = value;
            }
        }

        [System.NonSerialized]
        protected T _value;

        public T Load()
        {
            if (!string.IsNullOrEmpty(Path))
                return Resources.Load(Path, typeof(T)) as T;

            return null;
        }

        public void UnLoad()
        {
            if (_value != null)
                Resources.UnloadAsset(_value);

            _value = null;
        }

        public ResourceProperty()
        {
            _typeName = typeof(T).AssemblyQualifiedName;
        }

        public virtual void OnBeforeSerialize(){}

        public virtual void OnAfterDeserialize()
        {
            _typeName = typeof(T).AssemblyQualifiedName;
        }
    }
}
