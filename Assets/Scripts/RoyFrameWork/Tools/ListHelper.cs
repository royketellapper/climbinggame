﻿using UnityEngine;
using System.Collections.Generic;

namespace RoyFramework.Tools
{
    public static class ListHelper
    {
        public static void MoveElement<T>(this IList<T> list, int fromIndex, int toIndex)
        {
            fromIndex = Mathf.Clamp(fromIndex, 0, list.Count - 1);
            toIndex = Mathf.Clamp(toIndex, 0, list.Count - 1);

            if (fromIndex == toIndex)
                return;

            var element = list[fromIndex];

            if (fromIndex > toIndex)
            {
                list.RemoveAt(fromIndex);
                list.Insert(toIndex, element);
            }
            else
            {
                list.Insert(toIndex + 1, element);
                list.RemoveAt(fromIndex);
            }
        }

        public static void MoveUp<T>(this IList<T> list, int index)
        {
            MoveElement<T>(list, index, index + 1);
        }

        public static void MoveDown<T>(this IList<T> list, int index)
        {
            MoveElement<T>(list, index, index - 1);
        }
    }
}