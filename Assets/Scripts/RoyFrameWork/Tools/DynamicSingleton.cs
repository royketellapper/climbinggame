﻿using System.Collections.Generic;
using UnityEngine;

namespace RoyFramework.Tools
{
    public class DynamicSingleton<S> : MonoBehaviour where S : DynamicSingleton<S>
    {
        public string Id;
        protected static List<S> _instances = new List<S>();

        public static T GetInstance<T>(string id) where T : DynamicSingleton<S>
        {
            var singleton = _instances.Find(a => a is T && a.Id == id);

            if (singleton != null)
                return singleton as T;

            return null;
        }

        public static List<S> GetInstance<T>() where T : DynamicSingleton<S>
        {
            var singleton = _instances.FindAll(a => a is T);

            if (singleton != null)
                return singleton;

            return null;
        }

        public static DynamicSingleton<S> GetInstance(string id)
        {
            return _instances.Find(a => a is S && a.Id == id);
        }

        protected virtual void Awake()
        {
            _instances.Add(this as S);
        }

        protected virtual void OnDestroy()
        {
            _instances.Remove(this as S);
        }
    }
}