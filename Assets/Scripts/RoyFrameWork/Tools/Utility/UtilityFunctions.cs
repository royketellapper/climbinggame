﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using System.Linq;

namespace RoyFramework.Utility
{
    public enum TimeFormat
    {
        //H = hours
        //M = minutes
        //S = seconds
        //Ms = miliseconds

        None,
        SSMs,
        SSMsMs,
        SSMsMsMs,
        MMSS,
        MMSSMs,
        MMSSMsMs,
        HHMM,
        HHMMSS,
        HHMMSSMs,
        HHMMSSMsMs
    }

    public static class UtilityFunctions
    {
        public static bool PointInBox(Matrix4x4 mat, Vector3 size, Vector3 point)
        {
            Vector3 divideSize = size / 2;
            Vector3 local = mat.inverse.MultiplyPoint3x4(point);

            return (InRange(local.x, -divideSize.x, divideSize.x) &&
                InRange(local.y, -divideSize.y, divideSize.y) &&
                InRange(local.z, -divideSize.z, divideSize.z));

        }


        public static bool PointInPlane(Matrix4x4 mat, Vector2 size, Vector3 point)
        {
            Vector3 divideSize = size / 2;
            Vector3 local = mat.inverse.MultiplyPoint3x4(point);

            return (InRange(local.x, -divideSize.x, divideSize.x) &&
                InRange(local.z, -divideSize.y, divideSize.y));

        }

        public static bool InRange(float point, float min, float max)
        {
            return point > min && point < max;
        }

        public static bool InRange(int point, int min, int max)
        {
            return point > min && point < max;
        }

        public static Vector3 ClampPointInBox(Transform trans, Vector3 size, Vector3 loc)
        {
            Vector3 divideSize = size / 2;
            Vector3 local = trans.InverseTransformPoint(loc);
            local.x = Mathf.Clamp(local.x, -divideSize.x, divideSize.x);
            local.y = Mathf.Clamp(local.y, -divideSize.y, divideSize.y);
            local.z = Mathf.Clamp(local.z, -divideSize.z, divideSize.z);
            return trans.TransformPoint(local);
        }

        public static Vector3 ClampPointInPlane(Transform trans, Vector2 size, Vector3 loc)
        {
            Vector3 divideSize = size / 2;

            Vector3 local = trans.InverseTransformPoint(loc);
            local.x = Mathf.Clamp(local.x, -divideSize.x, divideSize.x);
            local.z = Mathf.Clamp(local.z, -divideSize.y, divideSize.y);
            return trans.TransformPoint(local);
        }

        //Set a sprite in material
        public static void SetSprite(this Material mat, Sprite sprite)
        {
            SetSprite(mat, "_MainTex", sprite);
        }

        //Set a sprite in material
        public static void SetSprite(this Material mat, string propertyName, Sprite sprite)
        {
            mat.SetTexture(propertyName, sprite.texture);
            mat.SetTextureOffset(propertyName, new Vector2(sprite.textureRect.x / sprite.texture.width, sprite.textureRect.y / sprite.texture.height));
            mat.SetTextureScale(propertyName, new Vector2(sprite.textureRect.width / sprite.texture.width, sprite.textureRect.height / sprite.texture.height));
        }

        public static string ConvertToMoney(this int money)
        {
            return money.ToString("#,0", CultureInfo.InvariantCulture);
        }

        public static string FormatToTime(this float input)
        {
            int seconds;
            int minutes;
            int milliSeconds;
            string r;

            if (input < 0)
                input = 0;

            minutes = Mathf.FloorToInt(input / 60);
            seconds = Mathf.FloorToInt(input - minutes * 60);
            milliSeconds = Mathf.FloorToInt((input * 60) % 60);

            r = (minutes < 10) ? "0" + minutes.ToString() : minutes.ToString();
            r += ":";
            r += (seconds < 10) ? "0" + seconds.ToString() : seconds.ToString();
            r += ":";
            r += (milliSeconds < 10) ? "0" + milliSeconds.ToString() : milliSeconds.ToString();

            return r;
        }

        public static string FormatToTime(this float input, TimeFormat format)
        {
            switch (format)
            {
                case TimeFormat.SSMs:

                    return string.Format("{0:00}:{1:0}",
                     Mathf.Floor(input) % 60,//seconds
                     Mathf.Floor((input * 10) % 10));//miliseconds

                case TimeFormat.SSMsMs:

                    return string.Format("{0:00}:{1:00}",
                     Mathf.Floor(input) % 60,//seconds
                     Mathf.Floor((input * 100) % 100));//miliseconds

                case TimeFormat.SSMsMsMs:

                    return string.Format("{0:00}:{1:000}",
                     Mathf.Floor(input) % 60,//seconds
                     Mathf.Floor((input * 1000) % 1000));//miliseconds

                case TimeFormat.MMSS:

                    return string.Format("{0:00}:{1:00}",
                     Mathf.Floor(input / 60),//minutes
                     Mathf.Floor(input) % 60);//seconds

                case TimeFormat.MMSSMs:

                    return string.Format("{0:00}:{1:00}:{2:0}",
                     Mathf.Floor(input / 60),//minutes
                     Mathf.Floor(input) % 60,//seconds
                     Mathf.Floor((input * 10) % 10));//miliseconds

                case TimeFormat.MMSSMsMs:

                    return string.Format("{0:00}:{1:00}:{2:00}",
                     Mathf.Floor(input / 60),//minutes
                     Mathf.Floor(input) % 60,//seconds
                     Mathf.Floor((input * 100) % 100));//miliseconds

                case TimeFormat.HHMM:

                    /*return string.Format("{0:00}:{1:00}",
                     Mathf.Floor(input) / 3600,//hours
                     Mathf.Floor((input % 3600) / 60));//minutes*/

                    return string.Format("{0:00}:{1:00}",
                        Mathf.Floor(input / 3600),//hours
                        Mathf.Floor((input / 60) % 60));//minutes

                case TimeFormat.HHMMSS:

                    /*return string.Format("{0:00}:{1:00}:{2:00}",
                    Mathf.Floor(input / 3600),//hours
                    Mathf.Floor((input) % 3600) % 60,//minutes
                    Mathf.Floor(((input) % 3600) % 60) / 60);//seconds*/

                    return string.Format("{0:00}:{1:00}:{2:00}",
                         Mathf.Floor(input / 3600),//hours
                         Mathf.Floor((input / 60) % 60),//minutes
                         Mathf.Floor(input % 60));//seconds

                case TimeFormat.HHMMSSMs:

                    /* return string.Format("{0:00}:{1:00}:{2:00}:{3:0}",
                     Mathf.Floor(input) / 3600,//hours
                     Mathf.Floor((input % 3600) % 60),//minutes
                     Mathf.Floor(((input % 3600) % 60) / 60),//seconds
                     Mathf.Floor((input * 10) % 10));//miliseconds*/

                    return string.Format("{0:00}:{1:00}:{2:00:{3:0}}",
                            Mathf.Floor(input / 3600),//hours
                            Mathf.Floor((input / 60) % 60),//minutes
                            Mathf.Floor(input % 60),//seconds
                            Mathf.Floor((input * 10) % 10));//miliseconds

                case TimeFormat.HHMMSSMsMs:

                    /* return string.Format("{0:00}:{1:00}:{2:00}:{3:00}",
                     Mathf.Floor(input) / 3600,//hours
                     Mathf.Floor((input % 3600) % 60),//minutes
                     Mathf.Floor(((input % 3600) % 60) / 60),//seconds
                     Mathf.Floor((input * 100) % 100));//miliseconds */

                    return string.Format("{0:00}:{1:00}:{2:00}:{3:00}",
                            Mathf.Floor(input / 3600),//hours
                            Mathf.Floor((input / 60) % 60),//minutes
                            Mathf.Floor(input % 60),//seconds
                            Mathf.Floor((input * 100) % 100));//miliseconds            
            }

            return "error";

        }

        /// <summary>
        /// Will Not give Null Error if not exist
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        public static void SetSaveActive(this GameObject obj, bool value)
        {
            if (obj != null)
                obj.SetActive(value);
        }

        public static void ResetTransform(this Transform transform)
        {
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }

        public static void ResetLocalTransform(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }

        public static void ResetLocalRectTransform(this RectTransform rectTransform)
        {
            rectTransform.localScale = Vector3.one;
            rectTransform.localRotation = Quaternion.identity;
            rectTransform.localPosition = Vector3.one;
            rectTransform.sizeDelta = Vector2.zero;
            rectTransform.anchoredPosition = Vector2.zero;
        }

        public static T CopyComponent<T>(T original, GameObject destination) where T : Component
        {
            System.Type type = original.GetType();
            var dst = destination.GetComponent(type) as T;
            if (!dst) dst = destination.AddComponent(type) as T;
            var fields = type.GetFields();
            foreach (var field in fields)
            {
                if (field.IsStatic) continue;
                field.SetValue(dst, field.GetValue(original));
            }
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (!prop.CanWrite || !prop.CanWrite || prop.Name == "name") continue;
                prop.SetValue(dst, prop.GetValue(original, null), null);
            }
            return dst as T;
        }

        public static float AppliedForce(Collision collision)
        {
            float magnitude = (collision.impulse / Time.fixedDeltaTime).magnitude / (750.0f);
                     
            return magnitude;
        }

        public static int TwoToThePowerOf(this int value)
        {
            return Mathf.Max(1 << value - 1, 1);
        }

        public static Transform GetClosest(this Transform[] collection, Vector3 refPoint)
        {
            Transform closest = null;
            float dist = 0.0f;

            foreach (Transform t in collection)
            {
                float curDist = (refPoint - t.position).sqrMagnitude;

                if (!closest || curDist < dist)
                {
                    closest = t;
                    dist = curDist;
                }
            }

            return closest;
        }

        public static void GetClosest(this Transform[] collection, Vector3 refPoint, out Transform closest, out float distance)
        {
            closest = null;
            float dist = 0.0f;

            foreach (Transform t in collection)
            {
                float curDist = (refPoint - t.position).sqrMagnitude;

                if (!closest || curDist < dist)
                {
                    closest = t;
                    dist = curDist;
                }
            }

            distance = dist;          
        }

        public static int GetIndex<T>(this T[] collection, T item) where T : UnityEngine.Object
        {
            for (int i = 0; i < collection.Length; i++)
            {
                if (collection[i] == item)
                    return i;
            }

            return -1;
        }

        public static T[] Add<T>(this T[] collection, T item) 
        {
            List<T> list = new List<T>(collection);
            list.Add(item);

            //return list.ToArray();
            collection = list.ToArray();

            return collection;
        }

        public static T[] Remove<T>(this T[] collection, T item)
        {
            List<T> list = new List<T>(collection);
            list.Remove(item);

            collection = list.ToArray();

            return collection;
        }

        public static T[] RemoveAt<T>(this T[] collection, int item)
        {
            List<T> list = new List<T>(collection);
            list.RemoveAt(item);

            collection = list.ToArray();

            return collection;
        }

        public static bool Contains<T>(this T[] collection, T item)
        {
            List<T> list = new List<T>(collection);

            return list.Contains(item);
        }

       

        public static Vector3 GetClosest(this Vector3[] collection, Vector3 refPoint)
        {
            Vector3 closest = Vector3.zero;
            float dist = 0.0f;

            foreach (Vector3 t in collection)
            {
                float curDist = (refPoint - t).sqrMagnitude;

                if (closest == Vector3.zero || curDist < dist)
                {
                    closest = t;
                    dist = curDist;
                }
            }

            return closest;
        }

        public static string AddSpacesBeforeCapitals(this string value)
        {
            string newString = value;

            Char[] chars = value.ToCharArray();
            int addCounter = 0;

            //i == 1 skips the first capital
            for (int i = 1; i < chars.Length; i++)
            {
                if (Char.IsUpper(chars[i]))
                {
                    newString = newString.Insert(i + addCounter, " ");
                    addCounter++;
                }
            }

            return newString;
        }

        public static T GetAsset<T>(string path, string name = "") where T : UnityEngine.Object
        {
            T Asset = default(T);

            if (string.IsNullOrEmpty(name))
            {
                Asset = (T)Resources.Load(path);
            }
            else
            {
                //Fetching a mesh from an fbx group needs to be done like this
                //Same as child sprites

                string[] split = name.Split(new string[] { "/" }, StringSplitOptions.None);
                string splitName = /*split.Length > 0 ?*/ split[split.Length - 1] /*: name*/;

                foreach (T obj in Resources.LoadAll<T>(path))
                {
                    if (obj.name == splitName)
                    {
                        Asset = obj;
                        break;
                    }
                }
            }

            return Asset;
        }

        public static string ToHexadecimal(this Color c)
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}", ToByte(c.r), ToByte(c.g), ToByte(c.b));
        }

        private static byte ToByte(float f)
        {
            f = Mathf.Clamp01(f);
            return (byte)(f * 255);
        }      

        public static void CombineMesh(Mesh[] source, Texture2D[] mainTextures, Texture2D[] maskTextures, Matrix4x4[] positions, int atlasSize, out Mesh combinedMesh, out Texture2D mainAtlas, out Texture2D maskAtlas)
        {

 
            mainAtlas = new Texture2D(atlasSize, atlasSize, TextureFormat.RGB24, false);
            maskAtlas = new Texture2D(atlasSize, atlasSize, TextureFormat.RGB24, false);

            Rect[] TextureCoordinates = mainAtlas.PackTextures(mainTextures, 0, atlasSize, true);
            maskAtlas.PackTextures(maskTextures, 0, atlasSize, true);

            CombineInstance[] _CombineInstance = new CombineInstance[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                Mesh mesh = source[i];
                Vector2[] uvs = mesh.uv;

                for (int u = 0; u < uvs.Length; u++)
                {
                    uvs[u].x = uvs[u].x * TextureCoordinates[i].width + TextureCoordinates[i].xMin;
                    uvs[u].y = uvs[u].y * TextureCoordinates[i].height + TextureCoordinates[i].yMin;
                }

                // required to create a copy of shared Mesh, Otherwise it would modify the original mesh
                //mesh = (Mesh)Instantiate(mesh);
                _CombineInstance[i].mesh = mesh;
                _CombineInstance[i].transform = positions[i];

                mesh.uv = uvs;
            }

            combinedMesh = new Mesh();
            combinedMesh.name = "Combined Mesh";
            combinedMesh.CombineMeshes(_CombineInstance);
        }

        public static void SetLayerRecursively(GameObject go, int layer, int ignoreLayer = -1)
        {
            go.layer = layer;

            Transform[] children = go.GetComponentsInChildren<Transform>();
            for (int i = 0; i < children.Length; i++)
            {
                if (ignoreLayer > -1 && children[i].gameObject.layer == ignoreLayer)
                    continue;

                children[i].gameObject.layer = layer;
            }
        }

        //Draws just the box at where it is currently hitting.
        public static void DrawBoxCastOnHit(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Vector3 direction, float hitInfoDistance, Color color)
        {
            origin = CastCenterOnCollision(origin, direction, hitInfoDistance);
            DrawBox(origin, halfExtents, orientation, color);
        }

        //Draws the full box from start of cast to its end distance. Can also pass in hitInfoDistance instead of full distance
        public static void DrawBoxCastBox(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Vector3 direction, float distance, Color color)
        {
            direction.Normalize();
            Box bottomBox = new Box(origin, halfExtents, orientation);
            Box topBox = new Box(origin + (direction * distance), halfExtents, orientation);

            Debug.DrawLine(bottomBox.backBottomLeft, topBox.backBottomLeft, color);
            Debug.DrawLine(bottomBox.backBottomRight, topBox.backBottomRight, color);
            Debug.DrawLine(bottomBox.backTopLeft, topBox.backTopLeft, color);
            Debug.DrawLine(bottomBox.backTopRight, topBox.backTopRight, color);
            Debug.DrawLine(bottomBox.frontTopLeft, topBox.frontTopLeft, color);
            Debug.DrawLine(bottomBox.frontTopRight, topBox.frontTopRight, color);
            Debug.DrawLine(bottomBox.frontBottomLeft, topBox.frontBottomLeft, color);
            Debug.DrawLine(bottomBox.frontBottomRight, topBox.frontBottomRight, color);

            DrawBox(bottomBox, color);
            DrawBox(topBox, color);
        }

        public static void DrawBox(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Color color)
        {
            DrawBox(new Box(origin, halfExtents, orientation), color);
        }
        public static void DrawBox(Box box, Color color)
        {
            Debug.DrawLine(box.frontTopLeft, box.frontTopRight, color);
            Debug.DrawLine(box.frontTopRight, box.frontBottomRight, color);
            Debug.DrawLine(box.frontBottomRight, box.frontBottomLeft, color);
            Debug.DrawLine(box.frontBottomLeft, box.frontTopLeft, color);

            Debug.DrawLine(box.backTopLeft, box.backTopRight, color);
            Debug.DrawLine(box.backTopRight, box.backBottomRight, color);
            Debug.DrawLine(box.backBottomRight, box.backBottomLeft, color);
            Debug.DrawLine(box.backBottomLeft, box.backTopLeft, color);

            Debug.DrawLine(box.frontTopLeft, box.backTopLeft, color);
            Debug.DrawLine(box.frontTopRight, box.backTopRight, color);
            Debug.DrawLine(box.frontBottomRight, box.backBottomRight, color);
            Debug.DrawLine(box.frontBottomLeft, box.backBottomLeft, color);
        }

        public struct Box
        {
            public Vector3 localFrontTopLeft { get; private set; }
            public Vector3 localFrontTopRight { get; private set; }
            public Vector3 localFrontBottomLeft { get; private set; }
            public Vector3 localFrontBottomRight { get; private set; }
            public Vector3 localBackTopLeft { get { return -localFrontBottomRight; } }
            public Vector3 localBackTopRight { get { return -localFrontBottomLeft; } }
            public Vector3 localBackBottomLeft { get { return -localFrontTopRight; } }
            public Vector3 localBackBottomRight { get { return -localFrontTopLeft; } }

            public Vector3 frontTopLeft { get { return localFrontTopLeft + origin; } }
            public Vector3 frontTopRight { get { return localFrontTopRight + origin; } }
            public Vector3 frontBottomLeft { get { return localFrontBottomLeft + origin; } }
            public Vector3 frontBottomRight { get { return localFrontBottomRight + origin; } }
            public Vector3 backTopLeft { get { return localBackTopLeft + origin; } }
            public Vector3 backTopRight { get { return localBackTopRight + origin; } }
            public Vector3 backBottomLeft { get { return localBackBottomLeft + origin; } }
            public Vector3 backBottomRight { get { return localBackBottomRight + origin; } }

            public Vector3 origin { get; private set; }

            public Box(Vector3 origin, Vector3 halfExtents, Quaternion orientation) : this(origin, halfExtents)
            {
                Rotate(orientation);
            }
            public Box(Vector3 origin, Vector3 halfExtents)
            {
                this.localFrontTopLeft = new Vector3(-halfExtents.x, halfExtents.y, -halfExtents.z);
                this.localFrontTopRight = new Vector3(halfExtents.x, halfExtents.y, -halfExtents.z);
                this.localFrontBottomLeft = new Vector3(-halfExtents.x, -halfExtents.y, -halfExtents.z);
                this.localFrontBottomRight = new Vector3(halfExtents.x, -halfExtents.y, -halfExtents.z);

                this.origin = origin;
            }


            public void Rotate(Quaternion orientation)
            {
                localFrontTopLeft = RotatePointAroundPivot(localFrontTopLeft, Vector3.zero, orientation);
                localFrontTopRight = RotatePointAroundPivot(localFrontTopRight, Vector3.zero, orientation);
                localFrontBottomLeft = RotatePointAroundPivot(localFrontBottomLeft, Vector3.zero, orientation);
                localFrontBottomRight = RotatePointAroundPivot(localFrontBottomRight, Vector3.zero, orientation);
            }
        }

        //This should work for all cast types
        public static Vector3 CastCenterOnCollision(Vector3 origin, Vector3 direction, float hitInfoDistance)
        {
            return origin + (direction.normalized * hitInfoDistance);
        }

        public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
        {
            Vector3 direction = point - pivot;
            return pivot + rotation * direction;
        }

        public static Vector3 StringToVector3(string sVector)
        {
            // Remove the parentheses
            if (sVector.StartsWith("(") && sVector.EndsWith(")"))
            {
                sVector = sVector.Substring(1, sVector.Length - 2);
            }

            // split the items
            string[] sArray = sVector.Split(',');

            // store as a Vector3
            Vector3 result = new Vector3(
                float.Parse(sArray[0]),
                float.Parse(sArray[1]),
                float.Parse(sArray[2]));

            return result;
        }
    }
}
