﻿//==============================================================================
//	TimeInfo.
//  Simpel helper class to save the time in Unity.
//  Use the [DisplayTime(true, true, true, true)] Attribute to get Unity Editor properties
//
//	Created by Roy Ketellapper
//  Version V0.1
//	© 2016, Vasco Games, Inc.  All Rights Reserved
//==============================================================================
using System;

namespace RoyFramework.TimeHelper
{
    [Serializable]
    public class TimeInfo
    {
        public int Days;
        public int Hours;
        public int Minutes;
        public int Seconds;

        public TimeInfo(int days = 0, int hours = 0, int minutes = 0, int seconds = 0)
        {
            Days = days;
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
        }

        public TimeSpan GetTimeSpan()
        {
            return new TimeSpan(Days, Hours, Minutes, Seconds);
        }
    }
}