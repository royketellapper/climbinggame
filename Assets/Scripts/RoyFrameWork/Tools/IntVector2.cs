﻿using ProtoBuf;
using UnityEngine;

namespace RoyFramework
{
    [System.Serializable]
    public struct IntVector2
    {
        public int x;
        public int y;

        public float Magnitude
        {
            get
            {
                return Mathf.Sqrt(x * y);
            }
        }

        public float SqrDistance
        {
            get
            {
                return x * y;
            }
        }

        public IntVector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static float Distance(IntVector2 a, IntVector2 b)
        {
            return Mathf.Sqrt(a.x * b.x + a.y * b.y);
        }

        public static float SqrMagnitude(IntVector2 a, IntVector2 b)
        {
            return a.x * b.x + a.y * b.y;
        }

        public static IntVector2 operator +(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.x + b.x, a.y + b.y);
        }

        public static IntVector2 operator -(IntVector2 a)
        {
            return new IntVector2(-a.x, -a.y);
        }
        public static IntVector2 operator -(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.x - b.x, a.y - b.y);
        }

        public static bool operator ==(IntVector2 a, IntVector2 b)
        {
            return (a.x == b.x) && (a.y == b.y);
        }

        public static bool operator !=(IntVector2 a, IntVector2 b)
        {
            return (a.x != b.x) || (a.y != b.y);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    };
}