﻿namespace RoyFramework.Utility
{
    public interface ILoadSave
    {
        bool HasKey(string key);

        int GetInt(string key, int defaultValue);

        string GetString(string key, string defaultValue);

        float GetFloat(string key, float defaultValue);

        void SetInt(string key, int value);

        void SetString(string key, string value);

        void SetFloat(string key, float value);

        void SetObject(string key, System.Object Value);

        T GetObject<T>(string key) where T : class;

        void ClearAll();

        void Clear(string key);

        void Save();
    }
}