﻿using UnityEngine;
using RoyFramework.Tools;

namespace RoyFramework.Utility
{
    public class PlayerPrefsLoadSave : ILoadSave
    {
        public bool HasKey (string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        public int GetInt(string key, int defaultValue)
        {
            return PlayerPrefs.GetInt(key, defaultValue);
        }

        public string GetString(string key, string defaultValue)
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }

        public float GetFloat(string key, float defaultValue)
        {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        public void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        public void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        public void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }

        public void ClearAll()
        {
            PlayerPrefs.DeleteAll();
        }

        public void Clear(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }

        public void Save()
        {
            PlayerPrefs.Save();
        }

        public void SetObject(string key, object Value)
        {
            PlayerPrefs.SetString(key, Value.XmlSerializeToString());
        }

        public T GetObject<T>(string key) where T : class
        {
            return PlayerPrefs.GetString(key, "").XmlDeserializeFromString<T>();
        }
    }
}