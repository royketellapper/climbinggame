﻿using System;
using UnityEngine;

namespace RoyFramework.Tools
{
    [Serializable]
    public class MinMaxInt
    {
        public int Min;
        public int Max;

        public float Lerp(float a) { return Mathf.Lerp(Min, Max, a); }
        public float LerpInvert(float a) { return Mathf.Lerp(Max, Min, a); }
        public float LerpAngle(float a) { return Mathf.LerpAngle(Max, Min, a); }
    }
}