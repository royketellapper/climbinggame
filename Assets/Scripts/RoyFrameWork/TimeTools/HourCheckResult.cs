﻿using System;

namespace RoyFramework.TimeTools
{
    public class HourCheckResult
    {
        public bool TimeSuccesfullChecked;
        public int DaysPassed;
        public DateTime TimeChecked;

        public HourCheckResult(int daysPassed, DateTime timeChecked, bool timeSuccesfullChecked)
        {
            DaysPassed = daysPassed;
            TimeChecked = timeChecked;
            TimeSuccesfullChecked = timeSuccesfullChecked;
        }
    }
}