﻿using System;

namespace RoyFramework.TimeTools
{
    public class ServerTimeResult
    {
        public readonly DateTime ServerTime;
        public readonly DateTime UTCTime;
        public readonly bool ServerTimeSuccesState;
        public readonly bool TamperedWithTime;

        public ServerTimeResult(DateTime serverTime, bool serverTimeSuccesState, DateTime utcTime, bool tamperedWith)
        {
            ServerTime = serverTime;
            ServerTimeSuccesState = serverTimeSuccesState;
            UTCTime = utcTime;
            TamperedWithTime = tamperedWith;
        }
    }
}