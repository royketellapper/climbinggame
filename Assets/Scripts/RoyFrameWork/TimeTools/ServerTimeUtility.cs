﻿using System;
using RoyFramework.Services.Impl;

namespace RoyFramework.TimeTools
{
    public class ServerTimeUtility
    {
        private const int DefaultDayLimit = 0;
        private const int DefaultHourLimit = 0;

        private DateTime _cachedServerTime;
        private DateTime _utcTime;
        private bool _tamperedWithTime;
        private ServerTimeResult _lastServerTimeResult;
        private DateTime _lastTimeChecked;
        private int _dayLimit = DefaultDayLimit;
        private int _hourLimit = DefaultHourLimit;

        public DateTime CachedServerTime { get { return _cachedServerTime; } }
        public bool IsTamperedWithTime { get { return _tamperedWithTime; } }
        public ServerTimeResult LastServerTimeResult { get { return _lastServerTimeResult; } }

        public void GetServerTime(Action<ServerTimeResult> callback)
        {
            if (IsAllowedToCheck())
                CheckServerTime(callback);
            else
                SendCallback(callback, _lastServerTimeResult);
        }

        private void CheckServerTime(Action<ServerTimeResult> callback)
        {
            var succesState = false;

            using (var serverTimeChecker = new ServerTimeChecker())
            {
                serverTimeChecker.GetServerTime((time, succes) =>
                {
                    succesState = succes;

                    if (succes)
                        _cachedServerTime = time;

                    _utcTime = DateTime.UtcNow;
                    var result = (Math.Abs(GetDays(time, _utcTime)) > _dayLimit);
                    _tamperedWithTime = (result && succes);
                    _lastTimeChecked = _utcTime;
                });
            }

            SendCallback(callback, new ServerTimeResult(_cachedServerTime, succesState, _utcTime, _tamperedWithTime));
        }

        private void SendCallback(Action<ServerTimeResult> callback, ServerTimeResult result)
        {
            if (callback != null)
                callback(result);
        }

        private TimeSpan GetDifference(DateTime first, DateTime second)
        {
            return _cachedServerTime - _utcTime;
        }

        private int GetDays(DateTime first, DateTime second)
        {
            return GetDifference(first, second).Days;
        }

        private int GetHours(DateTime first, DateTime second)
        {
            return GetDifference(first, second).Hours;
        }

        private bool IsAllowedToCheck()
        {
            if (_lastTimeChecked == new DateTime())
                return true;

            var hours = GetHours(_lastTimeChecked, DateTime.UtcNow);
            return Math.Abs(hours) > _hourLimit;
        }
    }
}