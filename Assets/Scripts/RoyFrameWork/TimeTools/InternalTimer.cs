﻿using System;
using System.Collections;

namespace RoyFramework.TimeTools
{
    [Serializable]
    public class InternalTimer
    {
        private const int SecondsInMinute = 60;
        private const int MinutesInHour = 60;
        private const float TimeCorrectionThreshold = 1.5f;

        private float _timeElapsed;
        private bool _isEnabled;
        private TimeSpan _timeSpan;
        private float _elapsed;
        private int _seconds;
        private int _minutes;
        private int _hours;

        public float TimeElapsedInSeconds { get { return _timeElapsed; } }
        public TimeSpan TimeElapsed { get { return _timeSpan; } }

        public Action<int> OnSecondsPassed;
        public Action<int> OnMinutesPassed;
        public Action<int> OnHourPassed;

        public InternalTimer() { }

        public void SetEnabled(bool isEnabled)
        {
            _isEnabled = isEnabled;
        }

        public IEnumerator UpdateTimer(float delta)
        {
            while (_isEnabled)
            {
                _timeElapsed += delta;
                _elapsed += delta;
                _timeSpan = new TimeSpan(0, 0, (int)_timeElapsed);

                AutoCorrectTime();

                if (_elapsed >= 1.0f)
                {
                    _elapsed = 0.0f;
                    _seconds++;

                    if (OnSecondsPassed != null)
                        OnSecondsPassed(_seconds);
                }

                if (_seconds >= SecondsInMinute)
                {
                    _seconds = 0;
                    _minutes++;

                    if (OnMinutesPassed != null)
                        OnMinutesPassed(_minutes);
                }

                if (_minutes >= MinutesInHour)
                {
                    _minutes = 0;
                    _hours++;

                    if (OnHourPassed != null)
                        OnHourPassed(_hours);
                }

                yield return _timeElapsed;
            }
        }

        private void AutoCorrectTime()
        {
            var diff = (_timeElapsed - (_minutes == 0 ? _seconds : (_minutes * SecondsInMinute) + _seconds));

            if (diff > TimeCorrectionThreshold)
                _seconds += (int)diff;
        }
    }
}