﻿using RoyFramework.Services.Impl;
using System;
using System.Collections;
using UnityEngine;

namespace RoyFramework.Tools
{
    public class ProtectedTime : Singleton<ProtectedTime>
    {
        protected const float NoServerTimeDelay = 180;
        protected const float ServerTimeDelay = 600;
        protected float UpdateTimeDelaySecs = NoServerTimeDelay;

        protected bool _hasServerTime;
        protected DateTime _serverTime;
        protected ServerTimeChecker _serverTimeChecker = new ServerTimeChecker();
        protected DateTime _currentTime;
        protected int _counter;
        protected long _totalPlayTime;

        [Serializable]
        public class TimeProtectorData
        {
            public long TotalPlayTime;
            
            public bool HasServerTime;
            public long Time;
            public long TimeOffset;
        }
        protected TimeProtectorData _timeProtectorData;

        //Public functions 
        public static DateTime GetUTCTime()
        {
            Instance.Load();
            return Instance._currentTime;
        }

        public static long GetPlayTime()
        {
            return Instance._totalPlayTime + (long)Time.time;
        }

        public static bool IsTimeValid()
        {
            Instance.Load();
            return Instance._timeProtectorData.HasServerTime;
        }

        public static void ForceTimeUpdate()
        {
            Instance.LoadServerTime(true);
        }

        //Protected functions
        protected void Load()
        {
            if (_timeProtectorData == null)
            {
                _timeProtectorData = PlayerPrefs.GetString("ProtectorTimeData", "").XmlDeserializeFromString<TimeProtectorData>();
                if (_timeProtectorData == null)
                {
                    _timeProtectorData = new TimeProtectorData();
                    _timeProtectorData.Time = DateTime.UtcNow.Ticks;
                    PlayerPrefs.SetString("ProtectorTimeData", _timeProtectorData.XmlSerializeToString());
                }

                _currentTime = new DateTime(_timeProtectorData.Time);

                LoadServerTime(true);
            }
        }

        protected void Save()
        {
            if (_timeProtectorData != null && _currentTime != null)
                _timeProtectorData.Time = _currentTime.Ticks;

            PlayerPrefs.SetString("ProtectorTimeData", _timeProtectorData.XmlSerializeToString());
        }

        protected override void Ini()
        {
            Load();
            StartCoroutine("AddSecond");
            StartCoroutine("UpdateTime");
        }

        private IEnumerator UpdateTime()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(UpdateTimeDelaySecs);
                LoadServerTime();
            }
        }

        private IEnumerator AddSecond()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(1);
                _currentTime = _currentTime.AddSeconds(1);
                _counter++;
            }
        }

        protected void LoadServerTime(bool forceUpdate = false)
        {
            if (_counter > 60 || forceUpdate)
            {
                _serverTimeChecker.GetServerTime((result, succeed) =>
                {
                    if (succeed)
                    {
                        bool hasServerTime = _timeProtectorData.HasServerTime;
                        _timeProtectorData.HasServerTime = true;
                        _timeProtectorData.Time = result.Ticks;
                        DateTime time = new DateTime(result.Ticks);

                        if (!hasServerTime)
                        {
                            TimeSpan serverTime = new TimeSpan(time.Hour, time.Minute, time.Second);
                            TimeSpan localTime = new TimeSpan(DateTime.UtcNow.Hour, DateTime.UtcNow.Minute, DateTime.UtcNow.Second);
                            _timeProtectorData.TimeOffset = (localTime - serverTime).Ticks;
                        }

                        _currentTime = time.Add(new TimeSpan(_timeProtectorData.TimeOffset));

                        UpdateTimeDelaySecs = ServerTimeDelay;
                    }
                    else if (!_timeProtectorData.HasServerTime)
                    {
                        _currentTime = DateTime.UtcNow;
                        UpdateTimeDelaySecs = NoServerTimeDelay;
                    }
                    else
                        UpdateTimeDelaySecs = NoServerTimeDelay;
                });

                _counter = 0;
                Save();
            }
        }

        #region SaveEvents
        public override void OnDestroy()
        {
            base.OnDestroy();
            Save();
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
                LoadServerTime();
            else
                Save();
        }
        #endregion
    }
}