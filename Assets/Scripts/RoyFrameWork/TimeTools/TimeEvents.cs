﻿using System;
using UnityEngine;

namespace RoyFramework.TimeTools
{
    /// <summary>
    /// Class to listen to time events
    /// </summary>
    public static class TimeEvents
    {
        /// <summary>
        /// Gets fired when a hour is passed
        /// </summary>
        public static Action<int> OnHourPassed;

        /// <summary>
        /// Gets fired when a minute is passed
        /// </summary>
        public static Action<int> OnMinutePassed;

        public static Action<int> OnSecondPassed;

        /// <summary>
        /// Gets fired when the user has set a custom time, thus tried to cheat. This part is still experimental tho.
        /// </summary>
        public static Action OnCheatDetected;

        private static HourCheckResult _lastHourCheck;

        /// <summary>
        /// This variable can be 0 if it fails to connect to the server
        /// </summary>
        public static DateTime CurrentUTCTime;

        public static Action<HourCheckResult> OnCheckResult;

        internal static Action _forceTimeCheck;

        /// <summary>
        /// Get the time of the last Check
        /// </summary>
        /// <param name="callback"></param>
        public static HourCheckResult GetLastTimeCheck()
        {
            var lastCheck = _lastHourCheck != null ? _lastHourCheck : new HourCheckResult(0, new DateTime(), false);
            return lastCheck;
        }

        /// <summary>
        /// Call this function if succesFullChecked is false
        /// Do Not Call this function in a Update!!
        /// </summary>
        /// <param name="callback"></param>
        public static void ForceTimeCheck()
        {
            if (_forceTimeCheck != null)
                _forceTimeCheck();
        }

        internal static void MinutePassed(int minutes)
        {
            if (OnMinutePassed != null)
                OnMinutePassed(minutes);
        }

        internal static void HourPassed(int hours)
        {
            if (OnHourPassed != null)
                OnHourPassed(hours);
        }

        internal static void SecondPassed(int seconds)
        {
            if (OnSecondPassed != null)
                OnSecondPassed(seconds);
        }

        internal static void UpdateTime(int days, DateTime timeChecked, bool succesFullChecked)
        {
            _lastHourCheck = new HourCheckResult(days, timeChecked, succesFullChecked);

            if (OnCheckResult != null)
                OnCheckResult(_lastHourCheck);
        }

        internal static void CheatedDetected()
        {
            if (OnCheatDetected != null)
                OnCheatDetected();
        }
    }
}