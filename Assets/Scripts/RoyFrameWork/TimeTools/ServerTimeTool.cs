﻿using UnityEngine;
using System;

namespace RoyFramework.TimeTools
{
    public class ServerTimeTool : Singleton<ServerTimeTool>
    {
        private const string LocalTimeSaveKey = "Local_Time";
        private const int OneDay = 1;
        private const int MinutesInHour = 60;

        private ServerTimeUtility _timeUtility = new ServerTimeUtility();
        private InternalTimer _timer = new InternalTimer();
        [SerializeField] private TimeFrame _timeRecheck;

        private DateTime _serverTime;
        private DateTime _localTime;
        private bool _tamperedWithTime;
        private int _minutesSindsLastRecheck;

        private double timeCounter = 0;
        private DateTime _timeUpdate;

        protected override void Ini()
        {
            Load();

            if(_timeRecheck != null)
                _timeRecheck.Validate();

            _timer.OnMinutesPassed += MinutesPassed;
            _timer.OnHourPassed += HourPassed;
            _timer.OnSecondsPassed += SecondsPassed;
            _timer.SetEnabled(true);          
        }

        private void OnEnable()
        {
            TimeEvents._forceTimeCheck += CheckDifference;
        }

        private void OnDisable()
        {
            TimeEvents._forceTimeCheck -= CheckDifference;
        }

        private void Start()
        {
            CheckDifference();
        }

        public override void OnDestroy()
        {
            _timer.OnMinutesPassed -= MinutesPassed;
            _timer.OnHourPassed -= HourPassed;
            _timer.OnSecondsPassed -= SecondsPassed;
        }

        private void Update()
        {
            _timer.UpdateTimer(Time.unscaledDeltaTime).MoveNext();

            //if (_serverTime != null)
            //{
                timeCounter += Time.unscaledDeltaTime;
                _timeUpdate = _serverTime.AddSeconds(timeCounter);
                TimeEvents.CurrentUTCTime = _timeUpdate;
            //}
        }

        internal void CheckDifference()
        {
            var savedLocalTime = _localTime;
            var currentLocalTime = GetLocalTime();
            var diff = currentLocalTime - savedLocalTime;
            var localDays = diff.Days;
            var defaultTime = new DateTime();

            if(savedLocalTime == defaultTime)
            {
                UpdateTime(0, currentLocalTime, true);
                _minutesSindsLastRecheck = 0;

                // save current local timer for future checkups references
                _localTime = currentLocalTime;
                Save();

                return;
            }
            
            if(localDays < OneDay)
            {
                UpdateTime(0, currentLocalTime, true);
                _minutesSindsLastRecheck = 0;
            }

            if (localDays >= OneDay)
            {
                // check with server if it's really a day passed
                var currentServerTime = GetServerTime();
                var serverDiff = currentServerTime - savedLocalTime;
                var serverDays = serverDiff.Days;

                if (currentServerTime != defaultTime && serverDays < OneDay)
                {
                    UpdateTime(serverDiff.Days, currentLocalTime, true);
                    _minutesSindsLastRecheck = 0;
                    return;
                }            
                else if(currentServerTime == defaultTime)
                {
                    UpdateTime(serverDiff.Days, currentLocalTime, false);
                    _minutesSindsLastRecheck = 0;
                    return;
                }

                if (serverDays >= OneDay)
                {
                    if (localDays == serverDays)
                    {
                        UpdateTime(serverDiff.Days, currentLocalTime, true);
                        _tamperedWithTime = false;

                        // save current local timer for future checkups references
                        _localTime = currentLocalTime;
                        Save();
                    }

                    if (localDays != serverDays)
                        _tamperedWithTime = true;
                }
            }

            _minutesSindsLastRecheck = 0;
        }

        private DateTime GetServerTime()
        {
            var timeToReturn = new DateTime();

            _timeUtility.GetServerTime((result) =>
            {
                timeToReturn = result.ServerTime;
                _tamperedWithTime = result.TamperedWithTime;
            });

            return timeToReturn;
        }

        private void UpdateTime(int days, DateTime timeChecked, bool succesFullChecked)
        {
            if (succesFullChecked)
            {
                timeCounter = 0;
                _serverTime = timeChecked;
                TimeEvents.CurrentUTCTime = _serverTime;
            }

            TimeEvents.UpdateTime(days, timeChecked, succesFullChecked);
        }

        private DateTime GetLocalTime()
        {
            return DateTime.UtcNow;
        }

        private void Load()
        {
            DateTime.TryParse(PlayerPrefs.GetString(LocalTimeSaveKey), out _localTime);
        }

        private void Save()
        {
            PlayerPrefs.SetString(LocalTimeSaveKey, _localTime.ToString());
        }

        private void MinutesPassed(int minutes)
        {
            TimeEvents.MinutePassed(minutes);

            _minutesSindsLastRecheck++;
            CheckForRecheck();
        }

        private void HourPassed(int hour)
        {
            TimeEvents.HourPassed(hour);
        }

        private void SecondsPassed(int seconds)
        {
            TimeEvents.SecondPassed(seconds);
        }

        private void CheckForRecheck()
        {
            if(_timeRecheck.FrameType == TimeFrame.Type.Minutes)
                if (_minutesSindsLastRecheck >= _timeRecheck.Amount)
                    CheckDifference();

            if (_timeRecheck.FrameType == TimeFrame.Type.Hours)
                if ((_minutesSindsLastRecheck / MinutesInHour) >= _timeRecheck.Amount)
                    CheckDifference();
        }

        [Serializable]
        public class TimeFrame
        {
            public const int MinutesMinimum = 10;
            public const int HoursMinimum = 1;

            public enum Type
            {
                Minutes,
                Hours,
            }

            public Type FrameType;
            public int Amount;

            public TimeFrame()
            {
                Validate();
            }

            public void Validate()
            {
                if(FrameType == Type.Minutes)
                    if (Amount <= MinutesMinimum)
                        Amount = MinutesMinimum;

                if (FrameType == Type.Hours)
                    if (Amount <= HoursMinimum)
                        Amount = HoursMinimum;
            }
        }
    }
}