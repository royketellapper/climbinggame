﻿#if UNITY_EDITOR_WIN

using UnityEngine;
using UnityEditor;
using Microsoft.Win32;
using System;
using System.Diagnostics;

public static class OpenNotepad
{
    public static String GetFilePath(UnityEngine.Object obj)
    {
        string assetPath = "";
        String[] pathSeparators = new String[] { "/" };
        string[] lines = AssetDatabase.GetAssetPath(obj).Split(pathSeparators, StringSplitOptions.RemoveEmptyEntries);

        for (int i = 1; i < lines.Length; i++)
        {
            if (i == 1)
                assetPath += lines[i];
            else
                assetPath += "/"+ lines[i];
        }

        return Application.dataPath + "/" + assetPath;
    }

    [MenuItem("Assets/Open in Notepad++")]
    static void CreateCustomGameObject()
    {
        string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\notepad++.exe";

        using (RegistryKey Key = Registry.LocalMachine.OpenSubKey(registry_key, false))
        {
            string notepad = Key.GetValue(null).ToString();

            UnityEngine.Object[] selected = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
            foreach (UnityEngine.Object obj in selected)
            {
                Process notePad = new Process();
                notePad.StartInfo.FileName = notepad;
                notePad.StartInfo.Arguments = GetFilePath(obj);
                notePad.Start();
            }
        }
    }
}
#endif