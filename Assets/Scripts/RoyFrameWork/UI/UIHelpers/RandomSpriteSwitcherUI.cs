﻿using UnityEngine;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    [RequireComponent(typeof(Image))]
    public class RandomSpriteSwitcherUI : MonoBehaviour
    {
        public Sprite[] Sprites;
        public bool RandomImageOnEnable = true;
        protected Image _image;

        protected virtual void Awake()
        {
            _image = GetComponent<Image>();
        }

        protected virtual void OnEnable()
        {
            if (RandomImageOnEnable)
                SetRandomImage();
        }

        public void SetRandomImage()
        {
            if (Sprites.Length != 0)
            {
                int index = Mathf.Clamp(Random.Range(0, Sprites.Length), 0, Sprites.Length - 1);
                _image.sprite = Sprites[index];
            }
        }
    }
}