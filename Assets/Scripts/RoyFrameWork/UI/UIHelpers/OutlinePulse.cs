﻿using UnityEngine;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    [RequireComponent(typeof(Outline))]
    public class OutlinePulse : MonoBehaviour
    {
        protected Outline[] _outline;
        protected Color[] _colors;

        public float Speed = 1;

        protected virtual void Awake()
        {
            _outline = GetComponents<Outline>();
            _colors = new Color[_outline.Length];

            for (int i = 0; i < _colors.Length; i++)
            {
                if (_outline[i] != null)
                    _colors[i] = _outline[i].effectColor;
            }
        }

        protected virtual void Update()
        {
            float cos = (Mathf.Cos(Time.unscaledTime * Speed) + 1) * 0.5f;

            for (int i = 0; i < _outline.Length; i++)
            {
                if (_outline == null)
                    return;

                Color color = _colors[i];
                color.a = _colors[i].a * cos;
                _outline[i].effectColor = color;
            }
        }
    }
}