﻿using UnityEngine;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    [RequireComponent(typeof(Image))]
    public class OSSpriteSwitcherUI : MonoBehaviour
    {
        public Sprite IOS;
        public Sprite Android;

        protected void Awake()
        {
            Image image = GetComponent<Image>();

#if UNITY_IOS
            image.sprite = IOS;
#elif UNITY_ANDROID
            image.sprite = Android;
#endif
        }
    }
}