﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    public enum State
    {
        None,
        Active,
        InActive,
        Selected,
        DisableAll
    }

    public class ButtonStateGroup : MonoBehaviour
    {
        public State ButtonState
        {
            set
            {
                _buttonState = value;
                SetState(_buttonState);
            }
            get
            {
                return _buttonState;
            }
        }

        [SerializeField]
        protected State _buttonState;

        [Space]
        public UIButton ActiveButton;
        public UIButton InActiveButton;
        public UIButton SelectedButton;

        [Header("Events")]
        public UnityEvent OnClick;

        protected virtual void Awake()
        {
            SetState(_buttonState);
        }

        public virtual void SetState(State buttonState)
        {
            if (buttonState == State.None)
                return;

            _buttonState = buttonState;
            SetButtonActive(ActiveButton, buttonState == State.Active, true);
            SetButtonActive(InActiveButton, buttonState == State.InActive);
            SetButtonActive(SelectedButton, buttonState == State.Selected, true);
        }

        public void SetText(string text)
        {
            if (ActiveButton != null && ActiveButton.Text != null)
                ActiveButton.Text.text = text;

            if (InActiveButton != null && InActiveButton.Text != null)
                InActiveButton.Text.text = text;

            if (SelectedButton != null && SelectedButton.Text != null)
                SelectedButton.Text.text = text;
        }

        protected virtual void SetButtonActive(UIButton button, bool value, bool useEvent = false)
        {
            if (button == null)
                return;

            button.gameObject.SetActive(value);

            if (useEvent)
            {
                if (value)
                    button.onClick.AddListener(OnButtonClick);
                else
                    button.onClick.RemoveListener(OnButtonClick);
            }
        }

        protected virtual void OnButtonClick()
        {
            if (OnClick != null)
                OnClick.Invoke();
        }
    }
}
