﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace RoyFramework.UI
{
    public static class RowHelper
    {
        public static List<T> CreateRows<T>(T rowObject, int totalRows, ScrollRect scrollRect, Color[] rowBackGroundColors = null, bool setActive = true) where T : RowItem
        {
            List<T> rows = new List<T>();

            float offset = rowObject.GetComponent<RectTransform>().rect.height;
            scrollRect.content.GetComponent<RectTransform>().sizeDelta = new Vector2(0, offset * totalRows);

            for (int x = 0; x < totalRows; x++)
            {
                T row = CreateRow<T>(rowObject, offset, x, scrollRect, rowBackGroundColors);
                row.gameObject.SetActive(setActive);
                rows.Add(row);
            }

            return rows;
        }

        public static T CreateRowsIfNotExist<T>(T rowObject,ref List<T> rowlist, int index, ScrollRect scrollRect, Color[] rowBackGroundColors = null) where T : RowItem
        {
            if (rowlist == null)
                rowlist = new List<T>();

            if (index < rowlist.Count)
            {
                rowlist[index].gameObject.SetActive(true);
                return rowlist[index];
            }

            float offset = rowObject.GetComponent<RectTransform>().rect.height;
            scrollRect.content.GetComponent<RectTransform>().sizeDelta = new Vector2(0, offset * (index + 1));

            for (int x = rowlist.Count; x < index + 1; x++)
            {
                T row = CreateRow<T>(rowObject, offset, x, scrollRect, rowBackGroundColors);
                rowlist.Add(row);
            }

            return rowlist[index];
        }

        public static T CreateRow<T>(T rowObject, float offset, int index, ScrollRect scrollRect, Color[] rowBackGroundColors) where T : RowItem
        {
            T row = Object.Instantiate(rowObject) as T;
            row.transform.SetParent(scrollRect.content);

            row.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (-offset * index) - (offset / 2));
            row.transform.localScale = Vector3.one;

            if (row.BackGround != null && rowBackGroundColors != null)
            {
                if (rowBackGroundColors.Length != 0)
                    row.BackGround.color = rowBackGroundColors[index % rowBackGroundColors.Length];
            }
            return row;
        }

        public static void SetRowsActive<T>(List<T> rowlist, bool value) where T : RowItem
        {
            if (rowlist == null)
                return;

            foreach (T row in rowlist)
            {
                if (row != null)
                    row.gameObject.SetActive(value);
            }
        }
    }
}
