﻿using UnityEngine;

namespace RoyFramework.UI
{
    [ExecuteInEditMode, RequireComponent(typeof(CanvasRenderer)), RequireComponent(typeof(MeshFilter))]
    public class MeshUI : MonoBehaviour
    {
        public Material[] Materials
        {
            get { return _materials;  }
            set
            {
                _materials = value;
                UpdateMesh();
            }
        }

        [SerializeField]
        protected Material[] _materials;

        public CanvasRenderer CanvasRenderer
        {
            get
            {
                if (_canvasRenderer == null)
                    _canvasRenderer = GetComponent<CanvasRenderer>();

                return _canvasRenderer;
            }
        }
        protected CanvasRenderer _canvasRenderer;

        public MeshFilter MeshFilter
        {
            get
            {
                if (_meshFilter == null)
                    _meshFilter = GetComponent<MeshFilter>();

                return _meshFilter;
            }
        }
        protected MeshFilter _meshFilter;
        protected Mesh _currentMesh;     

        protected virtual void LateUpdate()
        {
            if (_currentMesh != MeshFilter.sharedMesh)
            {
                _currentMesh = MeshFilter.sharedMesh;
                UpdateMesh();
            }
        }

        protected void OnEnable()
        {
            UpdateMesh();
        }

        public virtual void UpdateMesh()
        {
            CanvasRenderer.SetMesh(MeshFilter.sharedMesh);

            CanvasRenderer.materialCount = Materials.Length;
            for (var i = 0; i < Materials.Length; ++i)
                CanvasRenderer.SetMaterial(Materials[i], i);
        }

        protected virtual void OnDisable()
        {
            CanvasRenderer.SetMesh(null);
            CanvasRenderer.Clear();
        }
    }
}