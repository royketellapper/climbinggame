﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    public class UISwitchMenu : UIToggleGroup
    {
        [Header("UI Text")]       
        public PageExpressionInfo[] PageExpressions; // {Current} {Total} {CurrentName}

        [Header("Menus")]
        public RectTransform[] Menus;

        [HideInInspector]
        public RectTransform CurrentMenu;
             
        [System.Serializable]
        public class IndexEvent : UnityEvent<int> { }
        [Space]
        public IndexEvent OnIndexChanged;

        protected override int MaxSelection
        {
            get
            {
                int b = UIButtons.Length;
                int m = Menus.Length;
                return (b > m) ? b : m;
            }
        }

        public virtual void SetMenusActive(bool value)
        {
            for (int i = 0; i < Menus.Length; i++)
            {
                var item = Menus[i];

                if (item != null)
                {
                    if (value)
                        item.gameObject.SetMenuActive(SelectedIndex == i);
                    else
                        item.gameObject.SetMenuActive(false);              
                }
            }
        }

        public override void SetSelection(int index)
        {
            base.SetSelection(index);         

            if (Menus == null)
                return;

            for (int x = 0; x < Menus.Length; x++)
            {
                RectTransform menu = Menus[x];

                if (menu == null)
                    continue;

                menu.gameObject.SetMenuActive(x == SelectedIndex);
            }

            if (index < Menus.Length)
                CurrentMenu = Menus[index];

            if (OnIndexChanged != null)
                OnIndexChanged.Invoke(index);

            SetPageText(index, PageExpressions);
        }

        public virtual void SetPageText(int index, PageExpressionInfo pageExpressionInfo)
        {
            if (pageExpressionInfo == null || pageExpressionInfo.PageText == null)
                return;

            string pageText = pageExpressionInfo.PageExpression.Replace("{Current}", (index + 1).ToString());
            pageText = pageText.Replace("{Total}", MaxSelection.ToString());
            pageText = pageText.Replace("{CurrentName}", CurrentMenu.name);
            pageExpressionInfo.PageText.text = pageText;
        }

        public virtual void SetPageText(int index, PageExpressionInfo[] pageExpressionInfo)
        {
            foreach (var item in pageExpressionInfo)
                SetPageText(index, item);
        }
    }
}
