﻿using UnityEngine;

namespace RoyFramework.UI
{
    public class UIRotate : MonoBehaviour
    {
        public Transform target;
        public Vector3 speed;

        [Header("Wobble")]
        public bool EnableWobble;
        public Vector3 Offset;
        public Vector3 WobbleAmount;       
       

        void Awake()
        {
            if (target == null)
                target = transform;
        }

        void FixedUpdate()
        {
            if (target != null)
            {
                if (EnableWobble)
                {
                    Vector3 pos = Offset;
                    pos.x = (Mathf.Cos(Time.unscaledTime * speed.x) * WobbleAmount.x) + Offset.x;
                    pos.y = (Mathf.Cos(Time.unscaledTime * speed.y) * WobbleAmount.y) + Offset.y;
                    pos.z = (Mathf.Cos(Time.unscaledTime * speed.z) * WobbleAmount.z) + Offset.z;
                    target.eulerAngles = pos;
                }
                else
                    target.Rotate(speed);
            }
        }
    }
}
