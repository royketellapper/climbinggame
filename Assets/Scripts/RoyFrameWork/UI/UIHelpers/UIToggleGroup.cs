﻿using UnityEngine;
using System;

namespace RoyFramework.UI
{
    public class UIToggleGroup : MonoBehaviour
    {
        public enum SiblingIndexType
        {
            None,
            Front,
            Back
        }

        [Header("Buttons")]
        public UIButton[] UIButtons;
        [Space]
        public UIButton NextButton;
        public UIButton PreviousButton;

        [Header("Settings")]
        [SerializeField]
        protected int selectedIndex = 0;
        public virtual int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                if (selectedIndex != value)
                    SetSelection(value);
            }
        }
        protected virtual int MaxSelection
        {
            get { return (UIButtons != null) ? UIButtons.Length : 0; }
        }

        public bool AllowSwitchOff;
        public bool CanLoop = true;

        [Space]
        public bool InvertButtonActiveState;
        public SiblingIndexType SiblingIndex = SiblingIndexType.None;
        public ButtonState ButtonActiveState = ButtonState.SelectButton;
        public ButtonState NextType = ButtonState.DisableButton;
        public ButtonState PreviousType = ButtonState.DisableButton;

        public Action<int> OnSelected;
        [HideInInspector]
        public UIButton SelectedButton;
        protected int[] _siblingsIndex;

        protected virtual void Awake()
        {
            RegisterButtons();
            SetSelection(SelectedIndex);
        }

        public void RegisterButtons()
        {
            if (UIButtons != null)
            {
                _siblingsIndex = new int[UIButtons.Length];

                for (int x = 0; x < UIButtons.Length; x++)
                {
                    UIButton button = UIButtons[x];

                    if (button == null)
                        continue;

                    button.Toggle = false;

                    int tempIndex = x;
                    button.onClick.AddListener(() => ButtonOnClick(tempIndex));

                    _siblingsIndex[x] = button.transform.GetSiblingIndex();
                }
            }

            if (NextButton != null)
                NextButton.onClick.AddListener(Next);

            if (PreviousButton != null)
                PreviousButton.onClick.AddListener(Previous);
        }

        protected virtual void ButtonOnClick(int index)
        {
            SelectedIndex = (AllowSwitchOff && selectedIndex == index) ? -1 : index;

            if (OnSelected != null)
                OnSelected(index);
        }

        public virtual void SetButtonsActive(bool value)
        {
            for (int i = 0; i < UIButtons.Length; i++)
            {
                var item = UIButtons[i];

                if (item != null)
                    item.gameObject.SetActive(value);
            }
        }

        public virtual void SetButtonStateToAll(ButtonState buttonState, bool value)
        {
            foreach (var item in UIButtons)
            {
                if (item != null)
                    item.SetButtonState(buttonState, value);
            }
        }

        public virtual void SetSelection(int index)
        {
            selectedIndex = index;

            if (UIButtons == null)
                return;

            for (int x = 0; x < UIButtons.Length; x++)
            {
                UIButton button = UIButtons[x];

                if (button == null)
                    continue;

                button.SetButtonState(ButtonActiveState, (InvertButtonActiveState)? x != index : x == index);


                if (UIButtons[x] != null && SiblingIndex != SiblingIndexType.None)
                    UIButtons[x].transform.SetSiblingIndex(_siblingsIndex[x]);
            }

            switch (SiblingIndex)
            {
                case SiblingIndexType.Front:
                    UIButtons[index].transform.SetAsLastSibling();
                    break;
                case SiblingIndexType.Back:
                    UIButtons[index].transform.SetAsFirstSibling();
                    break;
            }

            if (index < UIButtons.Length)
                SelectedButton = UIButtons[index];

            if (NextButton != null && !CanLoop)
                NextButton.SetButtonState(NextType, index < MaxSelection - 1);

            if (PreviousButton != null && !CanLoop)
                PreviousButton.SetButtonState(PreviousType, index > 0);
        }

        public virtual void Next()
        {
            if (selectedIndex + 1 < MaxSelection)
                SetSelection(selectedIndex + 1);
            else if (CanLoop)
                SetSelection(0);
        }

        public virtual void Previous()
        {
            if (selectedIndex - 1 >= 0)
                SetSelection(selectedIndex - 1);
            else if (CanLoop)
                SetSelection(MaxSelection - 1);
        }
    }
}
