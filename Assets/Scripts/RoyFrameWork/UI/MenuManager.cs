﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

namespace RoyFramework.UI
{
    [RequireComponent(typeof(Canvas))]
    public class MenuManager : MonoBehaviour
    {
        //===================== Public Vars ====================
        public MenuInfo[] Menus;
        public Action<MenuInfo> OnMenuChanged;
        public static MenuManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = GameObject.FindObjectOfType<MenuManager>();

                return _instance;
            }
        }
        public MenuInfo CurrentMenu
        {  
            get  
            {
                if (_history == null)
                    return null;

                if (_history.Count != 0)
                    return _history[0];

                return null;
            }
        }

        public string CurrentMenuName
        {
            get
            {
                if (_history.Count != 0)
                    return _history[0].Name;
                else
                    return "";
            }
        }

        public int HistoryCount
        {
            get { return _history.Count; }
        }

        //======================= PendingMenu =====================
        protected MenuInfo _currentEffectMenu; // pendingMenu needs to wait when this menu finish the animation  <================== fix me
        protected MenuInfo _pendingMenu;  // Menu that is waiting for the currentEffectMenu menu to close
        protected MenuState _pendingMenuState; // The new state for this menu

        protected class MenuSceneLoadInfo
        {
            public string MenuId;
            public string SceneName;
            public MenuSettings MenuSettings;
        }
        protected static List<MenuSceneLoadInfo> _menuSceneLoadInfo = new List<MenuSceneLoadInfo>();

        //======================= Default menus ===================
        [MenuName]
        public string StartupMenu;
        [MenuName]
        public string QuitMenu;

        //======================= Out Of Focus Menu ===================
        [MenuName]
        public string OutOfFocusMenu;
        public bool IgnoreStartupMenu;

        //=================== Close Current Menu Menu =================
        public KeyCode CloseCurrentMenuKey = KeyCode.Escape;
        private const int RegisteredKey = 20;

        //===================== Protected Vars ====================
        protected List<MenuInfo> _history = new List<MenuInfo>();
        protected float _resetTimeScale;
        protected bool _started;
        [System.NonSerialized]
        protected static MenuManager _instance;
        protected class MessageWindowsInfo
        {
            public string id;
            public MessageMenu messageMenu;
        }
        protected Dictionary<MessageType, MessageWindowsInfo> messagesWindows = new Dictionary<MessageType, MessageWindowsInfo>();
        protected bool _isInitialised;
        protected Action _onInitialised;

        public Canvas Canvas { get; protected set; }
        public int DefaultSortingOrder { get; protected set; }

        //========================= Functions ======================
        protected virtual void Awake()
        {
            Canvas = GetComponent<Canvas>();
            DefaultSortingOrder = Canvas.sortingOrder;

            if (_instance == null)
                _instance = this;

            for (int x = 0; x < Menus.Length; x++)
            {
                MenuInfo menu = Menus[x];
                menu.MenuManager = this;

                if (menu.MenuRect == null)
                {
                    Debug.LogError("MenuManager: " + menu.Id + " is NULL (delete this array element if you don't need it)");
                    continue;
                }

                // Find Message Menu
                MessageMenu messageMenu = menu.MenuRect.GetComponent<MessageMenu>();
                if (messageMenu != null)
                {
                    if (!messagesWindows.ContainsKey(messageMenu.MessageType))
                    {
                        MessageWindowsInfo messagewindows = new MessageWindowsInfo();
                        messagewindows.id = menu.Id;
                        messagewindows.messageMenu = messageMenu;
                        messagesWindows.Add(messageMenu.MessageType, messagewindows);
                    }
                }

                // Turn off all menus
                if (menu.MenuRect != null)
                    menu.MenuRect.gameObject.SetActive(menu.InitializeMenu);
            }

            SceneManager.sceneLoaded += sceneLoaded;
        }

        protected virtual void Start()
        {
            foreach (var menu in Menus)
            {
                if (menu.InitializeMenu && StartupMenu != menu.Id)
                    menu.MenuRect.gameObject.SetActive(false);
            }

            _started = true;
            _resetTimeScale = Time.timeScale;
            OpenMenu(StartupMenu);

            _isInitialised = true;
            if (_onInitialised != null)
            {
                _onInitialised();
                _onInitialised = null;
            }
        }

        protected virtual void sceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            List<MenuSceneLoadInfo> sceneLoadInfo = _menuSceneLoadInfo.FindAll(x => x.SceneName == scene.name);

            foreach (var item in sceneLoadInfo)
            {
                OpenMenu(item.MenuId, item.MenuSettings);
                _menuSceneLoadInfo.Remove(item);
            }
        }

        protected virtual void OnApplicationFocus(bool focusStatus)
        {
            if (!_started)
                return;

            if (CurrentMenu != null && !IgnoreStartupMenu && StartupMenu != string.Empty)
                if (CurrentMenu.Id == StartupMenu)
                    return;

            OpenMenu(OutOfFocusMenu, true);
        }

        public virtual MenuInfo GetMenuInfo(string id)
        {
            if (Menus == null)
                return null;

            return Array.Find<MenuInfo>(Menus, m => m.Id == id);
        }

        public void WaitForInitialise(Action init)
        {
            if (_isInitialised)
                init();
            else
                _onInitialised += init;
        }

        public virtual void CloseAllMenus(bool closeStartUpMenu = false) //<==== fix me closeStartUpMenu
        {
            Time.timeScale = _resetTimeScale;

            if (Menus == null)
                return;

            foreach (MenuInfo menu in Menus)
            {
                if (closeStartUpMenu && menu.Id == StartupMenu)
                    continue;

                if (menu.MenuRect != null)
                    SetMenuState(menu, MenuState.Close);
            }
            _history.Clear();
            MenuChanged(null);
        }

        public static void OpenMenuInScene(string id, string sceneName, MenuSettings menuSettings = null)
        {
            if (SceneManager.GetActiveScene().name == sceneName)
                Instance.OpenMenu(id, menuSettings);
            else
            {
                MenuSceneLoadInfo menuSceneLoadInfo = new MenuSceneLoadInfo();
                menuSceneLoadInfo.MenuId = id;
                menuSceneLoadInfo.SceneName = sceneName;
                menuSceneLoadInfo.MenuSettings = menuSettings ?? new MenuSettings();
                _menuSceneLoadInfo.Add(menuSceneLoadInfo);
            }
        }

        public static void ClearAllOpenMenuInScenes()
        {
            _menuSceneLoadInfo.Clear();
        }

        public static void ClearOpenMenuInScenes(string id, string sceneName)
        {
           _menuSceneLoadInfo.RemoveAll(a => a.MenuId == id && a.SceneName == sceneName);
        }

        public static void ClearOpenMenuInScenes(string sceneName)
        {
            _menuSceneLoadInfo.RemoveAll(a => a.SceneName == sceneName);
        }

        public virtual MenuInfo OpenMenu(string id, MenuSettings menuSettings)
        {
            if (menuSettings == null)
                menuSettings = new MenuSettings();

            return OpenMenu(id, menuSettings.closeParent, menuSettings.saveInHistory);
        }

        public virtual MenuInfo OpenMenu(string id, bool closeParent = false, bool saveInHistory = true)
        {
            if (id == string.Empty)
                return null;

            int index = Array.FindIndex<MenuInfo>(Menus, m => m.Id == id);

            return OpenMenu(index, closeParent, saveInHistory);
        }

        public virtual MenuInfo OpenMenu(int index, bool closeParent = false, bool saveInHistory = true)
        {
            if (index < 0 || index > Menus.Length)
                return null;

            // Is the menu all open
            if (CurrentMenu == Menus[index])
                return CurrentMenu;

            // Save time scale
            if (_history.Count == 0)
                _resetTimeScale = Time.timeScale;

            // Disable Parent
            if (closeParent && _history.Count != 0)
                SetMenuState(_history[0], MenuState.Disable);

            // Open the menu
            MenuInfo menu = Menus[index];
            SetMenuState(menu, MenuState.Open);         
            Time.timeScale = menu.TimeScale;

            if (saveInHistory)
                AddMenusToHistory(menu);

            MenuChanged(menu);

            return menu;
        }

        public virtual void CloseMenu(string id)
        {
            if (id == string.Empty)
                return;

            MenuInfo menu = _history.Find(m => m.Id == id);
            if (menu == null)
                menu = Array.Find<MenuInfo>(Menus,m => m.Id == id);

            SetMenuState(menu, MenuState.Close);
            RemoveMenuFromHistory(menu);

            if (_history.Count > 0)
            {
                MenuInfo openMenu = _history[0];
                Time.timeScale = openMenu.TimeScale;
                SetMenuState(openMenu, MenuState.Open);
                MenuChanged(openMenu);
            }
            else
            {
                Time.timeScale = _resetTimeScale;
                Canvas.sortingOrder = DefaultSortingOrder;
                Canvas.overrideSorting = false;
                MenuChanged(null);
            }
        }

        public virtual void CloseMenu(int index)
        {
            if (index < 0 || index > Menus.Length)
                return;

            MenuInfo menu = Menus[index];
            SetMenuState(Array.Find<MenuInfo>(Menus, m => m.Id == _history[index].Id), MenuState.Close);
            RemoveMenuFromHistory(menu);

            if (_history.Count > 0)
            {
                MenuInfo openMenu = _history[0];
                Time.timeScale = openMenu.TimeScale;
                SetMenuState(openMenu, MenuState.Open);
                MenuChanged(openMenu);
            }
            else
            {
                Time.timeScale = _resetTimeScale;
                Canvas.sortingOrder = DefaultSortingOrder;
                Canvas.overrideSorting = false;
                MenuChanged(null);
            }
        }

        public virtual void CloseCurrentMenu(bool closeStartUpMenu = false)
        {
            if (_history.Count == 0)
                return;
            
            MenuInfo closeMenu = _history[0];

            if (!closeStartUpMenu && _history.Count == 1 && closeMenu != null && closeMenu.Id == StartupMenu)
                return;
            
            SetMenuState(closeMenu, MenuState.Close);
            RemoveMenuFromHistory(closeMenu);

            if (_history.Count > 0)
            {
                MenuInfo openMenu = _history[0];
                Time.timeScale = openMenu.TimeScale;
                SetMenuState(openMenu, MenuState.Open);

                if (_history.Count != 0 && StartupMenu != "")
                    MenuChanged(openMenu);
            }

            if (_history.Count == 0)
            {
                if (!closeStartUpMenu && !String.IsNullOrEmpty(StartupMenu))
                    OpenMenu(StartupMenu);
                else
                {
                    Time.timeScale = _resetTimeScale;
                    Canvas.sortingOrder = DefaultSortingOrder;
                    Canvas.overrideSorting = false;
                    MenuChanged(null);
                }
            }
        }

        //MessageBox functions
        public MessageMenu MessageBox(string message, MessageType messageType = MessageType.Default, Action onConformClick = null, Action onCancelClick = null)
        {
            return MessageBox(message, "", messageType, onConformClick, onCancelClick);
        }

        public virtual MessageMenu MessageBox(string message, string titel = "", MessageType messageType = MessageType.Default, Action onConformClick = null, Action onCancelClick = null)
        {
            return MessageBox(message, titel, true, messageType, onConformClick, onCancelClick);
        }

        public virtual MessageMenu MessageBox(string message, string titel = "", bool saveInhistory = true, MessageType messageType = MessageType.Default, Action onConformClick = null, Action onCancelClick = null)
        {
            if (!messagesWindows.ContainsKey(messageType))
                return null;

            MessageWindowsInfo messageMenu = messagesWindows[messageType];
            messageMenu.messageMenu.MenuId = messageMenu.id;
            messageMenu.messageMenu.Message = message;
            messageMenu.messageMenu.Titel = titel;
            messageMenu.messageMenu.Initialize(onConformClick, onCancelClick);
            OpenMenu(messageMenu.id,false, saveInhistory);

            return messageMenu.messageMenu;
        }

        public void AddMenusToHistory(Transform menu, string id)
        {
            MenuInfo MenuInfo = new MenuInfo();
            MenuInfo.Id = id;
            MenuInfo.MenuRect = menu;
            AddMenusToHistory(MenuInfo);
        }

        public void RemoveMenuFromHistory(string id)
        {
            if (_history.Count == 0)
                return;

            _history.RemoveAll(m => m.Id == id);
        }

        public void RemoveMenuFromHistory(Transform menu)
        {
            if (_history.Count == 0)
                return;

            _history.RemoveAll(m => m.MenuRect == menu);
        }

        //=============================== Helper functions ==================================
        // Menu History
        public void AddMenusToHistory(MenuInfo menu)
        {
            int index = _history.IndexOf(menu);

            if (index != 0 && index != -1)
                _history.RemoveAt(index);

            if (index != 0)
                _history.Insert(0, menu);
        }

        public void RemoveMenuFromHistory(MenuInfo menu)
        {
            if (_history.Count == 0)
                return;

            _history.Remove(menu);
        }

        // Set menu state
        protected virtual void SetMenuState(MenuInfo menu, MenuState menuState)
        {
            if (menu == null)
                return;

            if (_currentEffectMenu == menu)
            {
                menu.SetMenuState(menuState);
            }
            else if (IsMenuPlayingEffect())
            {
                _pendingMenu = menu;
                _pendingMenuState = menuState;
            }
            else
            {
                _currentEffectMenu = menu;
                menu.SetMenuState(menuState);
                menu.OnEffectFinish += MenuEffectFinish;
            }
        }

        protected bool IsMenuPlayingEffect()
        {
            if (_currentEffectMenu != null)
            {
                if (_currentEffectMenu.IsPlayingEffect)
                    return true;
            }

            return false;
        }

        protected virtual void MenuEffectFinish(MenuInfo menu)
        {
            if (_pendingMenu == null || menu == _pendingMenu)
                return;

            _pendingMenu.SetMenuState(_pendingMenuState);
            menu.OnEffectFinish -= MenuEffectFinish;
            _pendingMenu = null;
            _currentEffectMenu = _pendingMenu;
        }

        protected virtual void MenuChanged(MenuInfo menu)
        {
            if (OnMenuChanged != null)
                OnMenuChanged(menu);
        }

        protected virtual void OnDestroy()
        {
            Time.timeScale = _resetTimeScale;
            SceneManager.sceneLoaded -= sceneLoaded;
        }

        protected virtual void Update()
        {
            if (Input.GetKeyUp(CloseCurrentMenuKey))
            {
                if (CurrentMenu.Name == StartupMenu)
                    OpenMenu(QuitMenu);
                else
                    CloseCurrentMenu();
            }

            //if (CurrentMenu != null)
            //    Debug.Log(CurrentMenu.Id);    
        }

        protected virtual void OnEscapePress()
        {          
            if (/*!string.IsNullOrEmpty(StartupMenu) && */CurrentMenu != null && CurrentMenu.Name != StartupMenu)
                CloseCurrentMenu();
            else               
                OpenMenu(QuitMenu);
        }
    }
}
