﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RoyFramework.UI  
{
    [AddComponentMenu("VascoGames/UI/Button", 30), DisallowMultipleComponent]
    public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public bool Interactable
        {
            get { return _interactable; }
            set
            {
                if (_interactable != value)
                {
                    _interactable = value;
                    SetInteractable(value);
                }
            }
        }

        public Graphic Graphic
        {
            get { return _graphic; }
            set
            {
                if (_graphic != value)
                    foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
                    {
                        if (buttonBehaviour == null)
                            continue;
                        buttonBehaviour.Graphic = value;
                    }

                _graphic = value;
            }
        }

        public Transform Target
        {
            get { return _target; }
            set
            {
                if (_target != value)
                    foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
                    {
                        if (buttonBehaviour == null)
                            continue;

                        buttonBehaviour.Target = value;
                    }

                _target = value;
            }
        }

        public RectTransform Rect
        {
            get { return rect; }
            set
            {
                if (rect != value)
                    foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
                    {
                        if (buttonBehaviour == null)
                            continue;

                        buttonBehaviour.Rect = value;
                    }

                rect = value;
            }
        }

        public Text Text
        {
            get { return text; }
            set
            {
                if (text != value)
                    foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
                    {
                        if (buttonBehaviour == null)
                            continue;

                        buttonBehaviour.Text = value;
                    }

                text = value;
            }
        }

        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (_selected != value)
                {
                    _selected = value;
                    OnSelected();
                }
            }
        }

        public bool Toggle
        {
            get { return _toggle; }
            set { _toggle = value; }
        }

        [SerializeField]
        protected bool _toggle;

        [SerializeField]
		protected bool _selected = false;

        [SerializeField]
        protected bool _interactable = true;

        [SerializeField]
        protected Graphic _graphic;

        [SerializeField]
        protected Transform _target;

        [SerializeField]
        protected RectTransform rect;

        [SerializeField]
        protected Text text;

        public List<ButtonBehaviour> buttonBehaviours = new List<ButtonBehaviour>();
        public UnityEvent onClick;
        public UnityEvent onDown;
        public UnityEvent onUp;
        public UnityEvent onEnter;
        public UnityEvent onExit;
        public UnityEvent onBeginDrag;
        public UnityEvent onDrag;
        public UnityEvent onEndDrag;
        public BoolEvent onInteractable;

        protected virtual void Awake()
        {
            if (Target == null)
                Target = transform;

            if (Graphic == null)
                Graphic = Target.GetComponent<Graphic>();

            if (Graphic == null)
                Graphic = GetComponent<Graphic>();

            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                if (buttonBehaviour.UIButton == null)
                    buttonBehaviour.UIButton = this;

                buttonBehaviour.Graphic = _graphic;
                buttonBehaviour.Target = _target;
                buttonBehaviour.Rect = rect;
                buttonBehaviour.Text = text;
                buttonBehaviour.Ini();
            }

            //Fixme in Button v2
           /* ButtonBehaviour[] behaviours = GetComponentsInChildren<ButtonBehaviour>();

            foreach (ButtonBehaviour behaviour in behaviours)
            {
                behaviour.UIButton = this;
                //behaviour.Graphic = behaviour.GetComponent<Image>();
                behaviour.Text = behaviour.GetComponent<Text>();
                //behaviour.Target = behaviour.transform;
            }

            buttonBehaviours.AddRange(behaviours);*/

            SetInteractable(_interactable);
        }

        protected virtual void OnDestroy()
        {
            if (buttonBehaviours == null)
                return;

            foreach (ButtonBehaviour behaviours in buttonBehaviours)
            {
                if (behaviours != null)
                    Destroy(behaviours);
            }
        }

        protected virtual void OnSelected()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return;
#endif
            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                //Debug.Log(buttonBehaviour.Target.name);

                buttonBehaviour.OnSelected();
            }
        }

        protected virtual void SetInteractable(bool value)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return;
#endif
            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                if (buttonBehaviour.advancedSettings.UseInteractable)
                    buttonBehaviour.OnInteractable(value);
            }

            if (onInteractable != null)
                onInteractable.Invoke(value);
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
			if (_toggle)
				Selected = !Selected;

            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                if (buttonBehaviour.advancedSettings.UsePointerClick)
                    buttonBehaviour.OnPointerClick(eventData);
            }

            if (onClick != null && Interactable)
                onClick.Invoke();
        }

        public virtual void OnPointerDown(PointerEventData eventData = null)
        {
            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                if (buttonBehaviour.advancedSettings.UsePointerDown)
                    buttonBehaviour.OnPointerDown(eventData);
            }

            if (onDown != null && Interactable)
                onDown.Invoke();
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                if (buttonBehaviour.advancedSettings.UsePointerEnter)
                    buttonBehaviour.OnPointerEnter(eventData);
            }

            if (onEnter != null && Interactable)
                onEnter.Invoke();
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                if (buttonBehaviour.advancedSettings.UsePointerExit)
                    buttonBehaviour.OnPointerExit(eventData);
            }

            if (onExit != null && Interactable)
                onExit.Invoke();
        }

        public virtual void OnPointerUp(PointerEventData eventData = null)
        {
            foreach (ButtonBehaviour buttonBehaviour in buttonBehaviours)
            {
                if (buttonBehaviour == null)
                    continue;

                if (buttonBehaviour.advancedSettings.UsePointerUp)
                    buttonBehaviour.OnPointerUp(eventData);
            }

            if (onUp != null && Interactable)
                onUp.Invoke();
        }

        //Copy paste fix
#if UNITY_EDITOR
        public virtual void OnValidate()
        {
            if (buttonBehaviours == null)
                return;

            for(int x = 0; x < buttonBehaviours.Count; x++)
            {
                ButtonBehaviour behaviours = buttonBehaviours[x];

                if (behaviours != null)
                {
                    behaviours.hideFlags = HideFlags.HideInInspector;

                    if (behaviours.UIButton != this && behaviours.UIButton != null)
                    {
                        ButtonBehaviour copyBehaviour = (ButtonBehaviour)gameObject.AddComponent(buttonBehaviours[x].GetType());
                        EditorUtility.CopySerialized(behaviours, copyBehaviour);                       
                        copyBehaviour.UIButton = this;
                        copyBehaviour.hideFlags = HideFlags.HideInInspector;
                        buttonBehaviours[x] = copyBehaviour;
                    } 
                }
            }
        }
#endif
        public virtual void Reset()
        {
            if (buttonBehaviours == null)
                return;

            foreach (ButtonBehaviour behaviours in buttonBehaviours)
            {
                if (behaviours != null)
                    DestroyImmediate(behaviours);
            }
        }

        public void ToggleSelected()
        {
            Selected = !Selected;
        }

        public void SetButtonState(ButtonState buttonState, bool value)
        {
            switch (buttonState)
            {
                case ButtonState.DisableButton:
                        Interactable = value;
                    return;

                case ButtonState.SelectButton:
                        Selected = value;
                    return;

                case ButtonState.HideButton:
                    gameObject.SetActive(value);
                    return;
            }
        }
    }
}
