﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;



namespace RoyFramework.UI
{
    // No need to parent. Any translation will be applied externally to the droppable anyway. Might as well access its curDragable variable and apply the exact same transformations to it. This is to avoid layer rendering issues.

    public class UIDroppable : UIButton
    {
        [Space, Tooltip("Will use the size from Target")]
        public bool AutoSizeCollider = true;

        public override void Reset()
        {
            base.Reset();
            AutoSetup();
        }

        public BoxCollider2D myColl;

        bool colliderStay;

        // Carefull with this variable name. Is this the one attached or hover?
        public UIDraggable curDragable;

        protected override void Awake()
        {
            base.Awake();
            AutoSetup();
        }
        protected virtual void AutoSetup()
        {
            SetTarget();
            SetRect();

            SetCollider();
            SetImage();
        }
        protected virtual void SetTarget()
        {
            if (Target != null)
                return;
            Target = transform;
        }
        protected virtual void SetRect()
        {
            Rect = Target.transform as RectTransform;
            if (Rect == null)
                return;
        }

        protected virtual void SetCollider()
        {
            var t_gameObject = Target.gameObject;
            if (myColl == null)
                myColl = t_gameObject.GetComponent<BoxCollider2D>();
            if (myColl == null)
                myColl = t_gameObject.AddComponent<BoxCollider2D>();

            RectTransform temp = Target as RectTransform;
            if (AutoSizeCollider) myColl.size = temp.sizeDelta;
            myColl.isTrigger = true;

            Rigidbody2D body = t_gameObject.GetComponent<Rigidbody2D>();
            if (body == null)
                body = t_gameObject.AddComponent<Rigidbody2D>();

            body.isKinematic = true;
            body.collisionDetectionMode = CollisionDetectionMode2D.Discrete;
            body.constraints = RigidbodyConstraints2D.None;
        }
        protected virtual void SetImage()
        {
            if (Graphic != null)
                return;

            var t_gameObject = Target.gameObject;
            Graphic = t_gameObject.GetComponent<Image>();
            if (myColl == null)
                Graphic = t_gameObject.AddComponent<Image>();
        }


        public override void OnPointerClick(PointerEventData eventData)
        {

            base.OnPointerClick(eventData);

        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);

        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            //base.OnPointerEnter(eventData);

        }

        public override void OnPointerExit(PointerEventData eventData)
        {

            //if (!eventData.dragging)
            //   base.OnPointerExit(eventData);

        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);          
        }

        public virtual void OnTriggerEnter2D(Collider2D col)
        {
            curDragable = col.GetComponent<UIDraggable>();
            if (curDragable == null)
                curDragable = col.GetComponentInChildren<UIDraggable>();
        }


        public virtual void OnTriggerExit2D(Collider2D col)
        {
            curDragable = null;
        }


        // Remember that if moving this droppable externally, to also apply it to its "child". It's "assigned dragable".
        // Probably best to create a new move function though, this one seems outdated.
        public IEnumerator MoveToPosition(Transform moveTarget)
        {

            //myRect.anchoredPosition = lastPosition;

            //InTransition = true;

            Vector2 lastPosition = this.transform.position;
            Vector2 startPosition = moveTarget.transform.position;

            float startTime = Time.time;
            float journeyLength = Vector3.Distance(transform.position, lastPosition);

            while ((Vector2)transform.position != lastPosition)
            {
                float distCovered = (Time.time - startTime) /*speed*/;
                float fracJourney = distCovered / journeyLength;
                Vector3 temp_position = Vector3.Lerp(startPosition, lastPosition, fracJourney);
                moveTarget.transform.position = Vector3.Lerp(startPosition, lastPosition, fracJourney);
                if (curDragable != null)
                    curDragable.transform.position = temp_position;
                yield return new WaitForEndOfFrame();
            }

            Debug.Log("transition complete");

            moveTarget.transform.parent = this.transform;
            moveTarget.SetAsLastSibling();

            //InTransition = false;
            //like a special animation dellagate?
            //BreakTransition = false;
        }

        int Order()
        {
            return 1;
        }
    }
}
