﻿using UnityEngine;
using DG.Tweening;
using System;

namespace RoyFramework.UI
{
    public class MenuEffectLocation : MenuEffectTransform
    {
        protected Tweener tweener;

        protected override void IniEffect()
        {
            base.IniEffect();

            if (CloseEffect.enable)
            {
                if (CloseEffect.CoordinateType == CoordinateType.Local)
                    myTransform.localPosition = CloseEffect.Coordinate;
                else
                    myTransform.position = CloseEffect.Coordinate;           
            }
        }

        public override void PlayEffect(TransformEffect transformEffect)
        {
            if (!transformEffect.enable)
                return;

            SetActiveState(transformEffect.BeginState);

            if (tweener != null)
                tweener.Kill();

            if (transformEffect.CoordinateType == CoordinateType.Local)
                tweener = myTransform.DOLocalMove(transformEffect.Coordinate, transformEffect.Duration);
            else
                tweener = myTransform.DOMove(transformEffect.Coordinate, transformEffect.Duration);

            tweener.SetEase(transformEffect.Ease);
            tweener.SetUpdate(true);
            tweener.SetDelay(transformEffect.Delay);
            tweener.OnComplete(() => TransformEffectFinish(transformEffect));

            waitforEffectToFinish = transformEffect.WaitToFinish;
        }
    }
}