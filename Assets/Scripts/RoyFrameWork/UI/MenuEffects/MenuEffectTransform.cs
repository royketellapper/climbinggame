﻿using UnityEngine;
using DG.Tweening;
using System;

namespace RoyFramework.UI
{
    public abstract class MenuEffectTransform : MenuEffectBase
    {
        [System.Serializable]
        public class TransformEffect
        {
            public TransformEffect() { }
            public TransformEffect(ActiveState finishState)
            {
                FinishState = finishState;
            }

            public bool enable = true;

            //Animation
            public Ease Ease = Ease.Linear;
            public float Delay = 0;
            public float Duration = 1;
            public bool WaitToFinish;

            //ActiveState
            public ActiveState BeginState = ActiveState.Enable;
            public ActiveState FinishState = ActiveState.Disable;

            //Coordinate
            public CoordinateType CoordinateType;
            public Vector3 Coordinate;
        }

        public TransformEffect OpenEffect = new TransformEffect(ActiveState.None);
        public TransformEffect DisableEffect;
        public TransformEffect CloseEffect;

        public override bool WaitforEffectToFinish
        {
            get { return waitforEffectToFinish; }
        }
        protected bool waitforEffectToFinish;

        public override void MenuOpen()
        {
            PlayEffect(OpenEffect);
        }

        public override void MenuClose()
        {
            PlayEffect(CloseEffect);
        }

        public override void MenuDisable()
        {
            PlayEffect(DisableEffect);
        }

        public virtual void PlayEffect(TransformEffect transformEffect) { }

        protected virtual void TransformEffectFinish(TransformEffect Effect)
        {
            SetActiveState(Effect.FinishState);
            EffectFinish();
        }
    }
}
