﻿using UnityEngine;
using System;

namespace RoyFramework.UI
{
    public abstract class MenuEffectBase : MonoBehaviour, IMenuEffect
    {
        [MenuName]
        public string MenuId;
        public MenuInfo Menu { get; private set; }

        public virtual bool WaitforEffectToFinish
        {
            get { return false; }
        }
        public event Action<IMenuEffect> OnEffectFinish;

        protected Transform myTransform;

        protected virtual void Awake()
        {
            myTransform = transform;
            Menu = MenuManager.Instance.GetMenuInfo(MenuId);

            if (Menu == null)
                return;

            IniEffect();
        }

        protected virtual void IniEffect()
        {
            Menu.AddEffect(this);
        }

        protected virtual void OnDestroy()
        {
            if (Menu != null)
                Menu.RemoveEffect(this);
        }

        // ================ Callbacks ===================
        protected virtual void EffectFinish()
        {
            if (OnEffectFinish != null)
                OnEffectFinish(this);
        }

        protected virtual void SetActiveState(ActiveState state)
        {
            switch (state)
            {
                case ActiveState.Enable:
                    gameObject.SetActive(true);
                    break;

                case ActiveState.Disable:
                    gameObject.SetActive(false);
                    break;
            }
        }

        //================ Effect functions =============
        public abstract void MenuOpen();
        public abstract void MenuClose();
        public abstract void MenuDisable();
    }
}
