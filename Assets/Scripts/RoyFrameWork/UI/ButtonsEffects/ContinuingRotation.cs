﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace RoyFramework.UI
{
    public class ContinuingRotation : ButtonBehaviour
    {
        [System.Serializable]
        public class RotaionInfo
        {
            public bool Enable;
            public Vector3 Rotation;
        }

        public RotaionInfo Normal;
        public RotaionInfo Enter;
        public RotaionInfo Pressed;
        public RotaionInfo Disabled;
        public RotaionInfo Selected;
        protected bool _pressed;

        protected RotaionInfo GetNormalRotation()
        {
            if (!UIButton.Interactable)
                return Disabled;
            else if (UIButton.Selected)
                return (_pressed)? Pressed : Selected;
            else
                return (_pressed)? Pressed : Normal;
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            _pressed = false;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            _pressed = true;
        }        

        public override void OnPointerEnter(PointerEventData eventData)
        {
            _pressed = false;
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            _pressed = false;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            _pressed = false;
        }

        public void Update()
        {
            RotaionInfo currentRotaionInfo = GetNormalRotation();

            if (Target != null)
            {
                if (currentRotaionInfo.Enable)
                    Target.transform.Rotate(currentRotaionInfo.Rotation);
                else
                    Target.transform.rotation = Quaternion.Euler(currentRotaionInfo.Rotation);
            }
        }
    }
}
