﻿using UnityEngine;
using UnityEngine.EventSystems;
using RoyFramework.Audio;

namespace RoyFramework.UI
{
    public class Sound : ButtonBehaviour
    {
        public ButtonSound OverwriteSound;
        public AudioClip OnClick;
        public AudioClip OnUp;
        public AudioClip OnDown;
        public AudioClip OnEnter;
        public AudioClip OnExit;

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            if (OverwriteSound != null)
                AudioManager.Instance.PlayOneShot(OverwriteSound.OnClick, OverwriteSound.SoundType);
            else
                AudioManager.Instance.PlayOneShot(OnClick, 1, SoundType.UI);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            if (OverwriteSound != null)
                AudioManager.Instance.PlayOneShot(OverwriteSound.OnDown, OverwriteSound.SoundType);
            else
                AudioManager.Instance.PlayOneShot(OnDown, 1, SoundType.UI);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            if (OverwriteSound != null)
                AudioManager.Instance.PlayOneShot(OverwriteSound.OnEnter, OverwriteSound.SoundType);
            else
                AudioManager.Instance.PlayOneShot(OnEnter, 1, SoundType.UI);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            if (OverwriteSound != null)
                AudioManager.Instance.PlayOneShot(OverwriteSound.OnExit, OverwriteSound.SoundType);
            else
                AudioManager.Instance.PlayOneShot(OnExit, 1, SoundType.UI);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            if (OverwriteSound != null)
                AudioManager.Instance.PlayOneShot(OverwriteSound.OnUp, OverwriteSound.SoundType);
            else
                AudioManager.Instance.PlayOneShot(OnUp, 1 , SoundType.UI); ;
        }

        public void PlayClick()
        {
            if (OverwriteSound != null)
                AudioManager.Instance.PlayOneShot(OverwriteSound.OnClick, OverwriteSound.SoundType);
            else
                AudioManager.Instance.PlayOneShot(OnClick, 1, SoundType.UI);
        }
    }
}
