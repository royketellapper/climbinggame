﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace RoyFramework.UI
{
    public class ColorTint : ButtonBehaviour
    {
        public Color normalColor = Color.white;
        public Color highlightedColor = Color.white;
        public Color pressedColor = Color.gray;
        public Color disabledColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        public Color SelectedColor = Color.gray;
        public float fadeDuration = 0.1f;
        public Ease easetype = Ease.Linear;

        public override void OnInteractable(bool value)
        {
            if (Graphic == null)
                return;

            Graphic.DOColor((value) ? GetNormalColor() : disabledColor, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        protected Color GetNormalColor()
        {
            return (UIButton.Selected) ? SelectedColor : normalColor;
        }

        public override void OnSelected()
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

            Graphic.DOColor(GetNormalColor(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

            Graphic.DOColor(GetNormalColor(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

            SetDown();
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

            Graphic.DOColor(highlightedColor, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

            UnpressedState();
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

            SetUp();
        }

        public void SetDown()
        {
            Graphic.DOColor(pressedColor, fadeDuration)
             .SetEase(easetype)
             .SetUpdate(true);
        }

        public void SetUp()
        {
            Graphic.DOColor(GetNormalColor(), fadeDuration)
                           .SetEase(easetype)
                           .SetUpdate(true);
        }

        public void PressedState()
        {
            SetDown();
        }

        public void UnpressedState()
        {
            Graphic.DOColor(GetNormalColor(), fadeDuration)
               .SetEase(easetype)
               .SetUpdate(true);
        }
    }
}
