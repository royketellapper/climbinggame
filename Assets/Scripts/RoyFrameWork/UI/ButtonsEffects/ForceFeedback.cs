﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;

namespace RoyFramework.UI
{
    public class ForceFeedback : ButtonBehaviour
    {
        public bool onClick = true;
        public bool onUp;
        public bool onDown;

        public override void OnInteractable(bool value)
        {
            
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
#if ( !UNITY_EDITOR && !UNITY_STANDALONE)
            if (onClick)
                Handheld.Vibrate();
#endif
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
#if ( !UNITY_EDITOR && !UNITY_STANDALONE)
            if (onDown)
                Handheld.Vibrate();
#endif
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {

        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
#if ( !UNITY_EDITOR && !UNITY_STANDALONE)
            if (onUp)
                Handheld.Vibrate();
#endif
        }
    }
}
