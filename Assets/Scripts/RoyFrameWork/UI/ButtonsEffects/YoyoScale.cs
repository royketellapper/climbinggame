﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace RoyFramework.UI
{
    public class YoyoScale : ButtonBehaviour
    {
        public Vector3 NormalScale = new Vector3(0.3f, 0.3f, 0.3f);
        public Vector3 SelectedScale = new Vector3(0.1f, 0.1f, 0.1f);
        public float speed = 1;
        public float fadeInSpeed = 0.1f;

        protected bool disableScale;
        protected Vector3 defaultScale;
        protected Vector3 scale;
        protected float fadeTime = 1;

        public override void Ini()
        {
            defaultScale = Target.localScale;
        }

        public Vector3 GetScale()
        {
            return (UIButton.Selected) ? SelectedScale : NormalScale;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            disableScale = true;
            fadeTime = 0;
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            disableScale = true;
            fadeTime = 0;
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            disableScale = false;            
        }

        public void Update()
        {
            if (Target == null)
                return;

            if (!disableScale && UIButton.Interactable)
            {
                fadeTime += Time.unscaledDeltaTime * fadeInSpeed;
                fadeTime = Mathf.Clamp01(fadeTime);

                scale.x = defaultScale.x + Mathf.Cos(Time.unscaledTime * speed) * GetScale().x;
                scale.y = defaultScale.y + Mathf.Cos(Time.unscaledTime * speed) * GetScale().y;
                scale.z = defaultScale.z + Mathf.Cos(Time.unscaledTime * speed) * GetScale().z;
                Target.localScale = Vector3.Lerp(Target.localScale, scale, fadeTime);
            }
        }
    }
}