﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
namespace RoyFramework.UI
{
    public class RotationTween : ButtonBehaviour
    {
        public Vector3 normalRotation;
        public Vector3 highlightedRotation;
        public Vector3 pressedRotation;
        public Vector3 disabledRotation;
        public Vector3 SelectedRotation;

        public float fadeDuration = 0.1f;
        public Ease easetype = Ease.Linear;

        protected Vector3 GetNormalRotation()
        {
            return (UIButton.Selected) ? SelectedRotation : normalRotation;
        }

        public override void OnSelected()
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

            Target.DOLocalRotate(GetNormalRotation(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnInteractable(bool value)
        {
            Target.DOLocalRotate((value) ? GetNormalRotation() : disabledRotation, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            Target.DOLocalRotate(GetNormalRotation(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            SetDown();
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            Target.DOLocalRotate(highlightedRotation, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            Target.DOLocalRotate(GetNormalRotation(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            SetUp();
        }

        public void SetDown()
        {
            Target.DOLocalRotate(pressedRotation, fadeDuration)
               .SetEase(easetype)
               .SetUpdate(true);
        }

        public void SetUp()
        {
            Target.DOLocalRotate(GetNormalRotation(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }
    }
}
