﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace RoyFramework.UI
{
    public class TextColorTint : ButtonBehaviour
    {
        public Color normalColor = Color.white;
        public Color highlightedColor = Color.white;
        public Color pressedColor = Color.gray;
        public Color disabledColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        public Color SelectedColor = Color.gray;
        public float fadeDuration = 0.1f;
        public Ease easetype = Ease.Linear;

        protected Color GetNormalColor()
        {
            return (UIButton.Selected) ? SelectedColor : normalColor;
        }

        public override void OnSelected()
        {
            if (Text == null || !UIButton.Interactable)
                return;

            Text.DOColor(GetNormalColor(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnInteractable(bool value)
        {
            if (Text == null)
                return;

            Text.DOColor((value) ? GetNormalColor() : disabledColor, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (Text == null || !UIButton.Interactable)
                return;

            Text.DOColor(GetNormalColor(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (Text == null || !UIButton.Interactable)
                return;

            PressedState();
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (Text == null || !UIButton.Interactable)
                return;

            Text.DOColor(highlightedColor, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (Text == null || !UIButton.Interactable)
                return;

            Text.DOColor(GetNormalColor(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (Text == null || !UIButton.Interactable)
                return;

            UnpressedState();
        }

        public void PressedState()
        {
            Text.DOColor(pressedColor, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public void UnpressedState()
        {
            Text.DOColor(normalColor, fadeDuration)
               .SetEase(easetype)
               .SetUpdate(true);
        }
    }
}