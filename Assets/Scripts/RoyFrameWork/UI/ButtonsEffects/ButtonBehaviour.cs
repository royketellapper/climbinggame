﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RoyFramework.UI
{
    [System.Serializable]
    public abstract class ButtonBehaviour : MonoBehaviour
    {
        [HideInInspector]
        public UIButton UIButton;

        [HideInInspector]
        public Graphic Graphic;

        [HideInInspector]
        public Transform Target;

        [HideInInspector]
        public RectTransform Rect;

        [HideInInspector]
        public Text Text;

        [System.Serializable]
        public class ButtonSettings
        {
            public bool UseEnable = true;
            public bool UseDisable = true;
            public bool UseInteractable = true;
            public bool UsePointerClick = true;
            public bool UsePointerDown = true;
            public bool UsePointerEnter = true;
            public bool UsePointerExit = true;
            public bool UsePointerUp = true;
        }
        public ButtonSettings advancedSettings = new ButtonSettings();

        public virtual void Ini() { }

        public virtual void OnInteractable(bool value) { }

        public virtual void OnSelected() { }

        public virtual void OnPointerClick(PointerEventData eventData) { }

        public virtual void OnPointerDown(PointerEventData eventData = null) { }

        public virtual void OnPointerEnter (PointerEventData eventData) { }

        public virtual void OnPointerExit(PointerEventData eventData) { }

        public virtual void OnPointerUp(PointerEventData eventData = null) { }
    }
}
