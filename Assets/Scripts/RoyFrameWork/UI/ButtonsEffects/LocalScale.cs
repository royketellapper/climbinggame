﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace RoyFramework.UI
{
    public class LocalScale : ButtonBehaviour
    {
        public Vector3 normalScale = Vector3.one;
        public Vector3 highlightedScale = Vector3.one;
        public Vector3 pressedScale = new Vector3(0.9f, 0.9f, 0.9f);
        public Vector3 disabledScale = Vector3.one;
        public Vector3 SelectedScale =  new Vector3(0.9f, 0.9f, 0.9f);
        public float scaleByPercentage = 1;

        public float fadeDuration = 0.1f;
        public Ease easetype = Ease.Linear;

        private Vector3 ps = Vector3.zero;
        private Vector3 percentageScale
        {
            get
            {
                if (ps == Vector3.zero)
                    ps = SetRatio();

                return ps;
            }
        }


       /* private float wr = -1f;
        private float widthRatio
        {
            get
            {
                if (wr == -1f)
                {
                    wr = SetWidthRatio();                    
                }

                return wr;
            }
        } */

        private Vector3 SetRatio()
        {
            Vector3 ratio;

            if (Rect != null)
            {

                if (Rect.sizeDelta.y > Rect.sizeDelta.x)
                {
                    ratio = new Vector3(scaleByPercentage,
                        ((Rect.sizeDelta.x/Rect.sizeDelta.y)*(1.0f - scaleByPercentage)) + scaleByPercentage, 1.0f);
                }
                else
                {
                    ratio =
                        new Vector3(
                            ((Rect.sizeDelta.y/Rect.sizeDelta.x)*(1.0f - scaleByPercentage)) + scaleByPercentage,
                            scaleByPercentage, 1.0f);
                }
            }
            else
                ratio = Vector3.zero;

            return ratio;
                 
            //return ((Rect.sizeDelta.y / Rect.sizeDelta.x) * (1.0f - scaleByPercentage)) + scaleByPercentage;
        }

        protected Vector3 GetNormalScale()
        {
            return (UIButton.Selected) ? SelectedScale : normalScale;
        }

        public override void OnSelected()
        {
            if (Graphic == null || !UIButton.Interactable)
                return;

           // Debug.Log(gameObject.name + " wut!");

            Target.DOScale(GetNormalScale(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnInteractable(bool value)
        {
           // Debug.Log(gameObject.name + " wut!");

            Target.DOScale((value) ? GetNormalScale() : disabledScale, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;
           // Debug.Log(gameObject.name + " wut!");
            Target.DOScale(GetNormalScale(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;        

            SetDown();
        }        

        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;
            //Debug.Log(gameObject.name + " wut!");
            Target.DOScale(highlightedScale, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;
           // Debug.Log(gameObject.name + " wut!");
            Target.DOScale(GetNormalScale(), fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;        

            SetUp();
        }

        public void SetDown()
        {
            if (scaleByPercentage < 1.0f)
                Target.DOScale(percentageScale, fadeDuration)
                    .SetEase(easetype)
                    .SetUpdate(true);
            else
                Target.DOScale(pressedScale, fadeDuration)
                    .SetEase(easetype)
                    .SetUpdate(true);
        }

        public void SetUp()
        {
            Target.DOScale(GetNormalScale(), fadeDuration)
              .SetEase(easetype)
              .SetUpdate(true);
        }

        public void PressedState()
        {
           // Debug.Log(gameObject.name + " doscale!");
            Target.DOScale(pressedScale, fadeDuration)
                .SetEase(easetype)
                .SetUpdate(true);
        }

        public void UnpressedState()
        {
           // Debug.Log(gameObject.name + " Undoscale!");
            Target.DOScale(normalScale, fadeDuration)
               .SetEase(easetype)
               .SetUpdate(true);
        }
    }
}
