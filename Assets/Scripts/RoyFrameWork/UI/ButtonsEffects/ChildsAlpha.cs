﻿using DG.Tweening;
using UnityEngine.UI;
using UnityEngine;

namespace RoyFramework.UI
{
    public class ChildsAlpha : ButtonBehaviour
    {
        public float Duration = 1;
        public Ease Ease;

        [SerializeField]
        protected Graphic[] _graphics;
        protected Tween[] _tweens;

        public override void OnInteractable(bool value)
        {
            if (Graphic == null)
                return;

            if (_tweens == null)
                _tweens = new Tween[_graphics.Length];

            for (int i = 0; i < _graphics.Length; i++)
            {
                var graphic = _graphics[i];
                var tween = _tweens[i];

                if (tween != null)
                    tween.Kill();

                if (graphic != null)
                {
                    if (value)
                        _tweens[i] = graphic.DOFade(1, 0).SetUpdate(true).SetEase(Ease);
                    else
                        _tweens[i] = graphic.DOFade(0, 1).SetUpdate(true).SetEase(Ease);
                }
            }
        }
    }
}