﻿using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace RoyFramework.UI
{
    public class ActivateObjects : ButtonBehaviour
    {
        public enum IgnoreType
        {
            None,
            OnlyTrue,
            OnlyFalse
        }

        [System.Serializable]
        public class ActivateObjectInfo
        {
            public bool Invert;
            public IgnoreType IgnoreEvent;
            public BoolEvent Event;
        }

        //public ActivateObjectInfo[] OnClick;
        //public ActivateObjectInfo[] OnUp;
        //public ActivateObjectInfo[] OnDown;
        //public ActivateObjectInfo[] OnEnter;
        //public ActivateObjectInfo[] OnExit;
        public ActivateObjectInfo[] Selected;
        public ActivateObjectInfo[] Interactable;

        public override void Ini()
        {
            SetActive(Interactable, UIButton.Interactable);
            SetActive(Selected, UIButton.Selected);
        }

        protected void SetActive(ActivateObjectInfo[] ObjectInfo, bool value)
        {
            foreach (var item in ObjectInfo)
            {
                if (item.Event == null)
                    continue;

                SetActive(item, value);
            }
        }

        protected void SetActive(ActivateObjectInfo ObjectInfo, bool value)
        {
            switch (ObjectInfo.IgnoreEvent)
            {
                case IgnoreType.None:
                    ObjectInfo.Event.Invoke((ObjectInfo.Invert) ? !value : value);
                    break;
                case IgnoreType.OnlyTrue:
                    if (value)
                        ObjectInfo.Event.Invoke((ObjectInfo.Invert) ? !value : value);
                    break;
                case IgnoreType.OnlyFalse:
                    if (!value)
                        ObjectInfo.Event.Invoke((ObjectInfo.Invert) ? !value : value);
                    break;
            }
        }

        public override void OnInteractable(bool value)
        {
            SetActive(Interactable, value);
        }

        public override void OnSelected()
        {
            //SetActive(OnClick, false);
            //SetActive(OnUp, false);
            //SetActive(OnDown, false);
            //SetActive(OnEnter, false);
            //SetActive(OnExit, false);
            SetActive(Selected, UIButton.Selected);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            //SetActive(OnClick, true);
            //SetActive(OnUp, false);
            //SetActive(OnDown, false);
            //SetActive(OnEnter, false);
            //SetActive(OnExit, false);
            //SetActive(Selected, false);
    }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            //SetActive(OnClick, false);
            //SetActive(OnUp, false);
            //SetActive(OnDown, false);
            //SetActive(OnEnter, false);
            //SetActive(OnExit, false);
            //SetActive(Selected, false);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            //SetActive(OnClick, false);
            //SetActive(OnUp, false);
            //SetActive(OnDown, false);
            //SetActive(OnEnter, false);
            //SetActive(OnExit, false);
            //SetActive(Selected, false);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            //SetActive(OnClick, false);
            //SetActive(OnUp, false);
            //SetActive(OnDown, false);
            //SetActive(OnEnter, false);
            //SetActive(OnExit, false);
            //SetActive(Selected, false);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (!UIButton.Interactable)
                return;

            //SetActive(OnClick, false);
            //SetActive(OnUp, false);
            //SetActive(OnDown, false);
            //SetActive(OnEnter, false);
            //SetActive(OnExit, false);
            //SetActive(Selected, false);
        }
    }

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }
}
