﻿using UnityEngine;
using UnityEngine.UI;

using UnityEngine.EventSystems;
using System.Collections;



namespace RoyFramework.UI
{

    public class SpriteSwap : ButtonBehaviour
    {
        public Sprite NormalSprite;
        public Sprite HighlightedSprite;
        public Sprite PressedSprite;
        public Sprite DisabledSprite;
        public Sprite SelectedSprite;

        protected Sprite GetNormalSprite()
        {
            return (UIButton.Selected) ? SelectedSprite : NormalSprite;
        }

        public override void OnSelected()
        {
            Image image = (Image)Graphic;

            if (image == null || !UIButton.Interactable)
                return;

            image.sprite = GetNormalSprite();
        }

        public override void OnInteractable (bool value)
        {
            Image image = (Image)Graphic;

            if (image == null)
                return;

            image.sprite = (value) ? GetNormalSprite() : DisabledSprite;
        }

        public override void OnPointerClick (PointerEventData eventData)
        {
            Image image = (Image)Graphic;

            if (image == null || !UIButton.Interactable) 
                return;

            image.sprite = GetNormalSprite();
        }

        public override void OnPointerDown (PointerEventData eventData)
        {
            Image image = (Image)Graphic;

            if (image == null || !UIButton.Interactable)
                return;

            image.sprite = PressedSprite;
        }

        public override void OnPointerEnter (PointerEventData eventData)
        {
            Image image = (Image)Graphic;

            if (image == null || !UIButton.Interactable)
                return;

            image.sprite = HighlightedSprite;
        }

        public override void OnPointerExit (PointerEventData eventData)
        {
            Image image = (Image)Graphic;

            if (image == null || !UIButton.Interactable)
                return;

            image.sprite = GetNormalSprite();
        }

        public override void OnPointerUp (PointerEventData eventData)
        {
            Image image = (Image)Graphic;

            if (image == null || !UIButton.Interactable)
                return;

            image.sprite = GetNormalSprite();
        }
    }
}