﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

[ExecuteInEditMode, RequireComponent(typeof(CanvasRenderer))]
public class MeshCanvasRender : Graphic
{
    public Mesh mesh;
    public Vector3 Scale = Vector3.one;
    public float lol = 1;
    public int hoi = 0;

    public Mesh UpdateScale()
    {
        var TempMesh = UnityEngine.Object.Instantiate<Mesh>(mesh);

        var vertices = new Vector3[TempMesh.vertices.Length];

        Matrix4x4 mat = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Scale);

        for (var i = 0; i < vertices.Length; i++)
        {
            var vertex = mesh.vertices[i];
            vertex = mat.MultiplyPoint3x4(vertex); //((vertex - mesh.bounds.center) * scale);

            vertices[i] = vertex;
        }

        TempMesh.vertices = vertices;
        TempMesh.RecalculateBounds();

        return TempMesh;
    }


    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        Vector3[] normals = mesh.normals;
        Vector2[] uv = mesh.uv;

        List<UIVertex> vertexList = new List<UIVertex>(triangles.Length);

        UIVertex vertex;
        for (int i = 0; i < hoi; i++) 
        {
            vertex = new UIVertex();
            int triangle = triangles[i];

            vertex.position = ((vertices[triangle] - mesh.bounds.center) * lol);
            vertex.uv0 = uv[triangle];
            vertex.normal = normals[triangle];
            //vertex.tangent = mesh.tangents[0];
            //vertexList.Add(vertex);

            //if (i % 3 == 0)
            //  vertexList.Add(vertex);


           // if (i % 3 == 0)
           //     vh.AddTriangle(mesh.triangles[i], mesh.triangles[i + 1], mesh.triangles[i + 2]);

           vh.AddVert(vertex);
        }
       // vh.AddUIVertexQuad(vertexList.ToArray());

        //vh.FillMesh(mesh);
    }


    protected override void Awake()
    {
        base.Awake();
        GetComponent<CanvasRenderer>().SetMaterial(material, null);
    }

    /*
    public Material material;
    public float scale = 1f;

    public Vector3 Scale = Vector3.one;

    private CanvasRenderer canvasRenderer;

#if UNITY_EDITOR // only compile in editor
    private Mesh currentMesh;
    private Material currentMaterial;
    private float currentScale;
#endif

    public void Awake()
    {
        canvasRenderer = GetComponent<CanvasRenderer>();
    }

    public void OnEnable()
    {
        CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
        SetMesh();
    }

    public void OnDisable()
    {
        CanvasUpdateRegistry.UnRegisterCanvasElementForRebuild(this);


        canvasRenderer.Clear();
    }

#if UNITY_EDITOR // only compile in editor
    public void Update()
    {
        if (mesh != currentMesh || material != currentMaterial || !Mathf.Approximately(scale, currentScale))
        {
            SetMesh();
        }
    }
#endif

    public void SetMesh()
    {
        // clear the canvas renderer every time
        canvasRenderer.Clear();

#if UNITY_EDITOR // only compile in editor
        currentMesh = mesh;
        currentMaterial = material;
        currentScale = scale;
#endif

        if (mesh == null)
        {
            Debug.LogWarning("Mesh is null.");
            return;
        }
        else if (material == null)
        {
            Debug.LogWarning("Material is null.");
            return;
        }

        //List<UIVertex> list = ConvertMesh();

        canvasRenderer.SetMaterial(material, null);



        canvasRenderer.SetMesh(UpdateScale());
        //canvasRenderer.SetVertices(list);
    }

    public Mesh UpdateScale()
    {
        var TempMesh = UnityEngine.Object.Instantiate<Mesh>(currentMesh);

        var vertices = new Vector3[TempMesh.vertices.Length];

        Matrix4x4 mat = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Scale);

        for (var i = 0; i < vertices.Length; i++)
        {
            var vertex = mesh.vertices[i];
            vertex = mat.MultiplyPoint3x4(vertex); //((vertex - mesh.bounds.center) * scale);

            vertices[i] = vertex;
        }

        TempMesh.vertices = vertices;
        TempMesh.RecalculateBounds();

        return TempMesh;
    }

    public List<UIVertex> ConvertMesh()
    {
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        Vector3[] normals = mesh.normals;
        Vector2[] uv = mesh.uv;

        List<UIVertex> vertexList = new List<UIVertex>(triangles.Length);

        

        UIVertex vertex;
        for (int i = 0; i < triangles.Length; i++)
        {
            vertex = new UIVertex();
            int triangle = triangles[i];

            vertex.position = ((vertices[triangle] - mesh.bounds.center) * scale);
            vertex.uv0 = uv[triangle];
            vertex.normal = normals[triangle];

            vertexList.Add(vertex);

            if (i % 3 == 0)
                vertexList.Add(vertex);
        }

        return vertexList;
    }
    */
}
