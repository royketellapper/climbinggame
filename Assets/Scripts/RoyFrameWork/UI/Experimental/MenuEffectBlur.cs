﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    public class MenuEffectBlur : MenuScreenEffectBase
    {
        public float Intensity
        {
            get
            {
                return intensity;
            }
            set 
            {
                intensity = value;
                UpdateMaterial();
            }
        }
        protected float intensity;

        protected override void UpdateMaterial()
        {
            base.UpdateMaterial();
             
            if (material == null)
                return;

            material.SetColor("_Color", Color.Lerp(Color.white, color, color.a));
            material.SetFloat("_Intensity", intensity * color.a);
        }
    }
}
