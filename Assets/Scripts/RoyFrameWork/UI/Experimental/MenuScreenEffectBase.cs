﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    public abstract class MenuScreenEffectBase : Graphic
    {
        public Shader Shader; // Set this shader in the inspector

        public override Material material
        {
            get
            {
                if (m_Material == null)
                {
                    m_Material = new Material(Shader);
                    m_Material.hideFlags = HideFlags.DontSave;
                }
                return m_Material;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_Material != null)
                DestroyImmediate(m_Material);
        }

        public void FillScreen()
        {
            if (rectTransform == null)
                return;

            rectTransform.localScale = Vector3.one;
            rectTransform.localRotation = Quaternion.identity;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.pivot = Vector2.zero;
            rectTransform.sizeDelta = Vector2.zero;
            rectTransform.anchoredPosition = Vector2.zero;
            rectTransform.anchoredPosition3D = Vector3.zero;
        }
    }
}
