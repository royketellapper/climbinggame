﻿using UnityEngine;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    public class MenuEffectVignetting : MenuScreenEffectBase
    {
        public virtual float Intensity
        {
            get
            {
                return intensity;
            }
            set
            {
                intensity = value;
                UpdateMaterial();
            }
        }
        
        [SerializeField]
        protected float intensity;

        protected override void OnDisable()
        {
            base.OnDisable();

            if (m_Material != null)
                DestroyImmediate(m_Material);
        }

        protected float EaseOut(float t, float b, float c, float d)
        {
            return c * ((t = t / d - 1) * t * t + 1) + b;
        }

        protected override void UpdateMaterial()
        {
            base.UpdateMaterial();

            if (material == null)
                return;

            material.SetColor("_Color", color);
            float easeInOut = EaseOut(intensity, 0, 1, 1f);
            material.SetFloat("_Intensity", (1.0f / (1.0f - easeInOut) - 1.0f));
        }
    }
}
