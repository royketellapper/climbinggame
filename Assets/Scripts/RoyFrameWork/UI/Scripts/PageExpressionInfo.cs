﻿using UnityEngine;
using UnityEngine.UI;

namespace RoyFramework.UI
{
    [System.Serializable]
    public class PageExpressionInfo
    {
        [TextArea]
        public string PageExpression;
        public Text PageText;
    }
}