﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

namespace RoyFramework.UI
{
    [System.Serializable]
    public class MenuInfo
    {
        public string Id;
        public string Name;
        public Transform MenuRect;

        [Space]
        public float TimeScale = 1;
        public int MenuOrder = 0;
        public bool InitializeMenu;
        public bool IgnoreAnalyticsScreen;

        [Space]
        public bool OverrideSorting;
        public int CanvasSorting = 0;

        public MenuState MenuState { get; protected set; }
        public bool IsPlayingEffect { get; protected set; }

        [Space]
        public UnityEvent MenuOpen;
        public UnityEvent MenuDisable;
        public UnityEvent MenuClosed;

        public MenuManager MenuManager { get; set; }

        // ====================Play custom Menu Effects =====================
        public Action<MenuInfo> OnEffectFinish;

        protected class EffectsInfo
        {
            public IMenuEffect Effect;
            public bool WaitForEffectFinish = false;
        }

        //All registered effects
        protected List<EffectsInfo> effects = new List<EffectsInfo>();

        public void AddEffect(IMenuEffect effect)
        {
            if (effects.Find(e => e.Effect == effect) == null)
            {
                effect.OnEffectFinish += EffectFinish;
                EffectsInfo effectsInfo = new EffectsInfo();
                effectsInfo.Effect = effect;
                effects.Add(effectsInfo);
            }
        }

        public void RemoveEffect(IMenuEffect effect)
        {
            effect.OnEffectFinish -= EffectFinish;
            int index = effects.FindIndex(e => e.Effect == effect);

            if (index != -1)
                effects.RemoveAt(index);
        }

        public void SetMenuState(MenuState state)
        {
            if (MenuRect != null)
                MenuUtilitys.SetMenuInfo(MenuRect, this);

            // If the are no effects use default 
            if (effects.Count == 0)
            {
                if (MenuRect != null)                
                    MenuRect.gameObject.SetActive(state == MenuState.Open);

                switch (state)
                {
                    case MenuState.Open:
                        if (MenuRect != null)
                            MenuUtilitys.InterfaceFunctionCall<IMenuOpen>(MenuRect, "MenuOpen");

                        InvokeMenuState(MenuOpen);

                        if (MenuManager != null)
                        {
                            MenuManager.Canvas.overrideSorting = OverrideSorting;
                            MenuManager.Canvas.sortingOrder = CanvasSorting;
                        }

                        break;

                    case MenuState.Close:
                        if (MenuRect != null)
                            MenuUtilitys.InterfaceFunctionCall<IMenuOpen>(MenuRect, "MenuClose");

                        InvokeMenuState(MenuClosed);
                        break;

                    case MenuState.Disable:
                        if (MenuRect != null)
                            MenuUtilitys.InterfaceFunctionCall<IMenuDisable>(MenuRect, "MenuDisable");

                        InvokeMenuState(MenuDisable);
                        break;
                }                

                IsPlayingEffect = false;

                EffectFinishCallback();
            }
            else
            {
                MenuRect.gameObject.SetActive(true);

                bool WaitforEffect = false;
                foreach (EffectsInfo effectsInfo in effects)
                {
                    if (effectsInfo == null)
                        continue;

                    switch (state)
                    {
                        case MenuState.Open:
                            effectsInfo.Effect.MenuOpen();
                            MenuUtilitys.InterfaceFunctionCall<IMenuClose>(MenuRect, "MenuClose");
                            InvokeMenuState(MenuOpen); //<------- Fix me??

                            if (MenuOrder >= 0)
                                MenuRect.SetSiblingIndex(MenuOrder);

                            break;

                        case MenuState.Close:
                            effectsInfo.Effect.MenuClose();
                            MenuUtilitys.InterfaceFunctionCall<IMenuDisable>(MenuRect, "MenuDisable");
                            InvokeMenuState(MenuDisable); //<------- Fix me??
                            break;

                        case MenuState.Disable:
                            effectsInfo.Effect.MenuDisable();
                            MenuUtilitys.InterfaceFunctionCall<IMenuOpen>(MenuRect, "MenuOpen");
                            InvokeMenuState(MenuClosed); //<------- Fix me??
                            break;
                    }
                    effectsInfo.WaitForEffectFinish = effectsInfo.Effect.WaitforEffectToFinish;

                    if (effectsInfo.WaitForEffectFinish)
                        WaitforEffect = true;
                }

                IsPlayingEffect = (effects.Count == 0 || !WaitforEffect) ? false : true;
            }            

            MenuState = state;
        }

        /// ========= Call backs functions ===========
        protected void EffectFinish(IMenuEffect effect)
        {
            foreach (EffectsInfo effectsInfo in effects)
            {
                if (effectsInfo.Effect == effect)
                    effectsInfo.WaitForEffectFinish = effect.WaitforEffectToFinish;

                if (!effectsInfo.WaitForEffectFinish)
                    return;
            }
            IsPlayingEffect = false;

            EffectFinishCallback();
        }

        protected void InvokeMenuState(UnityEvent unityEvent)
        {
            if (unityEvent != null)
                unityEvent.Invoke();
        }

        protected void EffectFinishCallback()
        {
            if (OnEffectFinish != null)
                OnEffectFinish(this);
        }
    }
}
