﻿namespace RoyFramework.UI
{
    public enum ButtonState
    {
        None,
        DisableButton,
        SelectButton,
        HideButton
    }

    public enum MessageType
    {
        Default,
        Error,
        Warning,
        RateMe,
        Okay,
        ConfirmPurchase,
        YesNo,
        Custom1,
        Custom2,
        AchievementCompleted
    }

    public enum MenuState
    {
        Close,
        Disable,
        Open     
    }

    public enum CurrentWindowType
    {  
        Diasable,
        DiasableAll,
        Close,
        CloseAll,
        None
    }

    public enum ActiveState
    {
        None,
        Disable,
        Enable
    }

    public enum CoordinateType
    {
        Local,
        World
    }

    public enum CoordinatePositionsType
    {
        AnchorPos3D,
        LocalPosition,
        WorldPosition
    }
}