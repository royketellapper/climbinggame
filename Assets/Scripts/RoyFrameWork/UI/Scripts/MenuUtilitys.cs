﻿using System.Reflection;
using UnityEngine;

namespace RoyFramework.UI
{
    public static class MenuUtilitys
    {
        public static void SetMenuActive(this Transform trans, ActiveState state)
        {
            SetMenuActive(trans.gameObject, state);
        }

        public static void SetMenuActive(this GameObject obj, ActiveState state)
        {
            if (state != ActiveState.None)
                SetMenuActive(obj, state == ActiveState.Enable);
        }      

        public static void SetMenuActive(this Transform trans, bool value)
        {
            SetMenuActive(trans.gameObject, value);
        }

        public static void SetMenuActive(this GameObject obj, bool value)
        {
            if (obj == null || obj.activeSelf == value)
                return;

            if (value)
            {
                obj.SetActive(true);
                MenuUtilitys.InterfaceFunctionCall<IMenuOpen>(obj.transform, "MenuOpen");
            }
            else
            {
                MenuUtilitys.InterfaceFunctionCall<IMenuClose>(obj.transform, "MenuClose");
                obj.SetActive(false);
            }           
        }

        public static void SetMenuActive(this Transform trans, MenuState state)
        {
            SetMenuActive(trans.gameObject, state);
        }

        public static void SetMenuActive(this GameObject obj, MenuState state)
        {
            if (obj == null)
                return;

            switch (state)
            {
                case MenuState.Open:
                    obj.SetActive(true);
                    MenuUtilitys.InterfaceFunctionCall<IMenuOpen>(obj.transform, "MenuOpen");
                    return;

                case MenuState.Close:
                    MenuUtilitys.InterfaceFunctionCall<IMenuClose>(obj.transform, "MenuClose");
                    obj.SetActive(false);
                    return;

                case MenuState.Disable:
                    MenuUtilitys.InterfaceFunctionCall<IMenuDisable>(obj.transform, "MenuDisable");
                    obj.SetActive(false);
                    return;
            }
        }

        public static void InterfaceFunctionCall(Transform transform, MenuState state)
        {
            switch (state)
            {
                case MenuState.Open:
                    MenuUtilitys.InterfaceFunctionCall<IMenuOpen>(transform, "MenuOpen");    
                    return;

                case MenuState.Close:
                    MenuUtilitys.InterfaceFunctionCall<IMenuClose>(transform, "MenuClose");
                    return;

                case MenuState.Disable:
                    MenuUtilitys.InterfaceFunctionCall<IMenuDisable>(transform, "MenuDisable");
                    return;
            }
        }

        public static void InterfaceFunctionCall<T>(Transform transform, string methodName, object[] paratmeters = null) where T : class
        {
            T[] components = transform.GetComponents<T>();

            if (components == null)
                return;

            MethodInfo info = typeof(T).GetMethod(methodName);

            if (info == null)
                return;

            foreach (T comp in components)
            {
                if (comp != null)
                    info.Invoke(comp, paratmeters);
            }
        }

        public static void SetMenuInfo(Transform transform, MenuInfo value)
        {
            IMenuInfo[] components = transform.GetComponents<IMenuInfo>();

            if (components == null)
                return;

            foreach (IMenuInfo comp in components)
            {
                if (comp != null)
                    comp.MenuInfo = value;
            }
        }

        public static Vector3 GetPosition(this Transform trans, CoordinatePositionsType coordinateType)
        {
            switch (coordinateType)
            {
                case CoordinatePositionsType.AnchorPos3D:
                    RectTransform rect = trans as RectTransform;
                    return (rect != null)? rect.anchoredPosition3D : rect.localPosition;
                case CoordinatePositionsType.LocalPosition:
                    return trans.localPosition;
                case CoordinatePositionsType.WorldPosition:
                    return trans.position;
            }

            return trans.localPosition;
        }

        public static void SetPosition(this Transform trans, Vector3 pos, CoordinatePositionsType coordinateType)
        {
            switch (coordinateType)
            {
                case CoordinatePositionsType.AnchorPos3D:
                    RectTransform rect = trans as RectTransform;
                     if(rect != null)
                        rect.anchoredPosition3D = pos;
                    else
                        rect.localPosition = pos;
                    return;
                case CoordinatePositionsType.LocalPosition:
                    trans.localPosition = pos;
                    return;
                case CoordinatePositionsType.WorldPosition:
                    trans.position = pos;
                    return;
            }
        }
    }
}