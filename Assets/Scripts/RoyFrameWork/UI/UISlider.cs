﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

namespace RoyFramework.UI
{
    public class UISlider : Slider, IBeginDragHandler, IEndDragHandler
    {
        public ClickEvent onBeginDrag;
        public ClickEvent onDrag;
        public ClickEvent onEndDrag;

        public virtual void OnBeginDrag(PointerEventData eventData)
        {
            if (onBeginDrag != null && interactable)
                onBeginDrag.Invoke(eventData);
        }

        public override void OnDrag(PointerEventData eventData)
        {
            base.OnDrag(eventData);

            if (onDrag != null && interactable)
                onDrag.Invoke(eventData);
        }

        public virtual void OnEndDrag(PointerEventData eventData)
        {
            if (onBeginDrag != null && interactable)
                onBeginDrag.Invoke(eventData);

            value = Mathf.RoundToInt(value);

            if (onValueChanged != null)
                onValueChanged.Invoke(value);
        }

        protected override void Set(float input, bool sendCallback)
        {
            base.Set(input, false);
        }
    }

    public class ClickEvent : UnityEvent<PointerEventData> { }
}
