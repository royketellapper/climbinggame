﻿using UnityEngine;

namespace RoyFramework.UI
{
    public interface IMenuOpen
    {
        void MenuOpen();
    }
}