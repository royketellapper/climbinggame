﻿using UnityEngine;

namespace RoyFramework.UI
{
    public interface IMenuInfo
    {
        MenuInfo MenuInfo { set; }
    }
}