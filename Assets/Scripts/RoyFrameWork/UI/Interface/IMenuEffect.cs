﻿using UnityEngine;
using System;

namespace RoyFramework.UI
{
    public interface IMenuEffect
    {
        void MenuOpen();
        void MenuDisable();
        void MenuClose();

        event Action<IMenuEffect> OnEffectFinish;
        bool WaitforEffectToFinish { get; }
    }
}
