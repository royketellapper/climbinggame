﻿using UnityEngine;
using System.Linq;
using DG.Tweening;

namespace RoyFramework.UI
{
    public class UIEventSwitch : MonoBehaviour
    {
        [MenuName]
        public string[] Menu;
        public bool DisableMenuActive;
        public RectTransform Target;

        public Vector3 StartPos;
        public Vector3 EndPos;
        public float DurationPos = 1;
        public Ease EasePos;
        public float Delay;

        protected Tween _tweenPos;

        protected virtual void OnEnable()
        {
            foreach (var item in MenuManager.Instance.Menus)
            {
                if (Menu.Contains(item.Id))
                {
                    if (DisableMenuActive)
                    {
                        item.MenuOpen.AddListener(OpenMenu);
                        item.MenuClosed.AddListener(CloseMenu);
                        item.MenuDisable.AddListener(CloseMenu);
                    }
                    else
                    {
                        item.MenuOpen.AddListener(CloseMenu);
                        item.MenuClosed.AddListener(OpenMenu);
                        item.MenuDisable.AddListener(OpenMenu);

                    }
                }
            }
            MenuManager.Instance.WaitForInitialise(Initialise);
        }

        protected void Initialise()
        {
            if (MenuManager.Instance.CurrentMenu != null && Menu.Contains(MenuManager.Instance.CurrentMenu.Id) && DisableMenuActive)
            {
                Target.gameObject.SetActive(true);
                Target.anchoredPosition3D = StartPos;
            }
            else if (!DisableMenuActive)
            {
                Target.gameObject.SetActive(false);
                Target.anchoredPosition3D = EndPos;
            }
        }

        protected virtual void OnDisable()
        {
            foreach (var item in MenuManager.Instance.Menus)
            {
                if (Menu.Contains(item.Id))
                {
                    item.MenuOpen.RemoveListener(OpenMenu);
                    item.MenuClosed.RemoveListener(CloseMenu);
                    item.MenuDisable.RemoveListener(CloseMenu);
                }
            }
        }

        protected virtual void OpenMenu()
        {
            if (_tweenPos != null)
                _tweenPos.Kill();

            _tweenPos = Target.DOAnchorPos3D(EndPos, DurationPos)
                .SetEase(EasePos)
                .SetUpdate(true)
                .SetDelay(Delay)
                .OnComplete(() => Target.gameObject.SetActive(false));
        }

        protected virtual void CloseMenu()
        {
            if (_tweenPos != null)
                _tweenPos.Kill();

            _tweenPos = Target.DOAnchorPos3D(StartPos, DurationPos)
                .SetEase(EasePos)
                .SetDelay(Delay)
                .SetUpdate(true)
                .OnStart(() => Target.gameObject.SetActive(true));
        }
    }
}