﻿using DG.Tweening;
using System;
using UnityEngine;

namespace RoyFramework.UI
{
    public abstract class UIAnimationBase : MonoBehaviour
    {
        [System.Serializable]
        public class MenuNameInfo
        {
            [MenuName]
            public string Name;
            public bool Invert;
        }

        [SerializeField]
        protected MenuNameInfo[] _menus;
        [SerializeField]
        protected Transform _target;
        public Transform Target { get { return _target; } }

        public enum AnimationEventType
        {
            OpenClose,
            OpenCloseDisable
        }
        [SerializeField]
        protected AnimationEventType _animationEventType;

        [System.Serializable]
        public class TweenSettings
        {
            //Animation
            public Ease Ease;
            public bool IgnoreTimeScale = true;
            public float Delay = 0;
            public float Duration = 1;

            //ActiveState
            public ActiveState BeginState = ActiveState.Enable;
            public ActiveState EndState = ActiveState.Disable;

            //Coordinate
            //public CoordinateType CoordinateType;
            //public Vector3 Coordinate;

            public virtual void SetTweenSettings(UIAnimationBase animationsBase)
            {
                animationsBase._tween.SetEase(Ease);
                animationsBase._tween.SetUpdate(IgnoreTimeScale);
                animationsBase._tween.SetDelay(Delay);
                animationsBase._tween.OnStart(()=> animationsBase.OnStart(this));
                animationsBase._tween.OnComplete(() => animationsBase.OnComplete(this));
            }
        }

        protected Tween _tween;

        protected MenuNameInfo GetMenu(string id)
        {
            return Array.Find<MenuNameInfo>(_menus, a => a.Name == id);
        }

        protected virtual void AddListeners(TweenSettings menuOpen, TweenSettings menuClose, TweenSettings menuDisabled)
        {
            foreach (var item in MenuManager.Instance.Menus)
            {
                MenuNameInfo info = GetMenu(item.Id);

                if (info != null)
                {
                    switch (_animationEventType)
                    {
                        case AnimationEventType.OpenClose:
                            if (info.Invert)
                            {
                                item.MenuOpen.AddListener(CloseMenu);
                                item.MenuClosed.AddListener(OpenMenu);
                                item.MenuDisable.AddListener(OpenMenu);
                            }
                            else
                            {
                                item.MenuOpen.AddListener(OpenMenu);
                                item.MenuClosed.AddListener(CloseMenu);
                                item.MenuDisable.AddListener(CloseMenu);
                            }
                            break;
                        case AnimationEventType.OpenCloseDisable:
                            if (info.Invert)
                            {
                                item.MenuOpen.AddListener(CloseMenu);
                                item.MenuClosed.AddListener(OpenMenu);
                                item.MenuDisable.AddListener(DisableMenu);
                            }
                            else
                            {
                                item.MenuOpen.AddListener(OpenMenu);
                                item.MenuClosed.AddListener(CloseMenu);
                                item.MenuDisable.AddListener(DisableMenu);
                            }
                            break;
                    }
                }
            }
        }

        protected virtual void RemoveListeners(TweenSettings menuOpen, TweenSettings MenuDisabled, TweenSettings MenuClose)
        {
            foreach (var item in MenuManager.Instance.Menus)
            {
                MenuNameInfo info = GetMenu(item.Id);

                if (info != null)
                {
                    switch (_animationEventType)
                    {
                        case AnimationEventType.OpenClose:
                            item.MenuOpen.RemoveListener(OpenMenu);
                            item.MenuClosed.RemoveListener(CloseMenu);
                            item.MenuDisable.RemoveListener(CloseMenu);
                            break;
                        case AnimationEventType.OpenCloseDisable:
                            item.MenuOpen.RemoveListener(OpenMenu);
                            item.MenuClosed.RemoveListener(CloseMenu);
                            item.MenuDisable.RemoveListener(DisableMenu);
                            break;
                    }
                }
            }
        }

        protected virtual void OnEnable()
        {
            MenuManager.Instance.WaitForInitialise(InitialiseEnable);
        }

        protected virtual void OnDisable()
        {
            if (_tween != null)
                _tween.Kill();
        }

        protected virtual void OnStart(TweenSettings tweenSettings)
        {
            _target.SetMenuActive(tweenSettings.BeginState);
        }

        protected virtual void OnComplete(TweenSettings tweenSettings)
        {
            _target.SetMenuActive(tweenSettings.EndState);
        }

        protected virtual void InitialiseEnable() { }

        protected virtual void OpenMenu() { }

        protected virtual void CloseMenu() { }

        protected virtual void DisableMenu() { }

        protected virtual void SimpleMenu() { }
    }
}