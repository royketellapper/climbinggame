﻿using UnityEngine;
using DG.Tweening;
using System.Linq;

namespace RoyFramework.UI
{
    public class UIAnimationPosition : UIAnimationBase
    {
        [System.Serializable]
        public class PositionTweenSettings : TweenSettings
        {
            public Vector3 Position;
        }

        [SerializeField]
        protected PositionTweenSettings _openTweenSettings;
        [SerializeField]
        protected PositionTweenSettings _closeTweenSettings;
        [SerializeField]
        protected PositionTweenSettings _disableTweenSettings;
        [SerializeField]
        protected CoordinatePositionsType _coordinateType;

        protected override void OnEnable()
        {
            base.OnEnable();
            AddListeners(_openTweenSettings, _closeTweenSettings, _disableTweenSettings);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveListeners(_openTweenSettings, _closeTweenSettings, _disableTweenSettings);
        }

        protected override void InitialiseEnable()
        {
            //FIX ME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (MenuManager.Instance != null && MenuManager.Instance.CurrentMenu != null)
            {
                MenuNameInfo info = GetMenu(MenuManager.Instance.CurrentMenu.Id);

                if (MenuManager.Instance.CurrentMenu != null && info != null)// && DisableMenuActive)
                {
                    //Target.gameObject.SetActive(true);
                    //Target.SetPosition(_openTweenSettings.Position, _coordinateType);
                    OpenMenu();
                }
                else if (info != null && info.Invert)
                {
                    //Target.gameObject.SetActive(false);
                    //Target.SetPosition(_closeTweenSettings.Position, _coordinateType);
                    //OpenMenu();
                }
            }
        }

        protected override void OpenMenu()
        {
            base.OpenMenu();
            SetTween(_openTweenSettings);
        }

        protected override void CloseMenu()
        {
            base.CloseMenu();
            SetTween(_closeTweenSettings);
        }

        protected override void DisableMenu()
        {
            base.DisableMenu();
            SetTween(_disableTweenSettings);
        }

        protected virtual void SetTween(PositionTweenSettings positionTweenSettings)
        {
            if (_tween != null)
                _tween.Kill();

            switch (_coordinateType)
            {
                case CoordinatePositionsType.AnchorPos3D:
                    RectTransform rect = _target as RectTransform;

                    if (rect != null)
                        _tween = rect.DOAnchorPos3D(positionTweenSettings.Position, positionTweenSettings.Duration);
                    else
                        _tween = _target.DOLocalMove(positionTweenSettings.Position, positionTweenSettings.Duration);
                    break;
                case CoordinatePositionsType.LocalPosition:
                    _tween = _target.DOLocalMove(positionTweenSettings.Position, positionTweenSettings.Duration);
                    break;
                case CoordinatePositionsType.WorldPosition:
                    _tween = _target.DOMove(positionTweenSettings.Position, positionTweenSettings.Duration);
                    break;
            }

            positionTweenSettings.SetTweenSettings(this);
        }
    }
}