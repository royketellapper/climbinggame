﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

namespace RoyFramework.UI
{
    public class UIDraggable : UIButton
    {
        public override void Reset()
        {
            base.Reset();
            AutoSetup();
        }        

        [HideInInspector]
        public float _speed = 2;
        public bool SnapToCursor = false;

        public BoxCollider2D myColl;
        protected Vector3 offset = new Vector3();
        [HideInInspector]
        public bool pointerDown;
        [HideInInspector]
        public bool Dragging;

        public bool useParent;
        [HideInInspector]
        public Camera UICamera;
        public Canvas ParentCanvas;

        public bool UseDragControls = true;

        protected override void Awake()
        {
            base.Awake();

            if (UICamera == null)
            {
                UICamera = Camera.main;
                //if (Application.isEditor) Debug.LogError(DebugTools.UnsetField(this.GetType(), gameObject, "UICamera", UICamera));
            }

            AutoSetup();
        }
        protected virtual void AutoSetup()
        {
            SetTarget();
            SetRect();

            SetColliders();
            SetImage();
        }

        protected virtual void SetTarget()
        {
            if (Target != null)
                return;
            Target = transform;
        }

        protected virtual void SetRect()
        {
            Rect = Target.transform as RectTransform;
            if (Rect == null)
                return;
        }
        protected virtual void SetColliders()
        {
            if (myColl != null)
                return;

            var t_gameObject = Target.gameObject;
            myColl = t_gameObject.GetComponent<BoxCollider2D>();
            if (myColl == null)
                myColl = t_gameObject.AddComponent<BoxCollider2D>();

            RectTransform temp = Target as RectTransform;
            myColl.size = temp.sizeDelta;
        }
        protected virtual void SetImage()
        {
            if (Graphic != null)
                return;

            var t_gameObject = Target.gameObject;
            Graphic = t_gameObject.GetComponent<Image>();
            if (Graphic == null)
                Graphic = t_gameObject.AddComponent<Image>();
        }

        protected virtual void OnEnable()
        {
            if (Target == null)
            {
                Target = transform;
              //  Debug.LogError(DebugTools.UnsetField(this.GetType(), gameObject, "Target", Target));
            }
        }
        protected virtual void OnDisable()
        {
        }
     
        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!Interactable)
                return;

            base.OnPointerClick(eventData);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (!Interactable || Target == null || eventData.position == null)
                return;

            base.OnPointerDown(eventData);

            if (Target == null)
                return;

            pointerDown = true;

            offset.x = eventData.position.x - Target.position.x;
            offset.y = eventData.position.y - Target.position.y;
            offset.z = 0;

            Debug.Log("Down!");
            StartCoroutine(SetDraggedPosition(eventData));
        }

        //un used on android. (set in inspector)
        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (!Interactable)
                return;

            base.OnPointerEnter(eventData);
        }

        //un used on android. (set in inspector)
        public override void OnPointerExit(PointerEventData eventData)
        {
            if (!Interactable)
                return;

            base.OnPointerExit(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (!Interactable)
                return;

            base.OnPointerUp(eventData);

            pointerDown = false;
        }

        protected virtual IEnumerator SetDraggedPosition(PointerEventData eventData)
        {
            DragInitiate(eventData);
            while (pointerDown && UseDragControls)
            {
                DragUpdate(eventData);

                /*if (SnapToCursor)
                    Target.position = eventData.position;
                if (!SnapToCursor)
                    Target.position = new Vector3(eventData.position.x - offset.x, eventData.position.y - offset.y, Target.position.z);*/

                yield return new WaitForEndOfFrame();
            }
            DragEnd(eventData);
            yield return null;
        }      

        protected virtual void DragInitiate(PointerEventData eventData) { }
        protected virtual void DragStart() { }
        protected virtual void DragUpdate(PointerEventData eventData)
        {
            if (SnapToCursor)
                Target.position = eventData.position;
            if (!SnapToCursor)
                Target.position = new Vector3(eventData.position.x - offset.x, eventData.position.y - offset.y, Target.position.z);          
        }
        protected virtual void DragEnd(PointerEventData eventData) { }

    
    }
}

