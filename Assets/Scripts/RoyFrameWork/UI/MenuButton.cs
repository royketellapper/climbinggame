﻿using UnityEngine;

namespace RoyFramework.UI
{
    [RequireComponent(typeof(UIButton))]
    public class MenuButton : MonoBehaviour
    {
        public enum ButtonType
        {
            OpenMenu,
            CloseMenu,
            CloseAllMenus,
            CloseCurrentMenu
        }

        [MenuName]
        public string MenuId;
        public ButtonType Type;
        public bool CloseParentMenu = true;
        public bool SaveMenuToHistory = true;
        public bool CloseStartupMenu;

        public KeyCode Hotkey;
        public ButtonState ButtonActiveState;
        public UIButton UIButton { get; protected set; }      
        
        public virtual void OnPointerClick()
        {
            if (MenuManager.Instance == null)
                return;

            switch (Type)
            {
                case ButtonType.OpenMenu:
                    MenuManager.Instance.OpenMenu(MenuId, CloseParentMenu, SaveMenuToHistory);
                    return;

                case ButtonType.CloseMenu:
                    MenuManager.Instance.CloseMenu(MenuId);
                    return;

                case ButtonType.CloseAllMenus:
                    MenuManager.Instance.CloseAllMenus(CloseStartupMenu);
                    return;

                case ButtonType.CloseCurrentMenu:
                    MenuManager.Instance.CloseCurrentMenu(CloseStartupMenu);
                    return;
            }
        }

        protected virtual void Awake()
        {
            UIButton = GetComponent<UIButton>();

            if (MenuManager.Instance != null && ButtonActiveState != ButtonState.None)
            {
                MenuManager.Instance.OnMenuChanged += OnMenuChanged;
                OnMenuChanged(MenuManager.Instance.CurrentMenu);
            }         
        }

        protected virtual void OnEnable()
        {
            if (UIButton != null)
                UIButton.onClick.AddListener(OnPointerClick);
        }

        protected virtual void OnDisable()
        {
            if (UIButton != null)
                UIButton.onClick.RemoveListener(OnPointerClick);
        }

        protected virtual void OnMenuChanged(MenuInfo menuInfo)
        { 
            if (Type == ButtonType.CloseCurrentMenu)
            {
                if (MenuManager.Instance.HistoryCount == 1 && menuInfo.Id == MenuManager.Instance.StartupMenu && !CloseStartupMenu)
                    UIButton.SetButtonState(ButtonActiveState, false);
                else
                    UIButton.SetButtonState(ButtonActiveState, menuInfo != null);
            }
            else 
            {
                if (menuInfo != null)
                    UIButton.SetButtonState(ButtonActiveState, menuInfo.Id == MenuId);
                else
                    UIButton.SetButtonState(ButtonActiveState, false);
            }
        }

        protected virtual void OnDestroy()
        {
            if (MenuManager.Instance != null)
                MenuManager.Instance.OnMenuChanged -= OnMenuChanged;
        }

        protected virtual void Update()
        {
            if (Input.GetKeyDown(Hotkey))
                OnPointerClick();
        }
    }
}
