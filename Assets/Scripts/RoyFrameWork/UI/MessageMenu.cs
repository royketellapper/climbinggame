﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

namespace RoyFramework.UI
{
    public class MessageMenu : MonoBehaviour, IMenuClose
    {
        public Text TitelText;
        public Text Text;
        public UIButton[] ConfirmButton;
        public UIButton[] CancelButton;
        public MessageType MessageType;
        protected UnityEvent OnConformClick;
        protected UnityEvent OnCancelClick;
        public string MenuId { get; set; }

        public virtual string Titel
        {
            get
            {
                if (TitelText == null)
                    return "";
                return TitelText.text;
            }
            set
            {
                if (TitelText == null)
                    return;

                TitelText.text = value;
            }
        }

        public void SetConfirmButtonText(string text)
        {
            foreach (UIButton button in ConfirmButton)
            {
                if (button.Text != null)
                    button.Text.text = text;
            }
        }

        public void SetCancelButtonText(string text)
        {
            foreach (UIButton button in CancelButton)
            {
                if (button.Text != null)
                    button.Text.text = text;
            }
        }

        public virtual string Message
        {
            get
            {
                if (Text == null)
                    return "";
                return Text.text;
            }
            set
            {
                if (Text == null)
                    return;
                Text.text = value;
            }
        }

        //================= Functions ===================
        public virtual void ConfirmButtonClick()
        {
            if (OnConformClick != null)
                OnConformClick.Invoke();

            MenuManager.Instance.CloseMenu(MenuId);
        }

        public virtual void CancelButtonClick()
        {
            if (OnCancelClick != null)
                OnCancelClick.Invoke();

            MenuManager.Instance.CloseMenu(MenuId);
        }

        protected virtual void ButtonCallBack(UIButton[] buttons, UnityEvent OnClick)
        {
            if (buttons == null)
                return;

            foreach (UIButton button in buttons)
            {
                if (button == null)
                    continue;

                button.onClick.AddListener(() =>
                {
                    if (OnClick != null)
                        OnClick.Invoke();

                    ClearEvents();

                    MenuManager.Instance.CloseMenu(MenuId);
                });
            }
        }

        public void MenuClose()
        {
            ClearEvents();
        }

        protected virtual void ClearEvents()
        {
            if (OnConformClick != null)
                OnConformClick.RemoveAllListeners();

            if (OnCancelClick != null)
                OnCancelClick.RemoveAllListeners();
        }

        public virtual void Initialize(Action onConformClick, Action onCancelClick)
        {
            ClearEvents();
            OnConformClick = OnConformClick ?? new UnityEvent();
            OnCancelClick = OnCancelClick ?? new UnityEvent();         
            OnConformClick.AddListener(()=> 
            {
                if (onConformClick != null)
                    onConformClick();
            });

            OnCancelClick.AddListener(() =>
            {
                if (onCancelClick != null)
                    onCancelClick();
            });
            ButtonCallBack(ConfirmButton, OnConformClick);
            ButtonCallBack(CancelButton, OnCancelClick);
        }
    }
}
