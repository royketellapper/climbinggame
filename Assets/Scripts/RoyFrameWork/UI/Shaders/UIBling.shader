﻿Shader "Vasco/UI/UIBling"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_BlingColor("Bling Color", Color) = (1,1,1,1)
		_time("Time", Float) = 0
		_Scale("Bing Scale", Float) = 0

		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255

		_ColorMask("Color Mask", Float) = 15


		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip("Use Alpha Clip", Float) = 0
	}

		SubShader
	{
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
	}

		Stencil
	{
		Ref[_Stencil]
		Comp[_StencilComp]
		Pass[_StencilOp]
		ReadMask[_StencilReadMask]
		WriteMask[_StencilWriteMask]
	}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest[unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask[_ColorMask]

		Pass
	{
		Name "Default"
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 2.0

#define PI 3.1415926535897932384626433832795

#include "UnityCG.cginc"
#include "UnityUI.cginc"

#pragma multi_compile __ UNITY_UI_ALPHACLIP

		struct appdata_t
	{
		float4 vertex   : POSITION;
		float4 color    : COLOR;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex   : SV_POSITION;
		fixed4 color : COLOR;
		half2 texcoord  : TEXCOORD0;
		float4 worldPosition : TEXCOORD1;
	};

	half4 _Color;
	half4 _BlingColor;
	fixed4 _TextureSampleAdd;
	float4 _ClipRect;	
	float _time;
	float _Scale;

	v2f vert(appdata_t IN)
	{
		v2f OUT;
		OUT.worldPosition = IN.vertex;
		OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

		OUT.texcoord = IN.texcoord;

#ifdef UNITY_HALF_TEXEL_OFFSET
		OUT.vertex.xy += (_ScreenParams.zw - 1.0) * float2(-1,1) * OUT.vertex.w;
#endif

		OUT.color = IN.color * _Color;
		return OUT;
	}

	sampler2D _MainTex;

	fixed4 frag(v2f IN) : SV_Target
	{
		//half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;
		//color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);

		half4 img = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;
		float bling = sin(clamp(_time + (IN.texcoord.x * PI) * _Scale, 0, PI));
		img = lerp(img, half4(_BlingColor.xyz,1.0), bling * img.a * _BlingColor.a);
		img.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);


		//half4 c = tex2D(_MainTex, IN.uv_MainTex);
		//float bling = sin(clamp(_time + (IN.uv_MainTex.x * PI) * _Scale, 0, PI));
		//o.Albedo = lerp(c, _Color, bling * _Color.a);
		//o.Alpha = c.a;

#ifdef UNITY_UI_ALPHACLIP
		clip(img.a - 0.001);
#endif

		return img;
	}
		ENDCG
	}
	}
}
