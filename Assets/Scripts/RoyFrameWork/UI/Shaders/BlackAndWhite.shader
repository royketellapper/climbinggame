﻿Shader "Unlit/BlackAndWhite"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Opaque" }
		LOD 100
		GrabPass{ Tags{ "LightMode" = "Always" } }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 uvgrab : TEXCOORD0;
				float2 uv : TEXCOORD1;
				float4 color : TEXCOORD2;
				float4 vertex : SV_POSITION;		
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _GrabTexture;
			float4 _GrabTexture_TexelSize;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				#if UNITY_UV_STARTS_AT_TOP
				float scale = -1.0;
				#else
				float scale = 1.0;
				#endif
				o.uvgrab = ComputeGrabScreenPos(o.vertex);
				o.color = v.color;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2Dproj(_GrabTexture, i.uvgrab);
				
				fixed4 bw = (col.x + col.y + col.z) / 3;
				bw *= fixed4(i.color.xyz, 1); 
				bw = lerp (col, bw, i.color.a);
				col = lerp(col, bw, tex2D(_MainTex, i.uv).a);
				return col;
			}
			ENDCG
		}
	}
}
