// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "VascoGames/UI/MenuVignetting"
{
	Properties
	{
		_Intensity("Intensity", float) = 1
		_Color("Main Color", Color) = (1,1,1,1)
	}
	Category
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Opaque" }

		SubShader
		{
			GrabPass{ Tags{ "LightMode" = "Always" } }

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest

				#include "UnityCG.cginc"

				sampler2D _GrabTexture;
				fixed4 _Color;
				half _Intensity;

				struct appdata
				{
					float4 vertex : POSITION;
					float2 texcoord: TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex : SV_POSITION;
					float4 uvgrab : TEXCOORD0;
					float2 uv : TEXCOORD1;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);

			#if UNITY_UV_STARTS_AT_TOP
					float scale = -1.0;
			#else
					float scale = 1.0;
			#endif
					o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y * scale) + o.vertex.w) * 0.5;
					o.uvgrab.zw = o.vertex.zw;
					o.uv = v.texcoord;
					return o;
				}

				half4 frag(v2f i) : COLOR
				{
					half2 coords = (i.uv - 0.5) * 2.0;
					half coordDot = dot(coords,coords);
					float mask = 1.0 - coordDot * _Intensity * 0.1;
					half4 screenTex = tex2D(_GrabTexture, i.uvgrab);

					screenTex = lerp(_Color.rgba,screenTex , lerp(1 , clamp(mask, 0, 1), _Color.a));

					return  screenTex;
				}
				ENDCG
			}
		}
	}
	FallBack off
}
