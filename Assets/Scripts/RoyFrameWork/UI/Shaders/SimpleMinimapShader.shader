﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "VascoGames/SimpleMinimapShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			
			void vert (float4 vertex : POSITION, inout float2 uv : TEXCOORD0, out float4 outVertex : SV_POSITION )
			{
				outVertex = UnityObjectToClipPos(vertex);
				uv = uv;
			}
			
			fixed4 frag (float2 uv : TEXCOORD0) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, uv);
				return col;
			}
			ENDCG
		}
	}
}
