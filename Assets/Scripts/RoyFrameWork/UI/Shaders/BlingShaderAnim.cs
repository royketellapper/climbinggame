﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlingShaderAnim : MonoBehaviour
{
    public Material Mat;
    public float Speed = 1;
    public float Max = 1;
    public float Min = 0;

    protected float _time = 0;

	void Update ()
    {
        if (Mat != null)
        {
            _time += Time.deltaTime * Speed;
            _time = Mathf.Repeat(_time, Max);       
            Mat.SetFloat("_time", Min + _time);
        }
    }
}
