﻿using RoyFramework;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using RoyFramework.UI;
using VascoGamesEditor.UIEditor;
using VascoGamesEditor.Utility;

namespace RoyFramework.UIEditor
{
    [CustomEditor(typeof(UIButton), true), CanEditMultipleObjects] // CanEditMultipleObjects
    public class UIButtonEditor : Editor
    {
        protected SerializedProperty onClickProperty;
        protected SerializedProperty onDownProperty;
        protected SerializedProperty onUpProperty;
        protected SerializedProperty onEnterProperty;
        protected SerializedProperty onExitProperty;

        protected bool onClickToggle;
        protected bool onDownToggle;
        protected bool onUpToggle = true;
        protected bool onEnterToggle;
        protected bool onExitToggle;

        protected List<string> behaviourNames;
        protected List<Type> transitionTypes;
        protected int selectedTransition;
        protected class BehavioursMenu
        {
            public string name;
            public bool foldout = false;
            public ButtonBehaviour buttonBehaviour;
            public SerializedObject serializedObject;
        }
        protected List<BehavioursMenu> behavioursMenus = new List<BehavioursMenu>();
        protected string[] _ignoreProperty;

        protected virtual void OnEnable()
        {
            _ignoreProperty = new string[17] { "buttonBehaviours", "onClick", "onDown", "onUp", "onEnter", "onExit", "onBeginDrag", "onDrag", "onEndDrag", "onInteractable", "_interactable", "_selected", "_toggle", "_graphic", "_target", "text", "rect" };

            UIButton button = (UIButton)target;
            behavioursMenus.Clear();

            onClickProperty = serializedObject.FindProperty("onClick");
            onDownProperty = serializedObject.FindProperty("onDown");
            onUpProperty = serializedObject.FindProperty("onUp");
            onEnterProperty = serializedObject.FindProperty("onEnter");
            onExitProperty = serializedObject.FindProperty("onExit");
          
            transitionTypes = SubClassesHelper.GetSubclasses<ButtonBehaviour>();
            behaviourNames = SubClassesHelper.GetTypeNames(transitionTypes);

            if (button.Target == null)
                button.Target = button.transform;

            if (button.Rect == null)
                button.Rect = button.GetComponent<RectTransform>();

            if (button.Graphic == null)
                button.Graphic = button.Target.GetComponent<Image>();

            if (button.Graphic == null)
                button.Graphic = button.GetComponent<Image>();

            button.buttonBehaviours.RemoveAll(item => item == null);

            for (int x = 0; x < button.buttonBehaviours.Count; x++)
            {
                ButtonBehaviour buttonBehaviour = button.buttonBehaviours[x];
                AddMenuBehaviour(buttonBehaviour);
            }

            LoadEditorDefaultButtonValues();
        }

        private void AddMenuBehaviour(ButtonBehaviour buttonBehaviour)
        {
            BehavioursMenu menu = new BehavioursMenu();
            menu.name = SubClassesHelper.GetReadableTypeName(buttonBehaviour.GetType());
            menu.buttonBehaviour = buttonBehaviour;
            menu.serializedObject = new SerializedObject(buttonBehaviour);

            int index = transitionTypes.IndexOf(buttonBehaviour.GetType());
            if (index != -1)
                RemovePopupBehaviourInfo(index);

            behavioursMenus.Add(menu);
        }

        private void RemovePopupBehaviourInfo(int index)
        {
            transitionTypes.RemoveAt(index);
            behaviourNames.RemoveAt(index);
        }

        private void AddPopupBehaviourInfo(Type type)
        {
            transitionTypes.Add(type);
            behaviourNames.Add(SubClassesHelper.GetReadableTypeName(type));
        }

        public override void OnInspectorGUI()
        {      
            UIButton button = (UIButton)target;
            button.Interactable = EditorGUILayout.Toggle("Interactable", button.Interactable);
            button.Selected = EditorGUILayout.Toggle("Selected", button.Selected);
			button.Toggle = EditorGUILayout.Toggle("Toggle", button.Toggle);

            button.Graphic = (Graphic)EditorGUILayout.ObjectField("Graphic", button.Graphic, typeof(Graphic), true);
            button.Target = (Transform)EditorGUILayout.ObjectField("Target", button.Target, typeof(Transform), true);
            button.Text = (Text)EditorGUILayout.ObjectField("Text", button.Text, typeof(Text), true);

            EditorUtilityFunctions.DrawDefaultInspector(serializedObject, _ignoreProperty);

            GUILayout.BeginHorizontal();

            selectedTransition = EditorGUILayout.Popup("Effect", selectedTransition, behaviourNames.ToArray());

            if (GUILayout.Button("Add", GUILayout.Width(35f)) && transitionTypes.Count != 0)
            {
                ButtonBehaviour buttonBehaviour = (ButtonBehaviour)button.gameObject.AddComponent(transitionTypes[selectedTransition]);
                buttonBehaviour.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;
                buttonBehaviour.UIButton = button;
                button.buttonBehaviours.Add(buttonBehaviour);
                AddMenuBehaviour(buttonBehaviour);
                selectedTransition = 0;

                if (Application.isPlaying)
                    buttonBehaviour.Ini();
            }
           
            GUILayout.EndHorizontal();
            GUILayout.Space(3);
            EditorStyle.Line();
            GUILayout.Space(3);

            for (int x = 0; x < behavioursMenus.Count; x++)
            {
                BehavioursMenu behavioursMenu = behavioursMenus[x];

                GUILayout.BeginHorizontal();
                behavioursMenu.foldout = EditorGUILayout.Foldout(behavioursMenu.foldout, behavioursMenu.name);
                if (GUILayout.Button("X", EditorStyle.ButtonStyle, GUILayout.Width(20)))
                {
                    behavioursMenu.serializedObject.Dispose();
                    AddPopupBehaviourInfo(behavioursMenu.buttonBehaviour.GetType());
                    EditorApplication.delayCall = () => DestroyImmediate(behavioursMenu.buttonBehaviour, true);
                    button.buttonBehaviours.RemoveAt(x);
                    behavioursMenus.RemoveAt(x);                 
                    continue;
                }
                GUILayout.EndHorizontal();

                //Create behaviours UI               
                if (behavioursMenu.foldout)
                {
                    GUILayout.BeginVertical("box");
                    SerializedProperty property = behavioursMenu.serializedObject.GetIterator();
                    property.NextVisible(true);

                    while (property.NextVisible(false))
                        EditorGUILayout.PropertyField(property, true);

                    behavioursMenu.serializedObject.ApplyModifiedProperties();

                    GUILayout.EndVertical();
                }
            }

            GUILayout.Space(5);
            OnInspectorGUIToggleButtons();
            EditorUtility.SetDirty(button);
            GUILayout.Space(2);     
        }

        protected virtual void ClearEditorButtonValues()
        {
            EditorPrefs.DeleteKey(target.GetInstanceID() + "OnClick");
            EditorPrefs.DeleteKey(target.GetInstanceID() + "OnDown");
            EditorPrefs.DeleteKey(target.GetInstanceID() + "OnUp");
            EditorPrefs.DeleteKey(target.GetInstanceID() + "OnEnter");
            EditorPrefs.DeleteKey(target.GetInstanceID() + "OnExit");
        }

        protected virtual void LoadEditorDefaultButtonValues()
        {
            onClickToggle = EditorPrefs.GetBool(target.GetInstanceID() + "OnClick", true);
            onDownToggle = EditorPrefs.GetBool(target.GetInstanceID() + "OnDown");
            onUpToggle = EditorPrefs.GetBool(target.GetInstanceID() + "OnUp");
            onEnterToggle = EditorPrefs.GetBool(target.GetInstanceID() + "OnEnter");
            onExitToggle = EditorPrefs.GetBool(target.GetInstanceID() + "OnExit");
        }

        protected virtual void OnInspectorGUIToggleButtons()
        {
            EditorStyle.Label("Events");
            GUILayout.BeginHorizontal();

            onClickToggle = GUILayout.Toggle(onClickToggle, "OnClick", EditorStyle.ToggleButtonStyle);
            EditorPrefs.SetBool(target.GetInstanceID() + "OnClick", onClickToggle);

            onDownToggle = GUILayout.Toggle(onDownToggle, "OnDown", EditorStyle.ToggleButtonStyle);
            EditorPrefs.SetBool(target.GetInstanceID() + "OnDown", onDownToggle);

            onUpToggle = GUILayout.Toggle(onUpToggle, "OnUp", EditorStyle.ToggleButtonStyle);
            EditorPrefs.SetBool(target.GetInstanceID() + "OnUp", onUpToggle);

            onEnterToggle = GUILayout.Toggle(onEnterToggle, "OnEnter", EditorStyle.ToggleButtonStyle);
            EditorPrefs.SetBool(target.GetInstanceID() + "OnEnter", onEnterToggle);

            onExitToggle = GUILayout.Toggle(onExitToggle, "OnExit", EditorStyle.ToggleButtonStyle);
            EditorPrefs.SetBool(target.GetInstanceID() + "OnExit", onExitToggle);

            GUILayout.EndHorizontal();
            DrawGUIEvents();
            serializedObject.ApplyModifiedProperties();
        }

        protected virtual void DrawGUIEvents()
        {
            if (onClickToggle)
                EditorGUILayout.PropertyField(onClickProperty);       

            if (onDownToggle)
                EditorGUILayout.PropertyField(onDownProperty);

            if (onUpToggle)
                EditorGUILayout.PropertyField(onUpProperty);

            if (onEnterToggle)
                EditorGUILayout.PropertyField(onEnterProperty);

            if (onExitToggle)
                EditorGUILayout.PropertyField(onExitProperty);
        }

        public void OnDestroy()
        {
            if (Application.isEditor)
            {
                if (target != null)
                    return;

                foreach (BehavioursMenu menu in behavioursMenus)
                {
                    if (menu.buttonBehaviour != null)
                        DestroyImmediate(menu.buttonBehaviour);
                }
            }

            ClearEditorButtonValues();
        }
    }
}
