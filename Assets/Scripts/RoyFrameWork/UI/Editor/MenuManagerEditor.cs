﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using RoyFramework.UI;
using RoyFramework.UIEditor;
using VascoGamesEditor.Tools;

namespace VascoGamesEditor.UIEditor
{
    [CustomEditor(typeof(MenuManager))]
    public class MenuManagerEditor : Editor
    {
        protected SerializedProperty menuListProperty;
        protected SerializedProperty _startupMenuProperty;
        protected SerializedProperty _quitMenuProperty;
        protected SerializedProperty _outOfFocusMenuProperty;
        protected SerializedProperty _ignoreStartupMenuProperty;
        protected SerializedProperty _closeCurrentMenuKeyProperty;

        protected FactoryList _menuListList;

        protected void OnEnable()
        {
            menuListProperty = serializedObject.FindProperty("Menus");
            _startupMenuProperty = serializedObject.FindProperty("StartupMenu");
            _quitMenuProperty = serializedObject.FindProperty("QuitMenu");
            _outOfFocusMenuProperty = serializedObject.FindProperty("OutOfFocusMenu");
            _ignoreStartupMenuProperty = serializedObject.FindProperty("IgnoreStartupMenu");
            _closeCurrentMenuKeyProperty = serializedObject.FindProperty("CloseCurrentMenuKey");

            _menuListList = new FactoryList(menuListProperty);
        }

        public override void OnInspectorGUI()
        {
            _menuListList.DrawList();
            GUILayout.Space(5);

            EditorStyle.Label("Default Menus");
            GUILayout.BeginVertical("box");
            EditorGUILayout.PropertyField(_startupMenuProperty);
            EditorGUILayout.PropertyField(_quitMenuProperty);
            GUILayout.EndVertical();

            GUILayout.Space(5);
            EditorStyle.Label("Out Of Focus Menu");
            GUILayout.BeginVertical("box");
            EditorGUILayout.PropertyField(_outOfFocusMenuProperty);          
            EditorGUILayout.PropertyField(_ignoreStartupMenuProperty);
            GUILayout.EndVertical();

            EditorStyle.Label("Global Settings");
            EditorGUILayout.PropertyField(_closeCurrentMenuKeyProperty);

            if (GUI.changed)
                serializedObject.ApplyModifiedProperties();
        }
    }
}