﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using RoyFramework.UI;

namespace VascoGamesEditor.UI
{
    [CustomEditor(typeof(MenuButton), true)]
    public class MenuButtonEditor : Editor
    {
        private MenuNames startupInfo;
        private MenuButton menuButton;
        private SerializedProperty MenuIdProperty;

        protected readonly string[] IgnoreProperty = new string[] { "MenuId", "Type", "ButtonActiveState", "CloseParentMenu", "SaveMenuToHistory" , "Hotkey" , "CloseStartupMenu" };

        void OnEnable()
        {
            menuButton = (MenuButton)target;
            MenuIdProperty = serializedObject.FindProperty("MenuId");
        } 

        public override void OnInspectorGUI()
        {
            DrawMenuButton();
            GUILayout.Space(5);
            DrawDefaultPropertys();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(menuButton);
                serializedObject.ApplyModifiedProperties();
            }
        }

        protected virtual void DrawMenuButton()
        {
            menuButton.Type = (MenuButton.ButtonType)EditorGUILayout.EnumPopup("Type", menuButton.Type);

            GUILayout.BeginVertical("box");
            menuButton.ButtonActiveState = (ButtonState)EditorGUILayout.EnumPopup("Menu Active", menuButton.ButtonActiveState);

            switch (menuButton.Type)
            {
                case MenuButton.ButtonType.OpenMenu:
                    EditorGUILayout.PropertyField(MenuIdProperty);
                    menuButton.CloseParentMenu = EditorGUILayout.Toggle("Disable Menu", menuButton.CloseParentMenu);
                    menuButton.SaveMenuToHistory = EditorGUILayout.Toggle("Save To History", menuButton.SaveMenuToHistory);
                    break;

                case MenuButton.ButtonType.CloseMenu:
                    EditorGUILayout.PropertyField(MenuIdProperty);
                    break;

                case MenuButton.ButtonType.CloseAllMenus:
                case MenuButton.ButtonType.CloseCurrentMenu:
                    menuButton.CloseStartupMenu = EditorGUILayout.Toggle("Close Startup Menu", menuButton.CloseStartupMenu);
                    break;
            }
            GUILayout.EndVertical();
            EditorGUILayout.Space();

            menuButton.Hotkey = (KeyCode)EditorGUILayout.EnumPopup("Hotkey", menuButton.Hotkey);
        }

        protected virtual void DrawDefaultPropertys()
        {
            SerializedProperty property = serializedObject.GetIterator();
            property.NextVisible(true);

            while (property.NextVisible(false))
            {
                if (IgnoreProperty.Contains(property.name))
                    continue;

                EditorGUILayout.PropertyField(property, true);
            }
        }
    }
}
