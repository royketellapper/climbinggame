﻿using UnityEditor;
using UnityEngine;
using VascoGamesEditor.Utility;

namespace RoyFramework.UI
{
    [CustomEditor(typeof(UIEventSwitch), true), CanEditMultipleObjects]
    public class UIEventSwitchEditor : Editor
    {
        protected readonly string[] _ignoreProperty = new string[2] { "StartPos", "EndPos" };
        protected SerializedProperty StartPosProperty;
        protected SerializedProperty EndPosProperty;

        protected virtual void OnEnable()
        {
            StartPosProperty = serializedObject.FindProperty("StartPos");
            EndPosProperty = serializedObject.FindProperty("EndPos");
        }

        public override void OnInspectorGUI()
        {
            EditorUtilityFunctions.DrawDefaultInspector(serializedObject, _ignoreProperty);

            UIEventSwitch eventSwitch = (UIEventSwitch)target;

            GUILayout.BeginVertical("Box");
            EditorGUILayout.LabelField("Position Tween");
            EditorGUILayout.Space();
            DrawProperty(StartPosProperty);
            DrawProperty(EndPosProperty);

            if (GUILayout.Button("Swap positions"))
            {
                Vector3 startPos = eventSwitch.StartPos;
                Vector3 endPos = eventSwitch.EndPos;
                eventSwitch.StartPos = endPos;
                eventSwitch.EndPos = startPos;
            }

            GUILayout.EndHorizontal();

            EditorUtilityFunctions.GUIChanged(serializedObject);
        }

        protected void DrawProperty(SerializedProperty property)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(property);

            UIEventSwitch eventSwitch = (UIEventSwitch)target;

            if (GUILayout.Button("S", GUILayout.Width(20)) && eventSwitch.Target != null)
            {
                if (property.name == "StartPos")
                    eventSwitch.StartPos = ((RectTransform)eventSwitch.Target.transform).anchoredPosition3D;
                else if (property.name == "EndPos")
                    eventSwitch.EndPos = ((RectTransform)eventSwitch.Target.transform).anchoredPosition3D;
            }

            if (GUILayout.Button("G", GUILayout.Width(20)) && eventSwitch.Target != null)
            {
                if (property.name == "StartPos")
                    ((RectTransform)eventSwitch.Target.transform).anchoredPosition3D = eventSwitch.StartPos;
                else if (property.name == "EndPos")
                    ((RectTransform)eventSwitch.Target.transform).anchoredPosition3D = eventSwitch.EndPos;
            }

            EditorGUILayout.EndHorizontal();
        }
    }
}