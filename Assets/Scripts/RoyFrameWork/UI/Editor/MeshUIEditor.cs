﻿using UnityEditor;
using UnityEngine;
using RoyFramework.UI;

namespace VascoGamesEditor.UIEditor
{
    [CustomEditor(typeof(MeshUI),true), CanEditMultipleObjects]
    public class MeshUIEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUI.changed)
                ((MeshUI)target).UpdateMesh();
        }
    }
}
