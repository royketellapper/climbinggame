﻿using UnityEngine;
using UnityEditor;
using RoyFramework.UI;

namespace VascoGamesEditor.UI
{
    [CustomPropertyDrawer(typeof(MenuNameAttribute))]
    public class MenuNameDrawer : PropertyDrawer
    {
        private MenuNames menuNames;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            if (MenuManager.Instance != null)
            {
                if (menuNames == null)
                    menuNames = MenuNames.GetMenuNames(property.stringValue);

                if (property.propertyType == SerializedPropertyType.String)
                    property.stringValue = menuNames.MenuPopup(position, property.stringValue, label);
                else
                    EditorGUI.PropertyField(position, property, label, true);
            }
            else
                EditorGUI.PropertyField(position, property, label, true);

            EditorGUI.EndProperty();
        }
    }
}
