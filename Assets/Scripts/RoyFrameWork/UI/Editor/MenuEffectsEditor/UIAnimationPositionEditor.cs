﻿using System;
using UnityEditor;
using UnityEngine;
using RoyFramework.UI;
using RoyFramework.Utility;

namespace VascoGamesEditor.UI
{
    [CustomEditor(typeof(UIAnimationPosition), true), CanEditMultipleObjects]
    public class UIAnimationPositionEditor : UIAnimationBaseEditor
    {
        protected UIAnimationPosition _animationPosition;
        protected SerializedProperty _coordinateTypeSettingsProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            _openTweenSettingsProperty = serializedObject.FindProperty("_openTweenSettings");
            _closeTweenSettingsProperty = serializedObject.FindProperty("_closeTweenSettings");
            _disableTweenSettingsProperty = serializedObject.FindProperty("_disableTweenSettings");
            _coordinateTypeSettingsProperty = serializedObject.FindProperty("_coordinateType");

            _animationPosition = (UIAnimationPosition)target;
        }

        protected override void DrawMenuSettings()
        {
            base.DrawMenuSettings();
            EditorGUILayout.PropertyField(_coordinateTypeSettingsProperty);
        }

        protected override void DrawTween(SerializedProperty property)
        {
            SerializedProperty positionProperty = property.FindPropertyRelative("Position");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(positionProperty);
            if (GUILayout.Button("S", GUILayout.Width(20)) && _animationPosition.Target != null)
                positionProperty.vector3Value = _animationPosition.Target.GetPosition((CoordinatePositionsType)_animationEventTypeProperty.enumValueIndex);

            if (GUILayout.Button("G", GUILayout.Width(20)) && _animationPosition.Target != null)
                _animationPosition.Target.SetPosition(positionProperty.vector3Value, (CoordinatePositionsType)_animationEventTypeProperty.enumValueIndex);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Copy Position"))
                GUIUtility.systemCopyBuffer = _animationPosition.Target.GetPosition((CoordinatePositionsType)_animationEventTypeProperty.enumValueIndex).ToString();

            if (GUILayout.Button("Paste Position") && !String.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
                positionProperty.vector3Value = UtilityFunctions.StringToVector3(GUIUtility.systemCopyBuffer);

            EditorGUILayout.EndHorizontal();
        }
    }
}