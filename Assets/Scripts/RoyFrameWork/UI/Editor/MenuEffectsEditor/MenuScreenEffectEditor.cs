﻿using UnityEngine;
using UnityEditor;
using RoyFramework.UI;

namespace VascoGamesEditor.UI
{
    public abstract class MenuScreenEffectEditor : Editor
    {
        protected void DrawFullScreenButton()
        {
            if (GUILayout.Button("Fill Screen"))
                ((MenuScreenEffectBase)target).FillScreen();
        }
    }
}
