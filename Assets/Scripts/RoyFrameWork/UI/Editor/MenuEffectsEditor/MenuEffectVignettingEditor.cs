﻿using UnityEditor;
using UnityEngine;
using RoyFramework.UI;

namespace VascoGamesEditor.UI
{
    [CustomEditor(typeof(MenuEffectVignetting), true)]
    public class MenuEffectVignettingEditor : MenuScreenEffectEditor
    {
        protected MenuEffectVignetting menuEffect;

        protected void OnEnable()
        {
            menuEffect = (MenuEffectVignetting)target;
        }

        public override void OnInspectorGUI()
        {
            menuEffect.color = EditorGUILayout.ColorField("Color", menuEffect.color);
            menuEffect.Intensity = EditorGUILayout.Slider("Intensity", menuEffect.Intensity, 0, 1);
            menuEffect.raycastTarget = EditorGUILayout.Toggle("RaycastTarget", menuEffect.raycastTarget);

            DrawFullScreenButton();

            if (GUI.changed)
                EditorUtility.SetDirty(menuEffect);
        }
    }
}
