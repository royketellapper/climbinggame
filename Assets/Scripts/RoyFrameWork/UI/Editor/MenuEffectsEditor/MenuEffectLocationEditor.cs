﻿using UnityEngine;
using UnityEditor;
using RoyFramework.UI;
using System.Collections.Generic;
using DG.Tweening;
using VascoGamesEditor.UIEditor;

namespace VascoGamesEditor.UI
{
    [CustomEditor(typeof(MenuEffectLocation))]
    public class MenuEffectLocationEditor : Editor
    {
        protected Dictionary<MenuEffectLocation.TransformEffect, bool> effectFoldouts = new Dictionary<MenuEffectLocation.TransformEffect, bool>();
        protected SerializedProperty menuDropDownProperty;
        protected MenuEffectLocation menuEffect;

        protected void OnEnable()
        {
            menuEffect = (MenuEffectLocation)target;
            menuDropDownProperty = serializedObject.FindProperty("MenuId");
        }

        public override void OnInspectorGUI()
        {
            EditorStyle.Label("Menu");
            EditorGUILayout.PropertyField(menuDropDownProperty);
            EditorGUILayout.Space();

            EditorStyle.Label("Effect");
            DrawEffectUI(menuEffect.OpenEffect, "Open Position");
            DrawEffectUI(menuEffect.DisableEffect, "Disable Position");
            DrawEffectUI(menuEffect.CloseEffect, "Close Position");

            if (GUI.changed)
            {
                EditorUtility.SetDirty(menuEffect);
                serializedObject.ApplyModifiedProperties();
            }
        }

        public void DrawEffectUI(MenuEffectLocation.TransformEffect effect, string name)
        {
            bool foldout = false;
            if (effectFoldouts.ContainsKey(effect))
                foldout = effectFoldouts[effect];
            else
                effectFoldouts.Add(effect, false);
             
            EditorGUILayout.BeginHorizontal();
            effect.enable = EditorGUILayout.Toggle(effect.enable, GUILayout.Width(15));
            foldout = GUILayout.Toggle(foldout, name, EditorStyles.foldout);
            EditorGUILayout.EndHorizontal();

            if (foldout)
            {
                GUILayout.BeginVertical("box");

                //Animation Options
                EditorGUILayout.LabelField("Animation Options", EditorStyle.boldLabel);
                effect.Ease = (Ease)EditorGUILayout.EnumPopup("Ease", effect.Ease);
                effect.Delay = EditorGUILayout.FloatField("Delay", effect.Delay);
                effect.Duration = EditorGUILayout.FloatField("Duration", effect.Duration);                
                effect.WaitToFinish = EditorGUILayout.Toggle("Wait To Finish", effect.WaitToFinish);

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("State Options", EditorStyle.boldLabel);
                effect.BeginState = (ActiveState)EditorGUILayout.EnumPopup("Begin State", effect.BeginState);
                effect.FinishState = (ActiveState)EditorGUILayout.EnumPopup("Finish State", effect.FinishState);

                EditorGUILayout.Space();

                //Position
                EditorGUILayout.LabelField("Position", EditorStyle.boldLabel);
                effect.CoordinateType = (CoordinateType)EditorGUILayout.EnumPopup("Coordinate Type", effect.CoordinateType);
                effect.Coordinate = EditorGUILayout.Vector3Field("Position", effect.Coordinate);

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Set Position"))
                {
                    SaveUndo("Get Position");

                    Undo.RecordObject(menuEffect.transform, "Set Position");
                    effect.Coordinate = (effect.CoordinateType == CoordinateType.World)? menuEffect.transform.position : menuEffect.transform.localPosition;
                }
                if (GUILayout.Button("Get Position"))
                {
                    SaveUndo("Get Position");

                    if (effect.CoordinateType == CoordinateType.World)
                        menuEffect.transform.position = effect.Coordinate;
                    else
                        menuEffect.transform.localPosition = effect.Coordinate;
                }
                GUILayout.EndHorizontal();


                GUILayout.EndVertical();
            }
            effectFoldouts[effect] = foldout;
        }

        protected void SaveUndo(string name)
        {
            Object[] recordObject = new Object[2];
            recordObject[0] = menuEffect.transform;
            recordObject[1] = menuEffect;
            Undo.RecordObjects(recordObject, name);
        }
    }
}
