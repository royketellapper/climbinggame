﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using RoyFramework.UI;

namespace VascoGamesEditor.UI
{
    [CustomEditor(typeof(MenuEffectBlur), true)]
    public class MenuScreenBlur : MenuScreenEffectEditor
    {
        protected MenuEffectBlur menuEffect;

        protected void OnEnable()
        {
            menuEffect = (MenuEffectBlur)target;
        }

        public override void OnInspectorGUI()
        {
            menuEffect.color = EditorGUILayout.ColorField("Color", menuEffect.color);
            menuEffect.Intensity = EditorGUILayout.Slider("Intensity", menuEffect.Intensity, 0, 20);
            menuEffect.raycastTarget = EditorGUILayout.Toggle("RaycastTarget", menuEffect.raycastTarget);

            DrawFullScreenButton();

            if (GUI.changed)
                EditorUtility.SetDirty(menuEffect);
        }
    }
}
