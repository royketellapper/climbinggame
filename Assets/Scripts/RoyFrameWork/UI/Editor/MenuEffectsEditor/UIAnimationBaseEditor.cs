﻿using UnityEngine;
using UnityEditor;
using VascoGamesEditor.UIEditor;
using VascoGamesEditor.Utility;
using RoyFramework.UI;

namespace VascoGamesEditor.UI
{
    [CustomEditor(typeof(UIAnimationBase), true), CanEditMultipleObjects]
    public abstract class UIAnimationBaseEditor : Editor
    {
        protected SerializedProperty _menuProperty;
        protected SerializedProperty _targetProperty;
        protected SerializedProperty _animationEventTypeProperty;

        //Implement this in child class 
        protected SerializedProperty _openTweenSettingsProperty;
        protected SerializedProperty _closeTweenSettingsProperty;
        protected SerializedProperty _disableTweenSettingsProperty;
        protected UIAnimationBase _UIAnimationBase;

        protected virtual void OnEnable()
        {
            _menuProperty = serializedObject.FindProperty("_menus");
            _targetProperty = serializedObject.FindProperty("_target");        
            _animationEventTypeProperty = serializedObject.FindProperty("_animationEventType");
            _UIAnimationBase = (UIAnimationBase)target;
        }

        public override void OnInspectorGUI()
        {
            DrawMenuSettings();
            EditorGUILayout.Space();
            DrawTweensSettings();
            EditorUtilityFunctions.GUIChanged(serializedObject);
        }

        protected virtual void DrawMenuSettings()
        {
            EditorStyle.Label("Menu");
            EditorGUILayout.PropertyField(_menuProperty, true);
            EditorGUILayout.PropertyField(_targetProperty);
        }

        protected virtual void DrawTweensSettings()
        {
            EditorStyle.Label("Tweens");
            EditorGUILayout.PropertyField(_animationEventTypeProperty);

            switch (_animationEventTypeProperty.enumValueIndex)
            {
                case 0:
                    DrawDefaultTween(_openTweenSettingsProperty, "Open Tween");
                    DrawDefaultTween(_closeTweenSettingsProperty, "Close Tween");
                    break;
                case 1:
                    DrawDefaultTween(_openTweenSettingsProperty, "Open Tween");
                    DrawDefaultTween(_closeTweenSettingsProperty, "Close Tween");
                    DrawDefaultTween(_disableTweenSettingsProperty, "Disable Tween");
                    break;
            }
        }

        private void DrawDefaultTween(SerializedProperty property, string name)
        {
            if (property.isExpanded = GUILayout.Toggle(property.isExpanded, name, EditorStyles.foldout))
            {
                EditorGUILayout.BeginVertical("Box");
                EditorGUILayout.PropertyField(property.FindPropertyRelative("Ease"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("IgnoreTimeScale"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("Delay"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("Duration"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("BeginState"));
                EditorGUILayout.PropertyField(property.FindPropertyRelative("EndState"));
                DrawTween(property);
                EditorGUILayout.EndVertical();
            }
        }

        protected abstract void DrawTween(SerializedProperty property);
    }
}
