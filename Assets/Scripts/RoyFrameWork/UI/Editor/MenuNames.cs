﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using RoyFramework.UI;

namespace VascoGamesEditor.UI
{
    public class MenuNames
    {
        public string[] Names;
        public string[] Ids;

        public static MenuNames GetMenuNames(string currentId)
        {
            MenuNames menuNamesInfo = new MenuNames();
            List<string> Names = new List<string>();
            List<string> Ids = new List<string>();

            if (MenuManager.Instance == null)
                return menuNamesInfo;

            bool foundCurrentId = false;
            if (MenuManager.Instance.Menus != null)
            {
                for (int x = 0; x < MenuManager.Instance.Menus.Length; x++)
                {
                    MenuInfo menu = MenuManager.Instance.Menus[x];

                    if (menu.Id == currentId)
                        foundCurrentId = true;

                    Ids.Add(menu.Id);
                }
            }
            Ids.Distinct();
            Names.AddRange(Ids);
            Names.Insert(0,"---------------");
            Ids.Insert(0, string.Empty);

            if (!foundCurrentId && currentId != string.Empty)
            {
                Names.Add("*" + currentId);
                Ids.Add(currentId);
            }

            menuNamesInfo.Ids = Ids.ToArray();
            menuNamesInfo.Names = Names.ToArray();

            return menuNamesInfo;
        }

        public int Selected(string id)
        {
            int index = Array.FindIndex<string>(Ids, m => m == id);

            if (index == -1)
                return 0;

            return index;
        }

        public string MenuPopup(string currentId, string label)
        {
            int index = Selected(currentId);
            index = EditorGUILayout.Popup(label, index, Names);
            return Ids[index];
        }

        public string MenuPopup(Rect rect, string currentId, GUIContent label)
        {
            int index = Selected(currentId);
            index = EditorGUI.Popup(rect, label.text, index, Names);
            return Ids[index];
        }
    }
}
