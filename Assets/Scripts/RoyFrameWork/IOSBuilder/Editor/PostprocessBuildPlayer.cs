﻿#if UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.IO;
using System.Collections.Generic;

using UnityEditor.iOS.Xcode;
namespace VascoGames.IOS
{
	public class PostprocessBuildPlayer
	{
	    public static void AddCompileFlagsForFile(PBXProject project, string target, string path, string flag)
	    {
			
	        string file = project.FindFileGuidByProjectPath(path);
		
			if (file == "") 
			{
				Debug.LogWarning ("Unable to fine file: " + path);
				return;
			}

			//List<string> flags = project.GetCompileFlagsForFile(target, file);
			//if (flags == null)
			List<string>  flags = new List<string> ();

			flags.Add(flag);
	        project.SetCompileFlagsForFile(target, file, flags);
	    }

		[PostProcessBuild()]
	    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
	    {
	        if (buildTarget == BuildTarget.iOS)
	        {
	            string projPath = PBXProject.GetPBXProjectPath(path);
	            PBXProject proj = new PBXProject();
	            proj.ReadFromString(File.ReadAllText(projPath));

	            string target = proj.TargetGuidByName("Unity-iPhone");

	            proj.AddFrameworkToProject(target, "AdSupport.framework", true);
	            proj.AddFrameworkToProject(target, "CoreData.framework", true);
	            proj.AddFrameworkToProject(target, "AVFoundation.framework", true);
	            proj.AddFrameworkToProject(target, "CoreTelephony.framework", true);
	            proj.AddFrameworkToProject(target, "CoreGraphics.framework", true);
	            proj.AddFrameworkToProject(target, "CoreMedia.framework", true);
	            proj.AddFrameworkToProject(target, "StoreKit.framework", true);
	            proj.AddFrameworkToProject(target, "SystemConfiguration.framework", true);
	            proj.AddFrameworkToProject(target, "UIKit.framework", true);
	            proj.AddFrameworkToProject(target, "SystemConfiguration.framework", true);
	            proj.AddFrameworkToProject(target, "libz.tbd", true);
	            proj.AddFrameworkToProject(target, "libsqlite3.tbd", true);
				proj.AddFrameworkToProject(target, "libxml2.tbd", true);
		
				proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");
				proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");


				//AddCompileFlagsForFile(proj, target, "Libraries/Plugins/IOS/ALAdDelegateWrapper.m", "-fno-objc-arc");
				//AddCompileFlagsForFile(proj, target, "Libraries/Plugins/IOS/ALInterstitialCache.m", "-fno-objc-arc");
				//AddCompileFlagsForFile(proj, target, "Libraries/Plugins/IOS/ALManagedLoadDelegate.m", "-fno-objc-arc");
				//AddCompileFlagsForFile(proj, target, "Libraries/Plugins/IOS/AppLovinUnity.mm", "-fno-objc-arc");


				File.WriteAllText(projPath, proj.WriteToString());
	        }
	    }
	}
}
#endif