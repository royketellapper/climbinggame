﻿using RoyFramework.Services.Api;
using System;

namespace RoyFramework.Services.Impl
{
    public class InternetConnectivity : IInternetConnectivity
    {
        private const string Google = "http://google.com";
        private const string Schema = "schema.org/WebPage";

        private readonly IHTML _htmlService;
        private readonly string _url;

        public InternetConnectivity(IHTML htmlService) : this(htmlService, Google) { }

        public InternetConnectivity(IHTML htmlService, string url)
        {
            if (htmlService == null)
                throw new ArgumentNullException("htmlService");

            if (url == null)
                throw new ArgumentNullException("url");

            _htmlService = htmlService;
            _url = url;
        }

        ~InternetConnectivity()
        {
            Dispose(false);
        }

        public bool HasInternetConnectivity()
        {
            var html = _htmlService.GetHTML(_url);

            if (html == string.Empty)
                return false;
            else if (!html.Contains(Schema))
                return false;

            return true;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _htmlService.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}