﻿using System;
using System.IO;
using System.Net;
using RoyFramework.Services.Api;

namespace RoyFramework.Services.Impl
{
    public class HTMLFromUrl : IHTML
    {
        private const int TimeOut = 2000;
        private const int StatusCodeUpperLimit = 299;
        private const int StatusCodeLowerLimit = 200;
        private const int CharsLimit = 80;

        private readonly int _timeOut;
        private HttpWebRequest _request;

        public HTMLFromUrl() : this(TimeOut) { }
        public HTMLFromUrl(int timeout)
        {
            _timeOut = timeout;
        }

        ~HTMLFromUrl()
        {
            Dispose(false);
        }

        public string GetHTML(string resource)
        {
            string html = string.Empty;
            _request = (HttpWebRequest)WebRequest.Create(resource);
            _request.Timeout = _timeOut;
            _request.ReadWriteTimeout = _timeOut;

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)_request.GetResponse())
                {
                    var isSucces = (int)response.StatusCode < StatusCodeUpperLimit
                        && (int)response.StatusCode >= StatusCodeLowerLimit;

                    if (isSucces)
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            char[] chars = new char[CharsLimit];
                            reader.Read(chars, 0, chars.Length);
                            foreach (var c in chars)
                                html += c;
                        }
                    }
                }
            }
            catch
            {
                return string.Empty;
            }

            return html;
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                _request = null;

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}