﻿using RoyFramework.Services.Api;
using System;

namespace RoyFramework.Services.Impl
{
    public class ParseDateFromServerResponse : IDateTimeParser
    {
        private const int ModifiedJulianDate = 51544;
        private const int StartIndexUTC = 38;
        private const int StartIndexJulianDate = 1;
        private const int StartIndexYear = 7;
        private const int StartIndexMonth = 10;
        private const int StartIndexDay = 13;
        private const int StartIndexHour = 16;
        private const int StartIndexMinute = 19;
        private const int StartIndexSecond = 22;
        private const int LengthUTC = 9;
        private const int LengthDefault = 2;
        private const int LengthJulianDate = 5;
        private const int LengthResource = 47;

        private const string UTCNIST = "UTC(NIST)";

        public DateTime ParseDate(string resource)
        {
            var dateTime = new DateTime();

            if (resource != null)
            {
                if (resource.Length > LengthResource && 
                    resource.Substring(StartIndexUTC, LengthUTC).Equals(UTCNIST))
                {
                    int jd = ParseSubstring(resource, StartIndexJulianDate, LengthJulianDate);
                    int year = ParseSubstring(resource, StartIndexYear, LengthDefault);
                    int month = ParseSubstring(resource, StartIndexMonth, LengthDefault);
                    int day = ParseSubstring(resource, StartIndexDay, LengthDefault);
                    int hour = ParseSubstring(resource, StartIndexHour, LengthDefault);
                    int minute = ParseSubstring(resource, StartIndexMinute, LengthDefault);
                    int second = ParseSubstring(resource, StartIndexSecond, LengthDefault);

                    if (jd > ModifiedJulianDate)
                        year += 2000;
                    else
                        year += 1999;

                    dateTime = new DateTime(year, month, day, hour, minute, second);
                }
            }

            return dateTime;
        }

        private int ParseSubstring(string resource, int startIndex, int length)
        {
            int result = 0;
            int.TryParse(resource.Substring(startIndex, length), out result);
            return result;
        }
    }
}