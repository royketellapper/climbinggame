﻿using System;
using RoyFramework.Services.Api;

namespace RoyFramework.Services.Impl
{
    public class ServerTimeChecker : IServerTimeChecker
    {
        public const int DefaultPort = 13;
        public const int DefaultServerTimeOut = 2000;
        public const string Succes = "Succes";

        private readonly string[] _servers = new string[]
        {
            "time.nist.gov",
            "time-b.nist.gov",
            "time-a.nist.gov",
            "nist1-ny.ustiming.org",
            "nist1-chi.ustiming.org",
            "ntp-nist.ldsbc.edu",
            "nist1-la.ustiming.org"
        };

        private bool _isWorking = false;
        private bool _hasInternet;
        private Action<DateTime, bool> _callback;
        private IHTML _htmlService;
        private IInternetConnectivity _internetConnectity;
        private int _counter;
        private int _port;
        private int _serverTimeOut;

        public ServerTimeChecker() : this(DefaultServerTimeOut, DefaultPort) { }

        public ServerTimeChecker(int serverTimeOut) : this(serverTimeOut, DefaultPort) { }

        public ServerTimeChecker(int serverTimeOut, int port)
        {
            _htmlService = new HTMLFromUrl();
            _internetConnectity = new InternetConnectivity(_htmlService);
            _hasInternet = _internetConnectity.HasInternetConnectivity();

            _port = port;
            _serverTimeOut = serverTimeOut;
        }

        ~ServerTimeChecker()
        {
            Dispose(false);
        }

        public void GetServerTime(Action<DateTime, bool> callback)
        {
            if (_isWorking)
                return;

            _isWorking = true;
            _callback = callback;
            _counter = 0;

            for (var i = 0; i < _servers.Length; i++)
            {
                if (_isWorking)
                {
                    GetTimeFromConnection(_servers[i], _port, _serverTimeOut);
                }
            }
        }

        private void GetTimeFromConnection(string hostName, int port, int timeOut)
        {
            IDateTimeParser dateTimeParser = new ParseDateFromServerResponse();
            IStreamResponse streamResponse = new StreamResponse();
            IConnection connection = new TcpConnection(hostName, port, timeOut);

            var serverTimeService = new ServerTimeService(_hasInternet, dateTimeParser, connection, streamResponse);
            serverTimeService.GetServerTime(GetTimeCallback);
        }

        private void GetTimeCallback(DateTime dateTime, string state)
        {
            _counter++;

            if (state == Succes)
            {
                var newDateTime = new DateTime();
                var equalsNewDateTime = dateTime.Equals(newDateTime);
                ReportTime(dateTime, !equalsNewDateTime);

                _isWorking = false;
                return;
            }

            if (_counter >= _servers.Length)
            {
                _isWorking = false;
                ReportTime(dateTime, false);
            }
        }

        private void ReportTime(DateTime dateTime, bool succes)
        {
            if (_callback != null)
                _callback(dateTime, succes);
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _htmlService.Dispose();
                    _internetConnectity.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        { 
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}