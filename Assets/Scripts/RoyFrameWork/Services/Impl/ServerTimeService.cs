﻿using RoyFramework.Services.Api;
using System;

namespace RoyFramework.Services.Impl
{
    public class ServerTimeService : IServerTime
    {
        private const string NoInternetConnectivity = "No Internet Connectivity";
        private const string Succes = "Succes";

        private IInternetConnectivity _internetConnectivity;
        private IDateTimeParser _dateTimeParser;
        private IStreamResponse _streamResponse;
        private IConnection _connection;
        private string _state;
        private bool _hasInternet;

        public ServerTimeService(bool hasInternet, IDateTimeParser dateTimeParser, IConnection connection, IStreamResponse streamResponse)
        {
            if (dateTimeParser == null)
                throw new ArgumentNullException("dateTimeParser");

            if (connection == null)
                throw new ArgumentNullException("_connection");

            if (streamResponse == null)
                throw new ArgumentNullException("_streamResponse");

            _dateTimeParser = dateTimeParser;
            _connection = connection;
            _streamResponse = streamResponse;

            _hasInternet = hasInternet;
        }

        public ServerTimeService(IInternetConnectivity internetConnectivity, IDateTimeParser dateTimeParser, IConnection connection, IStreamResponse streamResponse)
        {
            if (internetConnectivity == null)
                throw new ArgumentNullException("internetConnectivity");

            if (dateTimeParser == null)
                throw new ArgumentNullException("dateTimeParser");

            if (connection == null)
                throw new ArgumentNullException("_connection");

            if (streamResponse == null)
                throw new ArgumentNullException("_streamResponse");

            _internetConnectivity = internetConnectivity;
            _dateTimeParser = dateTimeParser;
            _connection = connection;
            _streamResponse = streamResponse;

            _hasInternet = _internetConnectivity.HasInternetConnectivity();
        }

        ~ServerTimeService()
        {
            Dispose(false);
        }

        public void GetServerTime(Action<DateTime, string> callback)
        {
            var dateTime = new DateTime();
            _state = Succes;

            if (!_hasInternet)
                _state = NoInternetConnectivity;

            if (_hasInternet)
            {
                _connection.Connect((NetworkStream) => 
                {
                    var stream = NetworkStream; 

                    if (_connection.Error != string.Empty)
                        _state = _connection.Error;

                    var serverResponse = _streamResponse.GetResponse(stream);
                    dateTime = _dateTimeParser.ParseDate(serverResponse);

                });
            }

            if (callback != null)
                callback(dateTime, _state);
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _internetConnectivity.Dispose();
                    _connection.Dispose();
                    _streamResponse.Dispose();
                }

                _dateTimeParser = null;

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}