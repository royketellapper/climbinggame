﻿using RoyFramework.Services.Api;
using System.Net.Sockets;
using System;

namespace RoyFramework.Services.Impl
{
    public class TcpConnection : IConnection
    {
        public const string FailedToConnectMessage = "Failed to connect";
        public const string NoStreamMessage = "Couldn't get a network stream";

        private readonly TcpClient _client;
        private readonly int _timeOut;
        private readonly string _hostName;
        private readonly int _port;
        private string _error = string.Empty;

        public string Error { get { return _error; } }

        public TcpConnection(string hostName, int port, int timeOut)
        {
            _client = new TcpClient();

            _timeOut = timeOut;
            _hostName = hostName;
            _port = port;
        }

        ~TcpConnection()
        {
            Dispose(false);
        }

        public bool Connect(Action<NetworkStream> callback)
        {
            var state = new State(_client, true);
            IAsyncResult asyncResult = _client.BeginConnect(_hostName, _port, EndConnect, state);
            state.Succes = asyncResult.AsyncWaitHandle.WaitOne(_timeOut, false);

            if (!state.Succes || !_client.Connected)
            {
                _error = FailedToConnectMessage;
                return false;
            }

            var stream = _client.GetStream();

            if (stream == null)
            {
                _error = NoStreamMessage;
                return false;
            }

            if (callback != null)
                callback(stream);

            return true;
        }

        private void EndConnect(IAsyncResult asyncResult)
        {
            var state = (State)asyncResult.AsyncState;
            var client = state.Client;

            try
            {
                client.EndConnect(asyncResult);
            }
            catch { }

            if (client.Connected && state.Succes)
                return;

            client.Close();
        }

        internal class State
        {
            public readonly TcpClient Client;
            public bool Succes;

            public State(TcpClient client, bool succes)
            {
                Client = client;
                Succes = succes;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _client.Close();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}