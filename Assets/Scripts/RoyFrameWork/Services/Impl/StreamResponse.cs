﻿using RoyFramework.Services.Api;
using System.IO;
using System;

namespace RoyFramework.Services.Impl
{
    public class StreamResponse : IStreamResponse
    {
        private Stream _stream;

        public StreamResponse() { }

        ~StreamResponse()
        {
            Dispose(false);
        }

        public string GetResponse(Stream stream)
        {
            SetStream(stream);

            var response = string.Empty;

            using (var reader = new StreamReader(_stream))
            {
                response = reader.ReadToEnd();
            }

            return response;
        }

        private void SetStream(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            _stream = stream;
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _stream.Close();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}