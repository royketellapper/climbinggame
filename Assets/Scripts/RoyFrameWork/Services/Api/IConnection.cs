﻿using System.Net.Sockets;
using System;

namespace RoyFramework.Services.Api
{
    public interface IConnection : IDisposable
    {
        string Error { get; }
        bool Connect(Action<NetworkStream> callback);
    }
}