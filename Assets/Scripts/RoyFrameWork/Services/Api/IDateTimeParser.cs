﻿using System;

namespace RoyFramework.Services.Api
{
    public interface IDateTimeParser
    {
        DateTime ParseDate(string resource);
    }
}