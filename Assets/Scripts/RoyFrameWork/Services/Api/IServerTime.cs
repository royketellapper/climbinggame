﻿using System;

namespace RoyFramework.Services.Api
{
    public interface IServerTime : IDisposable
    {
        void GetServerTime(Action<DateTime, string> callback);
    }
}