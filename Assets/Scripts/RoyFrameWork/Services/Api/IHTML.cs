﻿using System;

namespace RoyFramework.Services.Api
{
    public interface IHTML : IDisposable
    {
        string GetHTML(string resource);
    }
}