﻿using System;

namespace RoyFramework.Services.Api
{
    public interface IInternetConnectivity : IDisposable
    {
        bool HasInternetConnectivity();
    }
}