﻿using System;
using System.IO;

namespace RoyFramework.Services.Api
{
    public interface IStreamResponse : IDisposable
    {
        string GetResponse(Stream stream);
    }
}