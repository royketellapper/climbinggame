﻿using System;

namespace RoyFramework.Services.Api
{
    public interface IServerTimeChecker : IDisposable
    {
        void GetServerTime(Action<DateTime, bool> callback);
    }
}