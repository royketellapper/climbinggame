﻿using RoyFramework.Services.Api;
using RoyFramework.Services.Impl;
using NUnit.Framework;

public class StreamResponseTest 
{
    //[Test]
    public void EditorTest()
    {
        string hostname = "time.nist.gov";
        int port = 13;
        int timeOut = 1000;

        //Arrange
        IConnection connection = new TcpConnection(hostname, port, timeOut);

        //Act
        //Try to rename the GameObject
        var response = string.Empty;
        connection.Connect((stream) =>
        {        
            using (var streamResponse = new StreamResponse())
            {
                response = streamResponse.GetResponse(stream);
            }

        });

        UnityEngine.Debug.Log("Response : " + response);

        //Assert
        //The object has a new name
        Assert.AreNotEqual(response, string.Empty);
    }
}