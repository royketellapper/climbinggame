﻿using RoyFramework.Services.Api;
using RoyFramework.Services.Impl;
using NUnit.Framework;

public class ConnectionTest 
{
    //[Test]
    public void EditorTest()
    {
        string hostname = "time.nist.gov";
        int port = 13;
        int timeOut = 1000;

        //Arrange
        IConnection connection = new TcpConnection(hostname, port, timeOut);

        //Act
        connection.Connect((stream) => {
            //Assert
            Assert.IsNotNull(stream);
        });
    }
}