﻿using RoyFramework.Services.Api;
using RoyFramework.Services.Impl;
using UnityEngine;
using NUnit.Framework;
using System;

public class ServerTimeServiceTest 
{
    [Test]
    public void EditorTest()
    {
        //Arrange
        IDateTimeParser dateTimeParser = new ParseDateFromServerResponse();
        IConnection connection = new TcpConnection("time.nist.gov", 13, 2000);
        IStreamResponse streamResponse = new StreamResponse();
        IHTML htmlService = new HTMLFromUrl();
        IInternetConnectivity internetConnectivity = new InternetConnectivity(htmlService);
        IServerTime serverTimeService = new ServerTimeService(internetConnectivity, dateTimeParser, connection, streamResponse);

        //Act
        //Try to rename the GameObject
        serverTimeService.GetServerTime(Callback);

        //Assert
        //The object has a new name
        Assert.IsNotNull(serverTimeService);
    }

    private void Callback(DateTime dateTime, string state)
    {
        Debug.Log("DateTime : " + dateTime);
        Debug.Log("State : " + state);        
    }
}