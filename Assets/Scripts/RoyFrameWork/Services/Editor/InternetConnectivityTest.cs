﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using RoyFramework.Services.Api;
using RoyFramework.Services.Impl;

public class InternetConnectivityTest 
{
    [Test]
    public void EditorTest()
    {
        //Arrange
        IHTML htmlService = new HTMLFromUrl();
        IInternetConnectivity internetConnectivity = new InternetConnectivity(htmlService);

        //Act
        //Try to rename the GameObject
        var hasConnectivity = internetConnectivity.HasInternetConnectivity();

        //Assert
        //The object has a new name
        Assert.AreEqual(hasConnectivity, true);
    }
}