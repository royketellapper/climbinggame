﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using RoyFramework.Services.Api;
using RoyFramework.Services.Impl;
using System.Text;

public class HTMLFromUrlTest 
{
    [Test]
    public void EditorTest()
    {
        //Arrange
        IHTML htmlService = new HTMLFromUrl();

        //Act
        //Try to rename the GameObject
        var html = htmlService.GetHTML("http://google.com");

        //Assert
        //The object has a new name
        Assert.AreEqual(html, GetString());
    }

    private string GetString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("<!doctype html><html ");
        stringBuilder.Append("itemscope=\"\" ");
        stringBuilder.Append("itemtype=\"http://schema.org/WebPage\" ");
        stringBuilder.Append("lang=\"nl\"");


        return stringBuilder.ToString();
    }
}