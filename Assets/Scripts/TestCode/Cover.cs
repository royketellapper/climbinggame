using UnityEngine;
//using UnityEditor;
using System.Collections;

public class Cover : MonoBehaviour 
{
	public float raduis;
	public Vector3 dir;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	/*
	// Update is called once per frame
	void Update () 
	{
		//RaycastHit[] wallHits = Physics.SphereCastAll(transform.position,raduis,dir);
		
		Collider[] colliderHits = Physics.OverlapSphere(transform.position,raduis);
		
		foreach(Collider cHits in colliderHits)
		{
			Vector3 closestPoint = cHits.ClosestPointOnBounds(transform.position);
			Vector3 dir = Vector3.Normalize(closestPoint - transform.position);
			
			if (Mathf.Abs(Vector3.Dot(dir, Vector3.up)) <= 0.7f)
			{
				RaycastHit hit;
				Ray ray = new Ray(transform.position, dir);
				

				if (cHits.Raycast(ray, out hit, raduis))
				{
					
						Debug.DrawLine(transform.position ,hit.point);	
					
					DebugSimbols.DrawArrow(hit.point, Quaternion.LookRotation(ray.direction) , 0.2f, 1, true);
					
					//Debug.DrawLine(transform.position ,closestPoint);	
				}
		}
		
			DebugSimbols.DrawSphere(transform.position, raduis, 30, true);
			
			
		}
		
	}
	*/
	
	void Update () 
	{
		RaycastHit[] wallHits = Physics.SphereCastAll(transform.position,raduis,transform.rotation * Vector3.forward);
		
		foreach(RaycastHit wHits in wallHits)
		{
			
			Debug.DrawLine(transform.position ,wHits.point);	
			
		}
		DebugSimbols.DrawArrow(transform.position, transform.rotation , 0.2f, 1, true);
		DebugSimbols.DrawSphere(transform.position, raduis, 30, true);
	}
}
