﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeFinderTest : MonoBehaviour
{
    public float MaxTopAngle = 0.9f;
    public float MaxSideAngle = 0.9f;

    public bool showLines;
    public bool RealTimeLines;

    public int Test;

    protected EdgeFinder _edgeFinder;



    void Start ()
    {
        var meshCollider = GetComponent<MeshCollider>();
        _edgeFinder = new EdgeFinder(meshCollider.sharedMesh);
        _edgeFinder.SetTransform(transform);
        UpdateEdge();
    }

    [ContextMenu("UpdateEdge")]
    void UpdateEdge()
    {
        _edgeFinder.MaxTopAngle = MaxTopAngle;
        _edgeFinder.MaxSideAngle = MaxSideAngle;

        _edgeFinder.CalulateEdges();
    }

    void Update ()
    {
        if (RealTimeLines)
            UpdateEdge();

        if (_edgeFinder == null)
            return;

        if (showLines)
        {
            foreach (var item in _edgeFinder._edgeTriangles)
            {
                //Debug.DrawLine(item.EdgeB.GetConectionVertex(0), item.EdgeB.GetConectionVertex(1), Color.blue);

                Debug.DrawLine(item.EdgeA.GetConectionVertex(0), item.EdgeA.GetConectionVertex(1), Color.blue);
                //item.DrawLines();// .EdgeA.DrawLines(Color.red);

            }
        }

        
        if (Test >= 0 && Test < _edgeFinder._edgeTriangles.Count)
        {
            var edge = _edgeFinder._edgeTriangles[Test];

            edge.EdgeB.DrawLines(Color.red);
            Debug.DrawLine(edge.EdgeA.GetConectionVertex(0), edge.EdgeA.GetConectionVertex(1), Color.yellow);
            

        }
    }
}
