﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class LineRotateTest : MonoBehaviour
{
    public Transform A, B;
    public float angle;
    public Vector3 Euler;

	Vector3 MidPoint ()
    {
        return A.position - ((A.position - B.position) / 2);
	}
	
	void Update ()
    {
        if (A == null || B == null)
            return;

        Quaternion euler = Quaternion.Euler(Euler);

        Vector3 midpoint = MidPoint();
        Vector3 normal = Vector3.Normalize(A.position - B.position);
        Quaternion rorr = Quaternion.LookRotation(A.position - B.position);

        Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up) * rorr;

        Matrix4x4 mat = Matrix4x4.TRS(midpoint, rorr * euler, Vector3.one);

        Debug.DrawLine(A.position , B.position);

        Vector3 point =  mat.MultiplyPoint(Vector3.left);

        Debug.DrawLine(midpoint, point);
    }
}
