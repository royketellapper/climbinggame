﻿using UnityEngine;
using System.Collections;

public class MathTest : MonoBehaviour 
{
	public GameObject A;
	public GameObject B;
	public float max = 10;

	void Start () 
	{
	
	}

	void Update () 
	{
		int OutOfRange;
		Debug.DrawLine(A.transform.position, B.transform.position);

		Vector3 A1 = A.transform.position;
		Vector3 B1 = B.transform.position;
		Vector3 point = transform.position;
		Vector3	PointDir = transform.rotation * Vector3.forward;

		Vector3 closestPointLine1 = Vector3.zero;
		Vector3 closestPointLine2 = Vector3.zero;
		Vector3 closestPointLine3 = Vector3.zero;
		//closestPointLine1 = TraceLine (A.transform.position , B.transform.position, transform.position, transform.rotation , false , max);

	

		Math3D.ClosestPointsOnTwoLines2(out closestPointLine1, out closestPointLine2, A1, (A1 - B1).normalized, point, PointDir,15);
		closestPointLine3 = Math3D.ProjectNormalOnLine(point,PointDir,closestPointLine1);


		//Math3D.ClosestPointsOnTwoLines2(out closestPointLine1, out closestPointLine2, A1, (A1 - B1).normalized, point, PointDir);

		Debug.DrawLine(point,point + PointDir * 15);
		DebugSimbols.DrawSphere(closestPointLine1,0.1f,15,true);
		DebugSimbols.DrawSphere(closestPointLine2,0.1f,15,true);
		DebugSimbols.DrawSphere(closestPointLine3,0.3f,15,true);


		//Debug.Log(Vector3.Dot((A1 - B1).normalized, (A1 - point).normalized) > 0.8f);

		//Vector3 point = Math3D.ProjectPointOnLine(A.transform.position, B.transform.position,transform.position);
		//IsInRange(A.transform.position, B.transform.position,ref point);

		//Debug.DrawRay(transform.position,transform.rotation * Vector3.forward * 10);

		//Debug.Log(IsInRange(A.transform.position , B.transform.position, Loc));


	}


	public static bool ClosestPointsOnTwoLines3(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2, float length)
	{
		closestPointLine1 = Vector3.zero;
		closestPointLine2 = Vector3.zero;
		Vector3 closestPointLine3 = Vector3.zero;
		Vector3 closestPointLine4 = Vector3.zero;
		
		//Vector3 point = linePoint2 + (lineVec2 * length);
		closestPointLine1 = Math3D.ProjectNormalOnLine(linePoint1, lineVec1, linePoint2);

		float L = Vector3.Distance(closestPointLine1,linePoint2);

		float a = Vector3.Dot(lineVec1, lineVec2) * L;

		closestPointLine2 = closestPointLine1 + (lineVec1 * a);



		return false;
	}


	public static bool ClosestPointsOnTwoLines2(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2, float length)
	{
		closestPointLine1 = Vector3.zero;
		closestPointLine2 = Vector3.zero;
		Vector3 closestPointLine3 = Vector3.zero;
		Vector3 closestPointLine4 = Vector3.zero;

		Vector3 point = linePoint2 + (lineVec2 * length);

		closestPointLine1 = Math3D.ProjectNormalOnLine(linePoint1, lineVec1, point);
		closestPointLine2 = Math3D.ProjectNormalOnLine(linePoint2, lineVec2, closestPointLine1);
		//DebugSimbols.DrawSphere(closestPointLine1,1.5f,15,true);

		//Debug.DrawLine(closestPointLine1,closestPointLine2);

		closestPointLine3 = Math3D.ProjectNormalOnLine(linePoint1, lineVec1, closestPointLine2);
		//closestPointLine4 = Math3D.ProjectNormalOnLine(linePoint2, lineVec2, closestPointLine3);
		//closestPointLine2 = Math3D.ProjectNormalOnLine(linePoint2, lineVec2, closestPointLine1);

		//if (Vector3.Dot(point - closestPointLine1, lineVec2) > 0)
		//{
			float L = Vector3.Distance(closestPointLine3,closestPointLine2);
			closestPointLine4 = closestPointLine3 + -lineVec1 * ( Mathf.Tan(Mathf.Asin(Vector3.Dot(lineVec1, lineVec2))) * L );
		//}

		//Debug.Log(Vector3.Dot(point - closestPointLine1, lineVec2));

		DebugSimbols.DrawSphere(closestPointLine4,0.3f,15,true);

		//Debug.DrawLine(closestPointLine3,closestPointLine2);

		return true;
	}

	bool IsInRange(Vector3 A, Vector3 B,ref Vector3 Point) 
	{
		float Length = Vector3.SqrMagnitude(A - B);


		if (Length - Vector3.SqrMagnitude(A - Point) < 0)
		{
			Point = B;
			return false;
		}

		if (Length - Vector3.SqrMagnitude(B - Point) < 0)
		{
			Point = A;
			return false;
		}
		
		return true;
	}

	Vector3 TraceLine (Vector3 A , Vector3  B,Vector3 HitLoc, Quaternion DestinationRot , bool FlipRotation, float MaxReach = 0)
	{
		Vector3 point = Math3D.ProjectPointOnLine(A, B, HitLoc);
		float Distance = Vector3.Distance(point , HitLoc); //Math3D.DistanceXZ(point,HitLoc);
			
		Vector3 ABNormal = Vector3.Normalize(A - B);
		float AngleDot = Vector3.Dot(Vector3.Normalize(HitLoc - point), DestinationRot * Vector3.left) / Vector3.Dot(DestinationRot * Vector3.left, ABNormal);

		Debug.Log(Vector3.Dot(Vector3.Normalize(HitLoc - point), DestinationRot * Vector3.left));



		//Vector3 test = HitLoc - point;
		//test.y = 0;

		//Vector3 ABNormal2D = ABNormal;
		//ABNormal2D.y = 0;

		//Debug.DrawRay(point, Vector3.Cross(test, ABNormal2D));

		//Debug.Log(Vector3.Cross(test, ABNormal2D).y < 0);

		float G = Distance * AngleDot;
		if (MaxReach > 0)
		{
			float Reach = Mathf.Clamp(MaxReach - Distance, 0, MaxReach);
			G = Mathf.Clamp(G, -Reach, Reach);
		}

		Vector3 NewHit = point + (ABNormal * G);

		//IsInRange(A, B,ref NewHit);

		//Debug.Log(Vector3.Dot(Vector3.Normalize(HitLoc - NewHit), DestinationRot * Vector3.forward));

		//float dot = Vector3.Dot(NewHit - HitLoc, DestinationRot * Vector3.forward );

		//Debug.Log(Vector3.Dot((NewHit - HitLoc).normalized, DestinationRot * Vector3.forward ) + "---" + Distance);

		//Debug.Log(dot > 0);

		return NewHit;
	}

	Vector3 TraceLine2 (Vector3 A , Vector3  B,Vector3 HitLoc, Quaternion DestinationRot , bool FlipRotation, float MaxReach = 0)
	{
		Vector3 point = Math3D.ProjectPointOnLine(A, B, HitLoc);
		float Distance = Vector3.Distance(point , HitLoc);
		Vector3 ABNormal = Vector3.Normalize(A - B);
		float AngleDot = Mathf.Asin(Vector3.Dot(DestinationRot * Vector3.forward, ABNormal)) * ((FlipRotation)? -1 : 1);

		float G = Distance * Mathf.Tan(AngleDot);
		if (MaxReach >= 0)
		{
			float Reach = Mathf.Clamp(MaxReach - Distance, 0, MaxReach);
			G = Mathf.Clamp(G, -Reach, Reach);
		}

		return point + ABNormal * G;
	}
}
