﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ThresholdTest : MonoBehaviour
{
    public Transform A, B, C;
    public float Threshold = 0.998f;

    void Update ()
    {
        Debug.DrawLine(A.position, B.position);
        Debug.DrawLine(C.position, B.position);

        Vector3 a, b;
        a = Vector3.Normalize(A.position - B.position);
        b = Vector3.Normalize(C.position - B.position);

        Debug.Log(-Vector3.Dot(a, b) +"----"+ (-Vector3.Dot(a, b) > Threshold));
    }
}
