﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ClimbLineTest : MonoBehaviour
{
    public ClimbStaticHandler ClimbHandler;
    public float radius = 3;


    void Update ()
    {
        if (ClimbHandler == null)
            return;

        ClimbPointHitInfo hitInfo;
        Vector3 closestPointLine;
        if (ClimbHandler.ClosestPointsOnTwoLines(out hitInfo, out closestPointLine, radius, transform.position, transform.forward))
        {
            Debug.DrawRay(transform.position, transform.forward * 3);
            DebugSimbols.DrawSphere(hitInfo.HitPoint, radius, 10, true);
        }
        
        //DebugSimbols.DrawSphere(closestPointLine, radius, 10, true);
    }
}
