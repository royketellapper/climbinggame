using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class Rumbel : MonoBehaviour
{
	[DllImport ("xInput")]
	private static extern bool InitDirectInput();
	
	[DllImport ("xInput")]
	private static extern bool RawForceFeedback(int leftForce, int rightForce);
	
	[DllImport ("xInput")]
	private static extern void FreeDirectInput();

	[DllImport ("xInput")]
	private static extern void UnacquireDevice();

	[DllImport ("xInput")]
	private static extern void AcquireDevice();
	
	public const int maxForce = 10000; // max Force Feedback
	public static bool canVibrate;
	

	
	// Use this for initialization
	void Awake () 
	{
        
		InitDirectInput();
		//OnApplicationFocus(true);
		//PlayRumble = ForceFeedbackHelper.InitDirectInput();
	}
	
	public static bool ForceFeedback(int leftForce, int rightForce)
	{
		if (canVibrate)
			return RawForceFeedback(leftForce, rightForce);
		
		return false;
	}
	
	void Start () 
	{
		AcquireDevice();
	}
	
	// Update is called once per frame
	void Update () 
	{		
		//if (PlayRumble)

	
	}
	/*
	private void OnApplicationFocus(bool focusStatus) 
	{
		if (focusStatus)
			AcquireDevice();
		else
			UnacquireDevice();		
    }
	*/
	
	private void OnDestroy () 
	{
        FreeDirectInput();
    }
}
