using UnityEngine;
using System.Collections;

public struct SplineInfo 
{
	public Vector3 position
	{
		get 
		{
			switch (positionType)
			{
				case PositionType.Vertex: 
					return splineVertex.position;
				case PositionType.Bezier: 
				float alpha = (float)bizierNumber / splineVertex.interpolationSteps;
				return Math3D.CubicInterpolate(spline.vertexList[vertexNumber + 1].position, spline.vertexList[vertexNumber + 1].tangentBegin , splineVertex.tangentEnd, splineVertex.position, alpha);
			}
			
			return Vector3.zero;
		}
	}
	
	public enum PositionType
	{
		Vertex,
		Bezier
	}
	
	public PositionType positionType;
	public Vector3 hitPosition;
	public int vertexNumber;
	public int bizierNumber;	
	public SplineVertex splineVertex;
	public Spline spline;
}
