using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SplineEditor : MonoBehaviour 
{
	public List<Spline> splineList = new List<Spline>();
	public const int boxColliderOffset = 5;
	
	public Spline CreateNewClimbingPoint(Type splineType) 
	{	
		Spline spline = (Spline)ScriptableObject.CreateInstance(splineType);
		spline.parent = gameObject;
		splineList.Add(spline);
		
		return spline;
    } 
	 
    public SplineVertex AddNewVertex(Spline spline) 
	{
		SplineVertex vertex = spline.AddNewVertex(gameObject);
		
		if (spline.vertexList.Count - 2 > 0)
		{
			Vector3 oldLocation = spline.vertexList[spline.vertexList.Count - 3].localPosition;
			Vector3 newLocation = spline.vertexList[spline.vertexList.Count - 2].localPosition;
			
			vertex.localPosition = newLocation + Vector3.Normalize(newLocation - oldLocation) * Vector3.Magnitude(oldLocation - newLocation) / 2;
		}
		else if (spline.vertexList.Count == 1)
			vertex.localPosition = new Vector3(-1,0,1);
		else
			vertex.localPosition = spline.vertexList[spline.vertexList.Count - 2].localPosition + (Vector3.right * 2);
		
		return vertex;
    }
	
	public SplineVertex InsertNewPoint (Spline spline, SplineVertex vertex) 
	{
		int arrayIndex = spline.vertexList.IndexOf(vertex);
			
		if (arrayIndex == spline.vertexList.Count - 1)
			return AddNewVertex(spline);
		
		Vector3 oldLocation = spline.vertexList[arrayIndex].localPosition;
		Vector3 newLocation = spline.vertexList[arrayIndex + 1].localPosition;
		vertex =  spline.InsertNewVertex(arrayIndex + 1, gameObject);	
		
		if (spline.vertexList.Count - 2 > 0)
			vertex.localPosition = ((oldLocation - newLocation) / 2) + newLocation;
		else if (spline.vertexList.Count == 1)
			vertex.localPosition = new Vector3(-1,0,1);
		else
			vertex.localPosition += Vector3.left * 5;
		
		return vertex;
    }
	
	public void RemoveVertex(SplineVertex vertex) 
	{
		foreach(Spline spline in splineList)
		{
			if (spline.vertexList.Contains(vertex))
			{
				if (spline.vertexList.Count <= 1)
					RemoveSplines(spline);
				else
				{
					spline.vertexList.Remove(vertex);
					DestroyImmediate(vertex);
				}
				
				return;
			}
		}	
	}
	
	public void RemoveSplines(Spline spline) 
	{
		Spline sp = splineList.Find(v => v.Equals(spline));
		splineList.Remove(sp);
		DestroyImmediate(sp);
    }
	
	public void RemoveAllSplines() 
	{
		splineList.Clear();
    }
	
    private void OnDrawGizmos() 
	{
		 Gizmos.DrawIcon(transform.position, "ClimbingPoints.png", true);
    }
	
	public void CreateBoxCollider()
	{
		if (splineList.Count == 0)
			return;
		
		BoxCollider boxCollider;
		boxCollider = gameObject.GetComponent<BoxCollider>();
		if (boxCollider == null)
			boxCollider = gameObject.AddComponent<BoxCollider>();
		boxCollider.isTrigger = true;
		
		Vector3 highestPosition = splineList[0].vertexList[0].position - transform.position;
		Vector3 lowestPosition = highestPosition;

		for (int A = 0; A < splineList.Count; A++)
		{
			for (int B = 0; B < splineList[A].vertexList.Count; B++)
			{
				Vector3 localPosition = splineList[A].vertexList[B].position - transform.position;
				
				if (highestPosition.x < localPosition.x) 
					highestPosition.x = localPosition.x;
				
				if (highestPosition.y < localPosition.y)
					highestPosition.y = localPosition.y;
				
				if (highestPosition.z < localPosition.z)
					highestPosition.z = localPosition.z;
				
				if (lowestPosition.x > localPosition.x)
					lowestPosition.x = localPosition.x;
				
				if (lowestPosition.y > localPosition.y)
					lowestPosition.y = localPosition.y;
				
				if (lowestPosition.z > localPosition.z)
					lowestPosition.z = localPosition.z;
			}
		}
		
		boxCollider.center = lowestPosition + (highestPosition - lowestPosition) / 2; 
		boxCollider.size = (highestPosition - lowestPosition) + new Vector3(boxColliderOffset,boxColliderOffset,boxColliderOffset);
	}
}
