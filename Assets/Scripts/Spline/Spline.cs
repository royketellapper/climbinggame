using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

[Serializable]
public class Spline : ScriptableObject//, IEnumerable
{
	public bool useDirection;
	public bool invertDirection;
	public bool isClosed;
	
	[HideInInspector]
	public GameObject parent;
	
	[HideInInspector]
	public List<SplineVertex> vertexList;
	public List<SplineInfo> pointList { get; set;}
	
	public int Count
	{
		get { return 0; }
	}
	
	protected virtual void OnEnable()
	{
		pointList = new List<SplineInfo>();
		for (int a = 0; a < vertexList.Count; a++)
		{
			SplineInfo splineInfo = new SplineInfo();
			splineInfo.spline = this;
			splineInfo.vertexNumber = a;
			splineInfo.splineVertex = vertexList[a];
			
			pointList.Add(splineInfo);
		}
	}
	
	public SplineVertex AddNewVertex(GameObject parent)
	{
		if (parent == null)
			Debug.LogError("Spline Vertex needs to have a parent");
		
		if (vertexList == null)
			vertexList = new List<SplineVertex>();
		
		SplineVertex splineVertex = (SplineVertex)ScriptableObject.CreateInstance<SplineVertex>();
		splineVertex.parent = parent;
		splineVertex.spline = this;
		vertexList.Add(splineVertex);
		
		if (pointList == null)
			pointList = new List<SplineInfo>();
		
		SplineInfo splineInfo = new SplineInfo();
		splineInfo.spline = this;
		splineInfo.vertexNumber = vertexList.Count - 1;
		splineInfo.splineVertex = splineVertex;
		pointList.Add(splineInfo);
		
		return splineVertex;
	} 
	
	public bool isClosedValid()
	{
		if (vertexList == null)
			return false;
		
		return (isClosed && vertexList.Count > 2);
	}
	
	public SplineVertex InsertNewVertex(int index,GameObject parent)
	{
		if (parent == null)
			Debug.LogError("Spline Vertex needs to have a parent");
		
		if (vertexList == null)
			vertexList = new List<SplineVertex>();
		
		SplineVertex splineVertex = (SplineVertex)ScriptableObject.CreateInstance<SplineVertex>();
		splineVertex.parent = parent;
		splineVertex.spline = this;
		vertexList.Insert(index,splineVertex);
		
		return splineVertex;
	} 
	
	public float LineDistance(int start, int end)
	{		
		float dist = 0;
		int total = Mathf.Abs(start - end);
		for (int a = start; a < total; a++)
		{
			if (vertexList[a].interpolationSteps <= 0)
				dist += Vector3.SqrMagnitude(vertexList[a].position - vertexList[a + 1].position);
			else
			{
				Vector3 startLoc, endLoc = Vector3.zero;
				int steps = vertexList[a].interpolationSteps + 1;
				for (int b = 1; b < steps; b++)
				{
					float alpha = (float)b / steps;
					startLoc =  Math3D.CubicInterpolate(vertexList[a + 1].position, vertexList[a + 1].tangentBegin , vertexList[a].tangentEnd, vertexList[a].position, alpha);
					
					if (b == 1)
						dist += Vector3.SqrMagnitude(vertexList[a].position - startLoc);
					else
						dist += Vector3.SqrMagnitude(startLoc - endLoc);

					endLoc = startLoc;
				}
			}
		}
		
		return Mathf.Sqrt(dist);
	}
	
	public float TotalLineDistance()
	{
		Vector3 oldLoc = new Vector3();
		float distance = 0;
		
		for (int a = 0; a < vertexList.Count; a++)
		{
			oldLoc += vertexList[a].position;
			
			if (a != 0)
				distance += Vector3.SqrMagnitude(oldLoc - vertexList[a].position);
		}
		
		if (isClosedValid())
			distance += Vector3.SqrMagnitude(vertexList[vertexList.Count - 1].position - vertexList[0].position);
		
		return Mathf.Sqrt(distance);
	}
	
	public SplineInfo OffsetHitLocation (int startvertex, float offSet, int totalLoops = 1)
	{
		SplineInfo hitInfo = new SplineInfo();
		int vertexNum = 0;
		int loopCount = 0;
		
		while(true)
		{
			if (vertexNum < vertexList.Count)
			{
				if (isClosedValid() && loopCount <= totalLoops)
				{
					loopCount++;
				}
				else
				{
					break;
				}
			}
			
			vertexNum++;
		}
		
		return hitInfo;
	}
	
	public SplineInfo HitLocation (Vector3 startLocation ,float maxRadius = -1, float minRadius = -1,  bool useDir = false, bool invertDir = false)
	{
		SplineInfo hitInfo = new SplineInfo();
		
		
		return hitInfo;
	}
	
	public SplineInfo HitLocation (Vector3 startLocation, Quaternion dirRotation ,float maxRadius = -1, float minRadius = -1, bool invertDir = false)
	{
		SplineInfo hitInfo = new SplineInfo();
		
		
		return hitInfo;
	}
	
	public SplineInfo AngleHitLocation (Vector3 startLocation, Quaternion dirRotation ,float maxRadius = -1, float minRadius = -1, bool invertDir = false)
	{
		SplineInfo hitInfo = new SplineInfo();
		
		
		return hitInfo;
	}
	
	public List<SplineInfo> CylinderHitLocation (Vector3 startLocation, Vector3 endLocation ,float Radius = -1, float minRadius = -1,bool useDir = false, bool invertDir = false)
	{
		List<SplineInfo> hitInfoList = new List<SplineInfo>();
		
		//SplineHitInfo hitInfo = new SplineHitInfo();
		
		
		return hitInfoList;
	}
	
	protected void OnDestroy () 
	{
    	if (vertexList != null)
			for (int a = 0; a < vertexList.Count; a++)
				DestroyImmediate(vertexList[a]);
	}	
}