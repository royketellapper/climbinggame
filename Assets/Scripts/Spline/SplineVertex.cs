using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SplineVertex : ScriptableObject
{
	[NonCopy] 
	public Vector3 position
	{
		// do iets met de tijd om te kijken deze function al verandert is 			Debug.Log(Time.frameCount);
		get { return parent.transform.TransformPoint(localPosition);} //parent.transform.localToWorldMatrix.MultiplyPoint(localPosition); }
    	set { localPosition = parent.transform.InverseTransformPoint(value);} //parent.transform.worldToLocalMatrix.MultiplyPoint(value); }		
	}
	
	[NonCopy]
	public GameObject parent
	{
		get 
		{ 
			if (parentObject == null)
				return spline.parent;
			else
				return parentObject;
		}
		
		set 
		{ 
			if (parentObject != value)
			{
				if (parentObject != null)
					localPosition = position - value.transform.position;
				parentObject = value; 
			}
		}
	}
	
	[NonCopy]
	[HideInInspector]
	public Vector3 localPosition;
	
	[NonCopy]
	[HideInInspector]
	public Spline spline;
	
	[NonCopy]
	[HideInInspector]
	[SerializeField]
	private GameObject parentObject;
	
	//Bezier properties
	[NonCopy]
	[HideInInspector]
	public int interpolationSteps;
	
	[NonCopy]
	public Vector3 tangentBegin
	{
		get { return parent.transform.TransformPoint(localTangentBegin + localPosition); }
    	set { localTangentBegin = (parent.transform.InverseTransformPoint(value) - localPosition); }		
	}
	
	[NonCopy]
	public Vector3 tangentEnd
	{
		get { return parent.transform.TransformPoint(localTangentEnd + localPosition); }
    	set { localTangentEnd = (parent.transform.InverseTransformPoint(value) - localPosition); }		
	}
	
	[NonCopy]
	[HideInInspector]
	public Vector3 localTangentBegin = new Vector3(-3,0,0); // Local space
	
	[NonCopy]
	[HideInInspector]
	public Vector3 localTangentEnd = new Vector3(3,0,0); // Local space
	
	[NonCopy]
	[HideInInspector]
	public bool breakTangent; // if the splineTangent is connected to the next line
	
	public bool isStatic = true;
}