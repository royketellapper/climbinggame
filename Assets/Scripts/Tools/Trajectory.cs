using UnityEngine;
using System.Collections;

public static class Trajectory 
{	
	public static Vector3 TrajectoryPath (Vector3 startLocation, float t, float v, float g, Vector2 a)
	{
		Vector3 vect = new Vector3();
		
		float vtc = v * t * Mathf.Cos(a.y);
		
		vect = startLocation;
		vect.x += Mathf.Cos(a.x) * vtc;
		vect.z += Mathf.Sin(a.x) * vtc;
		vect.y += vtc * Mathf.Tan(a.y) - ((g * Mathf.Pow(vtc, 2)) / (2 * Mathf.Pow(v, 2))) * (1 + Mathf.Pow(Mathf.Tan(a.y), 2));
		
		return vect;
	}	
	
	public static float MaximaleTravelTime (Vector3 startLocation, Vector3 endLocation, float v, float g, float a)
	{
		float som = v * Mathf.Sin(a) + Mathf.Sqrt(Mathf.Pow(v * Mathf.Sin(a), 2) + 2 * g * (startLocation.y - endLocation.y));	
		return som / g;
	}
	
	public static float TrajectoryMinimumTravelTime (Vector3 startLocation, Vector3 endLocation, float v, float g, float a)
	{
		float distance = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2));
		
		return distance / (v * Mathf.Cos(a));
	}
	
	public static Vector2 MaximaleAngle (Vector3 startLocation, Vector3 endLocation, float v, float g)
	{ 
		Vector2 vect = new Vector2();
		
		float m = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2)); //2D magnitude
		float vPow = Mathf.Pow(v,2);
		float root = Mathf.Sqrt(Mathf.Pow(v,4) - g * (g * Mathf.Pow(m,2) + 2 * (endLocation.y - startLocation.y) * vPow));
		root += vPow;	
		vect.y = Mathf.Atan(root / (g * m));
		vect.x = Mathf.Atan2(endLocation.z - startLocation.z, endLocation.x - startLocation.x);
		
		return vect;
	}
	
	public static Vector2 TrajectoryMinimumAngle (Vector3 startLocation, Vector3 endLocation, float v, float g)
	{ 
		Vector2 vect = new Vector2();
		
		float m = -Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2)); //2D magnitude
		float vPow = Mathf.Pow(v,2);
		float root = Mathf.Sqrt(Mathf.Pow(v,4) - g * (g * Mathf.Pow(m,2) + 2 * (endLocation.y - startLocation.y) * vPow));
		root -= vPow;	
		vect.y = Mathf.Atan(root / (g * m));
		vect.x = Mathf.Atan2(endLocation.z - startLocation.z, endLocation.x - startLocation.x);
		
		return vect;
	}
	
	// Velocity functions	
	public static float TrajectoryMaximaleVelocity (Vector3 startLocation, Vector3 endLocation, float g)
	{
		 float m = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2)); //2D magnitude
		 float y = endLocation.y - startLocation.y; 
		
		 float theta = Mathf.Atan ((y + Vector3.Distance(startLocation, endLocation)) / m); 
		 if (theta < 0) theta += Mathf.PI; 
		 return Mathf.Sqrt(-0.5f * g * Mathf.Pow(m ,2) * (1 + Mathf.Tan(theta) * Mathf.Tan(theta)) / (y - m * Mathf.Tan(theta))); 
	}
	
	public static float TrajectoryTerminalVelocity (Vector3 startLocation, Vector3 endLocation, float g, float a)
	{
		float m = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2)); 
		return Mathf.Sqrt((m * g) / Mathf.Sin(2 * a));
	}

    public static float AngleHeight(float height, float gravity, Vector3 startLocation, Vector3 endLocation)
    {
        height -= startLocation.y;

        float range = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x, 2) + Mathf.Pow(startLocation.z - endLocation.z, 2));
        float offsetHeight = endLocation.y - startLocation.y;
        float g = -Physics.gravity.y;

        float VerticalSpeed = Mathf.Sqrt(2 * gravity * height);
        float TravelTime = Mathf.Sqrt(2 * (height - offsetHeight) / g) + Mathf.Sqrt(2 * height / g);
        float HorizontalSpeed = range / TravelTime;
        float velocity = Mathf.Sqrt(Mathf.Pow(VerticalSpeed, 2) + Mathf.Pow(HorizontalSpeed, 2));

        return -Mathf.Atan2(VerticalSpeed / velocity, HorizontalSpeed / velocity) + Mathf.PI;
    }

    public static Vector3 Direction (float v , Vector2 a)
	{
		Vector3 vect = new Vector3();
		
		vect.x = v * (Mathf.Cos(a.x) * Mathf.Cos(a.y));
		vect.z = v * (Mathf.Sin(a.x) * Mathf.Cos(a.y));
		vect.y = v * Mathf.Sin(a.y);
		
		return vect;
	}
	
	public static float RemaininVelocity (Vector3 startLocation, Vector3 endLocation, float v, float g, float a)
	{
		float m = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x,2) + Mathf.Pow(startLocation.z - endLocation.z,2));
		
		float som1 = (g * m) / (v * Mathf.Cos(a));
		float som2 = Mathf.Pow(v, 2) - 2 * g * m * Mathf.Tan(a) + Mathf.Pow(som1,2);
		
		return Mathf.Sqrt(som2);
	}
	
	public static float DistanceVelocity (Vector3 startLocation, Vector3 endLocation, float g, float a)
	{
		return Mathf.Sqrt(Mathf.Abs(startLocation.x - endLocation.x) * g);
	}

    public static float LaunchVelocity(Vector3 startLocation, Vector3 endLocation, float angle)
    {
        float range = Mathf.Sqrt(Mathf.Pow(startLocation.x - endLocation.x, 2) + Mathf.Pow(startLocation.z - endLocation.z, 2));
        float offsetHeight = endLocation.y - startLocation.y;
        float gravity = Physics.gravity.y;

        float velocity = range * range * gravity;
        velocity /= range * Mathf.Sin(2 * angle) + 2 * offsetHeight * Mathf.Pow(Mathf.Cos(angle), 2);
        return Mathf.Sqrt(velocity);
    }

    //Distance functions
    public static float TrajectoryDistance (float v, float g, float a)
	{
		return (Mathf.Pow(v, 2) * Mathf.Sin(2 * a)) / g;
	}
	
	public static float TrajectoryLocationDistance (Vector3 startLocation, Vector3 endLocation, float v, float g, float a)
	{
		float som = (v * Mathf.Cos(a)) / g;
		som *= v * Mathf.Sin(a) + Mathf.Sqrt(Mathf.Pow(v * Mathf.Sin(a), 2) + 2 * g * -Mathf.Abs(startLocation.y - endLocation.y));
		
		return Mathf.Abs(som);
	}
	
	public static float TrajectoryMaxDistance(float v, float g)
	{
		return Mathf.Pow(v, 2) / g;
	}
	
	// Raycast
	public static bool TrajectoryRaycast(int numberOfRays, Vector3 startLocation, float v, float g, Vector2 a , float t ,out RaycastHit hit , int layerMask = -5)
	{
		hit = new RaycastHit();
		
		Vector3 oldLoc = startLocation;
		
		for (int num = 1; num <= numberOfRays; num++)
		{
			float point = ((float)num / numberOfRays) * t;			
			Vector3 loc = TrajectoryPath(startLocation, point, v, g, a);
			
			if (Physics.Raycast(loc, Vector3.Normalize(loc - oldLoc), out hit, Vector3.Distance(loc , oldLoc), layerMask))
				return true;

			oldLoc = loc;
		}
		
		return false;
	}
	
	public static bool TrajectoryRaycast(int numberOfRays, Vector3 startLocation, Vector3 endLocation, float v, float g ,out RaycastHit hit , int layerMask = -5)
	{
		hit = new RaycastHit();
		Vector2 a = MaximaleAngle(startLocation, endLocation, v, g);
		float t = MaximaleTravelTime(startLocation, endLocation, v, g, a.y);
		
		return TrajectoryRaycast(numberOfRays, startLocation, v, g, a, t,out hit, layerMask);	
	}
	
	public static bool TrajectoryRaycast(int numberOfRays, Vector3 startLocation, Vector3 endLocation, float g ,out RaycastHit hit , int layerMask = -5)
	{
		hit = new RaycastHit();
		float v = TrajectoryMaximaleVelocity(startLocation, endLocation, g) + 0.0001f;
		Vector2 a = MaximaleAngle(startLocation, endLocation, v, g);
		float t = MaximaleTravelTime(startLocation, endLocation, v, g, a.y);
		
		return TrajectoryRaycast(numberOfRays, startLocation, v, g, a, t,out hit, layerMask);	
	}
	
	public static bool TrajectoryRaycast(int numberOfRays, Vector3 startLocation, Vector3 endLocation ,out RaycastHit hit , int layerMask = -5)
	{
		hit = new RaycastHit();
		float g = Mathf.Abs(Physics.gravity.y);
		float v = TrajectoryMaximaleVelocity(startLocation, endLocation, g) + 0.0001f;
		Vector2 a = MaximaleAngle(startLocation, endLocation, v, g);
		float t = MaximaleTravelTime(startLocation, endLocation, v, g, a.y);
		
		return TrajectoryRaycast(numberOfRays, startLocation, v, g, a, t,out hit, layerMask);	
	}
	
	//Line cast
	public static bool TrajectoryLinecast(int numberOfRays, Vector3 startLocation, float v, float g, Vector2 a , float t , int layerMask = -5)
	{
		Vector3 oldLoc = startLocation;
		
		for (int num = 1; num <= numberOfRays; num++)
		{
			float point = ((float)num / numberOfRays) * t;			
			Vector3 loc = TrajectoryPath(startLocation, point, v, g, a);
			
			if (Physics.Raycast(oldLoc, loc, layerMask))
				return true;

			oldLoc = loc;
		}
		
		return false;
	}
	
	public static bool TrajectoryLinecast(int numberOfRays, Vector3 startLocation, Vector3 endLocation, float v, float g, int layerMask = -5)
	{
		Vector2 a = MaximaleAngle(startLocation, endLocation, v, g);
		float t = MaximaleTravelTime(startLocation, endLocation, v, g, a.y);
		
		return TrajectoryLinecast(numberOfRays, startLocation, v, g, a , t , layerMask);
	}
	
	public static bool TrajectoryLinecast(int numberOfRays, Vector3 startLocation, Vector3 endLocation, float g, int layerMask = -5)
	{
		float v = TrajectoryMaximaleVelocity(startLocation, endLocation, g) + 0.0001f;
		Vector2 a = MaximaleAngle(startLocation, endLocation, v, g);
		float t = MaximaleTravelTime(startLocation, endLocation, v, g, a.y);	
		
		return TrajectoryLinecast(numberOfRays, startLocation, v, g, a , t , layerMask);
	}
	
	public static bool TrajectoryLinecast(int numberOfRays, Vector3 startLocation, Vector3 endLocation, int layerMask = -5)
	{
		float g = Mathf.Abs(Physics.gravity.y);
		float v = TrajectoryMaximaleVelocity(startLocation, endLocation, g) + 0.0001f;
		Vector2 a = MaximaleAngle(startLocation, endLocation, v, g);
		float t = MaximaleTravelTime(startLocation, endLocation, v, g, a.y);	
		
		return TrajectoryLinecast(numberOfRays, startLocation, v, g, a , t , layerMask);
	}

    //public static Vector3 CalculateHighestPoint(Vector3 startPosition, float velocity, float angle)
    //{
    //    float gravity = -Physics.gravity.y;

    //    // Convert angle to radians
    //    float angleRad = angle;

    //    // Calculate initial velocity components
    //    float initialVelocityX = velocity * Mathf.Cos(angleRad);
    //    float initialVelocityY = velocity * Mathf.Sin(angleRad);

    //    // Calculate the time to reach the highest point
    //    float timeToHighestPoint = initialVelocityY / gravity;

    //    // Calculate the x and z positions at the highest point
    //    float xAtHighestPoint = startPosition.x + initialVelocityX * timeToHighestPoint;
    //    float zAtHighestPoint = startPosition.z; // Assuming no initial z-velocity

    //    // Calculate the y position at the highest point
    //    float yAtHighestPoint = startPosition.y + (initialVelocityY * timeToHighestPoint) - (0.5f * gravity * Mathf.Pow(timeToHighestPoint, 2));

    //    // Combine the x, y, and z positions into the highest point Vector3
    //    return new Vector3(xAtHighestPoint, yAtHighestPoint, zAtHighestPoint);
    //}

    public static Vector3 CalculateHighestPoint(Vector3 startPosition, Vector3 endPosition, float velocity, float angle)
    {
        float ax = Mathf.Atan2(startPosition.z - endPosition.z, startPosition.x - endPosition.x);

        float gravity = -Physics.gravity.y;

        // Calculate initial velocity components
        float initialVelocityX = velocity * Mathf.Cos(angle);
        float initialVelocityY = velocity * Mathf.Sin(angle);

        // Calculate the time to reach the highest point
        float timeToHighestPoint = initialVelocityY / gravity;

		// Calculate the x and z positions at the highest point
		float h = initialVelocityX * timeToHighestPoint;

        float xAtHighestPoint = startPosition.x + (Mathf.Cos(ax) * h);
        float zAtHighestPoint = startPosition.z + (Mathf.Sin(ax) * h);

        // Calculate the y position at the highest point
        float yAtHighestPoint = startPosition.y + (initialVelocityY * timeToHighestPoint) - (0.5f * gravity * Mathf.Pow(timeToHighestPoint, 2));

        // Combine the x, y, and z positions into the highest point Vector3
        return new Vector3(xAtHighestPoint, yAtHighestPoint, zAtHighestPoint);
    }

    public static Vector3 CalculateHighestPoint(Vector3 startPosition, float velocity, Vector2 radians)
    {
        float gravity = -Physics.gravity.y;

        // Calculate initial velocity components
        float initialVelocityX = velocity * Mathf.Cos(radians.y);
        float initialVelocityY = velocity * Mathf.Sin(radians.y);

        // Calculate the time to reach the highest point
        float timeToHighestPoint = initialVelocityY / gravity;

        // Calculate the x and z positions at the highest point
        float h = initialVelocityX * timeToHighestPoint;

        float xAtHighestPoint = startPosition.x + (Mathf.Cos(radians.x) * h);
        float zAtHighestPoint = startPosition.z + (Mathf.Sin(radians.x) * h);

        // Calculate the y position at the highest point
        float yAtHighestPoint = startPosition.y + (initialVelocityY * timeToHighestPoint) - (0.5f * gravity * Mathf.Pow(timeToHighestPoint, 2));

        // Combine the x, y, and z positions into the highest point Vector3
        return new Vector3(xAtHighestPoint, yAtHighestPoint, zAtHighestPoint);
    }
}
