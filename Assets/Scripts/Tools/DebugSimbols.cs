using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public static class DebugSimbols 
{
	public static void DrawArrow(Vector3 startLocation, Quaternion rotation, Vector3 scale, float length = 1 , bool debug = false)
	{
		#if UNITY_EDITOR
		length += 1;
		Vector3 offset = new Vector3();
		Matrix4x4 arrowMat = Matrix4x4.TRS(startLocation, rotation, scale);
		
		offset.z = length;
		Vector3 endPoint = arrowMat.MultiplyPoint3x4(offset);
		DrawLine(startLocation,endPoint, debug);
		
		offset.z -= 1;
		offset.x = 1;
		Vector3 arrowPoint = arrowMat.MultiplyPoint3x4(offset);
		DrawLine(endPoint, arrowPoint, debug);
		
		offset.x = -1;
		arrowPoint = arrowMat.MultiplyPoint3x4(offset);
		DrawLine(endPoint, arrowPoint, debug);
		#endif
	}

    public static void DrawArrow(Vector3 startLocation, Vector3 normal, float scale = 1, float length = 1, bool debug = false)
    {
    #if UNITY_EDITOR
        DrawArrow(startLocation, Quaternion.LookRotation(normal), Vector3.one * scale, length, debug);
    #endif
    }

    public static void DrawArrow(Vector3 startLocation, Quaternion rotation , float scale = 1, float length = 1, bool debug = false)
	{
		#if UNITY_EDITOR
		DrawArrow(startLocation ,rotation ,Vector3.one * scale,length , debug);
		#endif
	}
	
	public static void DrawSphere(Vector3 center, float radius, int segments, bool debug)
	{
		#if UNITY_EDITOR
		Vector3 p1 = center;
		Vector3 p2 = center;
		Vector3 p3 = center;
		Vector3 pos = center;
		
		for (int a = 0; a <= segments; a++)
		{
			float angle = ((float)a / segments) * Mathf.PI * 2;
			
			pos = center;
			pos.z = center.z + Mathf.Cos(angle) * radius;
			pos.x = center.x + Mathf.Sin(angle) * radius;
			
			if (a != 0)
				DrawLine(pos, p1, debug);
			p1 = pos;
			
			pos = center;
			pos.y = center.y + Mathf.Cos(angle) * radius;
			pos.x = center.x + Mathf.Sin(angle) * radius;
			
			if (a != 0)
				DrawLine(pos, p2, debug);
			p2 = pos;
			
			pos = center;
			pos.y = center.y + Mathf.Cos(angle) * radius;
			pos.z = center.z + Mathf.Sin(angle) * radius;
			
			if (a != 0)
				DrawLine(pos, p3, debug);
			p3 = pos;
		}
		#endif
	}
	
	private static void DrawLine(Vector3 start, Vector3 end,bool debug)
	{
		#if UNITY_EDITOR
		if (debug)
			Debug.DrawLine(start, end);
		else
			Handles.DrawLine(start, end);
		#endif
	}

	private static void DrawLine(Vector3 start, Vector3 end,bool debug, Color color)
	{
		#if UNITY_EDITOR
		if (debug)
			Debug.DrawLine(start, end, color);
		else
			Handles.DrawLine(start, end);
		#endif
	}
}
