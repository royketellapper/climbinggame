﻿using System;
using UnityEngine;

public class AnimationFixedComponent : MonoBehaviour
{
    [System.Serializable]
    public class Weighted
    {
        public WeightedMode In = WeightedMode.In;
        public WeightedMode Out = WeightedMode.Out;
    }

    public Weighted Active, InActive;

    public Transform FootLeft, FootRight;
    public float Distance = 1;

    public string AnimName = "running";
    public float KeyDistance = 0.03f;

    public AnimationClip Clip;

    bool activeR, activeL, firstR, firstL;
    public AnimationCurve CurveR, CurveL;
    Animator animator;
    AnimatorStateInfo animationState;

    bool Playing;

    float _time, _speed, _keyDistance, _length, _normalTime;

    public Action<AnimationFixedComponent> OnComplete;

    public static AnimationFixedComponent Instance;

    void Awake()
    {
        animator = GetComponent<Animator>();
        Instance = this;
    }

    [ContextMenu("Play")]
    public void Play()
    {
        Debug.Log(Clip.length);

        Play(AnimName, Clip.length);
    }

    public void Play(string animName, float length)
    {
        CurveR = new AnimationCurve();
        CurveL = new AnimationCurve();

        animator.Play(animName);
        Playing = true;
        _time = -(1f / length);
        _length = length;
        _speed = (1f / length) * 0.01f;   
        _keyDistance = (1f / length) * KeyDistance;
        _normalTime = 0;

        AnimName = animName;
        Debug.Log("Start " + animName);
    }

    // Update is called once per frame
    void Update()
    {
        if (Playing)
        {
            animator.SetFloat("Time", Mathf.Repeat(_time, 1f / _length));
            RayCastFoot(FootLeft, CurveL, _time, ref activeL, ref firstL);
            RayCastFoot(FootRight, CurveR, _time, ref activeR, ref firstR);
            _time += _speed;
            _normalTime = (_time * _length) / _length;

            Debug.Log(_normalTime);

            if (_time >= (1f / _length) * 2)
            {
                Playing = false;
                CleanUp(CurveR);
                CleanUp(CurveL);
                OnComplete?.Invoke(this);

                Debug.Log("Finish "+ AnimName);
            }
        }
    }

    void CleanUp(AnimationCurve curve)
    {
        if (!Array.Exists(curve.keys, k => k.time == 0))
        {
            float v = curve.Evaluate(0);
            Keyframe key = new Keyframe();
            key.time = 0;
            key.value = v;
            key.weightedMode = WeightedMode.Out;
            curve.AddKey(key);
        }

        if (!Array.Exists(curve.keys, k => k.time == 1))
        {

            float v = curve.Evaluate(1);
            Keyframe key = new Keyframe();
            key.time = 1;
            key.value = v;
            key.weightedMode = WeightedMode.In;
            curve.AddKey(key);
        }

        for (int i = 0; i < curve.keys.Length; i++)
        {
            Keyframe keyframe = curve.keys[i];

            if (keyframe.time < 0)
            {
                curve.RemoveKey(i);
                i--;
            }

            if (keyframe.time > 1)
            {
                curve.RemoveKey(i);
                i--;
            }
        }
    }

    void RayCastFoot(Transform trans, AnimationCurve curve, float time, ref bool active, ref bool first)
    {
        if (Physics.Raycast(trans.position, -trans.up, out RaycastHit hit, Distance))
        {
            if (!first)
            {
               // curve.AddKey(_normalTime, 1);
                first = true;
                active = true;
            }
            else if (!active)
            {
                Keyframe keyA = new Keyframe();
                keyA.weightedMode = Active.Out;
                keyA.time = _normalTime - _keyDistance;
                keyA.value = 0;
                curve.AddKey(keyA);

                Keyframe keyB = new Keyframe();
                keyB.weightedMode = Active.In;
                keyB.time = _normalTime + _keyDistance;
                keyB.value = 1;
                curve.AddKey(keyB);


                active = true;
            }
        }
        else
        {
            if (!first)
            {
                //curve.AddKey(_normalTime, 0);
                first = true;
                active = false;
            }
            else if (active)
            {
                Keyframe keyA = new Keyframe();
                keyA.weightedMode = Active.Out;
                keyA.time = _normalTime - _keyDistance;
                keyA.value = 1;
                curve.AddKey(keyA);

                Keyframe keyB = new Keyframe();
                keyB.weightedMode = Active.In;
                keyB.time = _normalTime + _keyDistance;
                keyB.value = 0;
                curve.AddKey(keyB);


                active = false;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (Physics.Raycast(FootLeft.position, -FootLeft.up, Distance))
            Debug.DrawRay(FootLeft.position, -FootLeft.up * Distance, Color.red);
        else
            Debug.DrawRay(FootLeft.position, -FootLeft.up * Distance, Color.white);

        if (Physics.Raycast(FootRight.position, -FootRight.up, Distance))
            Debug.DrawRay(FootRight.position, -FootRight.up * Distance, Color.red);
        else
            Debug.DrawRay(FootRight.position, -FootRight.up * Distance, Color.white);
    }
}
