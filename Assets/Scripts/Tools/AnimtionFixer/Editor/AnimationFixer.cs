﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using Object = UnityEngine.Object;

public class AnimationFixer : EditorWindow
{
    public class AnimationInfo
    {
        public AnimationInfo(string name, float length)
        {
            Name = name;
            Length = length;
        }

        public string Name;
        public float Length;

        public List<ClipAnimationInfoCurve> Curves = new List<ClipAnimationInfoCurve>();
    }

    public class AnimationData
    {
        public Object AnimationPrefab;
        public ModelImporter ModelImporter;
        public List<AnimationInfo> AnimationInfo = new List<AnimationInfo>();
        public int index = 0;
    }
    protected List<AnimationData> _animationCurves = new List<AnimationData>();
    protected int _currentAnimationIndex = 0;

    [SerializeField]
    public AnimatorController AnimatorController;
    //[SerializeField]
    //public AnimationFixedComponent AnimationFixedComponent;


    // protected Animator Animator;

    // Add menu item named "My Window" to the Window menu
    [MenuItem("VascoGames/Fix Animtion")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(AnimationFixer));
    }

    protected void OnEnable()
    {
        var data = EditorPrefs.GetString("AnimationFixer", JsonUtility.ToJson(this, false));
        JsonUtility.FromJsonOverwrite(data, this);
    }

    protected void OnDisable()
    {
        var data = JsonUtility.ToJson(this, false);
        EditorPrefs.SetString("AnimationFixer", data);
    }


    void OnGUI()
    {
        GUILayout.BeginVertical("Box");
        AnimatorController = EditorGUILayout.ObjectField("AnimatorController", AnimatorController, typeof(AnimatorController), true) as AnimatorController;
        // AnimationFixedComponent = EditorGUILayout.ObjectField("Animation Component", AnimationFixedComponent, typeof(AnimationFixedComponent), true) as AnimationFixedComponent;

        EditorGUI.BeginDisabledGroup(Selection.objects.Length == 0);
        if (GUILayout.Button("Test AnimtionController"))
            CreateAnimtionController();
        if (GUILayout.Button("Run"))
            Run();

        EditorGUI.EndDisabledGroup();

        GUILayout.EndVertical();
    }

    void CreateAnimtionController()
    {
        AnimatorController.layers = new AnimatorControllerLayer[0];
        AnimatorController.AddLayer("default");
        AnimatorController.parameters = new AnimatorControllerParameter[0];

        Object[] selectedObjs = Selection.objects;
        foreach (var selected in selectedObjs)
        {
            string path = AssetDatabase.GetAssetPath(selected);
            Object[] objects = AssetDatabase.LoadAllAssetsAtPath(path);
            AnimatorController.AddParameter("Time", AnimatorControllerParameterType.Float);

            foreach (Object obj in objects)
            {
                AnimationClip clip = obj as AnimationClip;
                if (clip != null && !clip.name.Contains("__preview__"))
                {
                    var state = AnimatorController.AddMotion(clip, 0);
                    state.timeParameter = "Time";
                    state.timeParameterActive = true;
                }
            }
        }
    }

    void Run()
    {
        EditorApplication.isPlaying = true;

        _currentAnimationIndex = 0;
        AnimationFixedComponent.Instance.OnComplete = null;
        AnimationFixedComponent.Instance.OnComplete = AnimationComplete;
        _animationCurves.Clear();

        AnimatorController.layers = new AnimatorControllerLayer[0];
        AnimatorController.AddLayer("default");
        AnimatorController.parameters = new AnimatorControllerParameter[0];

        Object[] selectedObjs = Selection.objects;
        foreach (var selected in selectedObjs)
        {
            AnimationData animationData = new AnimationData();

            string path = AssetDatabase.GetAssetPath(selected);
            animationData.ModelImporter = AssetImporter.GetAtPath(path) as ModelImporter;

            Object[] objects = AssetDatabase.LoadAllAssetsAtPath(path);
            AnimatorController.AddParameter("Time", AnimatorControllerParameterType.Float);

            foreach (Object obj in objects)
            {
                AnimationClip clip = obj as AnimationClip;
                if (clip != null && !clip.name.Contains("__preview__"))
                {
                    var state = AnimatorController.AddMotion(clip, 0);
                    state.timeParameter = "Time";
                    state.timeParameterActive = true;

                    animationData.AnimationInfo.Add(new AnimationInfo(clip.name, clip.length));
                }
            }


            _animationCurves.Add(animationData);
        }




        //Werkt
        //ClipAnimationInfoCurve[] curves = new ClipAnimationInfoCurve[3];
        //for (int i = 0; i < 3; i++)
        //{
        //    curves[i] = new ClipAnimationInfoCurve();
        //    curves[i].name = i.ToString();
        //    curves[i].curve = curve;
        //}
        //var clipAnim = modelImporter.clipAnimations;
        //clipAnim[select].curves = curves;
        //modelImporter.clipAnimations = clipAnim;




        //Debug.Log(modelImporter.defaultClipAnimations.Length);

        //EditorCurveBinding binding = new EditorCurveBinding();
        //binding.propertyName = "Test";
        ////binding.type = typeof(Object);

        ////AnimationUtility.SetObjectReferenceCurve

        //AnimationUtility.SetEditorCurve(AnimationClip, binding, curve);

        //AnimationClip.SetCurve()

        //ClipAnimationInfoCurve 
        // 

        NextAnimation(0);
    }


    void AnimationComplete(AnimationFixedComponent animeCurve)
    {
        var curData = _animationCurves[_currentAnimationIndex];
        var infoCurve = curData.AnimationInfo[curData.index];

        var clipCurve = new ClipAnimationInfoCurve();
        clipCurve.curve = animeCurve.CurveL;
        clipCurve.name = "FootLeft";
        infoCurve.Curves.Add(clipCurve);

        clipCurve = new ClipAnimationInfoCurve();
        clipCurve.curve = animeCurve.CurveR;
        clipCurve.name = "FootRight";
        infoCurve.Curves.Add(clipCurve);

        if (NextAnimation())
        {
            SaveCurveData();
            EditorUtility.DisplayDialog("Finish", "Finish adding curves", "Ok");
        }
    }

        bool NextAnimation(int index = 1)
    {
        if (_currentAnimationIndex < _animationCurves.Count)
        {
            var curData = _animationCurves[_currentAnimationIndex];
            curData.index += index;
            if (curData.index < curData.AnimationInfo.Count)
            {
                var animInfo = curData.AnimationInfo[curData.index];
                AnimationFixedComponent.Instance.Play(animInfo.Name, animInfo.Length);
                return false;
            }
            else
            {
                _currentAnimationIndex++;
                return NextAnimation(0);
            }
        }

        return true;
    }

    void SaveCurveData()
    {
        foreach (var animCurves in _animationCurves)
        {
            var defaultClips = animCurves.ModelImporter.clipAnimations;

            foreach (var animInfo in animCurves.AnimationInfo)
            {
                var clipAnimation = Array.Find(defaultClips, b => b.name == animInfo.Name);

                if (clipAnimation != null)
                    clipAnimation.curves = animInfo.Curves.ToArray();
            }

            animCurves.ModelImporter.clipAnimations = defaultClips;
            animCurves.ModelImporter.SaveAndReimport();
        }
    }
}
