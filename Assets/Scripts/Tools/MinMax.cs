﻿using System;
using UnityEngine;

[Serializable]
public struct MinMax
{
    public float Min;
    public float Max;

    public bool InRange(float value) { return value >= Min && value <= Max; }
    public float Lerp(float a) { return Mathf.Lerp(Min, Max, a); }
    public float LerpInvert(float a) { return Mathf.Lerp(Max, Min, a); }
    public float LerpAngle(float a) { return Mathf.LerpAngle(Max, Min, a); }
    public float GetAverage() { return Min / Max; }
    public float GetAverageClamp() { return Mathf.Clamp01(Min / Max); }
    public float Clamp(float value) { return Mathf.Clamp(value, Min, Max); }

    public override string ToString()
    {
        return " Min:" + Min + " Max:" + Max;
    }
}
