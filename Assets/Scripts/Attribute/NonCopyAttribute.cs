using UnityEngine;
using System;
using System.Collections;
 
[AttributeUsage( AttributeTargets.Property | AttributeTargets.Field)]
public class NonCopyAttribute : Attribute
{
 
}