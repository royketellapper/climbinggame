using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public abstract class State : ScriptableObject 
{
	public bool keepStateInMemory = true;
	
	[HideInInspector]
	public StateManager stateManager;
	
	public bool BeginState (Type stateName)
	{
		return true; 
	}
	
	public void UpdateState()
	{
		
	}
	
	public bool EndState (Type stateName)
	{
		return true;
	}
}
