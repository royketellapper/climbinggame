using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic; 

[System.Serializable]   
public class StateManager : MonoBehaviour 
{
	public List<State> states
	{
		get { return stateList; }
	}
	
	public delegate void StateChanged(State state);
	public event StateChanged stateAdded;
	public event StateChanged stateRemoved;
	
	[SerializeField] 
	private List<State> stateList = new List<State>();
	
	public Type OldStateType { get; private set;}
	public Type CurrentStateType { get; private set;}
	public State OldState { get; private set;}
	public State CurrentState { get; private set;}
	public object owner;
	
	public bool GotoState(Type stateName)
	{
		if (stateName == CurrentStateType)
			return true;
		
		State state = FindState(stateName);
		
		if (state == null || stateName == null)
			return false;
		
		if (CurrentState != null)
			if (!CurrentState.EndState(stateName))
				return false;
		
		if (!state.BeginState(stateName))
			return false;
		
		OldState = CurrentState;

		if (!CurrentState.keepStateInMemory)
		{
			stateList.Remove(CurrentState);
			
			if (stateRemoved != null)
				stateRemoved(CurrentState);
		}
		else
			OldStateType = CurrentState.GetType();
		
		CurrentState = state;
		CurrentStateType = state.GetType();
		
		return true;
	}
	
	// For debug only
	public void ForseState(Type stateName)
	{
		if (stateName == CurrentStateType)
			return;
		
		State state = FindState(stateName);
		
		if (state == null || stateName == null) 
			return;
		
		if (CurrentState != null)
			CurrentState.EndState(stateName);

		state.BeginState(stateName);
		
		OldState = CurrentState;

		if (!CurrentState.keepStateInMemory)
		{
			stateList.Remove(CurrentState);
			
			if (stateRemoved != null)
				stateRemoved(CurrentState);
		}
		else
			OldStateType = CurrentState.GetType();
		
		CurrentState = state;
		CurrentStateType = state.GetType();
	}
	
	
	public State FindState(Type stateName, bool createStateIfNotExist = true)
	{
		State state = null;
		
 		for (int a = 0; a < stateList.Count; a++)
		{
			if (stateList[a] != null)
			{
				if (stateList[a].GetType() == stateName)
					state = stateList[a];
			}
			else
				stateList.RemoveAt(a);
		}
		
		if (state == null && createStateIfNotExist) 
		{
			if (stateName.IsSubclassOf(typeof(State)))
			{
				state = (State)ScriptableObject.CreateInstance(stateName); //Activator.CreateInstance(stateName);
				state.stateManager = this;
				stateList.Add(state);
				
				if (stateAdded != null)
					stateAdded(state);
			}
			else
				Debug.LogWarning(stateName.Name + " is not a subclass of the class State.");
		}
		
		return state;
	}
	
	public void RemoveState (Type stateName)
	{
		State state = stateList.Find(a => a.GetType() == stateName);
		
		if (state != CurrentState)
		{
			if (stateRemoved != null)
				stateRemoved(state);
			
			stateList.Remove(state);
			DestroyImmediate(state);
		}
	}
	
	public void RemoveState (State state)
	{
		if (state != CurrentState && state != null)
		{
			stateList.Remove(state);
			
			if (stateRemoved != null)
				stateRemoved(state);
			
			DestroyImmediate(state);
		}
	}
	
	private void Update () 
	{
		if (CurrentState != null)
			CurrentState.UpdateState();
	}
}
