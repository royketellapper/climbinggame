﻿using RoyFramework.Game;
using RoyFrameworkEditor.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MovementController))]
public class MovementControllerEditor : Editor
{
    protected string[] _ignoreProperty;
    protected List<SerializedProperty> _stateProperty = new List<SerializedProperty>();
    protected List<Type> _stateTypes = new List<Type>();
    protected MovementController _movementController;

    public void OnEnable()
    {
        _movementController = target as MovementController;
        FindStates();
    }

    protected void FindStates()
    {
        Type movementController = typeof(MovementController);
        List<FieldInfo> propertys = new List<FieldInfo>(movementController.GetFields(BindingFlags.Public | BindingFlags.Instance));
        List<string> ignoreProperty = new List<string>();

        Type movementState = typeof(MovementState);
        for (int i = 0; i < propertys.Count; i++)
        {
            var item = propertys[i];
            if (item.FieldType.IsSubclassOf(movementState))
            {
                ignoreProperty.Add(item.Name);
                _stateProperty.Add(serializedObject.FindProperty(item.Name));
                _stateTypes.Add(item.FieldType);
            }
            else
            {
                propertys.RemoveAt(i);
                i--;
            }
        }

        _ignoreProperty = ignoreProperty.ToArray();
    }

    public override void OnInspectorGUI()
    {
        EditorUtilityFunctions.DrawDefaultInspector(serializedObject, _ignoreProperty);
        
        EditorGUILayout.LabelField("Movement state");
        int count = _stateProperty.Count;
        for (int i = 0; i < count; i++)
        {
            if (_movementController.CurrentState != null && _movementController.CurrentState.GetType() == _stateTypes[i])
            {
                GUI.color = Color.green;
                EditorGUILayout.BeginVertical("Box");
                GUI.color = Color.white;
            }
            else
            {
                GUI.color = Color.blue;
                EditorGUILayout.BeginVertical("Box");
                GUI.color = Color.white;
            }

            EditorGUILayout.PropertyField(_stateProperty[i], _stateProperty[i].isExpanded);
            EditorGUILayout.EndVertical();
           
        }

        EditorUtilityFunctions.GUIChanged(serializedObject);
    }
}
