﻿using UnityEngine;
using RoyFramework.Audio;

namespace RoyFramework
{
    public class PlayAudioAnimation : StateMachineBehaviour
    {
        public AudioClip[] Clips;
        public SoundType Type;
        public float Volume = 1;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            AudioManager.Instance.PlayRandomOneShot(Clips, Volume, Type);
        }
    }
}