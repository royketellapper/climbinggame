﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

namespace RoyFramework.Game
{
    [System.Serializable]
    public class WalkState : MovementState
    {
        //[Header("---! Layers !---")]
        [Tooltip("Layers that the character can walk on")]
        public LayerMask groundLayer = 1 << 0;


        [Header("RootMotion"), Range(0, 1)]
        public float SteeringControll = 0.5f;
        public float RotationSpeed = 5;

        //[Tooltip("Distance to became not grounded")]
        //[SerializeField]
        //protected float groundMinDistance = 0.2f;
        //[SerializeField]
        //protected float groundMaxDistance = 0.5f;

        //[Header("--- Locomotion Setup ---")]
        //[Tooltip("Check to control the character while jumping")]
        //public bool JumpAirControl = true;
        //[Tooltip("How much time the character will be jumping")]
        //public float JumpTimer = 0.3f;
        //[HideInInspector]
        //public float JumpCounter;
        //[Tooltip("Add Extra jump speed, based on your speed input the character will move forward")]
        //public float JumpForward = 3f;
        //[Tooltip("Add Extra jump height, if you want to jump only with Root Motion leave the value with 0.")]
        //public float JumpHeight = 4f;

        //[Tooltip("Check to drive the character using RootMotion of the animation")]
        //public bool useRootMotion = false;

        //[Tooltip("ADJUST IN PLAY MODE - Offset height limit for sters - GREY Raycast in front of the legs")]
        //public float StepOffsetEnd = 0.45f;
        //[Tooltip("ADJUST IN PLAY MODE - Offset height origin for sters, make sure to keep slight above the floor - GREY Raycast in front of the legs")]
        //public float StepOffsetStart = 0.05f;
        //[Tooltip("Higher value will result jittering on ramps, lower values will have difficulty on steps")]
        //public float StepSmooth = 4f;
        //[Tooltip("Max angle to walk")]
        //public float SlopeLimit = 45f;
        //[Tooltip("Apply extra gravity when the character is not grounded")]
        //public float ExtraGravity = -10f;

        protected float groundDistance;
        protected Vector2 _inputMovement;
        public RaycastHit groundHit;

        public override void OnEnable(MovementState oldState)
        {
            //Rigidbody
            _rigidbody.freezeRotation = true;

            //Input
            _inputMaster.Player.Movement.started += OnInputMovement;
            _inputMaster.Player.Movement.performed += OnInputMovement;
            _inputMaster.Player.Movement.canceled += OnInputMovement;
            _inputMaster.Player.Movement.Enable();
        }

        public override void OnDisable(MovementState newState)
        {
            //Input
            _inputMaster.Player.Movement.started -= OnInputMovement;
            _inputMaster.Player.Movement.performed -= OnInputMovement;
            _inputMaster.Player.Movement.canceled -= OnInputMovement;
            _inputMaster.Player.Movement.Disable();
        }

        protected void OnInputMovement(CallbackContext context)
        {
            _inputMovement = context.ReadValue<Vector2>();
        }

        public override void FixedUpdate()
        {

        }

        protected float _smootRotation;

        public override void OnMoveAnimator()
        {
            Camera camera = MovementController.Camera;
            Vector3 rot = camera.transform.eulerAngles;

            //Animtions
            float inputMagnitude = _inputMovement.magnitude;
            bool isMoving = inputMagnitude > 0.1;
            _animationController.SetMoving(inputMagnitude > 0.1);
            _animationController.SetInputMagnitude(inputMagnitude);
            if (isMoving)
                _animationController.SetStopMovingMagnitude(inputMagnitude);


            //Movements
            float delta = (rot.y * Mathf.Deg2Rad) + Mathf.Atan2(_inputMovement.x, _inputMovement.y);
            _smootRotation = Mathf.LerpAngle(_smootRotation, delta * Mathf.Rad2Deg, Time.deltaTime * RotationSpeed);


            float moveAngle = Mathf.DeltaAngle(_characterTransform.rotation.eulerAngles.y, _smootRotation);
            Vector3 moveDir = new Vector3(Mathf.Sin(_smootRotation * Mathf.Deg2Rad), 0, Mathf.Cos(_smootRotation * Mathf.Deg2Rad));
            Debug.DrawRay(_transform.position, moveDir);

            _animationController.SetInputAngle(moveAngle);

            // Debug.Log(_animator.deltaPosition.magnitude);

            moveDir *= _animator.deltaPosition.magnitude;
            moveDir = Vector3.Lerp(moveDir, _animator.deltaPosition, SteeringControll);

            Debug.DrawRay(_transform.position, Vector3.Normalize(moveDir) * 1, Color.red);

            SetMovement(moveDir, _animator.deltaRotation);
        }

        void CheckGroundDistance()
        {
            if (MovementController.CapsuleCollider != null)
            {
                // radius of the SphereCast
                float radius = MovementController.CapsuleCollider.radius;
                float colliderHeight = MovementController.CapsuleCollider.height;
                var dist = 10f;
                // position of the SphereCast origin starting at the base of the capsule
                Vector3 pos = MovementController.Transform.position + Vector3.up * radius;
                // ray for RayCast
                Ray ray1 = new Ray(MovementController.Transform.position + new Vector3(0, colliderHeight / 2, 0), Vector3.down);
                // ray for SphereCast
                Ray ray2 = new Ray(pos, -Vector3.up);
                // raycast for check the ground distance
                if (Physics.Raycast(ray1, out groundHit, colliderHeight / 2 + 2f, groundLayer))
                    dist = MovementController.Transform.position.y - groundHit.point.y;
                // sphere cast around the base of the capsule to check the ground distance
                if (Physics.SphereCast(ray2, radius * 0.9f, out groundHit, radius + 2f, groundLayer))
                {
                    // check if sphereCast distance is small than the ray cast distance
                    if (dist > (groundHit.distance - radius * 0.1f))
                        dist = (groundHit.distance - radius * 0.1f);
                }
                groundDistance = (float)System.Math.Round(dist, 2);
            }
        }

        //protected Vector3 angularVelocity;
        protected void SetMovement(Vector3 deltaPosition, Quaternion rotation, float speedMultiplier = 1)
        {
            if (Time.deltaTime == 0) return;

            // if (useRootMotion)
            // {
            Vector3 v = (deltaPosition * (speedMultiplier > 0 ? speedMultiplier : 1f)) / Time.deltaTime;
            v.y = _rigidbody.velocity.y;
            _rigidbody.velocity = Vector3.Lerp(_rigidbody.velocity, v, 20f * Time.deltaTime);


            _characterTransform.rotation *= rotation;

            //Debug.Log(_animator.angularVelocity);

            //_rigidbody.angularVelocity = _animator.angularVelocity;
            //}
            //else
            //{
            //    var velY = _transform.forward * velocity * speed;
            //    velY.y = _rigidbody.velocity.y;
            //    var velX = _transform.right * velocity * direction;
            //    velX.x = _rigidbody.velocity.x;

            //    if (isStrafing)
            //    {
            //        Vector3 v = (_transform.TransformDirection(new Vector3(input.x, 0, input.y)) * (velocity > 0 ? velocity : 1f));
            //        v.y = _rigidbody.velocity.y;
            //        _rigidbody.velocity = Vector3.Lerp(_rigidbody.velocity, v, 20f * Time.deltaTime);
            //    }
            //    else
            //    {
            //        _rigidbody.velocity = velY;
            //        _rigidbody.AddForce(_transform.forward * (velocity * speed) * Time.deltaTime, ForceMode.VelocityChange);
            //    }
            //}
        }

        //float GroundAngle()
        //{
        //    var groundAngle = Vector3.Angle(groundHit.normal, Vector3.up);
        //    return groundAngle;
        //}
    }
}