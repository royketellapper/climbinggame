﻿using RootMotion.FinalIK;
using UnityEngine;

namespace RoyFramework.Game
{
    [System.Serializable]
    public abstract class MovementState
    {
        public MovementController MovementController { get; set; }
        protected Transform _transform;
        protected AnimationController _animationController;
        protected FullBodyBipedIK _fullBodyBipedIK;
        protected CapsuleCollider _capsuleCollider;
        protected Rigidbody _rigidbody;
        protected Transform _characterTransform;
        protected Animator _animator;
        protected InputMaster _inputMaster;

        public virtual void Init()
        {
            _inputMaster = MovementController.InputMaster; 
            _transform = MovementController.transform;
            _animationController = MovementController.AnimationController;
            _fullBodyBipedIK = MovementController.FullBodyBipedIK;
            _capsuleCollider = MovementController.CapsuleCollider;
            _rigidbody = MovementController.Rigidbody;
            _characterTransform = MovementController.CharacterTransform;
            _animator = MovementController.AnimationController.Animator;
        }
        public virtual void OnEnable(MovementState oldState) { }
        public virtual void OnDisable(MovementState newState) { }
        public virtual void FixedUpdate() { }
        public virtual void OnMoveAnimator() { }
    }
}