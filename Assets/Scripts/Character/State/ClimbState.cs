﻿using UnityEngine;

namespace RoyFramework.Game
{
    [System.Serializable]
    public class ClimbState : MovementState
    {
        public override void OnEnable(MovementState oldState)
        {

        }

        public override void OnDisable(MovementState newState)
        {

        }

        public override void FixedUpdate()
        {

        }
    }
}