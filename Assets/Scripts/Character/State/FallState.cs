﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoyFramework.Game
{
    [System.Serializable]
    public class FallState : MovementState
    {
        public override void OnEnable(MovementState oldState)
        {

        }

        public override void OnDisable(MovementState newState)
        {

        }

        public override void FixedUpdate()
        {

        }
    }
}