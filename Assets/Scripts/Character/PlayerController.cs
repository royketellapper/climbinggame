﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoyFramework.Game
{
    [RequireComponent(typeof(MovementController))]
    public class PlayerController : MonoBehaviour
    {
        public MovementController MovementController { get; protected set; }
        public CameraController CameraController { get; protected set; }

        protected virtual void Awake()
        {
            MovementController = GetComponent<MovementController>();
            CameraController = Camera.main.GetComponent<CameraController>();
        }

        protected virtual void Update()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            //ThirdPersonState cameraState = CameraController.CurrentCameraState as ThirdPersonState;

            //if (cameraState != null)
        }
    }
}