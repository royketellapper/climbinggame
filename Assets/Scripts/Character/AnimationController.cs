﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoyFramework.Game
{
    public enum MovementStateType
    {
        Standing,
        Crouching,
        Falling,
        Climbing
    }

    public enum WeaponType
    {
        None,
        Pistol,
        AssaultRifle,
        Sniper
    }

    [RequireComponent(typeof(Animator))]
    public class AnimationController : MonoBehaviour
    {
        public Animator Animator
        {
            get
            {
                if (_animator == null)
                    _animator = GetComponent<Animator>();

                return _animator;
            }
        }
        protected Animator _animator;

        public Action OnMoveAnimator;

        protected int _inputAngleId, _inputMagnitudeId, _walkStartAngleId, _movementStateId, _weaponTypeId, _isMovingId;
        protected int _idleTypeId, _hardTurnId, _stopMovingMagnitude;

        protected virtual void Awake()
        {
            _inputAngleId = Animator.StringToHash("InputAngle");
            _inputMagnitudeId = Animator.StringToHash("InputMagnitude");
            _walkStartAngleId = Animator.StringToHash("WalkStartAngle");
            _movementStateId = Animator.StringToHash("MovementState");
            _weaponTypeId = Animator.StringToHash("WeaponType");
            _isMovingId = Animator.StringToHash("IsMoving");
            _idleTypeId = Animator.StringToHash("IdleType");
            _hardTurnId = Animator.StringToHash("HardTurn");
            _stopMovingMagnitude = Animator.StringToHash("StopMovingMagnitude");
        }

        public void HardTurn()
        {
            Animator.SetTrigger(_hardTurnId);
        }

        public void SetInputAngle(float value)
        {
            value = Mathf.Clamp(value, -35, 35);
            Animator.SetFloat(_inputAngleId, value);
        }

        public void StartMoveAngle(float value)
        {
            Animator.SetFloat(_walkStartAngleId, value);
        }

        public void SetInputMagnitude(float value)
        {
            Animator.SetFloat(_inputMagnitudeId, value);
        }

        public void SetStopMovingMagnitude(float value)
        {
            Animator.SetFloat(_stopMovingMagnitude, value);
        }

        public void SetMovementState(MovementStateType state)
        {
            Animator.SetInteger(_movementStateId, (int)state);
        }

        public void SetWeaponType(WeaponType state)
        {
            Animator.SetInteger(_weaponTypeId, (int)state);
        }

        public void SetMoving(bool value)
        {
            Animator.SetBool(_isMovingId, value);
        }

        protected virtual void OnAnimatorMove()
        {
            OnMoveAnimator?.Invoke();
        }
    }
}