﻿using RootMotion.FinalIK;
using System;
using UnityEngine;

namespace RoyFramework.Game
{
    [RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
    public class MovementController : MonoBehaviour
    {
        [Header("Physics")]
        public AnimationController AnimationController;
        public FullBodyBipedIK FullBodyBipedIK;
        public CapsuleCollider CapsuleCollider;
        public Rigidbody Rigidbody;
        public Transform CharacterTransform;

        public ClimbState ClimbState;
        public FallState FallState;  
        public WalkState WalkState;
        public MovementState[] MovementStates { get; set; }
        public MovementState CurrentState { get; set; }

        [NonSerialized]
        public PhysicMaterial maxFrictionPhysics, frictionPhysics, slippyPhysics;       // create PhysicMaterial for the Rigidbody

        public Vector3 MovementDirection { get; protected set; }
        public Action<Vector3> OnMovementDirection;

        public Transform Transform { get; protected set; }

        public InputMaster InputMaster;

        public Camera Camera
        {
            get
            {
                if (_camera == null)
                    _camera = Camera.main;

                return _camera;
            }
        }
        protected Camera _camera;

        protected virtual void SetStates()
        {
            MovementStates = new MovementState[]
            {
                ClimbState,
                FallState,
                WalkState
            };
        }

        protected virtual void Awake()
        {
            InputMaster = new InputMaster();

            InitPhysicMaterial();
            Transform = transform;

            SetStates();
            //Init states
            foreach (var item in MovementStates)
            {
                item.MovementController = this;
                item.Init();
            }

            SetMovementState<WalkState>();
        }

        protected virtual void OnEnable()
        {
            AnimationController.OnMoveAnimator += OnMoveAnimator;
        }

        protected virtual void OnDisable()
        {
            AnimationController.OnMoveAnimator -= OnMoveAnimator;
        }

        public void InitPhysicMaterial()
        {
            // slides the character through walls and edges
            frictionPhysics = new PhysicMaterial();
            frictionPhysics.name = "frictionPhysics";
            frictionPhysics.staticFriction = .25f;
            frictionPhysics.dynamicFriction = .25f;
            frictionPhysics.frictionCombine = PhysicMaterialCombine.Multiply;

            // prevents the collider from slipping on ramps
            maxFrictionPhysics = new PhysicMaterial();
            maxFrictionPhysics.name = "maxFrictionPhysics";
            maxFrictionPhysics.staticFriction = 1f;
            maxFrictionPhysics.dynamicFriction = 1f;
            maxFrictionPhysics.frictionCombine = PhysicMaterialCombine.Maximum;

            // air physics 
            slippyPhysics = new PhysicMaterial();
            slippyPhysics.name = "slippyPhysics";
            slippyPhysics.staticFriction = 0f;
            slippyPhysics.dynamicFriction = 0f;
            slippyPhysics.frictionCombine = PhysicMaterialCombine.Minimum;
        }

        public T SetMovementState<T>() where T : MovementState
        {
            Type type = typeof(T);
            var movement = Array.Find(MovementStates, a => a.GetType() == type);
            var oldState = CurrentState;          

            if (oldState != null)
                oldState.OnDisable(movement);

            CurrentState = movement;
            movement.OnEnable(oldState);

            return movement as T;
        }

        public void SetMovementDirection(Vector3 dir)
        {
            MovementDirection = dir;

            if (OnMovementDirection != null)
                OnMovementDirection(dir);
        }

        protected virtual void FixedUpdate()
        {
            CurrentState?.FixedUpdate();
        }

        protected virtual void OnMoveAnimator()
        {
            CurrentState?.OnMoveAnimator();
        }
    }
}