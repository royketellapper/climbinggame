using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;

public class ExposeProperties : Editor
{
	public delegate void PropertyChanged();
	public event PropertyChanged propertyChanged;
	
	private class PopupMenuInfo
	{
		public string className;
		public string NicifyClassName;
		public Type[] Subclasses;
		public string[] SubclassesNames;
		public int selectedClass;
	}
	
	private List<PopupMenuInfo> popupMenuInfo = new List<PopupMenuInfo>();
	private SerializedProperty sOldProperty;
	
	protected virtual void OnEnable()
	{
		this.hideFlags = HideFlags.DontSave;
	}
	
	//Get all classes that inherit a certain base class
	public static List<Type> GetSubclasses(Type baseClass)
	{
		Type[] classNames = Assembly.GetAssembly(baseClass).GetTypes();
		List<Type> names = new List<Type>();
		
		if (!baseClass.IsAbstract)
			names.Add(baseClass);
		
		foreach(Type className in classNames)
		{
			if (className.IsSubclassOf(baseClass) && !className.IsAbstract)
				names.Add(className);
		}
		
		return names;
	}
	
	public static List<String> GetTypeName(List<Type> typeArray)
	{
		List<String> typeNames = new List<String>();
		
		foreach(Type name in typeArray)
		{
			typeNames.Add(ObjectNames.NicifyVariableName(name.Name));
		}
		
		return typeNames;
	}
	
	public static String[] GetTypeName(Type[] typeArray)
	{
		List<String> typeNames = new List<String>();
		
		foreach(Type name in typeArray)
		{
			typeNames.Add(ObjectNames.NicifyVariableName(name.Name));
		}
		
		return typeNames.ToArray();
	}
	
	private void CreatePopupMenu(SerializedProperty sProperty, Type baseClass)
	{
		PopupMenuInfo menuInfo = popupMenuInfo.Find(a => a.className == sProperty.name);
		
		int selectedClass = 0;
		
		if (menuInfo == null)
		{
			menuInfo = new PopupMenuInfo();
			menuInfo.className = sProperty.name;
			menuInfo.NicifyClassName = ObjectNames.NicifyVariableName(sProperty.name);
			menuInfo.Subclasses = GetSubclasses(baseClass).ToArray();
			menuInfo.SubclassesNames = GetTypeName(menuInfo.Subclasses);
			popupMenuInfo.Add(menuInfo);
		}
		
		if (sProperty.objectReferenceValue != null)
		{
			if (sProperty.objectReferenceValue.GetType() != menuInfo.Subclasses[menuInfo.selectedClass])
			{
				for (; selectedClass < menuInfo.Subclasses.Length; selectedClass++)
				{
					if (menuInfo.Subclasses[selectedClass] == sProperty.objectReferenceValue.GetType())
						break;
				}
			}
			else
			  selectedClass = menuInfo.selectedClass;
		}
		menuInfo.selectedClass = EditorGUILayout.Popup(menuInfo.NicifyClassName ,selectedClass,menuInfo.SubclassesNames);
		
		if (sProperty.objectReferenceValue == null)
			sProperty.objectReferenceValue = ScriptableObject.CreateInstance(menuInfo.Subclasses[menuInfo.selectedClass]);
		else if (sProperty.objectReferenceValue.GetType() != menuInfo.Subclasses[menuInfo.selectedClass])
			sProperty.objectReferenceValue = ScriptableObject.CreateInstance(menuInfo.Subclasses[menuInfo.selectedClass]);
	}
	
	public void Expose(SerializedObject sObject)
	{
		if (sOldProperty != null)
			if(sOldProperty.serializedObject != sObject)
				popupMenuInfo.Clear();
		
		sObject.Update();
		SerializedProperty sProperty = sObject.GetIterator();
		
		sProperty.NextVisible(true);
		while (sProperty.NextVisible(true)) 
		{
			bool hidePropertyField = false;
			
			if (sProperty.propertyType == SerializedPropertyType.ObjectReference)
			{
				FieldInfo infos = sObject.targetObject.GetType().GetField(sProperty.name);
				if (infos != null)
				{
					if (infos.FieldType.IsSubclassOf(typeof(ScriptableObject)) || infos.FieldType == typeof(ScriptableObject))
					{
						object[] attributes = infos.GetCustomAttributes(true);
						
						foreach( object o in attributes )
						{
							if ( o.GetType() == typeof( ShowSubclassesPopup ) )
							{
								hidePropertyField = true;
								CreatePopupMenu(sProperty,infos.FieldType);
								break;
							}
						}
					}
				}
			}
			if (!hidePropertyField)
			{
				EditorGUILayout.PropertyField(sProperty);
				sObject.ApplyModifiedProperties();
			}
		}
		
		//Are the property changed in the last update
		if (sOldProperty != null)
			if (SerializedProperty.EqualContents(sProperty,sOldProperty) && propertyChanged != null)
				propertyChanged();
		
		sOldProperty = sProperty;
		sProperty.Reset();
	}
}
