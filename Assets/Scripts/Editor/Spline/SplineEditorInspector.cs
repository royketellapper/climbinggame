using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using System.Collections.Generic;

public class DefaultHandles
{
    public static bool Hidden
    {
        get
        {
            Type type = typeof(Tools);
            FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
            return ((bool)field.GetValue(null));
        }
        set
        {
            Type type = typeof(Tools);
            FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
            field.SetValue(null, value);
        }
    }
}

[CustomEditor(typeof(SplineEditor))]
public class SplineEditorInspector : Editor
{

    private const float dotCapScale = 0.05f;
    private SplineEditor splineEditor;
    private string[] vertexPropertiesNames;
    private Type[] SplineTypes;
    private SplineVertex selectedVertex;
    private SplineVertex selectedBezierObject;
    private enum GizmoSelectionType
    {
        Pivot,
        Vertex,
        Parent,
        BezierTangentEnd,
        BezierTangentBegin
    };
    private GizmoSelectionType gizmoSelection;
    private int selectedSplineNum;
    private SplineVertexInspector selectedVertexInspector;
    private Dictionary<SplineVertex, SplineVertexInspector> vertexInspectors = new Dictionary<SplineVertex, SplineVertexInspector>();

    //Serialized Data
    public SerializedObject serializedSplineData;
    public ExposeProperties exposeProperties;

    private static bool moveParentObj = true;
    //Foldouts
    private static bool propertieFoldout = true;
    private static bool positionFoldout = false;
    private static bool bezierFoldout = false;
    private static bool splinePropertiesFoldout = false;

    protected virtual void OnEnable()
    {
        exposeProperties = ScriptableObject.CreateInstance<ExposeProperties>();
        exposeProperties.hideFlags = HideFlags.DontSave;

        selectedVertex = null;
        splineEditor = target as SplineEditor;
        SplineTypes = ExposeProperties.GetSubclasses(typeof(Spline)).ToArray();
        vertexPropertiesNames = ExposeProperties.GetTypeName(ExposeProperties.GetSubclasses(typeof(Spline))).ToArray();
    }

    void OnDisable()
    {
        DefaultHandles.Hidden = false;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        selectedSplineNum = EditorGUILayout.Popup(selectedSplineNum, vertexPropertiesNames);

        if (GUILayout.Button("Create"))
        {
            Spline spline = splineEditor.CreateNewClimbingPoint(SplineTypes[selectedSplineNum]);
            selectedVertex = splineEditor.AddNewVertex(spline);
            selectedVertexInspector = GetVertexInspector(selectedVertex);
            serializedSplineData = new SerializedObject(spline);
            gizmoSelection = GizmoSelectionType.Vertex;
            EditorUtility.SetDirty(target);
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Add New Vertex") && selectedVertex != null)
        {
            selectedVertex = splineEditor.AddNewVertex(selectedVertex.spline);
            selectedVertexInspector = GetVertexInspector(selectedVertex);
            serializedSplineData = new SerializedObject(selectedVertex.spline);
            EditorUtility.SetDirty(target);
        }

        if (GUILayout.Button("Insert New Vertex") && selectedVertex != null)
        {
            selectedVertex = splineEditor.InsertNewPoint(selectedVertex.spline, selectedVertex);
            selectedVertexInspector = GetVertexInspector(selectedVertex);
            serializedSplineData = new SerializedObject(selectedVertex.spline);
            EditorUtility.SetDirty(target);
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Remove Selected") && selectedVertex != null)
        {
            int arrayIndex = selectedVertex.spline.vertexList.FindIndex(v => v.Equals(selectedVertex)) - 1;

            vertexInspectors.Remove(selectedVertex);
            splineEditor.RemoveVertex(selectedVertex);

            if (arrayIndex >= 0)
            {
                selectedVertex = selectedVertex.spline.vertexList[arrayIndex];
                selectedVertexInspector = vertexInspectors[selectedVertex];
            }
            else
                selectedVertex = null;

            EditorUtility.SetDirty(target);
        }

        if (GUILayout.Button("Remove Connected") && selectedVertex != null)
        {
            foreach (SplineVertex vertex in selectedVertex.spline.vertexList)
                vertexInspectors.Remove(vertex);

            splineEditor.RemoveSplines(selectedVertex.spline);
            selectedVertex = null;
            EditorUtility.SetDirty(target);
        }
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Remove All Vertices"))
        {
            vertexInspectors.Clear();
            splineEditor.RemoveAllSplines();
            selectedVertex = null;
            EditorUtility.SetDirty(target);
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.Space();

        if (selectedVertex != null)
        {
            //Show Positions Field
            positionFoldout = EditorGUILayout.Foldout(positionFoldout, "Positions");
            if (positionFoldout)
                ShowPositionField();

            // Show Bezier Field
            EditorGUILayout.Space();
            bezierFoldout = EditorGUILayout.Foldout(bezierFoldout, "Bezier");
            if (bezierFoldout)
            {
                selectedVertex.breakTangent = EditorGUILayout.Toggle("Break Tangent", selectedVertex.breakTangent);
                selectedVertex.interpolationSteps = EditorGUILayout.IntField("Interpolation Steps", Mathf.Clamp(selectedVertex.interpolationSteps, 0, 100));
                selectedVertex.localTangentBegin = EditorGUILayout.Vector3Field("Bezier Position Begin", selectedVertex.localTangentBegin);
                selectedVertex.localTangentEnd = EditorGUILayout.Vector3Field("Bezier Position End", selectedVertex.localTangentEnd);
            }

            // Spline Properties
            EditorGUILayout.Space();
            splinePropertiesFoldout = EditorGUILayout.Foldout(splinePropertiesFoldout, "Spline Properties");
            if (splinePropertiesFoldout)
                exposeProperties.Expose(serializedSplineData);

            // Show propertieField
            EditorGUILayout.Space();
            propertieFoldout = EditorGUILayout.Foldout(propertieFoldout, ObjectNames.NicifyVariableName(selectedVertex.GetType().Name) + " Properties");
            if (propertieFoldout)
                selectedVertexInspector.OnInspectorGUI();

        }
        EditorGUILayout.Space();
        if (GUILayout.Button("Create Box Collider"))
            splineEditor.CreateBoxCollider();

        if (GUI.changed)
            EditorUtility.SetDirty(target);
    }

    private void CreateNewGameObject()
    {
        int A = 0;
        int B = 0;
        for (A = 0; A < splineEditor.splineList.Count; A++)
            for (B = 0; B < splineEditor.splineList[A].vertexList.Count; B++)
                if (splineEditor.splineList[A].vertexList[B].Equals(selectedVertex))
                    break;

        GameObject Obj = new GameObject("Point-" + A + "-" + (B + 1));
        Obj.transform.parent = splineEditor.transform;
        Obj.transform.position = selectedVertex.position;
        selectedVertex.parent = Obj;
    }

    private void ShowPositionField()
    {
        selectedVertex.position = EditorGUILayout.Vector3Field("Position", selectedVertex.position);
        selectedVertex.localPosition = EditorGUILayout.Vector3Field("Local Position", selectedVertex.localPosition);
        EditorGUILayout.Space();

        moveParentObj = EditorGUILayout.Toggle("Move Parent", moveParentObj);
        EditorGUILayout.LabelField("Parent");
        EditorGUILayout.BeginHorizontal();
        selectedVertex.parent = (GameObject)EditorGUILayout.ObjectField(selectedVertex.parent, typeof(GameObject), true);
        if (GUILayout.Button("Cr. Obj"))
            CreateNewGameObject();
        EditorGUILayout.EndHorizontal();
    }

    private void OnSceneGUI()
    {
        Undo.SetSnapshotTarget(target, "Adjust Climbing Vertex");
        DrawLines();
        DrawPivotButton();
        DrawGizmoSelection();
    }

    private void DrawGizmoSelection()
    {
        switch (gizmoSelection)
        {
            case GizmoSelectionType.Parent:
                if (selectedVertex == null)
                    gizmoSelection = GizmoSelectionType.Pivot;

                if (moveParentObj && selectedVertex.parent.transform == splineEditor.transform)
                    gizmoSelection = GizmoSelectionType.Vertex;
                else
                {
                    selectedVertex.parent.transform.position = Handles.PositionHandle(selectedVertex.parent.transform.position, Quaternion.identity);
                    DefaultHandles.Hidden = true;
                }
                break;

            case GizmoSelectionType.Vertex:
                if (selectedVertex == null)
                    gizmoSelection = GizmoSelectionType.Pivot;
                else
                {

                    selectedVertex.position = Handles.PositionHandle(selectedVertex.position, Quaternion.identity);
                    DefaultHandles.Hidden = true;
                }
                break;

            case GizmoSelectionType.BezierTangentEnd:
                selectedBezierObject.tangentEnd = Handles.PositionHandle(selectedBezierObject.tangentEnd, Quaternion.identity);

                if (!selectedBezierObject.breakTangent)
                    selectedBezierObject.localTangentBegin = -selectedBezierObject.localTangentEnd;

                DefaultHandles.Hidden = true;
                break;

            case GizmoSelectionType.BezierTangentBegin:
                selectedBezierObject.tangentBegin = Handles.PositionHandle(selectedBezierObject.tangentBegin, Quaternion.identity);

                if (!selectedBezierObject.breakTangent)
                    selectedBezierObject.localTangentEnd = -selectedBezierObject.localTangentBegin;

                DefaultHandles.Hidden = true;
                break;

            case GizmoSelectionType.Pivot:
                DefaultHandles.Hidden = false;
                break;
        }
    }

    private void DrawPivotButton()
    {
        Quaternion lookAtRot = Quaternion.LookRotation(splineEditor.transform.position - Camera.current.transform.position);

        if (Handles.Button(splineEditor.transform.position, lookAtRot, 0.46f, 0.46f, Handles.DotHandleCap))
        {
            selectedVertex = null;
            gizmoSelection = GizmoSelectionType.Pivot;
            EditorUtility.SetDirty(target);
        }
    }

    private SplineVertexInspector CreateNewVertexInspector(SplineVertex vertex)
    {
        SplineVertexInspector vertexInspector = null;
        foreach (Type classType in Assembly.GetAssembly(typeof(SplineVertexInspector)).GetTypes())
        {
            if (classType.IsSubclassOf(typeof(SplineVertexInspector)))
            {
                CustomEditor[] attributes = (CustomEditor[])classType.GetCustomAttributes(typeof(CustomEditor), true);
                foreach (CustomEditor obj in attributes)
                {
                    //if (obj.m_InspectedType == vertex.spline.GetType()) //<--------- FIX ME_____------------------------
                    //{
                    vertexInspector = (SplineVertexInspector)Editor.CreateEditor(vertex, classType);
                    vertexInspector.splineEditor = splineEditor;
                    return vertexInspector;
                    //}
                }
            }
        }

        vertexInspector = (SplineVertexInspector)Editor.CreateEditor(vertex, typeof(SplineVertexInspector));
        vertexInspector.splineEditor = splineEditor;
        return vertexInspector;
    }

    private SplineVertexInspector GetVertexInspector(SplineVertex vertex)
    {
        SplineVertexInspector vertexInspector = null;

        if (vertexInspectors.ContainsKey(vertex))
            return vertexInspectors[vertex];
        else
        {
            vertexInspector = CreateNewVertexInspector(vertex);
            vertexInspectors.Add(vertex, vertexInspector);
        }

        return vertexInspector;
    }

    private void DrawLines()
    {
        Quaternion lookAtRot;
        float scale;
        Vector3 bezierLoc = new Vector3();

        for (int B = 0; B < splineEditor.splineList.Count; B++)
        {
            Spline spline = splineEditor.splineList[B];
            List<SplineVertex> vertex = spline.vertexList;

            //for (int A = 0; A < spline.pointList.Count; A++)
            //{
            //	Debug.Log(spline.pointList[A].position);
            //}

            for (int A = 0; A < vertex.Count; A++)
            {
                SplineVertexInspector vertexInspector = GetVertexInspector(vertex[A]);

                //Draw dot buttons
                lookAtRot = Quaternion.LookRotation(vertex[A].position - Camera.current.transform.position);

                scale = HandleUtility.GetHandleSize(vertex[A].position);
                if (Handles.Button(vertex[A].position, lookAtRot, scale * dotCapScale, scale * dotCapScale, Handles.DotHandleCap))
                {
                    selectedVertex = vertex[A];
                    gizmoSelection = (moveParentObj && selectedVertex.parent.transform != splineEditor.transform) ? GizmoSelectionType.Parent : GizmoSelectionType.Vertex;
                    selectedVertexInspector = vertexInspector;
                    serializedSplineData = new SerializedObject(spline);
                    EditorUtility.SetDirty(target);
                }

                // Bezier handels
                bool drawBezierhandel = false;
                if (A != 0)
                    if (vertex[A - 1].interpolationSteps != 0)
                        drawBezierhandel = true;
                if (A == 0)
                    if (vertex[vertex.Count - 1].interpolationSteps != 0)
                        drawBezierhandel = true;

                //end
                Handles.color = Color.yellow;
                if ((A != 0 || spline.isClosed) && drawBezierhandel)
                {
                    bezierLoc = vertex[A].tangentBegin;
                    Handles.DrawLine(bezierLoc, vertex[A].position);
                    scale = HandleUtility.GetHandleSize(bezierLoc);
                    if (Handles.Button(bezierLoc, lookAtRot, scale * dotCapScale, scale * dotCapScale, Handles.DotHandleCap))
                    {
                        gizmoSelection = GizmoSelectionType.BezierTangentBegin;
                        selectedVertex = vertex[A];
                        selectedBezierObject = vertex[A];
                        selectedVertexInspector = vertexInspector;
                        serializedSplineData = new SerializedObject(spline);
                        EditorUtility.SetDirty(target);
                    }
                }

                //begin
                if (((A + 1) < vertex.Count || spline.isClosed) && vertex[A].interpolationSteps != 0)
                {
                    bezierLoc = vertex[A].tangentEnd;
                    Handles.DrawLine(bezierLoc, vertex[A].position);
                    scale = HandleUtility.GetHandleSize(bezierLoc);
                    if (Handles.Button(bezierLoc, lookAtRot, scale * dotCapScale, scale * dotCapScale, Handles.DotHandleCap))
                    {
                        gizmoSelection = GizmoSelectionType.BezierTangentEnd;
                        selectedVertex = vertex[A];
                        selectedBezierObject = vertex[A];
                        selectedVertexInspector = vertexInspector;
                        serializedSplineData = new SerializedObject(spline);
                        EditorUtility.SetDirty(target);
                    }
                }
                Handles.color = Color.white;

                //Add DrawLines
                vertexInspector.DrawLine(spline, A);
            }
        }
    }

}

