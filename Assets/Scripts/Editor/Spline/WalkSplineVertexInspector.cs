using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(WalkSpline))]
public class WalkSplineVertexInspector : SplineVertexInspector 
{
	protected override void OnEnable()
	{
		base.OnEnable();
		baseLineColor = Color.cyan;
	}
}
