using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(HangSpline))]
public class HangSplineVertexInspector : SplineVertexInspector 
{
	private const float arrowScale = 0.2f;
	
	protected override void OnEnable()
	{
		base.OnEnable();
		baseLineColor = Color.green;
	}
	
	public override void DrawLine(Spline spline, int selected)
	{
		//Draw Lines
		Handles.color = baseLineColor;
		if (spline.vertexList[selected] != null && (selected + 1) < spline.vertexList.Count)
		{
			if (spline.vertexList[selected].interpolationSteps <= 0)
				DrawSimpelLine(spline.vertexList[selected], spline.vertexList[selected + 1]);
			else
				DrawBezierLine (spline.vertexList[selected], spline.vertexList[selected + 1]);
		}
		else if (spline.vertexList[selected] != null)
		{
			if (spline.isClosedValid())
			{
				if (spline.vertexList[selected].interpolationSteps <= 0)
					DrawSimpelLine(spline.vertexList[selected], spline.vertexList[0]);
				else
					DrawBezierLine (spline.vertexList[selected], spline.vertexList[0]);
			}
		}
		
		DrawVertexNumber(spline.vertexList[selected], selected);
		
		Handles.color = Color.white;
	}
				
	public virtual void DrawSimpelLine(SplineVertex startVertex, SplineVertex endVertex)
	{
		Vector3 startLoc, endLoc, midLoc;
		
		startLoc = startVertex.position;
		endLoc = endVertex.position;
		midLoc = startLoc + (endLoc - startLoc) / 2;
		Handles.DrawLine(startLoc, endLoc);
		
		if (startVertex.spline.useDirection)
		{
			float scale = HandleUtility.GetHandleSize(midLoc);
			Quaternion rot = Quaternion.Euler(0,Mathf.Atan2(startLoc.x - endLoc.x, startLoc.z - endLoc.z) * Mathf.Rad2Deg,0);
			rot *= (startVertex.spline.invertDirection)? Quaternion.Euler(0,90,0) : Quaternion.Euler(0,-90,0);
			DebugSimbols.DrawArrow(midLoc, rot,scale * arrowScale);
		}
	}
}
