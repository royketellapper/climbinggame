using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[CustomEditor(typeof(Spline))]
public class SplineVertexInspector : Editor
{
	public ExposeProperties exposeProperties;
	public SerializedObject serializedProperties;
	public SplineEditor splineEditor;
	public SplineVertex splineVertex;
	
	//Style
	public GUIStyle vertexNumberStyle = new GUIStyle();
	public Color baseLineColor = Color.white;
	
	protected virtual void OnEnable()
	{
		exposeProperties = ScriptableObject.CreateInstance<ExposeProperties>();
		exposeProperties.hideFlags = HideFlags.DontSave; 
		SetStyle();	
		splineVertex = target as SplineVertex;
		if (target != null)
			serializedProperties = new SerializedObject(target);
	}
	
	public override void OnInspectorGUI() 
	{
		exposeProperties.Expose(serializedProperties);
		
		EditorGUILayout.BeginHorizontal();	
		if (GUILayout.Button("All spline") && splineEditor != null)
		{
			foreach(Spline spline in splineEditor.splineList)
			{
				if (spline.GetType() != splineVertex.spline.GetType())
					continue;
				
				foreach(SplineVertex vertex in spline.vertexList)
				{
					if (vertex == splineVertex)
						continue;
					
					ObjectCopier.CopyClass(vertex, splineVertex);
				}
			}
		}
		if (GUILayout.Button("All Connected"))
		{
			foreach(SplineVertex vertex in splineVertex.spline.vertexList)
			{
				if (vertex == splineVertex)
					continue;
				
				ObjectCopier.CopyClass(vertex, splineVertex);
			}
		}
		EditorGUILayout.EndHorizontal();
	}
	
	public void SetStyle()
	{
		vertexNumberStyle.fontStyle = FontStyle.Bold;
		vertexNumberStyle.normal.textColor = Color.white;
	}
	
	public void DrawVertexNumber(SplineVertex vertex, int number)
	{
		Vector3 camDist = Camera.current.transform.position - vertex.position;
		if (Vector3.Dot(camDist,Camera.current.transform.rotation * Vector3.forward) < 0)
		{
			Quaternion camRot = Quaternion.LookRotation(camDist);
			Handles.Label(vertex.position + (camRot * Vector3.forward) * 2, (number + 1).ToString(), vertexNumberStyle);
		}
	}
	
	public virtual void DrawLine(Spline spline, int selected)
	{
		//Draw Lines
		Handles.color = baseLineColor;
		if (spline.vertexList[selected] != null && (selected + 1) < spline.vertexList.Count)
		{
			if (spline.vertexList[selected].interpolationSteps <= 0)
				Handles.DrawLine(spline.vertexList[selected].position, spline.vertexList[selected + 1].position);
			else
				DrawBezierLine (spline.vertexList[selected], spline.vertexList[selected + 1]);
		}
		else if (spline.vertexList[selected] != null)
		{
			if (spline.isClosedValid())
			{
				if (spline.vertexList[selected].interpolationSteps <= 0)
					Handles.DrawLine(spline.vertexList[selected].position, spline.vertexList[0].position);
				else
					DrawBezierLine (spline.vertexList[selected], spline.vertexList[0]);
			}
		}
		
		DrawVertexNumber(spline.vertexList[selected], selected);
		
		Handles.color = Color.white;
	}
	
	protected virtual void DrawBezierLine (SplineVertex startVertex, SplineVertex EndVertex)
	{
		Vector3 vect, oldvect = Vector3.zero;	
		int steps = startVertex.interpolationSteps + 1;
		for (int C = 1; C <= steps; C++)
		{	
			float alpha = (float)C / steps;
			
			vect =  Math3D.CubicInterpolate(EndVertex.position, EndVertex.tangentBegin , startVertex.tangentEnd, startVertex.position, alpha);
			
			if (C == 1)
				Handles.DrawLine(EndVertex.position, vect);
			else
				Handles.DrawLine(oldvect, vect);
			
			oldvect = vect;
		}
	}
}
