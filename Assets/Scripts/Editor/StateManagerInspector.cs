using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Collections.Generic;

public class StateProperties
{
	public State state;
	public Type 	stateType;
	public string 	stateName;
	public bool 	stateFoldout;
	public SerializedObject serializedObject;
}

[CustomEditor(typeof(StateManager))]
public class StateManagerInspector : Editor 
{
	private List<StateProperties> stateProperties = new List<StateProperties>();
	private int 			selectedState;
	private List<string> 	stateNames;
	private List<Type> 		stateTypes;
	private StateManager 	stateManager;
	private ExposeProperties exposeProperties; //ScriptableObject.CreateInstance<ExposeProperties>();
	
	private void OnEnable() 
	{
		exposeProperties = ScriptableObject.CreateInstance<ExposeProperties>();
		exposeProperties.hideFlags = HideFlags.DontSave;
		
		stateManager = target as StateManager;
		
		stateTypes = ExposeProperties.GetSubclasses(typeof(State));
		stateNames = ExposeProperties.GetTypeName(ExposeProperties.GetSubclasses(typeof(State)));
		
		stateManager.stateAdded += StateAdded;
		stateManager.stateRemoved += StateRemoved;
		
		SetStateProperties ();
	}
	
	private void OnDisable ()
	{
		stateManager.stateAdded -= StateAdded;
		stateManager.stateRemoved -= StateRemoved;
		DestroyImmediate(exposeProperties);
	}
	
	private void StateAdded(State state)
	{
		stateProperties.Add(CreateStateProperties(state));
	}
	
	private void StateRemoved(State state)
	{
		StateProperties properties = stateProperties.Find(a => a.state == state);
		
		if (properties != null)
			stateProperties.Remove(properties);
	}
	
	private void SetStateProperties ()
	{
		if (stateManager.states != null)
		{
			stateProperties.Clear();
			
			foreach (State state in stateManager.states)
				if (stateManager.states != null)
					stateProperties.Add(CreateStateProperties(state));
		}
	}
	
	public StateProperties CreateStateProperties(State state)
	{
		StateProperties properties = new StateProperties();
		properties.state = state;
		properties.stateType = state.GetType();
		properties.stateName = ObjectNames.NicifyVariableName(properties.stateType.Name);
		properties.serializedObject = new SerializedObject(state);
			
		return properties;
	}
	
	public override void OnInspectorGUI() 
	{
		EditorGUILayout.BeginVertical();
		EditorGUILayout.BeginHorizontal();
		
		selectedState = EditorGUILayout.Popup(selectedState,stateNames.ToArray());
		if (GUILayout.Button("Add State"))
			stateManager.FindState(stateTypes[selectedState]);
		
		EditorGUILayout.EndHorizontal();
		
		for (int a = 0; a < stateProperties.Count; a++)
		{
			bool isRemoved = false;
			EditorGUILayout.BeginHorizontal();
			string stateActive = (stateProperties[a].state == stateManager.CurrentState)? "* " : "";
			stateProperties[a].stateFoldout = EditorGUILayout.Foldout(stateProperties[a].stateFoldout,stateActive + stateProperties[a].stateName);
			if (GUILayout.Button("X",GUILayout.Width(19)))
			{
				stateManager.RemoveState(stateProperties[a].state);
				isRemoved = true;
				EditorUtility.SetDirty(target);	
			}
			EditorGUILayout.EndHorizontal();
			
			if (!isRemoved)
			{
				if (stateProperties[a].stateFoldout)
				{	
					exposeProperties.Expose(stateProperties[a].serializedObject);
					EditorUtility.SetDirty(target);	
				}
			}
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.Space();
	}
}
