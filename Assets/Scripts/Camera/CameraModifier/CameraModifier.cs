﻿using UnityEngine;

namespace RoyFramework.Game
{
    [System.Serializable]
    public abstract class CameraModifier
    {
        public CameraController CameraController { get; set; }
        public virtual Vector3 Position(Vector3 position) { return position; }
        public virtual Quaternion Rotation(Quaternion rotation) { return rotation; }
        public virtual void OnDrawGizmos() { }
    }
}