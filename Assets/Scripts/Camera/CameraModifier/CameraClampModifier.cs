﻿using UnityEngine;
using RoyFramework.Tools;

namespace RoyFramework.Game
{
    [System.Serializable]
    public class CameraClampModifier : CameraModifier
    {
        public Vector3 Size;
        public Vector3 Center;

        [Space]
        public bool ClampX = true;
        public bool ClampY = true;
        public bool ClampZ = true;

        public override Vector3 Position(Vector3 position)
        {
            Vector3 size = Size / 2;

            position.x = (ClampX)? Mathf.Clamp(position.x, Center.x - size.x, Center.x + size.x) : position.x;
            position.y = (ClampY) ? Mathf.Clamp(position.y, Center.y - size.y, Center.y + size.y) : position.y;
            position.z = (ClampZ) ? Mathf.Clamp(position.z, Center.z - size.z, Center.z + size.z) : position.z;

            return position;
        }

        public override void OnDrawGizmos()
        {
            Matrix4x4 mat = Matrix4x4.TRS(Center, Quaternion.identity, Vector3.one);
            DebugExtension.DrawLocalCube(mat, new Vector3(Size.x, Size.y, Size.z), Color.blue);
        }
    }
}