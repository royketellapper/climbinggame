﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchPanel : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public Action<PointerEventData> OnDragCallBack;
    public Action<PointerEventData> OnPointerDownCallBack;
    public Action<PointerEventData> OnPointerUpCallBack;

    protected PointerEventData lastEventData;
    protected bool _pressed;

    //protected virtual void OnEnable()
    //{

    //}

    protected virtual void OnDisable()
    {
        if (_pressed)
            OnPointerDown(lastEventData);

        _pressed = false;
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (OnDragCallBack != null)
            OnDragCallBack(eventData);

        lastEventData = eventData;
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (OnPointerDownCallBack != null)
            OnPointerDownCallBack(eventData);

        lastEventData = eventData;
        _pressed = true;
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        if (OnPointerUpCallBack != null)
            OnPointerUpCallBack(eventData);

        lastEventData = eventData;
        _pressed = false;
    }
}
