﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RoyFramework.Game
{
    [System.Serializable]
    public abstract class CameraState
    {
        public CameraController CameraController { get; set; }

        [NonSerialized]
        public Vector3 Position;
        public Quaternion Rotation { get; set; }

        protected List<int> _currentPointIds = new List<int>();

        [NonSerialized]
        public bool _isAwake = false;

        public virtual void OnDrag(PointerEventData eventData) { }

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            if (!_currentPointIds.Contains(eventData.pointerId))
                _currentPointIds.Add(eventData.pointerId);
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            _currentPointIds.Remove(eventData.pointerId);
        }

        public virtual void Awake() { }

        public virtual void OnEnable() { }

        public virtual void OnDisable()
        {
            _currentPointIds.Clear();
        }

        public virtual void LateUpdate() { }

        public virtual void FinalUpdate() { }

        public virtual void OnDrawGizmos() { }

        public virtual void AddTouchPanel(TouchPanel touchPanel)
        {
            if (touchPanel == null)
                return;

            touchPanel.OnDragCallBack += OnDrag;
            touchPanel.OnPointerDownCallBack += OnPointerDown;
            touchPanel.OnPointerUpCallBack += OnPointerUp;
        }

        public virtual void RemoveTouchPanel(TouchPanel touchPanel)
        {
            if (touchPanel == null)
                return;

            touchPanel.OnDragCallBack -= OnDrag;
            touchPanel.OnPointerDownCallBack -= OnPointerDown;
            touchPanel.OnPointerUpCallBack -= OnPointerUp;
        }

        public Vector3 ScreenSquareNormalize(Vector3 screenPoint)
        {
            float d = (Screen.height < Screen.width) ? Screen.height : Screen.width;
            return new Vector3(screenPoint.x / d, screenPoint.y / d, screenPoint.z);
        }
    }
}