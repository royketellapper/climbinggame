﻿using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

namespace RoyFramework.Game
{
    [System.Serializable]
    public class ThirdPersonState : CameraState
    {
        public struct Trans
        {
            public Vector3 Position;
            public Quaternion Rotation;
        }

        [Header("Position")]
        public MinMax Height;  
        public MinMax Yaw;
        public Vector3 StartRotation;

        [Header("Mouse")]
        public Vector2 MouseSensitivity = Vector2.one;
        public bool EnableMousesSmooth = true;
        public float MousesSmooth = 1;
        public bool LockMouse = true;

        [Header("Gamepad")]
        public Vector2 GamepadSensitivity = Vector2.one;
        public bool EnableGamepadSmooth = true;
        public float GamepadSmooth = 1;

        [Header("Collision")]
        public LayerMask LayerMask;
        public float DampSpeed = 1;
        public float Radius;

        public AnimationCurve OffsetCurveX;
        public AnimationCurve OffsetCurveY;
        public AnimationCurve OffsetCurveZ;

        [Space]
        public AnimationCurve DistanceCurve;

        protected float _distance, _currentDistance, _currentOffset;
        protected Vector3 _inputRotation;
        protected Quaternion _currentRotation;
        protected InputMaster _inputMaster;

        protected enum SmoothType
        {
            None,
            Mouse,
            GamePad
        }
        protected SmoothType _smoothType;

        public override void Awake()
        {
            base.Awake();
            _inputMaster = new InputMaster();
            _inputMaster.Player.CameraMouse.started += OnMouseMove;
            _inputMaster.Player.CameraMouse.performed += OnMouseMove;
            _inputMaster.Player.CameraMouse.canceled += OnMouseMove;

            _inputMaster.Player.CameraGamePad.started += OnGamePadMove;
            _inputMaster.Player.CameraGamePad.performed += OnGamePadMove;
            _inputMaster.Player.CameraGamePad.canceled += OnGamePadMove;

            _inputRotation = StartRotation;
            _currentRotation = Quaternion.Euler(StartRotation);

            _smoothType = SmoothType.None;
        }

        public override void OnEnable()
        {
            base.OnEnable();

            if (LockMouse)
                Cursor.lockState = CursorLockMode.Locked;

            _inputMaster.Player.CameraMouse.Enable();
            _inputMaster.Player.CameraGamePad.Enable();
        }

        public override void OnDisable()
        {
            base.OnDisable();
            _inputMaster.Player.CameraMouse.Disable();
            _inputMaster.Player.CameraGamePad.Disable();
        }

        protected void OnMouseMove(CallbackContext context)
        {
            Vector2 delta = context.ReadValue<Vector2>();
            _inputRotation.y += MouseSensitivity.x * delta.x;
            _inputRotation.x += MouseSensitivity.y * delta.y;
            _inputRotation.x = Yaw.Clamp(_inputRotation.x);
            _inputRotation.y = Math3D.NormaliseAngle(_inputRotation.y);

            if (context.started && delta.sqrMagnitude != 0)
                _smoothType = (EnableMousesSmooth) ? SmoothType.Mouse : SmoothType.None;
        }

        protected void OnGamePadMove(CallbackContext context)
        {
            Vector2 delta = context.ReadValue<Vector2>();
            _inputRotation.y += GamepadSensitivity.x * delta.x;
            _inputRotation.x += GamepadSensitivity.y * delta.y;
            _inputRotation.x = Yaw.Clamp(_inputRotation.x);
            _inputRotation.y = Math3D.NormaliseAngle(_inputRotation.y);

            if (context.started && delta.sqrMagnitude != 0)
                _smoothType = (EnableMousesSmooth)? SmoothType.GamePad : SmoothType.None;
        }

        public override void LateUpdate()
        {
            Transform target = CameraController.Target;
            Vector3 targetPos = target.position;

            //Input Smooth
            switch (_smoothType)
            {
                case SmoothType.Mouse:
                    _currentRotation = Quaternion.Slerp(_currentRotation, Quaternion.Euler(_inputRotation), Time.deltaTime * MousesSmooth);
                    break;
                case SmoothType.GamePad:
                    _currentRotation = Quaternion.Slerp(_currentRotation, Quaternion.Euler(_inputRotation), Time.deltaTime * GamepadSmooth);
                    break;
                default:
                    _currentRotation = Quaternion.Euler(_inputRotation);
                    break;
            }

            float angleX = Mathf.Asin(Mathf.Sin(_currentRotation.eulerAngles.x * Mathf.Deg2Rad)) * Mathf.Rad2Deg;
            //Debug.Log(_inputRotation.x + "  " + angleX);

            //Calc Position
            Vector3 posMin = targetPos + target.rotation * Vector3.up * Height.Min;
            Vector3 posMax = targetPos + target.rotation * Vector3.up * Height.Max;

            _distance = DistanceCurve.Evaluate(angleX);

            float dist01 = _currentDistance / _distance;

            Vector3 offset = GetCurveOffset(angleX);

            Trans transMin = CameraCollision(posMin, _currentRotation, offset);
            Trans transMax = CameraCollision(posMax, _currentRotation, offset);

            Vector3 pos = Vector3.Lerp(transMin.Position, transMax.Position, dist01);

            //Set final Position and Rotation 
            Rotation = _currentRotation;
            Position = pos;

        }

        protected Vector3 GetCurveOffset(float value)
        {
            Vector3 vect = new Vector3();
            vect.x = OffsetCurveX.Evaluate(value);
            vect.y = OffsetCurveY.Evaluate(value);
            vect.z = OffsetCurveZ.Evaluate(value);
            return vect;
        }

        protected virtual Trans CameraCollision(Vector3 position, Quaternion rotation, Vector3 offset)
        {
            Trans trans = new Trans();
            trans.Rotation = rotation;


            Vector3 euler = rotation.eulerAngles;
            Quaternion rot = Quaternion.Euler(0, euler.y, 0);

            //Debug.DrawRay(position, rot * offset);

            Vector3 offsetPos = rot * offset;
            Vector3 offseNormal = offsetPos.normalized;
            float offsetDist = offsetPos.magnitude;

            //Offset
            RaycastHit hit2;
            if (Physics.SphereCast(position, Radius + 0.1f, offseNormal, out hit2, offsetDist, LayerMask))
                _currentOffset = hit2.distance;
            else
                _currentOffset = offsetDist;

            offsetPos = position + offseNormal * _currentOffset;

            //Collision
            RaycastHit hit1;
            if (Physics.SphereCast(offsetPos, Radius, rotation * Vector3.back, out hit1, Mathf.Abs(_distance), LayerMask))
            {
                if (_currentDistance > hit1.distance)
                    _currentDistance = hit1.distance;
                else
                    _currentDistance = Mathf.Lerp(_currentDistance, hit1.distance, Time.deltaTime * DampSpeed);
            }
            else
                _currentDistance = Mathf.Lerp(_currentDistance, _distance, Time.deltaTime * DampSpeed);

            trans.Position = offsetPos + rotation * Vector3.back * _currentDistance;

            return trans;
        }
    }
}
