﻿using UnityEngine;
using UnityEngine.EventSystems;
using RoyFramework.Tools;

namespace RoyFramework.Game
{
    using Camera = UnityEngine.Camera;

    [System.Serializable]
    public class TopDownCameraState : CameraState
    {
        [Header("Settings")]
        public float PlaneLocation = 0;

        [Header("Zoom")]
        public MinMax ZoomClamp;
        public float ZoomSpeedPC = 55;
        public float ZoomSpeedMobile = 1;
        public float CurrentZoom = 5;

        [Header("Rotation")]
        public MinMax PichClamp;
        public float PichSpeed = 1;
        public float YawSpeed = 1;
        public float CurrentPich;
        public float CurrentYaw;

        protected float _touchPich, _touchYaw;
        protected float _touchMagnitude = 0;

        [Header("Smooth Settings")]
        public bool UseSmooth = true;
        public float Smooth = 11;
        public float MaxSmoothSpeed;

        [Header("Clamp position")]
        public bool UseClamp = true;
        public Vector2 Size;
        public Vector2 Center;

        public bool DisableDragMovement { get; set; }

        protected Plane _plane;
        protected Vector3[] _hitPoints = new Vector3[2];
        protected Vector3[] _screenTouchPos = new Vector3[2]; 
        protected Vector3 _hitPointDifferent;
        protected Vector3 _dragWorldPosition;


        protected Quaternion _currentRotation;

        protected int _currentPointId = -2; //-2 None, -1 Mouse end 0 1 2 3 Touch

        public Vector3 ClampPosition(Vector3 position)
        {
            if (!UseClamp)
                return position;

            Vector2 size = Size / 2;
            position.x = Mathf.Clamp(position.x, Center.x - size.x, Center.x + size.x);
            position.z = Mathf.Clamp(position.z, Center.y - size.y, Center.y + size.y);
            return position;
        }


        //private void OnMapPinch(Touch touchOne, Touch touchTwo)
        //{
        //    if (CameraTarget == CameraTarget.MoveToTarget)
        //        return;
        //    // Convert the touch positions and delta positions into viewport space for screen resolution independent zooming
        //    Vector2 screenTouchPos1 = cam.ScreenToViewportPoint(touchOne.position);
        //    Vector2 screenTouchPos2 = cam.ScreenToViewportPoint(touchTwo.position);
        //    Vector2 screenTouchPos1Delta = cam.ScreenToViewportPoint(touchOne.deltaPosition);
        //    Vector2 screenTouchPos2Delta = cam.ScreenToViewportPoint(touchTwo.deltaPosition);

        //    // Find the position in the previous frame of each touch.
        //    Vector2 touchOnePrevPos = screenTouchPos1 - screenTouchPos1Delta;
        //    Vector2 touchTwoPrevPos = screenTouchPos2 - screenTouchPos2Delta;

        //    // Find the magnitude of the vector (the distance) between the touches in each frame.
        //    float prevTouchDeltaMag = (touchOnePrevPos - touchTwoPrevPos).magnitude;
        //    float touchDeltaMag = (screenTouchPos1 - screenTouchPos2).magnitude;

        //    // Find the difference in the distances between each frame.
        //    float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

        //    fovTarget = Mathf.Clamp(fovTarget + (deltaMagnitudeDiff * FOV_MULTIPLIER), FOV_MIN, FOV_MAX);
        //}


        public override void OnDrag(PointerEventData eventData)
        {
            base.OnDrag(eventData);

            Camera cam = CameraController.Camera;

            //Calc camera movement drag
            if (!DisableDragMovement && _currentPointIds.Count == 1)
            {
                Ray ray = cam.ScreenPointToRay(eventData.position);

                float dist;
                if (_plane.Raycast(ray, out dist))
                {
                    _hitPointDifferent = _hitPoints[0] - ray.GetPoint(dist);
                    _dragWorldPosition.x += _hitPointDifferent.x;
                    _dragWorldPosition.y = PlaneLocation;
                    _dragWorldPosition.z += _hitPointDifferent.z;
                }
            }
        }

        public Vector3 GetPosition(int index)
        {
            if (index >= 0)
                return Input.GetTouch(index).position;
            else
                return Input.mousePosition;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            Camera cam = CameraController.Camera;

            //One finger
            if (_currentPointIds.Count != 0 && _currentPointId != eventData.pointerId)
            {
                _currentPointId = _currentPointIds[0];
                //_screenTouchPos[0] = cam.ScreenToViewportPoint(eventData.position);
                CalcHitPos(GetPosition(_currentPointId));
            }

            //Two fingers
            if (_currentPointIds.Count == 2) 
            {
                // Camera rotation
                Ray ray = CameraController.Camera.ScreenPointToRay(eventData.position);
                float dist;
                if (_plane.Raycast(ray, out dist))
                {
                    _hitPoints[1] = ray.GetPoint(dist);
                    //_screenTouchPos[1] = cam.ScreenToViewportPoint(eventData.position);

                    Vector3 pos1 = ScreenSquareNormalize(GetPosition(0));
                    Vector3 pos2 = ScreenSquareNormalize(GetPosition(1));
                    _screenTouchPos[0] = pos1;
                    _screenTouchPos[1] = pos2;

                    _touchPich = pos1.y + (pos1.y - pos2.y) / 2; //Bug
                    _touchMagnitude = Vector3.Distance(pos1, pos2);
                    _touchYaw = Mathf.Atan2(_hitPoints[0].x - _hitPoints[1].x, _hitPoints[0].z - _hitPoints[1].z) * Mathf.Rad2Deg;   
                } 
            }
        }

        public void CalcHitPos(Vector3 position)
        {
            Camera cam = CameraController.Camera;
            Ray ray = CameraController.Camera.ScreenPointToRay(position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                _plane = new Plane(Vector3.up, hit.point);
                _hitPoints[0] = hit.point;
            }
            else
            {
                _plane = new Plane(Vector3.up, new Vector3(0, PlaneLocation, 0));

                float enter;
                if (_plane.Raycast(ray, out enter))
                    _hitPoints[0] = ray.GetPoint(enter);
            }
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);

            if (_currentPointIds.Count != 0 && _currentPointId == eventData.pointerId)
            {
                _currentPointId = _currentPointIds[0];
                CalcHitPos(GetPosition(_currentPointId));
            }
            else if (_currentPointIds.Count == 0)
                _currentPointId = -2;
        }

        public override void OnEnable()
        {
            base.OnEnable();

            Rotation = CameraController.transform.rotation;
            _dragWorldPosition = CameraController.transform.position;
            Position = CameraController.transform.position;

            _currentRotation = Quaternion.Euler(CurrentPich, CurrentYaw, 0);
        }

        public override void OnDisable()
        {
            base.OnDisable();
        }

        public void CalcZoom()
        {
            float input = Input.GetAxis("Mouse ScrollWheel");
            CurrentZoom -= input * ZoomSpeedPC;
            CurrentZoom = ZoomClamp.Clamp(CurrentZoom);
        }

        public void CalcRotation()
        {
            DisableDragMovement = Input.GetKey(KeyCode.LeftAlt) && Input.GetMouseButton(0);
            //Keyboard rotation
            if (DisableDragMovement)
            {
                CurrentYaw += YawSpeed * Input.GetAxis("Mouse X");
                CurrentPich -= PichSpeed * Input.GetAxis("Mouse Y");
                CurrentPich = PichClamp.Clamp(CurrentPich);
                _currentRotation = Quaternion.Euler(CurrentPich, CurrentYaw, 0);
            }
            //Touch rotation
            else if (_currentPointIds.Count == 2)
            {
                Vector3 pos1 = ScreenSquareNormalize(GetPosition(0));
                Vector3 pos2 = ScreenSquareNormalize(GetPosition(1));
                Camera cam = CameraController.Camera;
                float[] delta = new float[3];

                for (int i = 0; i < 2; i++)
                {
                    Ray ray = cam.ScreenPointToRay(GetPosition(i));
                    float dist;
                    if (_plane.Raycast(ray, out dist))
                        _hitPoints[i] = ray.GetPoint(dist);
                }

                //Zoom
                float zoom = Vector3.Distance(pos1, pos2);
                delta[0] = (_touchMagnitude - zoom) * ZoomSpeedMobile;
                _touchMagnitude = zoom;
                //CurrentZoom += zoomDif;


                //Yaw
                delta[1] = _touchYaw - Mathf.Atan2(_hitPoints[0].x - _hitPoints[1].x, _hitPoints[0].z - _hitPoints[1].z) * Mathf.Rad2Deg;

                //Pich 
                //Test 
                float dif1 = pos1.y - _screenTouchPos[0].y;
                float dif2 = pos2.y - _screenTouchPos[1].y;
                float alpha;
                if (dif1 < dif2)
                    alpha = dif1 / dif2;
                else
                    alpha = dif2 / dif1;

                Debug.Log(alpha);


                //Werk
                float touchPich = pos1.y + (pos1.y - pos2.y) / 2;
                delta[2] = (_touchPich - touchPich) * PichSpeed;
                _touchPich = touchPich;
               
                //Find the best movement
                switch (FindTheBiggestNumber(delta))
                {
                    case 0:
                        CurrentZoom += delta[0];
                        break;
                    case 1:
                        CurrentYaw += delta[1];
                        break;
                    case 2:
                        CurrentPich += delta[2];
                        break;
                }

                //Set rotation
                CurrentPich = PichClamp.Clamp(CurrentPich);
                _currentRotation = Quaternion.Euler(CurrentPich, CurrentYaw, 0);
            }

            //if (_currentPointIds.Count != 0)
            //    Debug.Log(ScreenSquareNormalize(GetPosition(_currentPointIds[0])));
        }

        public override void LateUpdate()
        {
            //Debug.Log(Input.touchCount);

            _dragWorldPosition.y = PlaneLocation;

            //Smooth
            _hitPointDifferent.y = 0;
            _hitPointDifferent -= _hitPointDifferent * Smooth * Time.deltaTime;
            _hitPointDifferent = Vector3.Normalize(_hitPointDifferent) * Mathf.Min(_hitPointDifferent.magnitude, MaxSmoothSpeed);
            if (_currentPointIds.Count == 0 && UseSmooth)
                _dragWorldPosition += _hitPointDifferent;

            //Clamp position
            _dragWorldPosition = ClampPosition(_dragWorldPosition);

            //Rotation
            CalcRotation();

            //Calc zoom
            CalcZoom();

            //Final cam pos zoom and rotation
            Position = _dragWorldPosition + (_currentRotation * Vector3.back) * CurrentZoom;
            Rotation = _currentRotation;
        }

        protected int FindTheBiggestNumber(float[] values)
        {
            int index = 0;
            float bigNumber = 0;
            for (int i = 0; i < values.Length; i++)
            {
                float num = Mathf.Abs(values[i]);

                if (num > bigNumber)
                {
                    index = i;
                    bigNumber = num;
                }
            }

            return index;
        }

        //====================================Debug===========================================

        public override void OnDrawGizmos()
        {
            if (_currentPointIds.Count != 0 && !DisableDragMovement)
                DebugExtension.DrawCircle(_hitPoints[0], Color.blue, 10);

            if (UseClamp)
            {
                Matrix4x4 mat = Matrix4x4.TRS(new Vector3(Center.x, PlaneLocation, Center.y), Quaternion.identity, Vector3.one);
                DebugExtension.DrawLocalCube(mat, new Vector3(Size.x, 0, Size.y), Color.blue);
            }
        }
    }
}