﻿namespace RoyFramework.Game
{
    public interface IGetCameraState
    {
        CameraState GetCameraState();
    }
}