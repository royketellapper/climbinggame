﻿namespace RoyFramework.Game
{
    public interface IGetCameraModifier
    {
        CameraModifier GetCameraModifier();
    }
}