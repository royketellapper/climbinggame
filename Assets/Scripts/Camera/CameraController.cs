﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace RoyFramework.Game
{
    using Camera = UnityEngine.Camera;

    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        public TouchPanel CameraTouchPanel;
        public Transform Target;
        [Space]
        public CameraData DefaultCameraState;
        public CameraClampModifierData[] DefaultModifiers;
       
        
        public List<CameraModifier> CameraModifier { get { return _cameraModifier; } }
        public Camera Camera { get; protected set; }
        public CameraState CurrentCameraState { get; protected set; }

        protected CameraState _oldCameraState;
        protected List<CameraModifier> _cameraModifier = new List<CameraModifier>();

        //protected Tween _swithStateTween;

        protected virtual void Awake()
        {
            Camera = GetComponent<Camera>();
            SetCameraState(DefaultCameraState);

            foreach (var modifier in DefaultModifiers)
                AddModifier(modifier);
        }

        //protected virtual void OnEnable()
        //{

        //}

        //protected virtual void OnDisable()
        //{

        //}

        public void AddModifier(IGetCameraModifier cameraModifier)
        {
            if (cameraModifier != null)
                AddModifier(cameraModifier.GetCameraModifier());
        }

        public void AddModifier(CameraModifier cameraModifier)
        {
            cameraModifier.CameraController = this;
            _cameraModifier.Add(cameraModifier);
        }

        public void RemoveModifier(CameraModifier cameraModifier)
        {
            _cameraModifier.Remove(cameraModifier);
        }

        protected virtual void LateUpdate()
        {
            Transform trans = Camera.transform;
            Vector3 camPosition = trans.transform.position;
            Quaternion camRotation = trans.transform.rotation;

            //Camera state
            if (CurrentCameraState != null)
            {
                CurrentCameraState.LateUpdate();
                camPosition = CurrentCameraState.Position;
                camRotation = CurrentCameraState.Rotation;
            }

            //Modifiers
            foreach (var modifier in _cameraModifier)
            {
                camPosition = modifier.Position(camPosition);
                camRotation = modifier.Rotation(camRotation);
            }

            //Final cam position
            trans.position = camPosition;
            trans.rotation = camRotation;

            if (CurrentCameraState != null)
                CurrentCameraState.FinalUpdate();
        }

        protected void OnDrawGizmos()
        {
            if (CurrentCameraState != null)
                CurrentCameraState.OnDrawGizmos();

            foreach (var modifier in _cameraModifier)
                modifier.OnDrawGizmos();
        }

        public void SetCameraState(CameraState state)
        {
            if (CurrentCameraState != null)
            {
                CurrentCameraState.RemoveTouchPanel(CameraTouchPanel);
                CurrentCameraState.OnDisable();
            }

            state.AddTouchPanel(CameraTouchPanel);
            state.CameraController = this;

            _oldCameraState = CurrentCameraState;
            CurrentCameraState = state;

            if (!state._isAwake)
            {
                state.Awake();
                state._isAwake = true;
            }

            state.OnEnable();
        }

        public void SetCameraState(IGetCameraState state)
        {
            if (state != null)
                SetCameraState(state.GetCameraState());
        }
    }
}