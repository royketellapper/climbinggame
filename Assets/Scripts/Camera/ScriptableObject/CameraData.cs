﻿using UnityEngine;

namespace RoyFramework.Game
{
    public abstract class CameraData : ScriptableObject, IGetCameraState
    {
        public abstract CameraState GetCameraState();
    }
}