﻿using UnityEngine;

namespace RoyFramework.Game
{
    [System.Serializable, CreateAssetMenu(fileName = "TopDownCameraData", menuName = "Camera/State/Top Down Camera")]
    public class TopDownCameraData : CameraData
    {
        public TopDownCameraState TopDownCameraState;

        public override CameraState GetCameraState()
        {
            return TopDownCameraState;
        }
    }
}