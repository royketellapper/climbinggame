﻿using UnityEngine;

namespace RoyFramework.Game
{
    [System.Serializable]
    public abstract class CameraModifierData : ScriptableObject, IGetCameraModifier
    {
        public abstract CameraModifier GetCameraModifier();
    }
}