﻿using UnityEngine;

namespace RoyFramework.Game
{
    [System.Serializable, CreateAssetMenu(fileName = "CameraClampModifierData", menuName = "Camera/Modifier/Camera Clamp Modifier")]
    public class CameraClampModifierData : CameraModifierData
    {
        public CameraClampModifier Settings;

        public override CameraModifier GetCameraModifier()
        {
            return Settings;
        }
    }
}
