﻿using UnityEngine;

namespace RoyFramework.Game
{ 
    [System.Serializable, CreateAssetMenu(fileName = "ThirdPersonCameraData", menuName = "Camera/State/Third Person Camera Data")]
    public class ThirdPersonCameraData : CameraData
    {
        public ThirdPersonState ThirdPersonState;

        public override CameraState GetCameraState()
        {
            return ThirdPersonState;
        }
    }
}